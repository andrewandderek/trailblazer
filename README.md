# Trailblazer #

This is the Trailblazer Android app (and related things).

## Goals

1. Produce and deploy a GPS track recorder app for Android phones
1. Get familiar with Kotlin
1. Work together in a team
1. Do not steal or leak any personal data from user's phones

## What is Trailblazer?

![Screen](Support/screen.png)

Trailblazer is a simple, quick and easy to use track recorder, for hiking, running cycling etc. You can record and share your tracks. The app was inspired by MyTracks - a Google app that is now deprecated. The app is supported on all Android phones from Android 4 up.

### Why is it free?

The app is free and without adverts, so how do we monetise it? We do not get any revenue from the app store instead the app is open source so the authors can link it into their portfolios.

### What can it do?

- Start/stop/pause a track
- See your track on a map
- Manage your archive of tracks
- Share tracks with your friends
- Export to and import from GPX
- Export to and import from KML
- Import from KMZ
- Get statistics in any track: distance, speed, ascent etc.
- Compare, export and delete groups of tracks

### What is on the road map?

We have very limited resources. There is a lot we want to do but no promises on how quickly we can get round to it. Though if you want to contribute then that would be great.

- Save statistics to Google Sheets
- Integrate with Google Drive
- Voice commands to start and stop a track
- Text to speech prompts when comparing tracks

## How to work with the code

You can fork this repository and compile the code yourself. You cannot build a release APK as the signing key is not in the repo.

### How do I get set up?

1. Install the latest Android Studio (As a minimum it should be version 3.1)
1. Fork the team repository
1. Clone locally

### Building in Android Studio

The easiest method of building the project is to use Android Studio, you should ensure that you use the Android Studio embedded JDK as its going to work with the version of Android Gradle Plugin AGP

### Building from the command line

You can build from the command line using `gradlew assembleDebug` or `gradlew assembleRelease`. There are additional requirements for building the release version, or for using multiple JDKs on one machine, make sure you understand the [release process](RELEASE.md) before attempting to do a command line build.

## Contribution guidelines

If you want to contribute that would be great but its only going to work if we all know how we work together.

### For small isolated changes

1. Create a new feature branch in your fork, you should branch off MASTER and it should be called something like feature/my-small-thing
1. Do your work, but have a look at the pull request guidelines
1. Open a pull request from your fork feature/my-small-thing to the team repo MASTER
1. Respond to any comments, when all is good it will be merged into MASTER
1. Fast forward your fork MASTER
1. Delete your feature branch

### For larger projects

1. Talk to the team about the feature you want to work on and we will create a project branch in the team repo called project/my-large-thing, it will be branched from MASTER.
1. Create a new feature branch in your fork, you should branch off project/my-large-thing and it should be called something like feature/my-small-thing
1. Do your work, but have a look at the pull request guidelines
1. After each logical chunk open a pull request from your fork feature/my-small-thing to the team repo project/my-large-thing
1. Repeat for each logical chunk, either using a new feature/my-small-thing-2 branch or reuse feature/my-small-thing
1. When all the logical chunks are complete we wil then merge project/my-large-thing into MASTER (you may need to Fast Forward first)

### If a small change becomes large

If you start making a small change and realise that its going to be bigger than you thought dont just open a large pull request.

1. Talk to the team to get a project branch created - as above
1. The branch will be created from MASTER.
1. Fast forward you feature branch from master - if needed.
1. Open a pull request from your feature branch to the project branch.

### Pull Request Guidelines

1. **Think small** (no, smaller than that). Keep your pull requests small so that the reviewer can grasp what is going on. Its hard to define exactly how small is small but in general if your PR contains more than 20 files its too big.
1. **Include tests**. Please dont say that the tests will be in the next PR
1. **Write in Kotlin**. Yes we could write in other languages but one of this projects goals is to get familiar with Kotlin so lets stick to that.
1. **No adverts or selling personal data**. This is a free app we do not monetise by adverts or selling data so please dont. Of course if you are a contributor then you can link to the repo as part of your portfolio.
1. **Your code will be reviewed**. Its a good thing but it can be upsetting to have someone else comment on your code but the point of a review is that we all gain. The code base gets better and hopefully the coder and the reviewer learn as well, so embrace the process, nobody is being mean.
1. **Take care with the license requirements of dependencies**. This project is free and open source. You should be OK with GPL v2 or FreeBSD licenses but if you want to add a new dependency then you should probably get in contact first.

