# Trailblazer Release Process #

This is the process for building, testing and deploying Trailblazer to the app store.

## Prerequisite

1. **LocalOnly folder**. This folder contains the files needed to sign a release APK.
1. **Access to the Team repository**. You will need write access to the [Trailblazer team code repository][team-repo-url]
1. **Access to the Team Google Play Console**. You will need to have the required permissions to upload and release the Trailblazer app from the [play store console][play-console-url]
1. **A build machine**. Either a CI build server or your local developer machine can be used.

### What is the folder "LocalOnly"?

This folder contains the files needed to sign a release APK. The folder is present on developer machines as well as any build servers however it is not checked into GIT, hence the name. This folder is not needed to use Android Studio or Gradle to produce debug build of the app, it is only used with the release build type.

#### What files need to be in the folder?

The folder needs to contain.

1. trailblazer.keystore - this is the upload signing key
1. keystore.proerties - this contains the gradle build configurations for release signing

#### What happens if I dont have "LocalOnly"?

You will be able to build the debug variant as well as run all the tests, the only thing you will not be able to do is build the release APK, if you try you will see this error

```
FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':app:validateSigningRelease'.
> Keystore file not set for signing config release
```

### Command line tools

If you want to build and work with the code form the command line you will need to add the following locations to your path

Windows

1. Android SDK =  `%localappdata%\Android\sdk\platform-tools`
1. Java JDK
    - Embedded
      - Android Studio Electric Eel 2022.1.1 and above: `"%ProgramFiles%\Android\Android Studio\jbr\bin"`
      - Android Studio Dolphin and below: `"%ProgramFiles%\Android\Android Studio\jre\bin"`
    - or Stand Alone: `"%ProgramFiles%\Java\jdk1.8.0_92\bin"`

Mac

1. Android SDK = `~/Library/Android/sdk/platform-tools`
1. Java JDK
    - Embedded
      - Android Studio Electric Eel 2022.1.1 and above: `/Applications/Android Studio.app/Contents/jbr/Contents/Home`
      - Android Studio Dolphin and below: `/Applications/Android Studio.app/Contents/jre/Contents/Home`
    - or Stand Alone: `/Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home`

Note that if you use `gradlew assembleRelease` or `build.cmd` then it will use the environment variable `JAVA_HOME` to find the JDK. If that is set incorrectly you will see an error like this

```
FAILURE: Build failed with an exception.

* Where:
Build file 'D:\Data\Code\andrew-and-derek\trailblazer\TrailblazerAndroid\app\build.gradle' line: 17

* What went wrong:
A problem occurred evaluating project ':app'.
> Failed to apply plugin 'com.android.internal.application'.
   > Android Gradle plugin requires Java 17 to run. You are currently using Java 11.
      Your current JDK is located in C:\Program Files\Microsoft\jdk-11.0.14.9-hotspot
      You can try some of the following options:
       - changing the IDE settings.
       - changing the JAVA_HOME environment variable.
       - changing `org.gradle.java.home` in `gradle.properties`.
```

As you can see this error means that the configured JDK is incompatible with AGP.

You may want or need to override that variable, if you do then you can set it via the command line `gradlew assembleRelease "-Dorg.gradle.java.home=C:\Program Files\Android\Android Studio\jbr"`. The `build_set_jdk.cmd` script is an example of how to set the JDK used to be the one embedded in Android Studio JDK. This will only work if you have installed Android Studio in the default location and it contains a JDK that is compatibly with AGP.

## Running the tests

You can run the unit tests from the command line like this `gradlew testDebugUnitTest` (the build scripts automatically do this.)

The integration tests can be run in Android Studio or like this `gradlew :app:connectedDebugAndroidTest`. Note that the tests that write to the filesystem will only run on devices running Android 11+ (the permissions changes made by google have defeated the test runner). Also note that it will install *and uninstall* the debug version of the app so you might loose any data if you already have the debug app installed

## Deployment Process

The process to deploy a new app is as follows

1. Get agreement with the team that we shall deploy a release.
1. Ensure all code to be deployed is in the `master` branch
1. Create a new branch called `release/1.0.1` from `master` locally (where 1.0.1 is the version name we are releasing)
1. Edit the file `app/build.gradle`. In the section `android -> defaultConfig` we need to edit the `versionName` and `versionCode` values
1. Run the UnitTests and the IntegrationTests. If they are red then you need to abandon the release or fix the tests.
1. Add the release notes for the store listing to `Support/Releases/NOTES.md`
1. Ensure that all files have been committed before building the release APK.
1. Build the release APK, essentially this means running `gradlew assembleRelease`, there is a `build.cmd` (and a `build_set_jdk.cmd`, see above) script to do this and copy the APK to the `Support` folder
1. Install the APK on a test device using this command `adb install -r trailblazer-release-1.0.1-2-5a5ff5c.apk`, the APK name will reflect the version and the commit.
1. Run the smoke tests on the device. If the test fails check the existing production app if the problem already exists then log the issue and continue, if it is a new problem then you will need to fix it or abandon the release.
1. Upload the APK to the [play store console][play-console-url]
1. Refresh the release notes
1. Release to Internal Test, check the correct app is delivered to a handset (goto Settings page and look at the version)
1. Tag the commit with the version number, for example `1.0.1(2)`, which is `versionName(versionCode)`
1. Merge into `master` locally and push to the team repo, ensure the tag is pushed by using `git push team "1.0.1(2)"`
1. Promote to Production

## Smoke Tests

Check the following, estimated execution time should be less than 5 minutes

1. Start the app
1. Does it prompt for location permission? Allow location permission.
1. Is the (empty) track list displayed? Is the title `Trailblazer'?
1. Goto Settings, does the Version match this build?
1. Goto Open-source Licenses
1. Goto Privacy statement
1. Go back to the track list
1. Start a track, does the map display? Does it centre on your location? Does the title show `Date, time (recording)`?
1. Is the notification displayed?
1. Goto statistics
1. Pause the track, does the title change?
1. Goto statistics
1. Stop the track, does the title change?
1. Does it prompt for a track name?
1. Is the notification removed?
1. Goto statistics
1. Go back to Track List, rename the track and select it
1. Is it displayed and the title changed
1. Share the track via email
1. Export the track, is the file present?
1. Import a track, does it display and are the statistics available
1. Record a second track and delete it
1. Goto Track List, multiple select tracks and export them
1. Goto Track List, multiple select tracks and compare them
1. Goto Track List, multiple select tracks and combine them
1. Goto Track List, multiple select tracks and delete them

[team-repo-url]:		https://bitbucket.org/andrewandderek/trailblazer/src/master/
[play-console-url]:		https://play.google.com/apps/publish


