/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import java.io.InputStream

object AssetHelper {


    // This provides the context to get at resources/assets for the *tests*, not for the real app - if
    // you want assets for the real app use ApplicationProvider.getApplicationContext
    val testContext: Context
        get() = InstrumentationRegistry.getInstrumentation().context

    fun loadTestAssetFile(filename: String, replaceEol: Boolean): String {
        // we need espresso for this to work
        var fileContent = openTestAssetFile(filename).bufferedReader().use { it.readText() }
        if (replaceEol) {
            fileContent = fileContent.replace("\n", "")
            fileContent = fileContent.replace("\r", "")
            // sometimes we need to be able to put line breaks back in by using this token in the test files
            fileContent = fileContent.replace("~~NEWLINE~~", "\n")
        }
        return fileContent
    }

    fun loadTestAssetBinaryFile(filename: String): ByteArray {
        // we need espresso for this to work
        return openTestAssetFile(filename).buffered().use { it.readBytes() }
    }

    private fun openTestAssetFile(filename: String): InputStream {
        return testContext.assets.open(filename)
    }
}