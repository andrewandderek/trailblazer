/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxImporter
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*

@RunWith(AndroidJUnit4::class)
@MediumTest
class GpxImporterTests : ImporterTestsBase() {

    @Test
    fun deSerialiseUnknownFormatTrack() {
        // arrange
        createInMemoryInputStreamFromAssetFile("xyz/track_unknown_format.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertNull(importResult.track)
        Assert.assertEquals(ImportResult.UNKNOWN_FORMAT, importResult.result)
    }

    @Test
    fun deSerialiseTrack() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseSegmentedTrack() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1_segmented.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        Assert.assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseSegmentedTrack2() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1_segmented2.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        Assert.assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        Assert.assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrackWithPointsWithoutElevation() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1_missing_elevation.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        Assert.assertEquals( 3.12, importResult.track!!.currentPositionsSnapshot[0].altitude!!, 0.1)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot[1].altitude)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot[2].altitude)
    }

    @Test
    fun deSerialiseTrackWithPointsWithoutTimestamps() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1_missing_timestamps.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, false)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        Assert.assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrackWithPointsWithoutTimestampsAndElevation() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1_missing_timestamps_and_elevation.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot[0].altitude)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot[1].altitude)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot[2].altitude)
    }

    @Test
    fun deSerialiseTrackWithInvalidValues() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1_invalid.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        Assert.assertNotNull(importResult.track)
        Assert.assertEquals( "NAME1_INVALID", importResult.track?.name)
        Assert.assertEquals( "NOTES1_INVALID", importResult.track?.notes)
        Assert.assertEquals( "2017-11-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.started))
        Assert.assertEquals( 5, importResult.track!!.currentPositionsSnapshot.size)

        Assert.assertEquals( 0.0, importResult.track!!.currentPositionsSnapshot.get(0).latitude, 0.1)
        Assert.assertEquals( 0.0, importResult.track!!.currentPositionsSnapshot.get(0).longitude, 0.1)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot.get(0).altitude)
        Assert.assertEquals( "2017-11-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.currentPositionsSnapshot.get(0).timeRecorded))

        Assert.assertEquals( 0.0, importResult.track!!.currentPositionsSnapshot.get(1).latitude, 0.1)
        Assert.assertEquals( 0.0, importResult.track!!.currentPositionsSnapshot.get(1).longitude, 0.1)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot.get(1).altitude)
        Assert.assertEquals( "2017-11-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.currentPositionsSnapshot.get(1).timeRecorded))

        Assert.assertEquals( 0.0, importResult.track!!.currentPositionsSnapshot.get(2).latitude, 0.1)
        Assert.assertEquals( 0.0, importResult.track!!.currentPositionsSnapshot.get(2).longitude, 0.1)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot.get(2).altitude)
        Assert.assertEquals( "2017-11-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.currentPositionsSnapshot.get(2).timeRecorded))

        Assert.assertEquals( 1.123456, importResult.track!!.currentPositionsSnapshot.get(3).latitude, 0.1)
        Assert.assertEquals( -2.123456, importResult.track!!.currentPositionsSnapshot.get(3).longitude, 0.1)
        Assert.assertEquals( 3.12, importResult.track!!.currentPositionsSnapshot.get(3).altitude!!, 0.1)
        Assert.assertEquals( "2017-12-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.currentPositionsSnapshot.get(3).timeRecorded))

        Assert.assertEquals( 11.123456, importResult.track!!.currentPositionsSnapshot.get(4).latitude, 0.1)
        Assert.assertEquals( -12.123456, importResult.track!!.currentPositionsSnapshot.get(4).longitude, 0.1)
        Assert.assertNull(importResult.track!!.currentPositionsSnapshot.get(4).altitude)
        Assert.assertEquals( "2017-11-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.currentPositionsSnapshot.get(4).timeRecorded))

    }

    @Test
    fun deSerialiseTrackWithInvalidUri() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("AN INVALID URI")

        // assert
        Assert.assertEquals(ImportResult.IO_ERROR, importResult.result)
        Assert.assertNull(importResult.track)
    }

    @Test
    fun deSerialiseTrackWithEmptyUri() {
        // arrange
        createInMemoryInputStreamFromAssetFile("gpx/track1.xml")
        val importer = GpxImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)

        // act
        val importResult = importer.importTrack("")

        // assert
        Assert.assertEquals(ImportResult.IO_ERROR, importResult.result)
        Assert.assertNull(importResult.track)
    }

}