/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.logging.Slf4jLoggerFactory
import com.andrewandderek.trailblazer.model.TrackStatistics
import org.junit.Assert.assertEquals
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

open class DataTestsBase {
    protected lateinit var dbHelper: DatabaseHelper
    protected var loggerFactory: ILoggerFactory = Slf4jLoggerFactory()

    protected lateinit var now: Calendar
    protected lateinit var now2: Calendar

    /**
     * for the on-device tests we need a real database, not a mock one
     * we need a real db so we can test that our SQL in the app works properly
     * the test db is an in-memory db so that we can start from a known position for every test
     * this means that we can predict all the IDs and data layout and write tests for it
     * because its in-memory its pretty quick to generate and we are also testing our schema generation code
     */
    protected fun createInMemoryDatabase() {
        var context: Context = ApplicationProvider.getApplicationContext()
        var factory = DatabaseFactory(loggerFactory, context)
        dbHelper = factory.inMemoryDatabaseHelper
        dbHelper.open()         // we must open the DB this way otherwise we dont get our pragmas
    }

    protected fun setupNowDateTimes() {
        now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now2 = Calendar.getInstance()
        now2.time = now.time
        now2.add(Calendar.SECOND, 10)
    }

    protected fun getTimeOffsetBy(referenceTime: Calendar, field: Int, amount: Int): Calendar {
        val result = Calendar.getInstance()
        result.time = referenceTime.time
        result.add(field, amount)
        return result
    }

    private fun calendarAsUtcString(date: Calendar?):String {
        if (date == null) {
            return "<NULL>"
        }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("UTC");
        return dateFormat.format(date.time)
    }

    protected fun assertDatesEqual(message: String, date1: Calendar?, date2: Calendar?) {
        val date1String = calendarAsUtcString(date1)
        val date2String = calendarAsUtcString(date2)
        assertEquals(message, date1String, date2String)
    }

    protected fun setupStatistics(trackNo: Int): TrackStatistics {
        val base = (trackNo - 1) * 100
        val stats = TrackStatistics()
        stats.distanceInMetres = base + 1.0
        stats.averageMovingSpeedInMetresPerSecond = base + 2.0
        stats.averageSpeedInMetresPerSecond = base + 3.0
        stats.elapsedTimeInMilliseconds = base.toLong() * 1000 * 60
        stats.elevationDifferenceInMetres = base + 5.0
        stats.elevationGainInMetres = base + 6.0
        stats.elevationLossInMetres = base + 7.0
        stats.maxElevationInMetres = base + 8.0
        stats.minElevationInMetres = base + 9.0
        stats.movingTimeInMilliseconds = (base.toLong()+1) * 1000 * 60
        stats.recordedTimeInMilliseconds = (base.toLong()+2) * 1000 * 60
        return stats
    }
}