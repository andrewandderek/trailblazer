/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.cheeseknife

import android.content.Context
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.OnClick
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

class TestViewGroup(context: Context) : FrameLayout(context) {
    @BindView(TestTextViewId)
    lateinit var testTextView: TextView

    var clicked = false

    @OnClick(TestButtonId)
    fun onClick() {
        clicked = true
    }

    companion object {
        const val TestTextViewId = 123
        const val TestButtonId = 789
    }
}

@RunWith(AndroidJUnit4::class)
@SmallTest
class CheeseKnifeTests {

    private fun createTestView(): TestViewGroup {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val container = TestViewGroup(context)
        container.addView(TextView(context).apply { id = TestViewGroup.TestTextViewId })
        container.addView(Button(context).apply { id = TestViewGroup.TestButtonId })
        return container
    }

    @Test
    fun bindView_binds_view_correctly() {
        val container = createTestView()

        CheeseKnife.bind(container, container)

        assertNotNull(container.testTextView)
    }

    @Test
    fun onClick_binds_onClick_func_correctly() {
        val container = createTestView()

        CheeseKnife.bind(container, container)

        assertFalse(container.clicked)
        container.findViewById<Button>(TestViewGroup.TestButtonId).performClick()
        assertTrue(container.clicked)
    }
}