/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.data.repository.PositionRepository
import com.andrewandderek.trailblazer.data.repository.TrackRepository
import com.andrewandderek.trailblazer.exception.OrphanPositionException
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.Calendar

@RunWith(AndroidJUnit4::class)
@MediumTest
class TrackAndPositionRepositoryTests : DataTestsBase() {
    private lateinit var trackRepository: TrackRepository
    private lateinit var positionRepository: PositionRepository
    private lateinit var track1: Track
    private lateinit var track2: Track
    private lateinit var position1: Position
    private lateinit var position2: Position
    private lateinit var position3: Position
    private lateinit var date1: Calendar
    private lateinit var date2: Calendar

    @Before
    fun before_each_test() {
        createInMemoryDatabase()
        setupNowDateTimes()
        trackRepository = TrackRepository(loggerFactory, dbHelper)
        positionRepository = PositionRepository(loggerFactory, dbHelper)

        track1 = Track()
        track1.name = "NAME1"
        track1.notes = "NOTES1"
        date1 = Calendar.getInstance()
        date1.set(2017, 1, 29, 11, 3, 33)
        track1.started = date1
        position1 = Position(1.0, 2.0, 3.0, now, 100F, "POSITION_NOTES1")
        position2 = Position(11.0, 12.0, 13.0, now2, 200.2F, "POSITION_NOTES2")

        track2 = Track()
        track2.name = "NAME2"
        track2.notes = "NOTES2"
        date2 = Calendar.getInstance()
        date2.set(2017, 11, 30, 15, 37, 22)
        track2.started = date2
        position3 = Position(1.0, 2.0, 3.0, now, 66F, "POSITION_NOTES3")
    }

    @Test
    fun create_getWithPositions() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(position2)
        positionRepository.createFast(position2)

        // act
        var result = trackRepository.getWithPositions(1, positionRepository)

        // assert
        assertEquals(1L, result?.id)
        assertEquals(2, result?.currentPositionsSnapshot?.size)
    }

    @Test
    fun create_getWithPositionsAndAccuracy() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(position2)
        positionRepository.createFast(position2)

        // act
        var result = trackRepository.getWithPositionsAndAccuracy(1, 101F, positionRepository)

        // assert
        assertEquals(1L, result?.id)
        assertEquals(1, result?.currentPositionsSnapshot?.size)
    }

    @Test
    fun create_getWithPositions_NotFound() {
        // arrange

        // act
        var result = trackRepository.getWithPositions(2, positionRepository)

        // assert
        Assert.assertNull(result)
    }

    @Test
    fun create_delete_withPositions() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(position2)
        positionRepository.createFast(position2)
        trackRepository.deleteById(1)

        // act
        var result = positionRepository.getByTrackId(1)

        // assert
        // check the positions are gone
        assertEquals(0, result.size)
    }

    @Test(expected = OrphanPositionException::class)
    fun create_orphan() {
        // arrange
        track1.addNewPosition(position1)

        // act
        positionRepository.createFast(position1)
    }

    @Test
    fun create_getTrackList() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(position2)
        positionRepository.createFast(position2)
        track2 = trackRepository.create(track2)
        track2.addNewPosition(position3)
        positionRepository.createFast(position3)

        // act
        var result = trackRepository.getTrackList(positionRepository)

        // assert
        assertEquals(2, result.count())
        assertEquals("the list should be reverse sorted by date", 2, result[0].id)
        assertEquals("the list should be reverse sorted by date", 1, result[1].id)

        assertEquals("each track should only have the last position in the positions list", 1, result[0].currentPositionsSnapshot.count())
        assertEquals("each track should only have the last position in the positions list", "POSITION_NOTES3", result[0].currentPositionsSnapshot[0].notes)

        assertEquals("each track should only have the last position in the positions list", 1, result[1].currentPositionsSnapshot.count())
        assertEquals("each track should only have the last position in the positions list", "POSITION_NOTES2", result[1].currentPositionsSnapshot[0].notes)
    }

    @Test
    fun create_getTrackListWithoutAnyPositions() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(position2)
        positionRepository.createFast(position2)
        track2 = trackRepository.create(track2)
        track2.addNewPosition(position3)
        positionRepository.createFast(position3)

        // act
        var result = trackRepository.getTrackListWithoutAnyPositions()

        // assert
        assertEquals(2, result.count())
        assertEquals("the list should be sorted", 2, result[0].id)
        assertEquals("the list should be sorted", 1, result[1].id)

        assertEquals("the positions list should be empty", true, result[0].isEmpty)
        assertEquals("the positions list should be empty", true, result[1].isEmpty)
    }

    @Test
    fun create_getAll() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(position2)
        positionRepository.createFast(position2)
        track2 = trackRepository.create(track2)
        track2.addNewPosition(position3)
        positionRepository.createFast(position3)

        // act
        var result = trackRepository.getAll()

        // assert
        assertEquals(2, result.count())
        assertEquals("the list should be sorted", 1, result[0].id)
        assertEquals("the list should be sorted", 2, result[1].id)

        assertEquals("the positions list should be empty", true, result[0].isEmpty)
        assertEquals("the positions list should be empty", true, result[1].isEmpty)
    }

}