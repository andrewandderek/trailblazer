/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.PositionRepository
import com.andrewandderek.trailblazer.data.repository.TrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.TrackRepository
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.Calendar

@RunWith(AndroidJUnit4::class)
@MediumTest
class TrackRepositoryTests : DataTestsBase() {
    private lateinit var trackRepository: TrackRepository
    private lateinit var positionRepository: PositionRepository
    private lateinit var trackMetadataRepository: ITrackMetadataRepository
    private lateinit var track1: Track
    private lateinit var position1: Position
    private lateinit var recordedPosition: Position
    private lateinit var track2: Track
    private lateinit var position2: Position
    private lateinit var metadata1: TrackMetadata

    @Before
    fun before_each_test() {
        createInMemoryDatabase()
        setupNowDateTimes()
        trackRepository = TrackRepository(loggerFactory, dbHelper)
        trackMetadataRepository = TrackMetadataRepository(loggerFactory, dbHelper)
        positionRepository = PositionRepository(loggerFactory, dbHelper)

        track1 = Track()
        track1.name = "NAME1"
        track1.notes = "NOTES1"
        track1.started = getTimeOffsetBy(now, Calendar.SECOND, 10)
        position1 = Position(1.0, 2.0, 3.0, now, 100.0F, "POSITION_NOTES1")
        position1.timeRecorded = getTimeOffsetBy(now, Calendar.SECOND, 25)
        position1.trackId = 1
        recordedPosition = Position(1.0, 2.0, 3.0, now, 100.0F, "POSITION_NOTES_REC")
        recordedPosition.timeRecorded = getTimeOffsetBy(now, Calendar.SECOND, 50)
        recordedPosition.trackId = 1

        metadata1 = TrackMetadata()
        metadata1.trackId = 1
        metadata1.versionName = "VERSION_NAME_1"
        metadata1.versionCode = 101
        metadata1.ended = getTimeOffsetBy(now, Calendar.SECOND, 25)
        metadata1.statistics = setupStatistics(1)

        track2 = Track()
        track2.name = "NAME2"
        track2.notes = "NOTES2"
        track1.started = getTimeOffsetBy(now, Calendar.SECOND, 60)
        position2 = Position(1.0, 2.0, 3.0, now, 100.0F, "POSITION_NOTES2")
        position2.timeRecorded = getTimeOffsetBy(now, Calendar.SECOND, 65)
        position2.trackId = 2
    }

    @Test
    fun create_getById() {
        // arrange
        track1.id = 123      // this will be ignored
        trackRepository.create(track1)

        // act
        val result = trackRepository.getById(1)

        // assert
        assertEquals(1L, result!!.id)
        assertEquals(track1.name, result.name)
        assertEquals(track1.notes, result.notes)
        assertDatesEqual("comparing start dates", track1.started, result.started)
        assertNull(result.metadata)
    }

    @Test
    fun create_with_metadata_getById() {
        // arrange
        track1.id = 123      // this will be ignored
        trackRepository.create(track1)
        trackMetadataRepository.create(metadata1)

        // act
        val result = trackRepository.getById(1)

        // assert
        assertEquals(1L, result!!.id)
        assertEquals(track1.name, result.name)
        assertEquals(track1.notes, result.notes)
        assertDatesEqual("comparing start dates", track1.started, result.started)
        assertNotNull(result.metadata)
        // just check the metadata is there - we check all the properties in the metadata repo tests
        assertEquals(metadata1.versionName, result.metadata!!.versionName)
    }

    @Test
    fun create_getAll() {
        // arrange
        trackRepository.create(track1)
        trackMetadataRepository.create(metadata1)
        trackRepository.create(track2)

        // act
        val result = trackRepository.getAll()

        // assert
        assertEquals("There should only be 2 record",2, result.size)
        // the array should be unsorted - in DB order
        assertEquals(track1.name, result[0].name)
        assertNotNull(result[0].metadata)
        assertEquals(metadata1.versionName, result[0].metadata!!.versionName)
        assertEquals(track2.name, result[1].name)
        assertNull(result[1].metadata)
    }

    @Test
    fun create_getTracksWithoutAnyMetadata() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        trackMetadataRepository.create(metadata1)
        track2 = trackRepository.create(track2)
        track2.addNewPosition(position2)
        positionRepository.createFast(position2)

        // act
        val result = trackRepository.getTracksWithoutAnyMetadata()

        // assert
        assertEquals("There should only be 1 record",1, result.size)
        // the array should be unsorted - in DB order
        assertEquals(track2.name, result[0].name)
        assertNull(result[0].metadata)
    }

    @Test
    fun create_getTracksWithoutAnyMetadata_recording() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        positionRepository.createFast(position1)
        track1.addNewPosition(recordedPosition)
        positionRepository.createFast(recordedPosition)
        trackMetadataRepository.create(metadata1)
        track2 = trackRepository.create(track2)
        track2.addNewPosition(position2)
        positionRepository.createFast(position2)

        // act
        val noMetaResults = trackRepository.getTracksWithoutAnyMetadata()
        val incRecordingResults = trackRepository.getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()

        // assert
        // check the null metadata set
        assertEquals("There should only be 1 without metadata",1, noMetaResults.size)
        // the array should be unsorted - in DB order
        assertEquals(track2.name, noMetaResults[0].name)
        assertNull(noMetaResults[0].metadata)

        // check the set including the recorded track
        assertEquals("There should only be 2 including the recorded track",2, incRecordingResults.size)
        // the array should be unsorted - in DB order
        assertEquals(track1.name, incRecordingResults[0].name)
        assertEquals(metadata1.versionName, incRecordingResults[0].metadata!!.versionName)      // recording track has metadata
        assertEquals(track2.name, incRecordingResults[1].name)
        assertNull(incRecordingResults[1].metadata)
    }
    @Test
    fun create_one_getMaxId() {
        // arrange
        trackRepository.create(track1)

        // act
        val result = trackRepository.getMaxId()

        // assert
        assertEquals(1L, result)
    }

    @Test
    fun create_two_getMaxId() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)

        // act
        val result = trackRepository.getMaxId()

        // assert
        assertEquals(2L, result)
    }

    @Test
    fun update_getById() {
        // arrange
        trackRepository.create(track1)
        val newTrack = trackRepository.create(track2)
        newTrack.name = "UPDATED2"
        trackRepository.update(newTrack)

        // act
        val result = trackRepository.getById(2)

        // assert
        assertEquals(2L, result!!.id)
        // only the name should have changed from the original create
        assertEquals(newTrack.name, result.name)
        assertEquals(track2.notes, result.notes)
        assertDatesEqual("comparing start dates", track2.started, result.started)
        assertNull(result.metadata)
    }

    @Test
    fun deleteById_getAll() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackRepository.deleteById(1)

        // act
        val result = trackRepository.getAll()

        // assert
        assertEquals("There should only be 1 record",1, result.size)
        assertEquals(track2.name, result[0].name)
    }

    @Test
    fun deleteAll_getAll() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackRepository.deleteAll()

        // act
        val result = trackRepository.getAll()

        // assert
        assertEquals("The table should be empty",0, result.size)
    }
}

