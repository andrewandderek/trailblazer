/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import androidx.test.filters.MediumTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class FileSystemHelperTests : UtilityTestsBase() {
    private lateinit var fileSystemHelper: FileSystemHelper

    private val licenseFileContents = """
Kotlin
Copyright 2010-2018 JetBrains s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

    """.trimIndent()

    @Test
    fun getFolderFiles() {
        // arrange
        fileSystemHelper = FileSystemHelper(mockLoggerFactory, mockCrashReporter, context, mockContentResolver)

        // act
        val files = fileSystemHelper.getFolderFiles("license")

        // assert
        Assert.assertNotNull(files)
        Assert.assertEquals( 2, files!!.size)
        Assert.assertEquals( files[0], "license/folder/slf4j.txt")
        Assert.assertEquals( files[1], "license/kotlin.txt")
    }

    @Test
    fun getInvalidFolderFiles() {
        // arrange
        fileSystemHelper = FileSystemHelper(mockLoggerFactory, mockCrashReporter, context, mockContentResolver)

        // act
        val files = fileSystemHelper.getFolderFiles("folder_does_not_exist")

        // assert
        Assert.assertNotNull(files)
        Assert.assertEquals( 0, files!!.size)
    }

    @Test
    fun getFileContents() {
        // arrange
        fileSystemHelper = FileSystemHelper(mockLoggerFactory, mockCrashReporter, context, mockContentResolver)

        // act
        val text = fileSystemHelper.getFileContents("license/kotlin.txt", true)

        // assert
        Assert.assertNotNull(text)
        Assert.assertEquals( licenseFileContents, text)
    }

}