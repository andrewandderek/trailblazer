/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import android.content.ContentResolver
import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.utility.AndroidResourceProvider
import com.andrewandderek.trailblazer.utility.FileSystemHelper
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IEnvironmentInformationProvider
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISystemTime
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.stubbing.Answer
import org.slf4j.helpers.NOPLogger
import java.io.StringWriter
import java.io.Writer
import java.util.Calendar

open class ExporterTestsBase {

    protected var mockLoggerFactory: ILoggerFactory
    protected var mockEnvironmentProvider: IEnvironmentInformationProvider
    protected var mockCrashReporter: ICrashReporter
    protected var mockAnalytics: IAnalyticsEngine
    protected var mockSystemTime: ISystemTime
    protected var mockContentResolver: ContentResolver
    protected lateinit var mockFileSystemHelper: IFileSystemHelper
    protected lateinit var realFileSystemHelper: IFileSystemHelper
    protected lateinit var writer: Writer

    protected lateinit var track: Track
    protected lateinit var position1: Position
    protected lateinit var position2: Position
    protected lateinit var position3: Position
    protected lateinit var now: Calendar
    protected lateinit var now2: Calendar
    protected lateinit var now3: Calendar

    protected var mockStatisticsProvider: ITrackStatisticsProvider
    protected var mockAltitudeFormatter: IAltitudeFormatter
    protected var mockDistanceFormatter: IDistanceFormatter
    protected var mockSpeedFormatter: ISpeedFormatter
    protected var mockPaceFormatter: IPaceFormatter

    protected val context: Context
    protected var resourceProvider: IResourceProvider

    init {
        mockCrashReporter = mock(ICrashReporter::class.java)
        mockLoggerFactory = mock(ILoggerFactory::class.java)
        `when`(mockLoggerFactory.logger).thenReturn(NOPLogger.NOP_LOGGER)
        mockEnvironmentProvider = mock(IEnvironmentInformationProvider::class.java)
        `when`(mockEnvironmentProvider.getApplicationVersion()).thenReturn("TEST VERSION")
        mockSystemTime = mock(ISystemTime::class.java)
        mockContentResolver = mock(ContentResolver::class.java)
        mockAnalytics = mock(IAnalyticsEngine::class.java)
        `when`(mockSystemTime.getCurrentTime()).then(Answer {
            var now = Calendar.getInstance()
            now.set(2017, 10, 30, 15, 37, 22)
            return@Answer now
        })

        mockStatisticsProvider = mock(ITrackStatisticsProvider::class.java)
        mockAltitudeFormatter = mock(IAltitudeFormatter::class.java)
        mockDistanceFormatter = mock(IDistanceFormatter::class.java)
        mockSpeedFormatter = mock(ISpeedFormatter::class.java)
        mockPaceFormatter = mock(IPaceFormatter::class.java)

        // this is an integration test run on a real device - lets use and test the real resources
        context = getApplicationContext()
        resourceProvider = AndroidResourceProvider(context)
    }

    protected fun createInMemoryWriter() {
        writer = StringWriter(1024)
        mockFileSystemHelper = mock(IFileSystemHelper::class.java)
        `when`(mockFileSystemHelper.createWriter(anyString())).thenReturn(writer)
    }

    protected fun createRealFileSystemHelper() {
        realFileSystemHelper = FileSystemHelper(mockLoggerFactory, mockCrashReporter, context, mockContentResolver)
    }

    protected fun setupNowDateTimes() {
        now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now2 = Calendar.getInstance()
        now2.time = now.time
        now2.add(Calendar.SECOND, 10)
        now3 = Calendar.getInstance()
        now3.time = now2.time
        now3.add(Calendar.SECOND, 10)
    }

    protected fun setupTrack() {
        setupTrack(3.12, 13.12, 113.12, true)
    }

    protected fun setupTrack(altitude1: Double?, altitude2: Double?, altitude3: Double?, withTimestamps: Boolean) {
        track = Track()
        track.id = 123
        track.name = "NAME1"
        track.notes = "NOTES1"
        track.started = now
        position1 = Position(1.123456, -2.123456, altitude1, if (withTimestamps) now else null, 100.0F, "POSITION_NOTES1")
        position2 = Position(11.123456, -12.123456, altitude2, if (withTimestamps) now2 else null, 200.2F, "POSITION_NOTES2")
        position3 = Position(51.123456, -112.123456, altitude3, if (withTimestamps) now3 else null, 300.3F, "POSITION_NOTES3")
        track.addNewPosition(position1)
        track.addNewPosition(position2)
        track.addNewPosition(position3)
    }

    protected fun setupSegmentedTrack() {
        setupTrack()
        position1.segment = 0
        position2.segment = 0
        position3.segment = 1
    }

    protected fun setupSegmentedTrack2() {
        setupTrack()
        position1.segment = 0
        position2.segment = 1
        position3.segment = 2
    }

    protected fun setupTrackWithMissingAltitudes(withTimestamps: Boolean) {
        setupTrack(3.12, null, null, withTimestamps)
        position1.segment = 0
        position2.segment = 1
        position3.segment = 2
    }

    protected fun setupEmptyTrack() {
        setupTrack()
        track.reset()
    }

    protected fun setupStatistics() {
        val stats = TrackStatistics()
        stats.distanceInMetres = 1.0
        stats.averageMovingSpeedInMetresPerSecond = 2.0
        stats.averageSpeedInMetresPerSecond = 3.0
        stats.elapsedTimeInMilliseconds = 4 * 1000 * 60
        stats.elevationDifferenceInMetres = 5.0
        stats.elevationGainInMetres = 6.0
        stats.elevationLossInMetres = 7.0
        stats.maxElevationInMetres = 8.0
        stats.minElevationInMetres = 9.0
        stats.movingTimeInMilliseconds = 10 * 1000 * 60
        stats.recordedTimeInMilliseconds = 11  * 1000 * 60
        `when`(mockStatisticsProvider.getStatistics(track)).thenReturn(stats)

        `when`(mockAltitudeFormatter.formatAltitude(Mockito.anyDouble(), Mockito.anyBoolean())).thenAnswer {
            invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }

        `when`(mockDistanceFormatter.formatDistance(Mockito.anyDouble(), Mockito.anyBoolean())).thenAnswer {
            invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }

        `when`(mockSpeedFormatter.formatSpeed(Mockito.anyDouble(), Mockito.anyBoolean())).thenAnswer {
            invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }

        `when`(mockPaceFormatter.formatPace(Mockito.anyDouble(), Mockito.anyBoolean())).thenAnswer {
            invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }
    }
}