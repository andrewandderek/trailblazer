/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.exception.NotSupportedException
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmlImporter
import com.andrewandderek.trailblazer.utility.MockitoHelper.capture
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

@RunWith(AndroidJUnit4::class)
@MediumTest
class KmlImporterTests : ImporterTestsBase() {
    private var loggingCaptor: ArgumentCaptor<Throwable>

    init {
        loggingCaptor = ArgumentCaptor.forClass(Throwable::class.java)
    }

    private fun createKmlImporter(): KmlImporter {
        return KmlImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)
    }

    @Test
    fun deSerialiseUnknownFormatTrack() {
        // arrange
        createInMemoryInputStreamFromAssetFile("xyz/track_unknown_format.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.UNKNOWN_FORMAT, importResult.result)
        assertNull(importResult.track)
    }

    @Test
    fun deSerialiseTrack_trailblazer() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_trailblazer.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrack_trailblazer_SegmentedTrack() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_trailblazer_segmented.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrack_trailblazer_SegmentedTrack2() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_trailblazer_segmented2.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrack_trailblazer_missing_elevation() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_trailblazer_missing_elevation.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertEquals( 3.12, importResult.track!!.currentPositionsSnapshot[0].altitude!!, 0.1)
        assertNull(importResult.track!!.currentPositionsSnapshot[1].altitude)
        assertNull(importResult.track!!.currentPositionsSnapshot[2].altitude)
    }

    @Test
    fun deSerialiseTrack_google_earth() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrack_google_earth_date_only_timestamp() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth_date_only_timestamp.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, false)
        assertEquals( "2017-12-30T00:00:00Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(importResult.track!!.currentPositionsSnapshot.get(2).timeRecorded))
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
    }

    @Test
    fun deSerialiseTrack_google_earth_multiday() {
        // arrange
        // the track has 3 segments - 4 points, 5 points and 6 points
        createInMemoryInputStreamFromAssetFile("kml/track3_google_earth_multiday.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack3(importResult.track, true)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(3).segment)

        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(4).segment)
        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(5).segment)
        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(6).segment)
        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(7).segment)
        assertEquals( 1L, importResult.track!!.currentPositionsSnapshot.get(8).segment)

        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(9).segment)
        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(10).segment)
        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(11).segment)
        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(12).segment)
        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(13).segment)
        assertEquals( 2L, importResult.track!!.currentPositionsSnapshot.get(14).segment)
    }

    @Test
    fun deSerialiseTrack_google_my_maps() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_maps.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, false)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(0).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(1).segment)
        assertEquals( 0L, importResult.track!!.currentPositionsSnapshot.get(2).segment)

        // we deliberately put an unsupported element into the file
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect",true, exception is NotSupportedException)
        assertEquals("exception message incorrect - ${exception.message}",true, exception.message?.contains("gx:altitudeOffset"))
    }

    @Test
    fun deSerialiseTrack_google_my_maps2() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track2_google_maps.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertEquals( 539, importResult.track!!.currentPositionsSnapshot.size)
        // there are seven segments
        assertEquals( 6L, importResult.track!!.lastPositionInSnapshot!!.segment)
        assertEquals(true, importResult.track!!.notes.startsWith("Created by Google My Tracks"))
    }

    @Test
    fun deSerialiseTrack_google_my_tracks2() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track2_google_tracks.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertEquals( 3144, importResult.track!!.currentPositionsSnapshot.size)
        // there is one segments
        assertEquals( 0L, importResult.track!!.lastPositionInSnapshot!!.segment)
        assertEquals(true, importResult.track!!.notes.startsWith("Created by Google My Tracks"))
    }

    @Test
    fun deSerialiseTrack_google_earth_malformed_xml_1() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth_malformed_xml_1.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertEquals(0, importResult.track!!.currentPositionsSnapshot.size)
    }

    @Test
    fun deSerialiseTrack_google_earth_malformed_xml_2() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth_malformed_xml_2.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertTrack1(importResult.track, true)        // the XML is malformed but the track is there
    }

    @Test
    fun deSerialiseTrack_google_earth_missing_timestamp() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth_missing_timestamp.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.MISMATCHED_TIMESTAMP, importResult.result)
    }

    @Test
    fun deSerialiseTrack_google_earth_no_timestamps() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth_no_timestamps.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.NO_TIMESTAMP, importResult.result)
    }

    @Test
    fun deSerialiseTrack_google_earth_no_track() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_google_earth_no_track.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("file://TEST_FILENAME")

        // assert
        assertEquals(ImportResult.OK, importResult.result)
        assertEquals(0, importResult.track!!.currentPositionsSnapshot.size)
    }

    @Test
    fun deSerialiseTrackWithInvalidUri() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_trailblazer.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("AN INVALID URI")

        // assert
        assertEquals(ImportResult.IO_ERROR, importResult.result)
        assertNull(importResult.track)
    }

    @Test
    fun deSerialiseTrackWithEmptyUri() {
        // arrange
        createInMemoryInputStreamFromAssetFile("kml/track1_trailblazer.xml")
        val importer = createKmlImporter()

        // act
        val importResult = importer.importTrack("")

        // assert
        assertEquals(ImportResult.IO_ERROR, importResult.result)
        assertNull(importResult.track)
    }

}