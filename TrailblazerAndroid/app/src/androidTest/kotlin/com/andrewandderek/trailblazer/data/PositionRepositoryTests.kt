/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.data.repository.PositionRepository
import com.andrewandderek.trailblazer.data.repository.TrackRepository
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class PositionRepositoryTests : DataTestsBase() {
    private lateinit var trackRepository: TrackRepository
    private lateinit var positionRepository: PositionRepository
    private lateinit var track1: Track
    private lateinit var position1: Position
    private lateinit var position2: Position
    private lateinit var positionWithoutAltitude: Position

    @Before
    fun before_each_test() {
        createInMemoryDatabase()
        setupNowDateTimes()
        trackRepository = TrackRepository(loggerFactory, dbHelper)
        positionRepository = PositionRepository(loggerFactory, dbHelper)

        track1 = Track()
        track1.name = "NAME1"
        track1.notes = "NOTES1"

        position1 = Position(1.0, 2.0, 3.0, now, 100.0F, "POSITION_NOTES1")
        position1.trackId = 1
        position2 = Position(11.0, 12.0, 13.0, now2, 200.2F, "POSITION_NOTES2")
        position2.trackId = 1
        positionWithoutAltitude = Position(21.0, 22.0, null, now2, 300.2F, "POSITION_NOTES3")
        positionWithoutAltitude.trackId = 1
    }

    @Test
    fun create_getById() {
        // arrange
        trackRepository.create(track1)
        position1.id = 123
        positionRepository.create(position1)

        // act
        var result = positionRepository.getById(1)

        // assert
        Assert.assertEquals(1L, result?.id)
        Assert.assertEquals(position1.altitude!!, result!!.altitude!!, 0.001)
        Assert.assertEquals(position1.latitude, result.latitude, 0.001)
        Assert.assertEquals(position1.longitude, result.longitude, 0.001)
        Assert.assertEquals(position1.notes, result.notes)
        Assert.assertEquals(0L, result.segment)
        assertDatesEqual("comparing start dates", position1.timeRecorded, result.timeRecorded)
    }

    @Test
    fun create_getById_withoutAltitude() {
        // arrange
        trackRepository.create(track1)
        positionWithoutAltitude.id = 123
        positionRepository.create(positionWithoutAltitude)

        // act
        var result = positionRepository.getById(1)

        // assert
        Assert.assertEquals(1L, result?.id)
        Assert.assertNull(result!!.altitude)
        Assert.assertEquals(positionWithoutAltitude.latitude, result.latitude, 0.001)
        Assert.assertEquals(positionWithoutAltitude.longitude, result.longitude, 0.001)
        Assert.assertEquals(positionWithoutAltitude.notes, result.notes)
        Assert.assertEquals(0L, result.segment)
        assertDatesEqual("comparing start dates", positionWithoutAltitude.timeRecorded, positionWithoutAltitude.timeRecorded)
    }

    @Test
    fun createFast_getById() {
        // arrange
        trackRepository.create(track1)
        position1.id = 123
        position1.flags = 64
        position1.segment = 123
        var id = positionRepository.createFast(position1)

        // act
        var result = positionRepository.getById(id)

        // assert
        Assert.assertEquals(1L, id)
        Assert.assertEquals(1L, result!!.id)
        Assert.assertEquals(position1.altitude!!, result.altitude!!, 0.001)
        Assert.assertEquals(position1.latitude, result.latitude, 0.001)
        Assert.assertEquals(position1.longitude, result.longitude, 0.001)
        Assert.assertEquals(position1.accuracy, result.accuracy, 0.001F)
        Assert.assertEquals(position1.notes, result.notes)
        Assert.assertEquals(64L, result.flags)
        Assert.assertEquals(123L, result.segment)
        assertDatesEqual("comparing start dates", position1.timeRecorded, result.timeRecorded)
    }

    @Test
    fun update_getById() {
        // arrange
        trackRepository.create(track1)
        var newPosition = positionRepository.create(position1)
        newPosition.notes = "UPDATED"
        positionRepository.update(newPosition)

        // act
        var result = positionRepository.getById(1)

        // assert
        Assert.assertEquals(1L, result!!.id)
        // only the notes should be updated
        Assert.assertEquals(position1.altitude!!, result.altitude!!, 0.001)
        Assert.assertEquals(position1.latitude, result.latitude, 0.001)
        Assert.assertEquals(position1.longitude, result.longitude, 0.001)
        Assert.assertEquals(newPosition.notes, result.notes)
        assertDatesEqual("comparing start dates", position1.timeRecorded, result.timeRecorded)
    }

    @Test(expected = NotImplementedError::class)
    fun deleteById_getAll() {
        // arrange
        trackRepository.create(track1)
        positionRepository.create(position1)
        positionRepository.createFast(position2)
        positionRepository.deleteById(1)

        // act
        positionRepository.getAll()
    }

    @Test(expected = NotImplementedError::class)
    fun deleteAll_throws() {
        // arrange
        trackRepository.create(track1)
        positionRepository.create(position1)
        positionRepository.createFast(position2)

        // act
        positionRepository.deleteAll()
    }

    @Test(expected = NotImplementedError::class)
    fun getAll_throws() {
        // arrange
        trackRepository.create(track1)
        positionRepository.create(position1)
        positionRepository.createFast(position2)

        // act
        positionRepository.getAll()
    }

    @Test
    fun getByTrackId() {
        // arrange
        trackRepository.create(track1)
        positionRepository.create(position1)
        positionRepository.createFast(position2)

        // act
        var result = positionRepository.getByTrackId(1)

        // assert
        Assert.assertEquals("There should only be 2 records",2, result.size)
        Assert.assertEquals(position1.notes, result[0].notes)
        Assert.assertEquals(position2.notes, result[1].notes)
    }

    @Test
    fun createTrack_getByTrackId() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        track1.addNewPosition(position2)
        positionRepository.createTrack(track1.currentPositionsSnapshot)

        // act
        var result = positionRepository.getByTrackId(1)

        // assert
        Assert.assertEquals("There should only be 2 records",2, result.size)
        Assert.assertEquals(position1.notes, result[0].notes)
        Assert.assertEquals(position2.notes, result[1].notes)
    }

    @Test
    fun createTrack_getByTrackIdWithAccuracy() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        track1.addNewPosition(position2)
        positionRepository.createTrack(track1.currentPositionsSnapshot)

        // act
        var result = positionRepository.getByTrackIdWithAccuracy(1, 101F)

        // assert
        Assert.assertEquals("There should only be 1 record",1, result.size)
        Assert.assertEquals(position1.notes, result[0].notes)
    }

    @Test
    fun createTrack_with_error_getByTrackId() {
        // arrange
        track1 = trackRepository.create(track1)
        track1.addNewPosition(position1)
        track1.addNewPosition(position2)
        track1.currentPositionsSnapshot[1].trackId = -1        // causes an error as this is an orphan position
        positionRepository.createTrack(track1.currentPositionsSnapshot)

        // act
        var result = positionRepository.getByTrackId(1)

        // assert
        Assert.assertEquals("There should be 0 records",0, result.size)
    }

    @Test
    fun getLastPositionOnTrack() {
        // arrange
        trackRepository.create(track1)
        positionRepository.create(position1)
        positionRepository.createFast(position2)

        // act
        var result = positionRepository.getLastPositionOnTrack(1)

        // assert
        Assert.assertEquals(position2.notes, result?.notes)
    }

    @Test
    fun getByTrackId_NotFound() {
        // arrange
        trackRepository.create(track1)
        positionRepository.create(position1)
        positionRepository.createFast(position2)

        // act
        var result = positionRepository.getByTrackId(2)

        // assert
        Assert.assertEquals("There should be 0 records",0, result.size)
    }

    @Test
    fun getById_NotFound() {
        // arrange

        // act
        var result = positionRepository.getById(2)

        // assert
        Assert.assertNull(result)
    }

}