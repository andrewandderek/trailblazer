/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import android.content.ContentResolver
import com.andrewandderek.trailblazer.AssetHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.ISystemTime
import org.junit.Assert
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.stubbing.Answer
import org.slf4j.helpers.NOPLogger
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset
import java.util.Calendar


open class ImporterTestsBase {
    protected var mockCrashReporter: ICrashReporter
    protected var mockLoggerFactory: ILoggerFactory
    protected var mockSystemTime: ISystemTime
    protected lateinit var mockFileSystemHelper: IFileSystemHelper
    protected var mockContentResolver: ContentResolver
    private lateinit var input: InputStream

    init {
        mockCrashReporter = mock(ICrashReporter::class.java)
        mockContentResolver = mock(ContentResolver::class.java)
        mockLoggerFactory = mock(ILoggerFactory::class.java)
        `when`(mockLoggerFactory.logger).thenReturn(NOPLogger.NOP_LOGGER)
        mockSystemTime = mock(ISystemTime::class.java)
        `when`(mockSystemTime.getCurrentTime()).then(Answer {
            var now = Calendar.getInstance()
            now.set(2017, 10, 30, 15, 37, 22)
            return@Answer now
        })
    }

    protected fun createInMemoryInputStreamFromAssetBinaryFile(fileName: String) {
        val fileContent = AssetHelper.loadTestAssetBinaryFile(fileName)
        val inputBytes = ByteArrayInputStream(fileContent)
        mockFileSystemHelper = mock(IFileSystemHelper::class.java)
        `when`(mockFileSystemHelper.createInputStream(anyString())).thenReturn(inputBytes)
    }

    protected fun createInMemoryInputStreamFromAssetFile(fileName: String) {
        createInMemoryInputStreamFromString(AssetHelper.loadTestAssetFile(fileName, true))
    }

    private fun createInMemoryInputStreamFromString(fileContent: String) {
        input = ByteArrayInputStream(Charset.forName("UTF-8").encode(fileContent).array())
        mockFileSystemHelper = mock(IFileSystemHelper::class.java)
        `when`(mockFileSystemHelper.createInputStream(anyString())).thenReturn(input)
    }

    protected fun convertCalendarToXmlSchemaDateTimeStringAllowNull(date: Calendar?): String {
        date?.let {
            return CalendarFormatter.convertCalendarToXmlSchemaDateTimeString(it)
        }
        return "<NULL>"
    }

    protected fun assertTrack1(track: Track?, withTimestamps: Boolean) {
        Assert.assertNotNull(track!!)
        Assert.assertEquals( "NAME1", track.name)
        Assert.assertEquals( "NOTES1", track.notes)
        if (withTimestamps) {
            Assert.assertEquals( "2017-12-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.started))
        }
        Assert.assertEquals( 3, track.currentPositionsSnapshot.size)

        Assert.assertEquals( 1.123456, track.currentPositionsSnapshot.get(0).latitude, 0.1)
        Assert.assertEquals( -2.123456, track.currentPositionsSnapshot.get(0).longitude, 0.1)
        Assert.assertEquals( 3.12, track.currentPositionsSnapshot.get(0).altitude!!, 0.1)
        if (withTimestamps) {
            Assert.assertEquals( "2017-12-30T02:37:22Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.currentPositionsSnapshot.get(0).timeRecorded))
        }

        Assert.assertEquals( 11.123456, track.currentPositionsSnapshot.get(1).latitude, 0.1)
        Assert.assertEquals( -12.123456, track.currentPositionsSnapshot.get(1).longitude, 0.1)
        Assert.assertEquals( 13.12, track.currentPositionsSnapshot.get(1).altitude!!, 0.1)
        if (withTimestamps) {
            Assert.assertEquals( "2017-12-30T02:37:32Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.currentPositionsSnapshot.get(1).timeRecorded))
        }

        Assert.assertEquals( 51.123456, track.currentPositionsSnapshot.get(2).latitude, 0.1)
        Assert.assertEquals( -112.123456, track.currentPositionsSnapshot.get(2).longitude, 0.1)
        Assert.assertEquals( 113.12, track.currentPositionsSnapshot.get(2).altitude!!, 0.1)
        if (withTimestamps) {
            Assert.assertEquals( "2017-12-30T02:37:42Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.currentPositionsSnapshot.get(2).timeRecorded))
        }
    }

    protected fun assertTrack3(track: Track?, withTimestamps: Boolean) {
        Assert.assertNotNull(track!!)
        Assert.assertEquals( "Milford Track Day 2", track.name)
        Assert.assertEquals( "NOTES-3-2", track.notes)
        if (withTimestamps) {
            Assert.assertEquals( "2024-02-05T02:16:02Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.started))
        }
        Assert.assertEquals( 15, track.currentPositionsSnapshot.size)

        // the track has 3 segments - 4 points, 5 points and 6 points

        // lets check the first point on each of the three segments
        Assert.assertEquals( -44.9314379, track.currentPositionsSnapshot.get(0).latitude, 0.1)
        Assert.assertEquals( 167.9293003, track.currentPositionsSnapshot.get(0).longitude, 0.1)
        Assert.assertEquals( 232.9818115234375, track.currentPositionsSnapshot.get(0).altitude!!, 0.1)
        if (withTimestamps) {
            Assert.assertEquals( "2024-02-05T02:16:02Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.currentPositionsSnapshot.get(0).timeRecorded))
        }

        Assert.assertEquals( -44.9214466, track.currentPositionsSnapshot.get(4).latitude, 0.1)
        Assert.assertEquals( 167.9286027, track.currentPositionsSnapshot.get(4).longitude, 0.1)
        Assert.assertEquals( 219.9619750976563, track.currentPositionsSnapshot.get(4).altitude!!, 0.1)
        if (withTimestamps) {
            Assert.assertEquals( "2024-02-05T19:34:39Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.currentPositionsSnapshot.get(4).timeRecorded))
        }

        Assert.assertEquals( -44.8692177, track.currentPositionsSnapshot.get(9).latitude, 0.1)
        Assert.assertEquals( 167.8396356, track.currentPositionsSnapshot.get(9).longitude, 0.1)
        Assert.assertEquals( 312.4342041015625, track.currentPositionsSnapshot.get(9).altitude!!, 0.1)
        if (withTimestamps) {
            Assert.assertEquals( "2024-02-05T23:36:51Z", convertCalendarToXmlSchemaDateTimeStringAllowNull(track.currentPositionsSnapshot.get(9).timeRecorded))
        }
    }

}