/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.TrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.TrackRepository
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.Calendar

@RunWith(AndroidJUnit4::class)
@MediumTest
class TrackMetadataRepositoryTests : DataTestsBase() {
    private lateinit var trackMetadataRepository: ITrackMetadataRepository
    private lateinit var trackRepository: TrackRepository

    private lateinit var track1: Track
    private lateinit var track2: Track
    private lateinit var metadata1: TrackMetadata
    private lateinit var metadata2: TrackMetadata
    private lateinit var metadata3: TrackMetadata

    @Before
    fun before_each_test() {
        createInMemoryDatabase()
        setupNowDateTimes()
        trackMetadataRepository = TrackMetadataRepository(loggerFactory, dbHelper)
        trackRepository = TrackRepository(loggerFactory, dbHelper)

        track1 = Track()
        track1.name = "NAME1"
        track1.notes = "NOTES1"
        track1.started = getTimeOffsetBy(now, Calendar.SECOND, 10)

        metadata1 = TrackMetadata()
        metadata1.trackId = 1
        metadata1.versionName = "VERSION_NAME_1"
        metadata1.versionCode = 101
        metadata1.ended = getTimeOffsetBy(now, Calendar.SECOND, 25)
        metadata1.statistics = setupStatistics(1)

        metadata2 = TrackMetadata()
        metadata2.trackId = 1
        metadata2.versionName = "VERSION_NAME_2"
        metadata2.versionCode = 102
        metadata2.ended = getTimeOffsetBy(now, Calendar.SECOND, 30)
        metadata2.statistics = setupStatistics(2)

        track2 = Track()
        track2.name = "NAME2"
        track2.notes = "NOTES2"
        track2.started = getTimeOffsetBy(now, Calendar.SECOND, 60)

        metadata3 = TrackMetadata()
        metadata3.trackId = 2
        metadata3.versionName = "VERSION_NAME_3"
        metadata3.versionCode = 103
        metadata3.ended = null
        metadata3.statistics = setupStatistics(3)
    }

    private fun assertMetadataEqual(meta1: TrackMetadata, meta2: TrackMetadata) {
        assertEquals("metadata track id", meta1.trackId, meta2.trackId)
        assertEquals("metadata version name", meta1.versionName, meta2.versionName)
        assertEquals("metadata version code", meta1.versionCode, meta2.versionCode)
        assertDatesEqual("comparing end dates", meta1.ended, meta2.ended)
        assertEquals("comparing stats", meta1.statistics, meta2.statistics)
    }

    @Test
    fun empty_getById() {
        // arrange

        // act
        val result = trackMetadataRepository.getById(1L)

        // assert
        assertNull(result)
    }

    @Test
    fun empty_getAll() {
        // arrange

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should 0 records", 0, result.size)
    }

    @Test(expected = NullPointerException::class)
    fun create_without_track_fails() {
        // arrange

        // act
        trackMetadataRepository.create(metadata1)
    }

    @Test
    fun create_getById() {
        // arrange
        trackRepository.create(track1)
        trackMetadataRepository.create(metadata1)

        // act
        val result = trackMetadataRepository.getById(1L)

        // assert
        assertNotNull(result)
        assertMetadataEqual(metadata1, result!!)
    }

    @Test
    fun create_getAll() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata3)

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should 2 records", 2, result.size)
        assertMetadataEqual(metadata1, result[0])
        assertMetadataEqual(metadata3, result[1])
    }

    @Test
    fun create_getWhereVersionCodeBefore_subset() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata3)

        // act
        var result = trackMetadataRepository.getWhereVersionCodeBefore(103)

        // assert
        assertEquals("There should 1 record", 1, result.size)
        assertMetadataEqual(metadata1, result[0])
    }

    @Test
    fun create_getWhereVersionCodeBefore_all() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata3)

        // act
        var result = trackMetadataRepository.getWhereVersionCodeBefore(999)

        // assert
        assertEquals("There should 2 records", 2, result.size)
        assertMetadataEqual(metadata1, result[0])
        assertMetadataEqual(metadata3, result[1])
    }

    @Test
    fun create_twice_getAll() {
        // arrange
        trackRepository.create(track1)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata2)

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should only be 1 record", 1, result.size)
        // the second metadata should replace the first
        assertMetadataEqual(metadata2, result[0])
    }

    @Test
    fun update_getAll() {
        // arrange
        trackRepository.create(track1)
        trackMetadataRepository.update(metadata1)

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should only be 1 record", 1, result.size)
        assertMetadataEqual(metadata1, result[0])
    }

    @Test
    fun update_twice_getAll() {
        // arrange
        trackRepository.create(track1)
        trackMetadataRepository.update(metadata1)
        trackMetadataRepository.update(metadata2)

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should only be 1 record", 1, result.size)
        // the second metadata should replace the first
        assertMetadataEqual(metadata2, result[0])
    }

    @Test
    fun delete_track_cascade_getAll() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata3)
        trackRepository.deleteById(1L)          // should cascade delete the metadata

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should 1 records", 1, result.size)
        assertMetadataEqual(metadata3, result[0])
    }

    @Test
    fun deleteById_getAll() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata3)
        trackMetadataRepository.deleteById(metadata1.trackId)

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should 1 records", 1, result.size)
        assertMetadataEqual(metadata3, result[0])
    }

    @Test
    fun deleteAll_getAll() {
        // arrange
        trackRepository.create(track1)
        trackRepository.create(track2)
        trackMetadataRepository.create(metadata1)
        trackMetadataRepository.create(metadata3)
        trackMetadataRepository.deleteAll()

        // act
        var result = trackMetadataRepository.getAll()

        // assert
        assertEquals("There should 0 records", 0, result.size)
    }

}