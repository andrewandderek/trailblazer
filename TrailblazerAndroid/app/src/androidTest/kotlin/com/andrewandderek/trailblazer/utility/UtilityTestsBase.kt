/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.content.ContentResolver
import android.content.Context
import com.andrewandderek.trailblazer.AssetHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.slf4j.helpers.NOPLogger

open class UtilityTestsBase {
    protected var mockLoggerFactory: ILoggerFactory
    protected var mockCrashReporter: ICrashReporter
    protected val context: Context
    protected val mockContentResolver: ContentResolver

    init {
        context = AssetHelper.testContext
        mockLoggerFactory = mock(ILoggerFactory::class.java)
        mockCrashReporter = mock(ICrashReporter::class.java)
        mockContentResolver = mock(ContentResolver::class.java)
        Mockito.`when`(mockLoggerFactory.logger).thenReturn(NOPLogger.NOP_LOGGER)
    }
}