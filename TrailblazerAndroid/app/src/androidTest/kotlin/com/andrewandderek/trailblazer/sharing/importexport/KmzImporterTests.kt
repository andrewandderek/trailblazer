/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmlImporter
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmzImporter
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class KmzImporterTests : ImporterTestsBase() {

    @Test
    fun deSerialiseTrack_kmz_mytracks_no_kml_files() {
        // arrange
        createInMemoryInputStreamFromAssetBinaryFile("kmz/no_kml_files.kmz")
        // we use a real KML importer as these are integration tests and we want to test end to end decoding of a KMZ file
        val kmlImporter = KmlImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)
        val kmzImporter = KmzImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, kmlImporter, mockCrashReporter)

        // act
        val importResult = kmzImporter.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.UNKNOWN_FORMAT, importResult.result)
    }

    @Test
    fun deSerialiseTrack_kmz_mytracks() {
        // arrange
        createInMemoryInputStreamFromAssetBinaryFile("kmz/sunday_brekkie_mytracks.kmz")
        // we use a real KML importer as these are integration tests and we want to test end to end decoding of a KMZ file
        val kmlImporter = KmlImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)
        val kmzImporter = KmzImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, kmlImporter, mockCrashReporter)

        // act
        val importResult = kmzImporter.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        Assert.assertEquals("To Sunday Brekkie ", importResult.track!!.name)
        Assert.assertEquals(993, importResult.track!!.currentPositionsSnapshot.size)
    }

    @Test
    fun deSerialiseTrack_kmz_mytracks_multiple_files() {
        // arrange
        createInMemoryInputStreamFromAssetBinaryFile("kmz/sunday_brekkie_mytracks_multiple_files.kmz")
        // we use a real KML importer as these are integration tests and we want to test end to end decoding of a KMZ file
        val kmlImporter = KmlImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)
        val kmzImporter = KmzImporter(mockLoggerFactory, mockFileSystemHelper, mockSystemTime, mockContentResolver, kmlImporter, mockCrashReporter)

        // act
        val importResult = kmzImporter.importTrack("file://TEST_FILENAME")

        // assert
        Assert.assertEquals(ImportResult.OK, importResult.result)
        Assert.assertEquals("To Sunday Brekkie ", importResult.track!!.name)
        Assert.assertEquals(993, importResult.track!!.currentPositionsSnapshot.size)
    }



}