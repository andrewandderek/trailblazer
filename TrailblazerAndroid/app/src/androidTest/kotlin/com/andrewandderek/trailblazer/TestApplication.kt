/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer

class TestApplication : AndroidApplication() {
    override fun initialiseDatabase() {
        // we dont want to create the default DB for the tests its just not useful
        loggerFactory.logger.debug("database initialisation suppressed")
    }

    override fun scheduleWorkerTasks() {
        // we dont want to schedule tasks - if that changes then change this
        loggerFactory.logger.debug("worker tasks suppressed")
    }
}