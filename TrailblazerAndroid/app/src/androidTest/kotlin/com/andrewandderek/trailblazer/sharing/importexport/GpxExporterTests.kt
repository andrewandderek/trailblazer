/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.andrewandderek.trailblazer.AssetHelper
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxExporter
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class GpxExporterTests : ExporterTestsBase()
{
    private lateinit var exporter: GpxExporter

    @Before
    fun before_each_test() {
        createInMemoryWriter()
        setupNowDateTimes()
        exporter = GpxExporter(mockLoggerFactory, mockFileSystemHelper, mockEnvironmentProvider)
    }

    @Test
    fun serialiseEmptyTrack() {
        // arrange
        setupEmptyTrack()
        setupStatistics()

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1_empty.xml", true), writer.toString())
    }

    @Test
    fun serialiseTrack() {
        // arrange
        setupTrack()

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1.xml", true), writer.toString())
    }

    @Test
    fun serialiseSegmentedTrack() {
        // arrange
        setupSegmentedTrack()

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1_segmented.xml", true), writer.toString())
    }

    @Test
    fun serialiseSegmentedTrack2() {
        // arrange
        setupSegmentedTrack2()

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1_segmented2.xml", true), writer.toString())
    }

    @Test
    fun serialiseTrackWithMissingAltitudes() {
        // arrange
        setupTrackWithMissingAltitudes(true)

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1_missing_elevation.xml", true), writer.toString())
    }

    @Test
    fun serialiseTrackWithMissingTimestamps() {
        // arrange
        setupTrack(3.12, 13.12, 113.12, false)

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1_missing_timestamps.xml", true), writer.toString())
    }

    @Test
    fun serialiseTrackWithMissingTimestampsAndAltitudes() {
        // arrange
        setupTrack(null, null, null, false)

        // act
        exporter.exportTrack(track, "TESTFILENAME")

        // assert
        Assert.assertEquals(AssetHelper.loadTestAssetFile("gpx/track1_missing_timestamps_and_elevation.xml", true), writer.toString())
    }
}