/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import androidx.test.filters.LargeTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxExporter
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxImporter
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmlExporter
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmlImporter
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.io.FileNotFoundException

@RunWith(AndroidJUnit4::class)
@LargeTest
class ExportToAndImportFromFileSystemTests : ExporterTestsBase()
{
    private lateinit var gpxExporter: GpxExporter
    private lateinit var gpxImporter: GpxImporter
    private lateinit var kmlExporter: KmlExporter
    private lateinit var kmlImporter: KmlImporter

    @Before
    fun before_each_test() {
        createRealFileSystemHelper()
        setupNowDateTimes()
        gpxExporter = GpxExporter(mockLoggerFactory, realFileSystemHelper, mockEnvironmentProvider)
        gpxImporter = GpxImporter(mockLoggerFactory, realFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)
        kmlImporter = KmlImporter(mockLoggerFactory, realFileSystemHelper, mockSystemTime, mockContentResolver, mockCrashReporter)
        kmlExporter = KmlExporter(
                mockLoggerFactory,
                realFileSystemHelper,
                mockEnvironmentProvider,
                mockSystemTime,
                mockStatisticsProvider,
                mockAltitudeFormatter,
                mockDistanceFormatter,
                mockSpeedFormatter,
                mockPaceFormatter,
                resourceProvider)

        // delete the application folder
        val folder = File(realFileSystemHelper.getApplicationFolderOnSdCard())
        // dont worry the debug build has a different application folder so it will not delete data from the production app
        deleteRecursive(folder)
        // make sure its gone
        assertApplicationFolderIsGone()
    }

    fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory && fileOrDirectory.listFiles() != null) {
            for (child in fileOrDirectory.listFiles()!!)
                deleteRecursive(child)
        }
        if (fileOrDirectory.exists()) {
            val result = fileOrDirectory.delete()
            assertTrue("test folder: ${fileOrDirectory.absoluteFile} should be deleted", result)
        }
    }

    fun assertApplicationFolderIsGone() {
        // we need to make sure we start from a known state - the folder should not be there before each test
        val folderName = realFileSystemHelper.getApplicationFolderOnSdCard()
        val folder = File(folderName)
        assertFalse("test folder: ${folderName} should not exist", folder.exists())
    }

    @Test
    fun serialiseGpxTrack() {
        // arrange
        setupTrack()
        val filename = gpxExporter.getExportFilename(track.name, true)

        // act
        gpxExporter.exportTrack(track, filename)
        val importResult = gpxImporter.importTrack("file://" + filename)

        // assert
        // not all the fields will be replicated as not all of them are supported in GPX
        assertEquals(track.name, importResult.track?.name)
        assertEquals(track.notes, importResult.track?.notes)
        assertEquals(track.positionCount, importResult.track?.positionCount)
    }

    @Test
    fun serialiseKmlTrack() {
        // arrange
        setupTrack()
        setupStatistics()
        val filename = kmlExporter.getExportFilename(track.name, true)

        // act
        kmlExporter.exportTrack(track, filename)
        // as file:// uris do not support rewinding and we try KMZ before KML we are also testing the RETRY mechanism
        val importResult = kmlImporter.importTrack("file://" + filename)

        // assert
        // not all the fields will be replicated as not all of them are supported in KML
        assertEquals(track.name, importResult.track?.name)
        assertEquals(track.notes, importResult.track?.notes)
        assertEquals(track.positionCount, importResult.track?.positionCount)
    }

    @Test(expected = FileNotFoundException::class)
    fun serialise_gpx_track_fails_if_folder_not_created() {
        // arrange
        setupTrack()
        var filename = gpxExporter.getExportFilename(track.name, false)

        // act
        gpxExporter.exportTrack(track, filename)
    }

    @Test(expected = FileNotFoundException::class)
    fun serialise_kml_track_fails_if_folder_not_created() {
        // arrange
        setupTrack()
        var filename = kmlExporter.getExportFilename(track.name, false)

        // act
        kmlExporter.exportTrack(track, filename)
    }

}