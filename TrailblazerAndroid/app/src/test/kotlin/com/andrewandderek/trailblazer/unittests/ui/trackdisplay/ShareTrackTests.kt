/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.exception.ExportException
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ShareTrackTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()

        presenter.bind(mockview, null)
        presenter.start()

        `when`(mockResourceProvider.getString(R.string.email_share_title)).thenReturn("TEST SHARE TITLE")
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    // the code for shareTrack and exportTrack is largely the same
    // as such all the error handling is the same code and unit tested in the ExportTrack tests
    // we only need to test the successful paths here

    @Test
    fun share_analytics_written() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(), anyString())).thenReturn(true)

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.ACTION_SHARE_ID, AnalyticsConstants.ACTION_SHARE_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun share_the_displayed_track() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(), anyString())).thenReturn(true)
        `when`(mockKmlExporter.exportTrack(anyObject(), anyString())).thenReturn(true)

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockGpxExporter, times(1)).getShareFilename("TEST TRACK")
        verify(mockGpxExporter, never()).getExportFilename(anyObject(), anyBoolean())
        verify(mockKmlExporter, times(1)).getShareFilename("TEST TRACK")
        verify(mockKmlExporter, never()).getExportFilename(anyObject(), anyBoolean())
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).startIntent(mockIntent, "TEST SHARE TITLE")
    }

    @Test
    fun share_the_displayed_track_with_one_failure() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        // kotlin != java
        // see https://stackoverflow.com/questions/48978304/throwing-exceptions-with-mockito-in-kotlin
        `when`(mockGpxExporter.exportTrack(anyObject(), anyString())).thenAnswer {
            throw ExportException("TEST", null)
        }
        `when`(mockKmlExporter.exportTrack(anyObject(), anyString())).thenReturn(true)

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockGpxExporter, times(1)).getShareFilename("TEST TRACK")
        verify(mockGpxExporter, never()).getExportFilename(anyObject(), anyBoolean())
        verify(mockKmlExporter, times(1)).getShareFilename("TEST TRACK")
        verify(mockKmlExporter, never()).getExportFilename(anyObject(), anyBoolean())
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).startIntent(mockIntent, "TEST SHARE TITLE")
    }

    @Test
    fun share_the_displayed_track_with_all_failures() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        // kotlin != java
        // see https://stackoverflow.com/questions/48978304/throwing-exceptions-with-mockito-in-kotlin
        `when`(mockGpxExporter.exportTrack(anyObject(), anyString())).thenAnswer {
            throw ExportException("TEST", null)
        }
        `when`(mockKmlExporter.exportTrack(anyObject(), anyString())).thenAnswer {
            throw ExportException("TEST", null)
        }

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockGpxExporter, times(1)).getShareFilename("TEST TRACK")
        verify(mockGpxExporter, never()).getExportFilename(anyObject(), anyBoolean())
        verify(mockKmlExporter, times(1)).getShareFilename("TEST TRACK")
        verify(mockKmlExporter, never()).getExportFilename(anyObject(), anyBoolean())
        verify(mockview, times(1)).showMessage(anyInt())
        verify(mockview, never()).startIntent(anyObject(), anyString())
    }


}