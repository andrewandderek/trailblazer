/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.statistics.IDistanceProvider
import com.andrewandderek.trailblazer.statistics.TrackStatisticsProvider
import com.andrewandderek.trailblazer.statistics.elevation.DefaultElevationSettings
import com.andrewandderek.trailblazer.statistics.elevation.ElevationAlgorithmFactory
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithmFactory
import com.andrewandderek.trailblazer.statistics.elevation.IElevationSettings
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`

class TrackStatisticsProviderTests : MockingAnnotationSetup() {
    private val TEST_STATISTICS_DELTA = 0.001

    // we need to mock this or make this an integration test - not sure which is the right thing to do
    @Mock protected lateinit var mockDistanceProvider: IDistanceProvider

    protected lateinit var elevationAlgorithmFactory: IElevationAlgorithmFactory
    protected lateinit var elevationSettings: IElevationSettings

    private lateinit var statisticsProvider: TrackStatisticsProvider

    @Before
    fun setupTest() {
        elevationSettings = DefaultElevationSettings()
        elevationAlgorithmFactory = ElevationAlgorithmFactory(elevationSettings)

        statisticsProvider = TrackStatisticsProvider(mockDistanceProvider, elevationAlgorithmFactory, elevationSettings)
    }

    private fun createEmptyTrack(): Track {
        var track = Track()
        track.started.timeInMillis = 0      // so we can just use offsets in the track
        return track
    }

    private fun createTrackWithPositions(): Track {
        val track = createEmptyTrack()

        track.addNewPosition(Position(1.1, 2.2, 3.3, 100))
        track.addNewPosition(Position(4.4, 5.5, 6.6, 200))

        return track
    }

    private fun createTrackWithInaccuratePositions(): Track {
        val track = createEmptyTrack()

        track.addNewPosition(Position(1.1, 2.2, 3.3, 100))
        // this position is inaccurate and should be filtered
        track.addNewPosition(Position(2.2, 3.3, 4.4, 100, 100F))
        track.addNewPosition(Position(4.4, 5.5, 6.6, 200))

        return track
    }

    @Test
    fun getStatistics_emptyTrack_returnsZeroedStatistics() {
        // arrange
        val track = createEmptyTrack()

        // act
        val result = statisticsProvider.getStatistics(track)

        // assert
        assertEquals(0.0, result.distanceInMetres, TEST_STATISTICS_DELTA)
        assertEquals(0, result.elapsedTimeInMilliseconds)
        assertEquals(0, result.recordedTimeInMilliseconds)
    }

    @Test
    fun getStatistics_nonEmptyTrack_callsAllCalculatorsWithAllPositionsAndUpdatesStatistics() {
        // arrange
        val track = createTrackWithPositions()
        `when`(mockDistanceProvider.getDistanceInMetres(1.1, 2.2, 4.4, 5.5))
                .thenReturn(6.6)

        // act
        val result = statisticsProvider.getStatistics(track)

        // assert
        // The mock calculators update distance and elapsed time based on the positions
        assertEquals(6.6, result.distanceInMetres, TEST_STATISTICS_DELTA)
        assertEquals(200, result.elapsedTimeInMilliseconds)
    }

    @Test
    fun getStatistics_nonEmptyTrack_callsAllCalculatorsWithAllFilteredPositionsAndUpdatesStatistics() {
        // arrange
        val track = createTrackWithInaccuratePositions()
        `when`(mockDistanceProvider.getDistanceInMetres(1.1, 2.2, 4.4, 5.5))
                .thenReturn(6.6)

        // act
        val result = statisticsProvider.getStatistics(track)

        // assert
        // The mock calculators update distance and elapsed time based on the positions
        assertEquals(6.6, result.distanceInMetres, TEST_STATISTICS_DELTA)
        assertEquals(200, result.elapsedTimeInMilliseconds)
    }

}