/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.metadata.track.singletrackmetadatagenerator

import com.andrewandderek.trailblazer.BuildConfig
import com.andrewandderek.trailblazer.helpers.CalendarHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.unittests.metadata.track.GeneratorSetup
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class GenerateMetadataForTrackTests : GeneratorSetup() {

    @Before
    fun before_each_test() {
        setupGenerator()
    }

    private fun verifyBasicMetadataProperties(metadata: TrackMetadata, trackNo: Long) {
        assertEquals(trackNo, metadata.trackId)
        assertEquals(BuildConfig.VERSION_NAME, metadata.versionName)
        assertEquals(BuildConfig.VERSION_CODE, metadata.versionCode)
    }

    @Test
    fun generator_sets_track_ended_time_correctly() {
        // arrange
        val track = TrackHelper.setupTrack(1, true)

        // act
        singleGenerator.generateMetadataForTrack(track)

        // assert
        verify(mockTrackMetadataRepository, times(1)).update(capture(metadataCaptor))
        val metadata = metadataCaptor.value

        verifyBasicMetadataProperties(metadata, 1)
        CalendarHelper.assertDatesEqual("end data", track.lastPositionInSnapshot!!.timeRecorded, metadata.ended)
    }

    @Test
    fun generator_sets_track_ended_time_correctly_when_there_are_no_positions() {
        // arrange
        val track = TrackHelper.setupTrack(1)

        // act
        singleGenerator.generateMetadataForTrack(track)

        // assert
        verify(mockTrackMetadataRepository, times(1)).update(capture(metadataCaptor))
        val metadata = metadataCaptor.value

        verifyBasicMetadataProperties(metadata, 1)
        assertNull(metadata.ended)
    }

    @Test
    fun generator_sets_track_statistics() {
        // arrange
        val track = TrackHelper.setupTrack(1)

        // act
        singleGenerator.generateMetadataForTrack(track)

        // assert
        verify(mockTrackMetadataRepository, times(1)).update(capture(metadataCaptor))
        val metadata = metadataCaptor.value

        verifyBasicMetadataProperties(metadata, 1)
        assertEquals(trackStatistics, metadata.statistics)
    }
}