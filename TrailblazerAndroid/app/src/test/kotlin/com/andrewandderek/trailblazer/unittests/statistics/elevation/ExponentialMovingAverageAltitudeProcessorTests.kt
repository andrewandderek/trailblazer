/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics.elevation

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.statistics.elevation.ExponentialMovingAverageAltitudeProcessor
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ExponentialMovingAverageAltitudeProcessorTests {

    private lateinit var processor: ExponentialMovingAverageAltitudeProcessor

    @Before
    fun setupTest() {
        processor = ExponentialMovingAverageAltitudeProcessor()
    }

    fun createPosition(altitude: Double?, processedAltitude: Double? = null): Position {
        val position = Position(1.0, 2.0, altitude, 0)
        position.processedAltitude = processedAltitude

        return position
    }

    @Test
    fun reset_positionWithoutAltitude_setsProcessedAltitudeToNull() {
        // arrange
        // We would never have this really, just checking that the processedAltitude is set to null
        val position = createPosition(null, 1.2)

        // act
        processor.reset(position)

        // assert
        Assert.assertNull(position.processedAltitude)
    }

    @Test
    fun reset_positionWithAltitude_setsProcessedAltitudeToOriginalAltitude() {
        // arrange
        val position = createPosition(2.3)

        // act
        processor.reset(position)

        // assert
        Assert.assertEquals(2.3, position.processedAltitude)
    }

    @Test
    fun process_positionWithoutAltitude_setsProcessedAltitudeToCurrentAverage() {
        // arrange
        val position1 = createPosition(3.4)
        val position2 = createPosition(null)
        processor.reset(position1)

        // act
        processor.process(position2)

        // assert
        Assert.assertEquals(3.4, position2.processedAltitude)
    }

    @Test
    fun process_positionWithAltitude_calculatesNewAverageAndSetsProcessedAltitude() {
        // arrange
        val position1 = createPosition(2.0)
        val position2 = createPosition(4.0)
        val position3 = createPosition(6.0)
        val position4 = createPosition(8.0)
        processor.reset(position1)

        // act
        processor.process(position2)
        processor.process(position3)
        processor.process(position4)

        // assert
        // These are correct for an alpha of 0.05
        Assert.assertEquals(2.1, position2.processedAltitude)
        Assert.assertEquals(2.295, position3.processedAltitude)
        Assert.assertEquals(2.58025, position4.processedAltitude)
    }
}