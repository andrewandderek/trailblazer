/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify

class TrackSearchActionTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun searchActionStart_sets_empty_list_text_correctly() {
        // arrange

        // act
        presenter.searchActionStart()

        // assert
        verify(mockview, Mockito.times(1)).setEmptyListText(R.string.no_matching_tracks_text)
    }

    @Test
    fun searchActionStart_hides_non_search_ui() {
        // arrange

        // act
        presenter.searchActionStart()

        // assert
        verify(mockview, Mockito.times(1)).hideNonSearchUi()
    }

    @Test
    fun searchActionEnd_sets_empty_list_text_correctly() {
        // arrange

        // act
        presenter.searchActionEnd()

        // assert
        verify(mockview, Mockito.times(1)).setEmptyListText(R.string.no_tracks_text)
    }

    @Test
    fun searchActionEnd_shows_non_search_ui() {
        // arrange

        // act
        presenter.searchActionEnd()

        // assert
        verify(mockview, Mockito.times(1)).showNonSearchUi()
    }

    @Test
    fun searchActionEnd_resets_search_text() {
        // arrange
        presenter.searchActionStart()
        presenter.trackNameSearchTextChange("blah")

        // act
        presenter.searchActionEnd()

        // assert
        assertNull(presenter.trackNameSearch)
    }
}