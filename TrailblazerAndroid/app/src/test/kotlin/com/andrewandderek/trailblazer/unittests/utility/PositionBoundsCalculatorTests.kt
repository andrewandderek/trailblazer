/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.utility

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.utility.PositionBoundsCalculator
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test

class PositionBoundsCalculatorTests {

    companion object {
        const val BOUNDS_TEST_DELTA = 0.001
    }

    private lateinit var calculator: PositionBoundsCalculator

    @Before
    fun before_each_test() {
        calculator = PositionBoundsCalculator()
    }

    private fun createPosition(latitude: Double, longitude: Double): Position {
        return Position(latitude, longitude, null, 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun calculateBounds_empty_list_throws() {
        // arrange

        // act
        calculator.calculateBounds(emptyList())

        // assert
        fail("Exception should have been thrown")
    }

    @Test
    fun calculateBounds_single_position_provides_both_corners() {
        // arrange
        val singlePosition = createPosition(1.2, 3.4)

        // act
        val result = calculator.calculateBounds(listOf(singlePosition))

        // assert
        assertEquals(1.2, result.southWest.latitude, BOUNDS_TEST_DELTA)
        assertEquals(3.4, result.southWest.longitude, BOUNDS_TEST_DELTA)
        assertEquals(1.2, result.northEast.latitude, BOUNDS_TEST_DELTA)
        assertEquals(3.4, result.northEast.longitude, BOUNDS_TEST_DELTA)
    }

    @Test
    fun calculateBounds_multiple_positions_uses_min_and_max_lat_and_longs() {
        // arrange
        val positions = listOf(
                createPosition(5.5, 55.5),
                createPosition(4.4, 66.6),
                createPosition(6.6, 33.3),
                createPosition(3.3, 77.7),
                createPosition(7.7, 44.4))

        // act
        val result = calculator.calculateBounds(positions)

        // assert
        assertEquals(3.3, result.southWest.latitude, BOUNDS_TEST_DELTA)
        assertEquals(33.3, result.southWest.longitude, BOUNDS_TEST_DELTA)
        assertEquals(7.7, result.northEast.latitude, BOUNDS_TEST_DELTA)
        assertEquals(77.7, result.northEast.longitude, BOUNDS_TEST_DELTA)
    }
}