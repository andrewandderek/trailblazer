/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.event.TrackImportedEvent
import com.andrewandderek.trailblazer.event.TrackRenamedEvent
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class EventBusTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun unknown_event_is_ignored() {
        // arrange

        // act
        eventBus.publish("UNKNOWN")

        // assert
        verify(mockview, never()).tracksLoaded()
    }

    @Test
    fun status_change_updates_ui() {
        // arrange
        reset(mockview)

        // act
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, 1, false))

        // assert
        verify(mockview, times(1)).tracksLoaded()
        verify(mockview, times(1)).setTitle("TEST_TITLE")
    }

    @Test
    fun status_change_triggers_rename() {
        // arrange
        presenter.loadTracks()

        // act
        reset(mockview)             // we only want to verify the event processing
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, 1, false))

        // assert
        verify(mockview, times(1)).promptForTrackNameAfterStopping("1", "Track With points")
    }

    @Test
    fun status_change_from_notification_updates_ui() {
        // arrange
        reset(mockview)

        // act
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, 1, true))

        // assert
        verify(mockview, times(1)).tracksLoaded()
    }

    @Test
    fun status_change_from_notification_does_not_triggers_rename() {
        // arrange
        presenter.loadTracks()

        // act
        reset(mockview)             // we only want to verify the event processing
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, 1, true))

        // assert
        verify(mockview, never()).promptForTrackNameAfterStopping(anyString(), anyString())
    }

    @Test
    fun track_imported_event_reloads_tracks() {
        // arrange
        reset(mockview)

        // act
        eventBus.publish(TrackImportedEvent(123))

        // assert
        verify(mockTrackRepository, times(1)).getTrackListWithoutAnyPositions()
        verify(mockview, times(1)).startProgress()
        verify(mockview, times(1)).completeProgress()
    }

    @Test
    fun track_renamed_event_reloads_tracks() {
        // arrange
        reset(mockview)

        // act
        eventBus.publish(TrackRenamedEvent(123, "old name", "new name"))

        // assert
        verify(mockTrackRepository, times(1)).getTrackListWithoutAnyPositions()
        verify(mockview, times(1)).startProgress()
        verify(mockview, times(1)).completeProgress()
    }

    @Test
    fun track_renamed_event_does_optimised_update() {
        // arrange
        presenter.loadTracks()

        // act
        reset(mockview)             // we only want to verify the event processing
        reset(mockTrackRepository)
        eventBus.publish(TrackRenamedEvent(1, "old name", "new name"))

        // assert
        verify(mockTrackRepository, never()).getTrackListWithoutAnyPositions()
        verify(mockview, never()).startProgress()
        verify(mockview, never()).completeProgress()
        verify(mockview, times(1)).tracksLoaded()
    }
}