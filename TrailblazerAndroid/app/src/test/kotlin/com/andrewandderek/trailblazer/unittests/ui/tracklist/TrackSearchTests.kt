/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.model.Track
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import java.util.concurrent.TimeUnit

class TrackSearchTests : TrackListPresenterSetup() {

    private lateinit var scheduler: TestScheduler

    @Before
    fun before_each_test() {
        scheduler = TestScheduler()
        setupPresenter(scheduler)
        `when`(mockResourceProvider.getString(R.string.track_list_item_title_fmt)).thenReturn("TEST_TITLE %s")
        presenter.bind(mockview, null)
        setupTrackListItems(3)
        presenter.loadTracks()
        presenter.searchActionStart()
    }

    @After
    fun after_each_test() {
        presenter.searchActionEnd()
        presenter.unbind(mockview)
    }

    private fun setupNonMatchingTrack(track: Track, searchText: String) {
        `when`(mockTrackMatcher.isMatch(track.name, searchText)).thenReturn(false)
    }

    private fun setSearchTextAndAdvanceTimePastDebounce(searchText: String?) {
        presenter.trackNameSearchTextChange(searchText)
        scheduler.advanceTimeBy(500, TimeUnit.MILLISECONDS)
    }

    @Test
    fun trackList_null_search_text_set_does_not_call_track_matcher() {
        // arrange

        // act
        setSearchTextAndAdvanceTimePastDebounce(null)

        // assert
        verify(mockCrashReporter, never()).logNonFatalException(anyObject())
        verify(mockTrackMatcher, never()).isMatch(anyObject(), anyObject())
    }

    @Test
    fun trackList_empty_search_text_set_does_not_call_track_matcher() {
        // arrange

        // act
        setSearchTextAndAdvanceTimePastDebounce("")

        // assert
        verify(mockCrashReporter, never()).logNonFatalException(anyObject())
        verify(mockTrackMatcher, never()).isMatch(anyObject(), anyObject())
    }

    @Test
    fun trackList_search_text_set_calls_track_matcher_for_each_track() {
        // arrange

        // act
        setSearchTextAndAdvanceTimePastDebounce("blah")

        // assert
        verify(mockTrackMatcher, times(1)).isMatch("Track With points", "blah")
        verify(mockTrackMatcher, times(1)).isMatch("Track Without points", "blah")
        verify(mockTrackMatcher, times(1)).isMatch("Another Track With points", "blah")
    }

    @Test
    fun trackList_removes_non_matching_tracks_from_visible_items() {
        // arrange
        setupNonMatchingTrack(trackWithoutPositions, "banana")

        // act
        setSearchTextAndAdvanceTimePastDebounce("banana")

        // assert
        assertEquals(2, presenter.itemCount)
        assertEquals(trackWithPositions.id, presenter.getTrackId(0))
        assertEquals(anotherTrackWithPositionsAndMetadata.id, presenter.getTrackId(1))
    }

    @Test
    fun trackList_uses_visible_items_when_getting_track_labels() {
        // arrange
        setupNonMatchingTrack(trackWithPositions, "foo")

        // act
        setSearchTextAndAdvanceTimePastDebounce("foo")

        // assert
        assertEquals(2, presenter.itemCount)
        assertEquals("TEST_TITLE Track Without points", presenter.getTrackLabel(0))
        assertEquals("TEST_TITLE Another Track With points", presenter.getTrackLabel(1))
    }

    @Test
    fun trackList_search_text_set_notifies_view_tracks_loaded() {
        // arrange

        // act
        setSearchTextAndAdvanceTimePastDebounce("blah")

        // assert
        // 1 when the tracks are initially loaded, 1 for the search
        verify(mockview, times(2)).tracksLoaded()
    }

    @Test
    fun trackList_search_text_set_second_time_after_debounce_notifies_view_tracks_loaded_again() {
        // arrange

        // act
        setSearchTextAndAdvanceTimePastDebounce("blah")
        setSearchTextAndAdvanceTimePastDebounce("blah blah")

        // assert
        // 1 when the tracks are initially loaded, 1 for each of the searches
        verify(mockview, times(3)).tracksLoaded()
    }

    @Test
    fun trackList_search_text_set_multiple_times_within_debounce_only_notifies_view_tracks_loaded_for_last_search() {
        // arrange

        // act
        presenter.trackNameSearchTextChange("blah")
        presenter.trackNameSearchTextChange("blah b")
        presenter.trackNameSearchTextChange("blah bla")
        setSearchTextAndAdvanceTimePastDebounce("blah blah")

        // assert
        // 1 when the tracks are initially loaded, 1 for the last search
        verify(mockview, times(2)).tracksLoaded()
    }

    @Test
    fun trackList_clearing_search_text_makes_all_tracks_visible() {
        // arrange
        setupNonMatchingTrack(trackWithoutPositions, "banana")
        setSearchTextAndAdvanceTimePastDebounce("banana")
        assertEquals(2, presenter.itemCount)

        // act
        presenter.trackNameSearchTextClear()

        // assert
        assertEquals(3, presenter.itemCount)
        assertEquals(trackWithPositions.id, presenter.getTrackId(0))
        assertEquals(trackWithoutPositions.id, presenter.getTrackId(1))
        assertEquals(anotherTrackWithPositionsAndMetadata.id, presenter.getTrackId(2))
    }
}