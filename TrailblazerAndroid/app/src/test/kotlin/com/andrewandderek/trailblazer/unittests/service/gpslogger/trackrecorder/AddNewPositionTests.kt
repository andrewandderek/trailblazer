/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.helpers.SystemTimeHelper
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import io.reactivex.rxjava3.schedulers.Schedulers
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsEqual.equalTo
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class AddNewPositionTests : TrackRecorderSetup() {
    private lateinit var position1: Position
    private lateinit var position2: Position

    @Before
    fun before_each_test() {
        setupTrackRecorder()
        position1 = Position(1.0, 2.0, 3.0, SystemTimeHelper.testDate(), 100F)
        position2 = Position(11.0, 12.0, 13.0, SystemTimeHelper.testDate(), 100F)
        track = Track()
        track.id = 1    // we need to pretend its been saved to the DB
        `when`(mockTrackRepository.create(MockitoHelper.anyObject())).thenReturn(track)
    }

    @After
    fun after_each_test() {
        trackRecorder.stop(false)
    }

    @Test
    fun add_first_position() {
        // arrange
        trackRecorder.createNewTrack()
        trackRecorder.resume(false)

        // act
        trackRecorder.addNewPosition(mockService, position1)

        // assert
        verify(mockPositionRepository, times(1)).createFast(position1)
        Assert.assertEquals("there should only be one position", 1, track.currentPositionsSnapshot.count())
    }

    @Test
    fun add_second_position() {
        // arrange
        trackRecorder.createNewTrack()
        trackRecorder.resume(false)
        trackRecorder.addNewPosition(mockService, position1)

        // act
        trackRecorder.addNewPosition(mockService, position2)

        // assert
        verify(mockPositionRepository, times(1)).createFast(position1)
        verify(mockPositionRepository, times(1)).createFast(position2)
        Assert.assertEquals("there should be two positions", 2, track.currentPositionsSnapshot.count())
        assertThat(track.currentPositionsSnapshot[0], `is`(equalTo(position1)))
        assertThat(track.currentPositionsSnapshot[1], `is`(equalTo(position2)))
        assertThat(position1.segment, `is`(0L))
        assertThat(position2.segment, `is`(0L))
    }

    @Test
    fun add_second_position_with_pause() {
        // arrange
        trackRecorder.createNewTrack()
        trackRecorder.resume(false)
        trackRecorder.addNewPosition(mockService, position1)
        trackRecorder.pause(false)
        trackRecorder.resume(false)

        // act
        trackRecorder.addNewPosition(mockService, position2)

        // assert
        verify(mockPositionRepository, times(1)).createFast(position1)
        verify(mockPositionRepository, times(1)).createFast(position2)
        Assert.assertEquals("there should be two positions", 2, track.currentPositionsSnapshot.count())
        assertThat(track.currentPositionsSnapshot[0], `is`(equalTo(position1)))
        assertThat(track.currentPositionsSnapshot[1], `is`(equalTo(position2)))
        assertThat(position1.segment, `is`(0L))
        assertThat(position2.segment, `is`(1L))
    }

    @Test
    fun add_first_position_fires_subscribers() {
        // arrange
        var observedPoints: List<Position>? = null
        var numberOfTriggeredUpdates: Int = 0
        trackRecorder.createNewTrack()
        trackRecorder.resume(false)
        var observable = trackRecorder.currentTrackPositions
        var trackPositionsSubscriber = observable
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(
                        { points ->
                            observedPoints = points
                            numberOfTriggeredUpdates += 1
                        }
                )


        // act
        trackRecorder.addNewPosition(mockService, position2)
        trackPositionsSubscriber.dispose()

        // assert
        Assert.assertEquals("we should have observed one change", 1, numberOfTriggeredUpdates)
        Assert.assertEquals("we should have observed one point", 1, observedPoints!!.count())
    }

    @Test
    fun add_second_position_fires_subscribers() {
        // arrange
        var observedPoints: List<Position>? = null
        var numberOfTriggeredUpdates: Int = 0
        trackRecorder.createNewTrack()
        trackRecorder.resume(false)
        var observable = trackRecorder.currentTrackPositions
        var trackPositionsSubscriber = observable
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(
                        { points ->
                            observedPoints = points
                            numberOfTriggeredUpdates += 1
                        }
                )
        trackRecorder.addNewPosition(mockService, position2)


        // act
        trackRecorder.addNewPosition(mockService, position2)
        trackPositionsSubscriber.dispose()

        // assert
        Assert.assertEquals("we should have observed 2 changes", 2, numberOfTriggeredUpdates)
        Assert.assertEquals("we should have observed 2 points", 2, observedPoints!!.count())
    }

    @Test
    fun add_position_when_not_recording() {
        // arrange
        trackRecorder.createNewTrack()
        trackRecorder.resume(false)
        trackRecorder.pause(false)

        // act
        trackRecorder.addNewPosition(mockService, position1)

        // assert
        verify(mockPositionRepository, never()).createFast(position1)
        Assert.assertEquals("there should be no recorded positions", 0, track.currentPositionsSnapshot.count())
    }
}