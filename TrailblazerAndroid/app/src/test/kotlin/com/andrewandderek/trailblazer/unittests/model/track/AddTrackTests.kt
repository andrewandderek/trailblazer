/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class AddTrackTests : TrackSetup() {
    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun addTrack_whenEmpty() {
        // arrange
        val track = setupEmptyTrack()
        val otherTrack = setupTrackWithTwoPoints(true, true)

        // act
        track.addTrack(otherTrack)

        // assert
        assertEquals(false, track.isEmpty)
        assertEquals(0L, track.lastPositionInSnapshotSegment)
        assertEquals(2, track.currentPositionsSnapshot.size)
    }

    @Test
    fun addTrack_whenNotEmpty() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)
        val otherTrack = setupTrackWithTwoPoints(true, true)

        // act
        track.addTrack(otherTrack)

        // assert
        assertEquals(false, track.isEmpty)
        assertEquals(1L, track.lastPositionInSnapshotSegment)
        assertEquals(4, track.currentPositionsSnapshot.size)
    }

    @Test
    fun addTrack_whenSegmented() {
        // arrange
        val track = setupSegmentedTrack(true, true)
        val otherTrack = setupTrackWithTwoPoints(true, true)

        // act
        track.addTrack(otherTrack)

        // assert
        assertEquals(false, track.isEmpty)
        assertEquals(
            3L,
            track.lastPositionInSnapshotSegment
        )       // there is an empty segment
        assertEquals(5, track.currentPositionsSnapshot.size)
    }


    @Test
    fun addTrack_whenEmpty_otherSegmented() {
        // arrange
        val track = setupEmptyTrack()
        val otherTrack = setupSegmentedTrack(true, true)

        // act
        track.addTrack(otherTrack)

        // assert
        assertEquals(false, track.isEmpty)
        assertEquals(
            2L,
            track.lastPositionInSnapshotSegment
        )           // there is an empty segment
        assertEquals(3, track.currentPositionsSnapshot.size)
    }

    @Test
    fun addTrack_whenNotEmpty_otherSegmented() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)
        val otherTrack = setupSegmentedTrack(true, true)

        // act
        track.addTrack(otherTrack)

        // assert
        assertEquals(false, track.isEmpty)
        assertEquals(
            3L,
            track.lastPositionInSnapshotSegment
        )            // there is an empty segment
        assertEquals(5, track.currentPositionsSnapshot.size)
    }

    @Test
    fun addTrack_whenSegmented_otherSegmented() {
        // arrange
        val track = setupSegmentedTrack(true, true)
        val otherTrack = setupSegmentedTrack(true, true)

        // act
        track.addTrack(otherTrack)

        // assert
        assertEquals(false, track.isEmpty)
        assertEquals(
            5L,
            track.lastPositionInSnapshotSegment
        )           // there is an empty segment in each track
        assertEquals(6, track.currentPositionsSnapshot.size)
    }

}