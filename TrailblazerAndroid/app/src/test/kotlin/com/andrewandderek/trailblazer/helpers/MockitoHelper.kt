/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import org.mockito.ArgumentCaptor
import org.mockito.Mockito

object MockitoHelper {
    // we have a problem - Mockito any() can return a null object and this will clash with Kotlin functions that do not accept a nullable type
    // we could use mockito-kotlin
    //  https://github.com/nhaarman/mockito-kotlin
    // but I think that using these two small functions mean we dont need the dependency to use any()
    // see
    //  https://medium.com/@elye.project/befriending-kotlin-and-mockito-1c2e7b0ef791
    //  https://stackoverflow.com/questions/30305217/is-it-possible-to-use-mockito-in-kotlin
    fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }
    @Suppress("UNCHECKED_CAST")
    fun <T> uninitialized(): T =  null as T

    // also we cannot write mock = mock(GenericClass<Type>::class.java)
    // as we get this error - Gradle: Only classes are allowed on the left hand side of a class literal
    // see
    // https://discuss.kotlinlang.org/t/kotlin-unit-test-failing-when-using-generics-and-mockito/1866
    inline fun <reified T: Any> mockGenericClass() = Mockito.mock(T::class.java)

    // use this in place of captor.capture() if you are trying to capture an argument that is not nullable
    fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

     // Returns Mockito.eq() as nullable type to avoid java.lang.IllegalStateException when
     // null is returned.
     //
     // Generic T is nullable because implicitly bounded by Any?.
     //
    fun <T> eq(obj: T): T = Mockito.eq<T>(obj)
}
