/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.exception.TrackCombineException
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Captor
import org.mockito.Mockito.never
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.stubbing.Answer

class MultiselectCombineTests : TrackListPresenterSetup() {
    @Captor
    private lateinit var loggingCaptor: ArgumentCaptor<Throwable>
    @Captor
    private lateinit var trackCaptor: ArgumentCaptor<Track>
    @Captor
    private lateinit var positionsCaptor: ArgumentCaptor<List<Position>>

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
        setupTrackListItems(3)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun not_in_select_mode_and_combine() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.onActionItemClicked(R.id.action_combine_track)

        // assert
        verify(mockview, never()).promptForCombinedTrackName(anyString())
    }

    @Test
    fun nothing_selected_and_combined() {
        // arrange
        presenter.loadTracks()
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(1, 0)

        // act
        presenter.onActionItemClicked(R.id.action_combine_track)

        // assert
        verify(mockview, never()).promptForCombinedTrackName(anyString())
    }

    @Test
    fun item_1_selected_and_combined() {
        // arrange
        loadTracksAndSelect1()

        // act
        presenter.onActionItemClicked(R.id.action_combine_track)

        // assert
        verify(mockview, times(1)).showMessage(R.string.combine_select_error)
        verify(mockview, never()).promptForCombinedTrackName(anyString())
    }

    @Test
    fun item_1_and_2_selected_and_combined() {
        // arrange
        loadTracksAndSelect1and2()

        // act
        presenter.onActionItemClicked(R.id.action_combine_track)

        // assert
        verify(mockview, times(1)).promptForCombinedTrackName("COMBINE_TRACK_NAME Track With points")
    }

    @Test
    fun item_1_and_3_selected_and_combined_and_confirmed() {
        // arrange
        loadTracksAndSelect1and3()
        presenter.onActionItemClicked(R.id.action_combine_track)
        `when`(mockTrackRepository.create(anyObject())).then(Answer {
            val track = it.arguments[0] as Track
            track.id = 99       // pretend it got created
            return@Answer track
        })

        // act
        reset(mockview)
        presenter.confirmedCombineSelectedTracks(COMBINED_NAME)

        // assert
        // tracks combined
        verify(mockTrackRepository, times(1)).create(anyObject())
        verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(1, Position.ACCURACY_THRESHOLD, mockPositionRepository)
        verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(3, Position.ACCURACY_THRESHOLD, mockPositionRepository)

        verify(mockTrackRepository, times(1)).update(capture(trackCaptor))
        // it should be the new track
        assertEquals(99, trackCaptor.value.id)
        // it should have the date of the first track
        assertEquals("2017-12-30T02:37:22Z", CalendarFormatter.convertCalendarToXmlSchemaDateTimeString(trackCaptor.value.started))
        verify(mockPositionRepository, times(1)).createTrack(capture(positionsCaptor))
        // both positions should be there - the Track.AddTrack() tests will test the other combinations
        assertEquals(2, positionsCaptor.value.size)

        // UI updated
        verify(mockview, times(1)).startProgress(R.string.track_multi_combining_progress, 3)
        verify(mockview, times(1)).updateProgress(1)
        verify(mockview, times(1)).updateProgress(2)
        verify(mockview, times(1)).updateProgress(3)
        verify(mockview, times(2)).completeProgress()               // once for combining and once for loading
        verify(mockview, times(1)).showMessage("TEST_MULTI_COMBINED 2")
        verify(mockservice, never()).stopTracking(false)
    }

    @Test
    fun item_1_and_2_selected_and_combined_and_confirmed_throws() {
        // arrange
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_combine_track)
        `when`(mockTrackRepository.create(anyObject())).thenThrow(NullPointerException("ERROR"))

        // act
        reset(mockview)
        presenter.confirmedCombineSelectedTracks(COMBINED_NAME)

        // assert
        // UI updated
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).showMessage(R.string.track_combine_error)
    }

    @Test
    fun item_1_and_2_selected_and_deleted_and_confirmed_throws_and_logs() {
        // arrange
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_combine_track)
        `when`(mockTrackRepository.create(anyObject())).thenThrow(NullPointerException("ERROR"))

        // act
        reset(mockview)
        presenter.confirmedCombineSelectedTracks(COMBINED_NAME)

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect", true, exception is TrackCombineException)
        assertEquals(
            "inner exception type incorrect",
            true,
            exception.cause is NullPointerException
        )
    }
}