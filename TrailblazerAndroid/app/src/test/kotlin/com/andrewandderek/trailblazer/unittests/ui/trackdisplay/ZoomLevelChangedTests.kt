/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify

class ZoomLevelChangedTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
    }

    @Test
    fun zoom_level_is_saved_to_settings_when_changed() {
        // arrange

        // act
        presenter.mapZoomLevelChanged(12.34F)

        // assert
        verify(mockZoomSettings).zoomLevel = 12.34F
    }
}