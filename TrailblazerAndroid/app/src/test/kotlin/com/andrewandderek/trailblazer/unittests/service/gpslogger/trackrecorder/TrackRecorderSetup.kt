/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.event.RxEventBus
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.MockitoHelper.eq
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.SystemTimeHelper
import com.andrewandderek.trailblazer.helpers.UnitsHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.metadata.track.ISingleTrackMetadataGenerator
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.TrackRecorder
import com.andrewandderek.trailblazer.settings.IRecordingStateProvider
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.utility.ISystemTime
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.mockito.Mock
import org.mockito.Mockito.anyFloat
import org.mockito.Mockito.`when`
import java.util.Calendar

open class TrackRecorderSetup : MockingAnnotationSetup() {
    protected lateinit var trackRecorder: TrackRecorder

    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var mockSystemTime: ISystemTime
    private lateinit var distanceFormatter: IDistanceFormatter
    private lateinit var paceFormatter: IPaceFormatter

    @Mock protected lateinit var mockService: IGpsLoggerService
    @Mock protected lateinit var mockTrackRepository: ITrackRepository
    @Mock protected lateinit var mockPositionRepository: IPositionRepository
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockStatisticsProvider: ITrackStatisticsProvider
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockRecordingState: IRecordingStateProvider
    @Mock protected lateinit var mockMetadataGenerator: ISingleTrackMetadataGenerator

    protected lateinit var track: Track

    protected lateinit var eventBus: IEventBus
    protected lateinit var mockScheduler: ISchedulerProvider
    protected var statusChangedEventSubscriber: IEventBusSubscription? = null
    protected var publishedStatusChangedEvent: RecordingStatusChangedEvent? = null
    protected var metadataChangedEventSubscriber: IEventBusSubscription? = null
    protected var publishedMetadataChangedEvent: TrackMetadataUpdatedEvent? = null

    protected fun setupTrackRecorder(scheduler: TestScheduler? = null) {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockScheduler = if (scheduler == null) RxHelper.setupSchedulerProvider() else RxHelper.setupTestSchedulerProvider(scheduler)
        mockSystemTime = SystemTimeHelper.setupMockSystemTime()
        distanceFormatter = UnitsHelper.setupDistanceFormatter(PreferredUnits.METRIC.toString())
        paceFormatter = UnitsHelper.setupPaceFormatter(PreferredUnits.METRIC.toString())

        eventBus = RxEventBus(
            mockLoggerFactory,
            mockCrashReporter,
            mockScheduler
        )

        trackRecorder = TrackRecorder(
                mockLoggerFactory,
                mockTrackRepository,
                mockPositionRepository,
                mockSystemTime,
                mockResourceProvider,
                mockStatisticsProvider,
                distanceFormatter,
                paceFormatter,
                mockCrashReporter,
                mockRecordingState,
                eventBus,
                mockScheduler,
                mockMetadataGenerator,
            )

        `when`(mockResourceProvider.getString(R.string.service_notification_content_fmt)).thenReturn("TEST_NOTIFICATION_CONTENT %1\$s, %2\$s, %3\$s")
        `when`(mockResourceProvider.getString(R.string.service_notification_title_fmt)).thenReturn("TEST_NOTIFICATION_TITLE %s")
        `when`(mockResourceProvider.getString(R.string.service_notification_title_paused)).thenReturn("TEST_NOTIFICATION_TITLE_PAUSED")
        `when`(mockResourceProvider.getString(R.string.service_notification_accessibility_fmt)).thenReturn("TEST_NOTIFICATION_ACCESSIBILITY %s")
    }

    protected fun getStatistics(distance: Double, duration: Long) :TrackStatistics {
        var statistics = TrackStatistics()
        statistics.distanceInMetres = distance
        statistics.recordedTimeInMilliseconds = duration
        return statistics
    }

    protected fun getTestNow() : Calendar {
        var now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now.set(Calendar.MILLISECOND, 0)
        return now
    }

    protected fun getTestNow(offsetSeconds: Int) : Calendar {
        var now = getTestNow()
        now.add(Calendar.SECOND,offsetSeconds)
        return now
    }

    protected fun createTrackInRepository() {
        `when`(mockTrackRepository.create(anyObject())).thenAnswer {
            invocation ->
            val args = invocation.arguments
            var track = args[0] as Track
            track.id = 123              // pretend its been stored in the DB
            track.started = (args[0] as Track).started
            return@thenAnswer track
        }
    }

    protected fun setupGetWithPositionsAndAccuracy(id: Long, track: Track) {
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(eq(id), anyFloat(), anyObject())).thenReturn(track)
    }

    protected fun setupEventSubscriber() {
        // we dont mock the event bus because it will run quite happily in the unit tests so lets test as much as we can
        publishedStatusChangedEvent = null   // reset the event we captured
        statusChangedEventSubscriber = eventBus.subscribe(RecordingStatusChangedEvent::class.java)
        { event ->
            publishedStatusChangedEvent = event
        }

        publishedMetadataChangedEvent = null
        metadataChangedEventSubscriber = eventBus.subscribe(TrackMetadataUpdatedEvent::class.java)
        { event ->
            publishedMetadataChangedEvent = event
        }
    }
}