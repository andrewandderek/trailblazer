/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.statistics.TrackTimeCalculator
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class TrackTimeCalculatorTests {
    private lateinit var calculator: TrackTimeCalculator
    private lateinit var baseTime: Calendar

    @Before
    fun setupTest() {
        calculator = TrackTimeCalculator()
        baseTime = Calendar.getInstance()
    }

    private fun createPosition(latitude: Double, longitude: Double, timeOffsetSeconds: Int): Position {
        return createPositionWithSegment(latitude, longitude, timeOffsetSeconds, 0)
    }

    private fun createPositionWithSegment(latitude: Double, longitude: Double, timeOffsetSeconds: Int, segment: Long): Position {
        val positionTimeInMillis = baseTime.timeInMillis + (timeOffsetSeconds * 1000)
        val position = Position(latitude, longitude, 3.4, positionTimeInMillis)
        position.segment = segment

        return position
    }

    private fun createTrack(): Track {
        val track = Track()
        track.started = baseTime
        track.addNewPosition(createPosition(1.2, 1.3,10))
        track.addNewPosition(createPosition(1.2, 1.3, 15))
        track.addNewPosition(createPosition(3.4, 1.3, 25))
        track.addNewPosition(createPosition(3.4, 4.5, 40))
        return track
    }

    private fun createTrackWithPauses(): Track {
        val track = Track()
        track.started = baseTime
        track.addNewPosition(createPositionWithSegment(1.2, 1.3,10, 0))
        track.addNewPosition(createPositionWithSegment(1.2, 1.3, 15, 0))
        track.addNewPosition(createPositionWithSegment(3.4, 1.3, 25, 0))
        track.addNewPosition(createPositionWithSegment(3.6, 4.5, 40, 1))
        track.addNewPosition(createPositionWithSegment(5.6, 4.5, 45, 1))
        // No movement on this last segment
        track.addNewPosition(createPositionWithSegment(7.8, 5.4, 140, 2))
        track.addNewPosition(createPositionWithSegment(7.8, 5.4, 145, 2))
        return track
    }

    private fun addTrackPositionsToCalculator(track: Track, statistics: TrackStatistics) {
        for (i in 0..(track.currentPositionsSnapshot.size - 1)) {
            calculator.addPosition(track.started, track.currentPositionsSnapshot, i, statistics)
        }
    }

    @Test
    fun addPosition_forFirstPosition_shouldSetElapsedTimeToDifferenceInMillisecondsBetweenFirstPositionAndStartTime() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)

        // assert
        Assert.assertEquals(10 * 1000, statistics.elapsedTimeInMilliseconds)
    }

    @Test
    fun addPosition_forMultiplePositions_shouldSetElapsedTimeToDifferenceInMillisecondsBetweenLastPositionAndStartTime() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        addTrackPositionsToCalculator(track, statistics)

        // assert
        Assert.assertEquals(40 * 1000, statistics.elapsedTimeInMilliseconds)
    }

    @Test
    fun addPosition_trackWithoutPauses_shouldSetRecordedTimeToDifferenceInMillisecondsBetweenLastPositionAndStartTime() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        addTrackPositionsToCalculator(track, statistics)

        // assert
        Assert.assertEquals(40 * 1000, statistics.recordedTimeInMilliseconds)
    }

    @Test
    fun addPosition_trackWithPauses_shouldSetRecordedTimeToSumOfElapsedSegmentTimes() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrackWithPauses()

        // act
        addTrackPositionsToCalculator(track, statistics)

        // assert
        Assert.assertEquals((25 + 5 + 5) * 1000, statistics.recordedTimeInMilliseconds)
    }

    @Test
    fun addPosition_forFirstPosition_shouldSetMovingTimeToDifferenceInMillisecondsBetweenFirstPositionAndStartTime() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)

        // assert
        Assert.assertEquals(10 * 1000, statistics.movingTimeInMilliseconds)
    }

    @Test
    fun addPosition_whenPositionIsSameLatLongAsPrevious_shouldNotChangeMovingTime() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 1, statistics)

        // assert
        Assert.assertEquals(10 * 1000, statistics.movingTimeInMilliseconds)
    }

    @Test
    fun addPosition_forMultiplePositions_shouldSetMovingTimeToSumOfDifferencesWhenPositionChanged() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        addTrackPositionsToCalculator(track, statistics)

        // assert
        Assert.assertEquals((10 + (25-15) + (40-25)) * 1000, statistics.movingTimeInMilliseconds)
    }

    @Test
    fun addPosition_trackWithPauses_shouldSetMovingTimeCorrectly() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrackWithPauses()

        // act
        addTrackPositionsToCalculator(track, statistics)

        // assert
        Assert.assertEquals((10 + (25-15) + (45-40)) * 1000, statistics.movingTimeInMilliseconds)
    }
}
