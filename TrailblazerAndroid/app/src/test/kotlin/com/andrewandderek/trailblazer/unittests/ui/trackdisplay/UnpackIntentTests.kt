/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import android.content.Intent
import android.net.Uri
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.trackdisplay.TrackDisplayPresenter
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Test
import org.mockito.Mockito

class UnpackIntentTests : TrackDisplayPresenterSetup() {

    private lateinit var intent: Intent
    private lateinit var intentUri: Uri

    private fun setupIntentWithUri(uriString: String, promptForConfirmation: Boolean = true) {
        intentUri = Mockito.mock(Uri::class.java)
        Mockito.`when`(intent.data).thenReturn(intentUri)
        Mockito.`when`(intentUri.toString()).thenReturn(uriString)
        Mockito.`when`(intent.getBooleanExtra(TrackDisplayPresenter.PARAM_PROMPT_FOR_IMPORT_CONFIRMATION, true)).thenReturn(promptForConfirmation)
    }

    private fun setupIntentWithTrackId(trackId: Long) {
        Mockito.`when`(intent.getLongExtra(TrackDisplayPresenter.PARAM_TRACK_ID, -1)).thenReturn(trackId)
    }

    private fun setup(scheduler: TestScheduler? = null) {
        intent = Mockito.mock(Intent::class.java)

        setupPresenter(scheduler)
        setupNowDateTimes()
        setupPoints()

        Mockito.`when`(mockResourceProvider.getString(R.string.import_track_prompt_fmt)).thenReturn("TEST_IMPORT %s")

        presenter.bind(mockview, null)
        presenter.start()
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun unpack_intent_null() {
        // arrange
        setup()

        // act
        presenter.unpackIntent(intent)

        // assert
    }

    @Test
    fun unpack_intent_prompts_to_import_by_default() {
        // arrange
        setup()
        setupIntentWithUri("file://filename")

        // act
        presenter.unpackIntent(intent)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).promptToImport("TEST_IMPORT file://filename", "file://filename")
    }

    @Test
    fun unpack_intent_does_not_prompt_to_import_if_prompt_param_set_to_false() {
        // arrange
        setup()
        setupIntentWithUri("file://filename", false)

        // act
        presenter.unpackIntent(intent)

        // assert
        Mockito.verify(mockview, Mockito.never()).promptToImport("TEST_IMPORT file://filename", "file://filename")
    }

    @Test
    fun unpack_intent_imports_track_if_prompt_param_set_to_false() {
        // arrange
        setup()
        setupIntentWithUri("file://filename", false)

        // act
        presenter.unpackIntent(intent)

        // assert
        // We're not testing all the import here, just that the importTrack func is called:
        // import functionality is tested by ImportTrackTests
        Mockito.verify(mockAnalyticsEngine, Mockito.times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_ID, AnalyticsConstants.ACTION_IMPORT_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun unpack_intent_writes_analytics() {
        // arrange
        setup()
        setupIntentWithUri("file://filename")

        // act
        presenter.unpackIntent(intent)

        // assert
        Mockito.verify(mockAnalyticsEngine, Mockito.times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_SHARED_ID, AnalyticsConstants.ACTION_IMPORT_SHARED_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun unpack_intent_reentered() {
        // arrange

        // lets start an import - but it cannot complete as we killed the scheduler
        val scheduler = TestScheduler()
        setup(scheduler)
        scheduler.shutdown()
        presenter.importTrack("file://filename1")

        setupIntentWithUri("file://filename2")

        // act
        presenter.unpackIntent(intent)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).showMessage(R.string.track_being_imported)
    }

    @Test
    fun unpack_intent_with_track_id_displays_track() {
        // arrange
        setup()
        setupIntentWithTrackId(123)

        // act
        presenter.unpackIntent(intent)

        // assert
        Mockito.verify(mockTrackRepository, Mockito.times(1)).getWithPositionsAndAccuracy(123, Position.ACCURACY_THRESHOLD, mockPositionRepository)
    }
}