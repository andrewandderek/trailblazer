/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.settings.RecordingState
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.reset
import org.mockito.Mockito.verify

class ResumeTests : TrackRecorderSetup() {
    @Before
    fun before_each_test() {
        setupTrackRecorder()
        createTrackInRepository()
        setupEventSubscriber()
    }

    @Test
    fun resume_sets_correct_state() {
        // arrange
        trackRecorder.createNewTrack()
        reset(mockRecordingState)           // as the create will have altered the state

        // act
        trackRecorder.resume(false)

        // assert
        verify(mockRecordingState).recordingState = RecordingState(123L, 0L, false, true)
    }

    @Test
    fun resume_triggers_eventbus() {
        // arrange
        trackRecorder.createNewTrack()
        reset(mockRecordingState)           // as the create will have altered the state

        // act
        trackRecorder.resume(false)

        // assert
        assertEquals(123L, publishedStatusChangedEvent?.trackId)
        assertEquals(TrackRecordingControls.ACTION_RESUME, publishedStatusChangedEvent?.statusChangedTo)
        assertEquals(false, publishedStatusChangedEvent?.fromNotification)
    }
}

