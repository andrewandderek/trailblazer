/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import java.util.Calendar

open class TrackSetup {
    protected lateinit var yesterday: Calendar

    protected lateinit var now: Calendar
    protected lateinit var now2: Calendar
    protected lateinit var now3: Calendar

    protected lateinit var position1: Position
    protected lateinit var position2: Position
    protected lateinit var position3: Position

    protected fun setupNowDateTimes() {
        now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now2 = Calendar.getInstance()
        now2.time = now.time
        now2.add(Calendar.SECOND, 10)
        now3 = Calendar.getInstance()
        now3.time = now2.time
        now3.add(Calendar.SECOND, 15)

        yesterday = Calendar.getInstance()
        yesterday.set(2017, 11, 29, 15, 37, 22)
    }

    protected fun setupTrackWithTwoPoints(withAltitude: Boolean, withTimestamps: Boolean): Track {
        val track = Track()
        track.id = 1    // we need to pretend its been saved to the DB
        track.started = now
        track.name = "Track With 2 points"
        track.notes = "NOTES_TRACK"
        position1 = Position(
            1.0,
            2.0,
            if (withAltitude) 3.0 else null,
            if (withTimestamps) now else null,
            100F,
            "POSITION_NOTES_1"
        )
        position2 = Position(
            11.0,
            12.0,
            if (withAltitude) 13.0 else null,
            if (withTimestamps) now2 else null,
            100F,
            "POSITION_NOTES_2"
        )
        track.addNewPosition(position1)
        track.addNewPosition(position2)

        return track
    }

        protected fun setupMultiDaySegmentedTrack(withAltitude: Boolean): Track {
            val track = setupSegmentedTrack(withAltitude, true)
            track.started = yesterday
            return track
        }

        protected fun setupSegmentedTrack(withAltitude: Boolean, withTimestamps: Boolean): Track {
            val track = Track()
            track.id = 1    // we need to pretend its been saved to the DB
            track.started = now
            track.name = "Track With 2 segments"
            track.notes = "NOTES_SEG_TRACK"
            position1 = Position(
                1.0,
                2.0,
                if (withAltitude) 3.0 else null,
                if (withTimestamps) now else null,
                100F,
                "POSITION_NOTES_1"
            )
            position2 = Position(
                11.0,
                12.0,
                if (withAltitude) 13.0 else null,
                if (withTimestamps) now2 else null,
                100F,
                "POSITION_NOTES_2"
            )
            track.addNewPosition(position1)
            track.addNewPosition(position2)
            position3 = Position(
                21.0,
                22.0,
                if (withAltitude) 23.0 else null,
                if (withTimestamps) now3 else null,
                100F,
                "POSITION_NOTES_3",
                2                   // note that this will leave segment 1 empty
            )
            track.addNewPosition(position3)

            return track
        }

    protected fun setupEmptyTrack(): Track {
        val track = Track()
        track.id = 1    // we need to pretend its been saved to the DB
        track.name = "Empty Track"
        track.notes = "NOTES_TRACK"
        return track
    }
}