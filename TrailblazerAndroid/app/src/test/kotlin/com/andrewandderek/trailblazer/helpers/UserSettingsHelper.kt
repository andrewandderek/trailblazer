/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.settings.UserSettings
import com.andrewandderek.trailblazer.units.*
import com.andrewandderek.trailblazer.utility.IPreferencesProvider
import com.andrewandderek.trailblazer.utility.IResourceProvider
import org.mockito.Mockito
import org.mockito.Mockito.`when`

object UserSettingsHelper {
    /**
     * setup a mock UserSettings
     */
    fun setupMockMetricUserSettings(): IUserSettings {
        var mockUserSettings = Mockito.mock(IUserSettings::class.java)
        `when`(mockUserSettings.distanceUnit).thenReturn(DistanceUnit.Kilometres)
        `when`(mockUserSettings.distanceUnitLabel).thenReturn("MDU")
        `when`(mockUserSettings.altitudeUnit).thenReturn(AltitudeUnit.Metres)
        `when`(mockUserSettings.altitudeUnitLabel).thenReturn("MAU")
        `when`(mockUserSettings.paceUnit).thenReturn(PaceUnit.MinutesPerKilometre)
        `when`(mockUserSettings.paceUnitLabel).thenReturn("MPU")
        `when`(mockUserSettings.speedUnit).thenReturn(SpeedUnit.KilometresPerHour)
        `when`(mockUserSettings.speedUnitLabel).thenReturn("MSU")
        return mockUserSettings
    }

    /**
     * setup a real UserSettings - backed by mock resources and preferences
     */
    fun setupUserSettings(units: String,
                          trackId: Long = -1,
                          trackSegment: Long = -1,
                          paused: Boolean = false,
                          recording: Boolean = false,
                          ): IUserSettings {
        var mockResources: IResourceProvider = Mockito.mock(IResourceProvider::class.java)
        var mockPreferences: IPreferencesProvider = Mockito.mock(IPreferencesProvider::class.java)
        var mockLoggerFactory: ILoggerFactory = LoggingHelper.setupMockLoggingFactory()
        var userSettings: IUserSettings = UserSettings(mockLoggerFactory, mockResources, mockPreferences)

        `when`(mockResources.getString(R.string.settings_preferred_units_key)).thenReturn("SETTINGS_KEY")
        `when`(mockPreferences.getPreferenceString("SETTINGS_KEY", PreferredUnits.METRIC.toString())).thenReturn(units)

        `when`(mockResources.getString(R.string.setting_recording_state_track_id_key)).thenReturn("TRACKID_KEY")
        `when`(mockResources.getString(R.string.setting_recording_state_current_segment_key)).thenReturn("TRACKSEG_KEY")
        `when`(mockResources.getString(R.string.setting_recording_state_is_paused_key)).thenReturn("PAUSED_KEY")
        `when`(mockResources.getString(R.string.setting_recording_state_is_recording_key)).thenReturn("RECORDING_KEY")
        `when`(mockPreferences.getPreferenceLong("TRACKID_KEY", -1)).thenReturn(trackId)
        `when`(mockPreferences.getPreferenceLong("TRACKSEG_KEY", -1)).thenReturn(trackSegment)
        `when`(mockPreferences.getPreferenceBoolean("PAUSED_KEY", false)).thenReturn(paused)
        `when`(mockPreferences.getPreferenceBoolean("RECORDING_KEY", false)).thenReturn(recording)

        `when`(mockResources.getString(R.string.speed_label_imperial)).thenReturn("SPEED_LABEL_IMPERIAL")
        `when`(mockResources.getString(R.string.speed_label_metric)).thenReturn("SPEED_LABEL_METRIC")
        `when`(mockResources.getString(R.string.pace_label_imperial)).thenReturn("PACE_LABEL_IMPERIAL")
        `when`(mockResources.getString(R.string.pace_label_metric)).thenReturn("PACE_LABEL_METRIC")
        `when`(mockResources.getString(R.string.distance_label_imperial)).thenReturn("DISTANCE_LABEL_IMPERIAL")
        `when`(mockResources.getString(R.string.distance_label_metric)).thenReturn("DISTANCE_LABEL_METRIC")
        `when`(mockResources.getString(R.string.altitude_label_imperial)).thenReturn("ALTITUDE_LABEL_IMPERIAL")
        `when`(mockResources.getString(R.string.altitude_label_metric)).thenReturn("ALTITUDE_LABEL_METRIC")

        return userSettings
    }

}


