/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.settings.opensourcelicense

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class BindTests : OpenSourceLicensePresenterSetup() {
    @Before
    fun before_each_test() {
        setupPresenter()
    }

    @Test
    fun presenter_binds_to_view() {
        // arrange

        // act
        presenter.bind(mockview)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).addTextBlock("title", "line2\n")
        Mockito.verify(mockview, Mockito.times(1)).scrollToTopOfText()
    }

}