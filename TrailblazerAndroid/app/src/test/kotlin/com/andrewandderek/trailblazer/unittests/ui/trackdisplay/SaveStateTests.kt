/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import android.os.Bundle
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.never

class SaveStateTests : TrackDisplayPresenterSetup() {
    @Before
    fun before_each_test() {
        setupPresenter()
    }

    @Test
    fun saveState_does_nothing_with_null_bundle() {
        // arrange
        setupDisplayTrack()
        presenter.bind(mockview, null)
        presenter.start()
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        presenter.saveState(null)

        // assert
        // we are asserting it does not crash
    }

    @Test
    fun saveState_does_nothing_when_nothing_is_displayed() {
        // arrange
        setupDisplayTrack()
        presenter.bind(mockview, null)
        presenter.start()
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        var bundle = Mockito.mock(Bundle::class.java)
        presenter.saveState(bundle)

        // assert
        Mockito.verify(bundle, never()).putLong(anyString(), eq(DISPLAY_TRACK_ID))
    }

    @Test
    fun saveState_saves_the_display_track() {
        // arrange
        setupDisplayTrack()
        presenter.bind(mockview, null)
        presenter.start()
        presenter.connectService(mockbinder)        // attach the subscribers
        presenter.displayTrack(DISPLAY_TRACK_ID)

        // act
        var bundle = Mockito.mock(Bundle::class.java)
        presenter.saveState(bundle)

        // assert
        Mockito.verify(bundle, times(1)).putLong(anyString(), eq(DISPLAY_TRACK_ID))
    }
}