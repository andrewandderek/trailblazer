/*
 *  Copyright 2017-2022 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

class ActionAvailableTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
        presenter.start()
        `when`(mockPermissionChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun action_available_settings_always_available() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_settings)

        // assert
        assertEquals(true, result)
    }

    @Test
    fun action_available_import_track_always_available() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_import_track)

        // assert
        assertEquals(true, result)
    }

    @Test
    fun action_available_search_always_available() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_search)

        // assert
        assertEquals(true, result)
    }

    @Test
    fun action_available_track_control_available_when_search_mode_never_entered() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        assertEquals(true, result)
    }

    @Test
    fun action_available_track_control_not_available_when_in_search_mode() {
        // arrange
        presenter.searchActionStart()

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        assertEquals(false, result)
    }

    @Test
    fun action_available_track_control_available_again_after_leaving_search_mode() {
        // arrange
        presenter.searchActionStart()
        presenter.searchActionEnd()

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        assertEquals(true, result)
    }
}