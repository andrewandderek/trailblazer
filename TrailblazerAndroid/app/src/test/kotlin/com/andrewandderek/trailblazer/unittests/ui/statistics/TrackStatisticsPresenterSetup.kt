/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.statistics

import android.content.Intent
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.UnitsHelper
import com.andrewandderek.trailblazer.helpers.UserSettingsHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.sharing.importexport.IBitmapExporter
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.ui.statistics.ITrackStatisticsView
import com.andrewandderek.trailblazer.ui.statistics.TrackStatisticsPresenter
import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import org.mockito.Mock
import org.mockito.Mockito.`when`
import java.util.Calendar

open class TrackStatisticsPresenterSetup : MockingAnnotationSetup() {
    protected val TRACK_ID = 789L
    protected val TRACK_NAME = "Track name"
    protected val TRACK_NOTES = "Track notes"
    protected val TRACK_LOAD_ERROR = "Error loading track"
    private val SHARE_FILENAME = "SHARE FILENAME"

    protected lateinit var track: Track
    protected lateinit var yesterday: Calendar
    protected lateinit var now: Calendar
    protected lateinit var now2: Calendar

    protected lateinit var presenter: TrackStatisticsPresenter
    private lateinit var mockLoggerFactory: ILoggerFactory
    private lateinit var mockSchedulerProvider: ISchedulerProvider
    private lateinit var userSettings: IUserSettings
    private lateinit var distanceFormatter: IDistanceFormatter
    private lateinit var speedFormatter: ISpeedFormatter
    private lateinit var paceFormatter: IPaceFormatter
    private lateinit var altitudeFormatter: IAltitudeFormatter

    @Mock protected lateinit var mockView: ITrackStatisticsView
    @Mock protected lateinit var mockTrackRepository: ITrackRepository
    @Mock protected lateinit var mockPositionRepository: IPositionRepository
    @Mock protected lateinit var mockStatisticsProvider: ITrackStatisticsProvider
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockAnalyticsEngine: IAnalyticsEngine
    @Mock protected lateinit var mockSharer: IIntentSharer
    @Mock protected lateinit var mockExporter: IBitmapExporter
    @Mock protected lateinit var mockIntent: Intent

    protected fun setupTrack(withAltitude: Boolean, withTimestamps: Boolean) {
        track = Track()
        track.name = TRACK_NAME
        track.notes = TRACK_NOTES

        val position1 = Position(
            1.0,
            2.0,
            if (withAltitude) 3.0 else null,
            0
        )
        if (!withTimestamps) {
            position1.timeRecorded = null
        }

        track.addNewPosition(position1)

        `when`(mockTrackRepository.getWithPositionsAndAccuracy(TRACK_ID, Position.ACCURACY_THRESHOLD, mockPositionRepository))
                .thenReturn(track)
    }

    protected fun setupMultidaySegmentedTrack(withAltitude: Boolean) {
        now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now2 = Calendar.getInstance()
        now2.time = now.time
        now2.add(Calendar.SECOND, 10)
        yesterday = Calendar.getInstance()
        yesterday.set(2017, 11, 29, 15, 37, 22)

        track = Track()
        track.name = TRACK_NAME
        track.notes = TRACK_NOTES
        track.started = yesterday

        val position1 = Position(
            1.0,
            2.0,
            if (withAltitude) 3.0 else null,
            0
        )
        position1.timeRecorded = now
        track.addNewPosition(position1)

        val position2 = Position(
            11.0,
            12.0,
            if (withAltitude) 13.0 else null,
            0
        )
        position1.timeRecorded = now2
        position2.segment = 1
        track.addNewPosition(position2)

        `when`(mockTrackRepository.getWithPositionsAndAccuracy(TRACK_ID, Position.ACCURACY_THRESHOLD, mockPositionRepository))
            .thenReturn(track)
    }
    protected fun setupStatistics() {
        // 2:15:23
        val recordedTime = ((2 * 60 * 60) + (15 * 60) + 23) * 1000L
        val movingTime = recordedTime - 11000
        val statistics = TrackStatistics(
                1234.56,
                999,
                recordedTime,
                movingTime,
                1.0,
                2.0,
                2.0,
                333.45,
                331.45,
                777.56,
                888.99)
        `when`(mockStatisticsProvider.getStatistics(track)).thenReturn(statistics)
    }

    protected fun setupPresenter() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockSchedulerProvider = RxHelper.setupSchedulerProvider()
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())
        altitudeFormatter = UnitsHelper.setupAltitudeFormatter(PreferredUnits.METRIC.toString())
        speedFormatter = UnitsHelper.setupSpeedFormatter(PreferredUnits.METRIC.toString())
        paceFormatter = UnitsHelper.setupPaceFormatter(PreferredUnits.METRIC.toString())
        distanceFormatter = UnitsHelper.setupDistanceFormatter(PreferredUnits.METRIC.toString())

        `when`(mockResourceProvider.getString(R.string.error_loading_track)).thenReturn(TRACK_LOAD_ERROR)
        `when`(mockResourceProvider.getString(R.string.stats_segment_line_fmt)).thenReturn("%s, %s - %s, %s")

        presenter = TrackStatisticsPresenter(
                mockLoggerFactory,
                mockSchedulerProvider,
                mockTrackRepository,
                mockPositionRepository,
                mockStatisticsProvider,
                mockResourceProvider,
                mockCrashReporter,
                userSettings,
                altitudeFormatter,
                distanceFormatter,
                speedFormatter,
                paceFormatter,
                mockAnalyticsEngine,
                mockSharer,
                mockExporter
        )

        `when`(mockExporter.getShareFilename(MockitoHelper.anyObject())).thenReturn(SHARE_FILENAME)
        `when`(mockSharer.getShareIntentWithSingleFile(MockitoHelper.anyObject(), MockitoHelper.anyObject())).thenReturn(mockIntent)
    }
}