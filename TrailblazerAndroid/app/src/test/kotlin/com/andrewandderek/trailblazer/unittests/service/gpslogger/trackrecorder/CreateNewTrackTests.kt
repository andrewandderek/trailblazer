/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.settings.RecordingState
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class CreateNewTrackTests : TrackRecorderSetup() {

    @Before
    fun before_each_test() {
        setupTrackRecorder()
        createTrackInRepository()
    }

    @Test
    fun add_track_fires_subscriptions() {
        // arrange
        var observedTrack: Track? = null
        var numberOfTriggeredUpdates: Int = 0
        var observable = trackRecorder.currentTrack
        var trackSubscriber = observable
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(
                        { track ->
                            observedTrack = track
                            numberOfTriggeredUpdates += 1
                        }
                )

        // act
        trackRecorder.createNewTrack()
        trackSubscriber.dispose()

        // assert
        assertEquals("we should have observed one change", 1, numberOfTriggeredUpdates)
        assertEquals("we should have observed the currentRecordingTrack", 123, observedTrack!!.id)
    }

    @Test
    fun add_track_sets_correct_name() {
        // arrange
        `when`(mockSystemTime.getCurrentTime()).thenReturn(getTestNow())

        // act
        trackRecorder.createNewTrack()

        // assert
        assertEquals("30 Dec, 15:37", trackRecorder.currentTrackObject!!.name)
    }

    @Test
    fun add_track_sets_correct_start() {
        // arrange
        `when`(mockSystemTime.getCurrentTime()).thenReturn(getTestNow())

        // act
        trackRecorder.createNewTrack()

        // assert
        assertEquals("Sat 30, Dec, 2017 15:37:22", CalendarFormatter.convertCalendarToDateTimeString(trackRecorder.currentTrackObject!!.started))
    }

    @Test
    fun add_track_twice_sets_correct_name() {
        // arrange
        `when`(mockSystemTime.getCurrentTime())
                .thenReturn(getTestNow())
                .thenReturn(getTestNow(120))

        // act
        trackRecorder.createNewTrack()
        trackRecorder.stop(false)
        trackRecorder.createNewTrack()

        // assert
        assertEquals("30 Dec, 15:39", trackRecorder.currentTrackObject!!.name)
    }

    @Test
    fun add_track_twice_sets_correct_start() {
        // arrange
        `when`(mockSystemTime.getCurrentTime())
                .thenReturn(getTestNow())
                .thenReturn(getTestNow(120))

        // act
        trackRecorder.createNewTrack()
        trackRecorder.stop(false)
        trackRecorder.createNewTrack()

        // assert
        assertEquals("Sat 30, Dec, 2017 15:39:22", CalendarFormatter.convertCalendarToDateTimeString(trackRecorder.currentTrackObject!!.started))
    }


    @Test
    fun add_track_sets_correct_notification() {
        // arrange
        `when`(mockSystemTime.getCurrentTime()).thenReturn(getTestNow())
        `when`(mockStatisticsProvider.getStatistics(anyObject())).thenReturn(getStatistics(123.45, 65 * 1000))

        // act
        trackRecorder.createNewTrack()

        // assert
        assertEquals("TEST_NOTIFICATION_TITLE 15:37", trackRecorder.getNotificationTitle())
        assertEquals("TEST_NOTIFICATION_CONTENT 00:01:05, 0.12 DISTANCE_LABEL_METRIC, 0:00 PACE_LABEL_METRIC", trackRecorder.getNotificationText())
    }

    @Test
    fun add_track_sets_correct_state() {
        // arrange
        `when`(mockSystemTime.getCurrentTime()).thenReturn(getTestNow())

        // act
        trackRecorder.createNewTrack()

        // assert
        verify(mockRecordingState).recordingState = RecordingState(123L, 0L, false, false)
    }

}