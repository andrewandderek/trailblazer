/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import org.junit.Assert.assertEquals
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

object CalendarHelper {
    fun getTimeOffsetBy(referenceTime: Calendar, field: Int, amount: Int): Calendar {
        val result = Calendar.getInstance()
        result.time = referenceTime.time
        result.add(field, amount)
        return result
    }

    private fun calendarAsUtcString(date: Calendar?):String {
        if (date == null) {
            return "<NULL>"
        }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("UTC");
        return dateFormat.format(date.time)
    }

    fun assertDatesEqual(message: String, date1: Calendar?, date2: Calendar?) {
        val date1String = calendarAsUtcString(date1)
        val date2String = calendarAsUtcString(date2)
        assertEquals(message, date1String, date2String)
    }
}