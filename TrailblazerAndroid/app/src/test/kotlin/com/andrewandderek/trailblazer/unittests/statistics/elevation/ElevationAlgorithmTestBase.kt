/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics.elevation

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.statistics.elevation.IAltitudeProcessor
import com.andrewandderek.trailblazer.statistics.elevation.IElevationSettings
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import org.mockito.Mock
import org.mockito.Mockito.doAnswer
import org.mockito.invocation.InvocationOnMock

open class ElevationAlgorithmTestBase : MockingAnnotationSetup() {
    protected val TEST_ELEVATION_DELTA = 0.001

    @Mock protected lateinit var mockAltitudeProcessor: IAltitudeProcessor
    @Mock protected lateinit var mockElevationSettings: IElevationSettings

    protected fun createPosition(altitude: Double): Position {
        return Position(altitude, altitude, altitude, 0)
    }

    protected fun createPositions(altitudes: DoubleArray): List<Position> {

        return altitudes.map { altitude -> createPosition(altitude) }
    }

    protected fun createTrack(altitudes: DoubleArray): Track {
        val positions = createPositions(altitudes)

        val track = Track()
        positions.forEach { position -> track.addNewPosition(position) }

        return track
    }

    protected fun setupFakeProcessor() {
        doAnswer { invocation ->
            fakeAltitudeProcessor(invocation)
            null
        }
        .`when`(mockAltitudeProcessor).reset(MockitoHelper.anyObject())

        doAnswer { invocation ->
            fakeAltitudeProcessor(invocation)
            null
        }
        .`when`(mockAltitudeProcessor).process(MockitoHelper.anyObject())

    }

    private fun fakeAltitudeProcessor(invocation: InvocationOnMock) {
        val position = invocation.getArgument(0) as Position
        position.processedAltitude = (position.altitude ?: 0.0) / 2
    }
}