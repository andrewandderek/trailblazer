/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.model.Track
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class DisplayTrackTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()

        presenter.bind(mockview, null)
        presenter.start()
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun draw_no_track() {
        // arrange

        // act
        presenter.displayTrack(-1L)

        // assert
        verify(mockview, never()).centreMapAndZoom(anyObject(), anyFloat())
        verify(mockview, never()).drawAllPositions(anyObject())
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).setTitle("TEST_APP_NAME")
    }

    @Test
    fun draw_track_not_found() {
        // arrange

        // act
        presenter.displayTrack(111)

        // assert
        verify(mockview, never()).centreMapAndZoom(anyObject(), anyFloat())
        verify(mockview, never()).drawAllPositions(anyObject())
        verify(mockview, times(1)).showMessage(anyInt())
        verify(mockview, times(1)).setTitle("TEST_APP_NAME")
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
    }

    // this action happens when we dont select a track to display - we select back instead
    @Test
    fun refresh_title_no_track() {
        // arrange
        currentRecordingTrack = Track()
        currentRecordingTrack.name = "test empty track"

        // act
        presenter.refreshTitle()

        // assert
        verify(mockview, times(1)).setTitle("TEST_APP_NAME")

        verify(mockview, never()).centreMapAndZoom(anyObject(), anyFloat())
        verify(mockview, never()).drawAllPositions(anyObject())
        verify(mockview, never()).updateAvailableActions()
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, never()).startProgress(anyInt())
        verify(mockview, never()).completeProgress()
    }

    @Test
    fun draw_a_track() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()

        // act
        presenter.displayTrack(DISPLAY_TRACK_ID)

        // assert
        verify(mockview, times(1)).centreMapAndZoomToFit(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).updateAvailableActions()
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).setTitle("TEST TRACK")
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
    }

    @Test
    fun draw_paused_track() {
        // arrange
        startRecording()
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)
        pauseRecording()
        setupDisplayRecordingTrack()

        // act
        reset(mockview)                             // we only want to verify the "act" section
        presenter.displayTrack(RECORDING_TRACK_ID)

        // assert
        verify(mockview, times(1)).centreMapAndZoomToFit(currentRecordingTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(currentRecordingTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).updateAvailableActions()
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).setTitle("TEST_TITLE_PAUSE TEST TRACK")
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
    }

    @Test
    fun update_recording_track_while_displaying() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()
        startRecording()
        presenter.displayTrack(DISPLAY_TRACK_ID)

        // act
        currentRecordingTrack.addNewPosition(position3)
        currentRecordingTrack.addNewPosition(position4)

        // assert
        // we do not display the currentRecordingTrack
        verify(mockview, never()).centreMapAndZoom(MockitoHelper.eq(position3), anyFloat())
        verify(mockview, never()).drawAllPositions(currentRecordingTrack.currentPositionsSnapshot)
        verify(mockview, never()).addPosition(position3)
        verify(mockview, never()).addPosition(position4)
        // we do display the displayTrack
        verify(mockview, times(1)).drawAllPositions(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).setTitle("TEST TRACK")
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
    }

    @Test
    fun update_paused_track_while_displaying_paused_track() {
        // arrange
        startRecording()
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)
        pauseRecording()
        setupDisplayRecordingTrack()
        presenter.displayTrack(RECORDING_TRACK_ID)

        // act
        reset(mockview)                             // we only want to verify the "act" section
        unPauseRecording()
        currentRecordingTrack.addNewPosition(position3)

        // assert
        // we start displaying the current recording track
        verify(mockview, times(1)).centreMapAndZoom(position3, USER_ZOOM_LEVEL)
        verify(mockview, times(1)).drawAllPositions(currentRecordingTrack.currentPositionsSnapshot)
    }

    @Test
    fun draw_a_track_while_recording() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()
        startRecording()

        // act
        reset(mockview)                             // we only want to verify the "act" section
        currentRecordingTrack.addNewPosition(position3)
        presenter.displayTrack(DISPLAY_TRACK_ID)
        currentRecordingTrack.addNewPosition(position4)

        // assert
        // we display the recording up to position3, then we only show the display currentRecordingTrack
        verify(mockview, times(1)).centreMapAndZoom(position3, USER_ZOOM_LEVEL)
        verify(mockview, times(1)).drawAllPositions(listOf(position3))
        verify(mockview, never()).addPosition(position4)
        verify(mockview, times(1)).centreMapAndZoomToFit(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).setTitle("TEST_TITLE_REC TEST TRACK")
        verify(mockview, times(1)).setTitle("TEST TRACK")
    }

    @Test
    fun draw_the_currently_recording_track_while_recording() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()
        `when`(mockTrackRepository.getWithPositions(RECORDING_TRACK_ID,mockPositionRepository)).thenReturn(currentRecordingTrack)
        startRecording()

        // act
        reset(mockview)                             // we only want to verify the "act" section
        currentRecordingTrack.addNewPosition(position3)
        presenter.displayTrack(RECORDING_TRACK_ID)
        currentRecordingTrack.addNewPosition(position4)

        // assert
        // we should carry on displaying the currently recording currentRecordingTrack
        verify(mockview, times(1)).centreMapAndZoom(position3, USER_ZOOM_LEVEL)
        verify(mockview, times(1)).drawAllPositions(listOf(position3))
        verify(mockview, times(1)).addPosition(position4)
        verify(mockview, times(1)).setTitle("TEST_TITLE_REC TEST TRACK")
        verify(mockview, never()).centreMapAndZoom(MockitoHelper.eq(position1), anyFloat())
        verify(mockview, never()).drawAllPositions(displayTrack.currentPositionsSnapshot)
    }

    @Test
    fun draw_the_currently_recording_track_while_displaying_a_different_track() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()
        `when`(mockTrackRepository.getWithPositions(RECORDING_TRACK_ID,mockPositionRepository)).thenReturn(currentRecordingTrack)
        startRecording()

        // act
        reset(mockview)                             // we only want to verify the "act" section
        presenter.displayTrack(DISPLAY_TRACK_ID)
        currentRecordingTrack.addNewPosition(position3)
        presenter.displayTrack(RECORDING_TRACK_ID)
        currentRecordingTrack.addNewPosition(position4)

        // assert
        // we should carry on displaying the display track and then switch across to the currentRecordingTrack
        verify(mockview, times(1)).centreMapAndZoomToFit(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(displayTrack.currentPositionsSnapshot)
        verify(mockview, never()).centreMapAndZoom(MockitoHelper.eq(position3), anyFloat())
        verify(mockview, times(1)).centreMapAndZoomToFit(currentRecordingTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(currentRecordingTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).setTitle("TEST TRACK")

    }

    @Test
    fun draw_two_tracks_actually_draws_both_of_them() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()

        // act
        presenter.displayTrack(DISPLAY_TRACK_ID)
        presenter.displayTrack(DISPLAY_TRACK_ID)

        // assert
        verify(mockview, times(2)).centreMapAndZoomToFit(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(2)).drawAllPositions(displayTrack.currentPositionsSnapshot)
        verify(mockview, times(2)).updateAvailableActions()
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(2)).setTitle("TEST TRACK")
    }

    @Test
    fun draw_the_currently_recording_track_handles_render_exception() {
        // arrange
        startRecording()
        reset(mockview)                             // we only want to verify the "act" section
        `when`(mockview.drawAllPositions(anyObject())).thenThrow(NullPointerException("UNIT TESTS"))

        // act
        currentRecordingTrack.addNewPosition(position3)

        // assert
        // we should not crash - we should display an error message
        verify(mockview, times(1)).showMessage(R.string.error_updating_position)
    }

    @Test
    fun attach_the_subscribers_handles_exception() {
        // arrange
        startRecording()
        `when`(mockScheduler.ioThread()).thenThrow(NullPointerException("UNIT TESTS"))

        // act
        observableTrack.onNext(currentRecordingTrack)       // kick the subscribers

        // assert
        // we should not crash - we should display an error message
        verify(mockview, times(1)).showMessage(R.string.error_updating_track)
    }

    @Test
    fun connect_service_completes_after_drawing_a_track_should_not_move_map_to_current_position() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)

        // act
        reset(mockview)                             // we only want to verify the "act" section
        startRecording()
        observableCurrentPosition.onNext(currentPosition)   // trigger current position so we can test subscription

        // assert
        verify(mockview, never()).centreMapAndZoom(anyObject(), anyFloat())
    }
}