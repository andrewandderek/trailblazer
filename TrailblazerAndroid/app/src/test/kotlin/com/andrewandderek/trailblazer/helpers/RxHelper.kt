/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import org.mockito.Mockito
import org.mockito.Mockito.`when`

object RxHelper {
    /**
     * this gives you a provider that will execute any async code immediately
     * this will effectively turn async methods into sync methods for testing
     */
    fun setupSchedulerProvider() : ISchedulerProvider {
        return setupTestSchedulerProvider(Schedulers.trampoline())
    }

    /**
     * this will allow you to specify the scheduler for a test
     * this can be useful to check what happens during an async operation by blocking it
     */
    fun setupTestSchedulerProvider(scheduler: Scheduler) : ISchedulerProvider {
        var mockScheduler = Mockito.mock(ISchedulerProvider::class.java)
        `when`(mockScheduler.androidMainThread()).thenReturn(scheduler)
        `when`(mockScheduler.newThread()).thenReturn(scheduler)
        `when`(mockScheduler.ioThread()).thenReturn(scheduler)
        `when`(mockScheduler.computation()).thenReturn(scheduler)
        return mockScheduler
    }
}
