/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.statistics.TrackSpeedCalculator
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class TrackSpeedCalculatorTests {
    private val TEST_SPEED_DELTA = 0.001

    private lateinit var trackStartTime: Calendar
    private lateinit var positions: List<Position>
    private lateinit var calculator: TrackSpeedCalculator

    @Before
    fun setupTest() {
        // We don't care about the start time or positions, the calculator just uses pre-calculated distance and time
        trackStartTime = Calendar.getInstance()
        positions = ArrayList()

        calculator = TrackSpeedCalculator()
    }

    @Test
    fun addPosition_recordedTimeIsNonZero_setsAverageSpeedCorrectly() {
        // arrange
        val statistics = TrackStatistics()
        statistics.distanceInMetres = 30.0
        statistics.recordedTimeInMilliseconds = 60000

        // act
        calculator.addPosition(trackStartTime, positions, 0, statistics)

        // assert
        Assert.assertEquals(0.5, statistics.averageSpeedInMetresPerSecond, TEST_SPEED_DELTA)
    }

    @Test
    fun addPosition_recordedTimeIsZero_setsAverageSpeedToZero() {
        // arrange
        val statistics = TrackStatistics()
        statistics.distanceInMetres = 30.0
        statistics.recordedTimeInMilliseconds = 0

        // act
        calculator.addPosition(trackStartTime, positions, 0, statistics)

        // assert
        Assert.assertEquals(0.0, statistics.averageSpeedInMetresPerSecond, TEST_SPEED_DELTA)
    }

    @Test
    fun addPosition_movingTimeIsNonZero_setsAverageMovingSpeedCorrectly() {
        // arrange
        val statistics = TrackStatistics()
        statistics.distanceInMetres = 24.0
        statistics.movingTimeInMilliseconds = 120000

        // act
        calculator.addPosition(trackStartTime, positions, 0, statistics)

        // assert
        Assert.assertEquals(0.2, statistics.averageMovingSpeedInMetresPerSecond, TEST_SPEED_DELTA)
    }

    @Test
    fun addPosition_movingTimeIsZero_setsAverageMovingSpeedToZero() {
        // arrange
        val statistics = TrackStatistics()
        statistics.distanceInMetres = 24.0
        statistics.movingTimeInMilliseconds = 0

        // act
        calculator.addPosition(trackStartTime, positions, 0, statistics)

        // assert
        Assert.assertEquals(0.0, statistics.averageMovingSpeedInMetresPerSecond, TEST_SPEED_DELTA)
    }

}