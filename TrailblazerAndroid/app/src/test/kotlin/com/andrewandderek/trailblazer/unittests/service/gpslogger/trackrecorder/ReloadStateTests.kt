/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.exception.RecordingReloadedAfterForcedExitException
import com.andrewandderek.trailblazer.exception.RecordingReloadedAfterForcedExitFailedException
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.settings.RecordingState
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ReloadStateTests : TrackRecorderSetup() {
    @Captor protected lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    private val emptyState: RecordingState = RecordingState(-1, -1, false, false)
    private val stoppedState: RecordingState = RecordingState(1, 1, false, false)
    private val recordingState: RecordingState = RecordingState(123, 5, false, true)
    private val pausedState: RecordingState = RecordingState(124, 5, true, false)
    private val missingState: RecordingState = RecordingState(666, 1, false, true)

    @Before
    fun before_each_test() {
        setupTrackRecorder()
    }

    @Test
    fun reload_ignored_if_state_empty() {
        // arrange
        `when`(mockRecordingState.recordingState).thenReturn(emptyState)

        // act
        trackRecorder.reloadState()

        // assert
        assertEquals(-1, trackRecorder.currentTrackObject!!.id)
        assertEquals("track recording",false, trackRecorder.isTrackRecording)
        assertEquals("track paused", false, trackRecorder.isTrackRecordingPaused)
        // we only log when we do something
        verify(mockCrashReporter, never()).logNonFatalException(anyObject())
    }

    @Test
    fun reload_ignored_if_state_track_stopped() {
        // arrange
        `when`(mockRecordingState.recordingState).thenReturn(stoppedState)

        // act
        trackRecorder.reloadState()

        // assert
        assertEquals(-1, trackRecorder.currentTrackObject!!.id)
        assertEquals("track recording",false, trackRecorder.isTrackRecording)
        assertEquals("track paused", false, trackRecorder.isTrackRecordingPaused)
        // we only log when we do something
        verify(mockCrashReporter, never()).logNonFatalException(anyObject())
    }

    @Test
    fun reload_recording_track() {
        // arrange
        var track = Track()
        track.id = recordingState.trackId
        `when`(mockRecordingState.recordingState).thenReturn(recordingState)
        setupGetWithPositionsAndAccuracy(recordingState.trackId, track)

        // act
        trackRecorder.reloadState()

        // assert
        assertEquals(recordingState.trackId, trackRecorder.currentTrackObject!!.id)
        assertEquals("track recording",true, trackRecorder.isTrackRecording)
        assertEquals("track paused", false, trackRecorder.isTrackRecordingPaused)

        verify(mockCrashReporter, times(1)).logNonFatalException(MockitoHelper.capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect",true, exception is RecordingReloadedAfterForcedExitException)
    }

    @Test
    fun reload_paused_track() {
        // arrange
        var track = Track()
        track.id = pausedState.trackId
        `when`(mockRecordingState.recordingState).thenReturn(pausedState)
        setupGetWithPositionsAndAccuracy(pausedState.trackId, track)

        // act
        trackRecorder.reloadState()

        // assert
        assertEquals(pausedState.trackId, trackRecorder.currentTrackObject!!.id)
        assertEquals("track recording",false, trackRecorder.isTrackRecording)
        assertEquals("track paused", true, trackRecorder.isTrackRecordingPaused)

        verify(mockCrashReporter, times(1)).logNonFatalException(MockitoHelper.capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect",true, exception is RecordingReloadedAfterForcedExitException)
    }

    @Test
    fun reload_missing_track() {
        // arrange
        `when`(mockRecordingState.recordingState).thenReturn(missingState)

        // act
        trackRecorder.reloadState()

        // assert
        assertEquals(-1, trackRecorder.currentTrackObject!!.id)
        assertEquals("track recording",false, trackRecorder.isTrackRecording)
        assertEquals("track paused", false, trackRecorder.isTrackRecordingPaused)

        verify(mockCrashReporter, times(1)).logNonFatalException(MockitoHelper.capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect",true, exception is RecordingReloadedAfterForcedExitFailedException)
    }
}