/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.settings.settings

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class ConnectServiceTests : SettingsFragmentViewModelSetup() {

    @Before
    fun before_each_test() {
        setupViewModel()
        setupEventSubscriber()
    }

    @Test
    fun initial_state_with_no_recording_track() {
        // arrange
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(false)

        // act
        viewModel.connectService(mockbinder)

        // assert
        verify(mockTrackRecordingObserver, times(1)).onChanged(false)
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockTrackRecordingObserver);
    }

    @Test
    fun initial_state_with_track_recording() {
        // arrange
        `when`(mockservice.isTracking).thenReturn(true)
        `when`(mockservice.isPaused).thenReturn(false)

        // act
        viewModel.connectService(mockbinder)

        // assert
        verify(mockTrackRecordingObserver, times(1)).onChanged(true)
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockTrackRecordingObserver);
    }

    @Test
    fun initial_state_with_track_paused() {
        // arrange
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(true)

        // act
        viewModel.connectService(mockbinder)

        // assert
        verify(mockTrackRecordingObserver, times(1)).onChanged(true)
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockTrackRecordingObserver);
    }

}

