/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class TrackItemEditTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        `when`(mockResourceProvider.getString(R.string.track_list_delete_prompt_fmt)).thenReturn("TEST_DELETE %s %s")
        `when`(mockResourceProvider.getString(R.string.track_list_delete_recording_prompt_fmt)).thenReturn("TEST_DELETE_RECORDING %s %s")
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun trackList_track_out_of_range_edit() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.editTrackName(-1)

        // assert
        verify(mockview, never()).promptForTrackName(anyString(), anyString())
    }

    @Test
    fun trackList_track_1_edit() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.editTrackName(1)

        // assert
        verify(mockview, times(1)).promptForTrackName("1", "Track With points")
    }

    @Test
    fun trackList_track_1_recording_edit() {
        // arrange
        trackIsBeingRecorded(trackWithPositions)
        presenter.loadTracks()

        // act
        presenter.editTrackName(1)

        // assert
        verify(mockview, times(1)).promptForTrackName("1", "Track With points")
    }


    @Test
    fun trackList_track_1_edit_confirmed() {
        // arrange
        presenter.loadTracks()
        presenter.editTrackName(1)

        // act
        reset(mockview)     // we only want to verify the rename action
        presenter.confirmedEditName("1", "NAME EDITED")

        // assert
        verify(mockTrackRepository, times(1)).update(trackWithPositions)
        assertEquals("NAME EDITED", trackWithPositions.name)
        verify(mockview, times(1)).tracksLoaded()
    }
}