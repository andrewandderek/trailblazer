/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.settings.RecordingState
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class StopTests : TrackRecorderSetup() {

    @Before
    fun before_each_test() {
        setupTrackRecorder()
        createTrackInRepository()
        setupEventSubscriber()
    }

    @Test
    fun stop_does_not_kill_last_recording() {
        // arrange
        trackRecorder.createNewTrack()

        // act
        trackRecorder.stop(false)

        // assert
        assertNotNull(trackRecorder.currentTrackObject)
        assertEquals(123, trackRecorder.currentTrackObject!!.id)
    }

    @Test
    fun stop_sets_correct_state() {
        // arrange
        trackRecorder.createNewTrack()
        reset(mockRecordingState)           // as the create will have altered the state

        // act
        trackRecorder.stop(false)

        // assert
        verify(mockRecordingState).recordingState = RecordingState(123L, 0L, false, false)
    }

    @Test
    fun stop_generates_the_metadata() {
        // arrange
        trackRecorder.createNewTrack()

        // act
        trackRecorder.stop(false)

        // assert
        verify(mockMetadataGenerator, times(1)).generateMetadataForTrack(trackRecorder.currentTrackObject!!)
        assertEquals(listOf(123L), publishedMetadataChangedEvent?.trackIds)
    }

    @Test
    fun stop_triggers_eventbus() {
        // arrange
        trackRecorder.createNewTrack()
        reset(mockRecordingState)           // as the create will have altered the state

        // act
        trackRecorder.stop(false)

        // assert
        assertEquals(123L, publishedStatusChangedEvent?.trackId)
        assertEquals(TrackRecordingControls.ACTION_STOP, publishedStatusChangedEvent?.statusChangedTo)
        assertEquals(false, publishedStatusChangedEvent?.fromNotification)
    }

    @Test
    fun stop_from_notification_triggers_eventbus() {
        // arrange
        trackRecorder.createNewTrack()
        reset(mockRecordingState)           // as the create will have altered the state

        // act
        trackRecorder.stop(true)

        // assert
        assertEquals(123L, publishedStatusChangedEvent?.trackId)
        assertEquals(TrackRecordingControls.ACTION_STOP, publishedStatusChangedEvent?.statusChangedTo)
        assertEquals(true, publishedStatusChangedEvent?.fromNotification)
    }
}

