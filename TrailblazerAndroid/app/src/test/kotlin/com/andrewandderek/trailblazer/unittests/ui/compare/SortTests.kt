/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.compare

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.ui.compare.SortedTrackList
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class SortTests : CompareViewModelSetup() {

    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun sort_moving_time_asc() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_MOVING_TIME",
                "TEST_ORDER_ASC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))

        // act
        viewModel.setSortBy(0)
        reset(mockSortedList)       // we are only interested in the final state
        viewModel.setSortDirection(0)

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun sort_moving_time_desc() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_MOVING_TIME",
                "TEST_ORDER_DESC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))

        // act
        viewModel.setSortBy(0)
        reset(mockSortedList)       // we are only interested in the final state
        viewModel.setSortDirection(1)

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun sort_distance_asc() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_DISTANCE",
                "TEST_ORDER_ASC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))

        // act
        viewModel.setSortBy(1)
        reset(mockSortedList)       // we are only interested in the final state
        viewModel.setSortDirection(0)

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun sort_distance_desc() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_DISTANCE",
                "TEST_ORDER_DESC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))

        // act
        viewModel.setSortBy(1)
        reset(mockSortedList)       // we are only interested in the final state
        viewModel.setSortDirection(1)

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun sort_speed_asc() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_SPEED",
                "TEST_ORDER_ASC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))

        // act
        viewModel.setSortBy(2)
        reset(mockSortedList)       // we are only interested in the final state
        viewModel.setSortDirection(0)

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun sort_speed_desc() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_SPEED",
                "TEST_ORDER_DESC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))

        // act
        viewModel.setSortBy(2)
        reset(mockSortedList)       // we are only interested in the final state
        viewModel.setSortDirection(1)

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }
}