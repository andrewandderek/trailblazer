/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.receiver.trackrecordingcontrols

import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class PerformActionTests : TrackRecordingControlsSetup() {

    @Before
    fun before_each_test() {
        setupTrackRecordingControls()
    }

    // unknown action tests

    @Test
    fun unknown_binds() {
        // arrange
        controls.performAction("UNKNOWN")

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockServiceController, times(1)).bindToService(controls)
        verify(mockServiceController, times(1)).unbindFromService()
    }

    @Test
    fun unknown_performs_no_action() {
        // arrange
        controls.performAction("UNKNOWN")

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockservice, never()).stopTracking(anyBoolean())
        verify(mockservice, never()).resumeTracking(anyBoolean())
        verify(mockservice, never()).pauseTracking(anyBoolean())
    }

    @Test
    fun unknown_records_no_analytics() {
        // arrange
        controls.performAction("UNKNOWN")

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockAnalyticsEngine, never()).recordClick(anyString(), anyString(), anyString())
    }

    // stop tests

    @Test
    fun stop_does_bind() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_STOP)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockServiceController, times(1)).bindToService(controls)
        verify(mockServiceController, times(1)).unbindFromService()
    }

    @Test
    fun stop_does_stop() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_STOP)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockservice, times(1)).stopTracking(true)
        verify(mockservice, never()).resumeTracking(true)
        verify(mockservice, never()).pauseTracking(true)
    }

    @Test
    fun stop_does_record_analytics() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_STOP)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.HUD_STOP_ID, AnalyticsConstants.HUD_STOP_NAME, AnalyticsConstants.HUD_TYPE)
    }

    // pause tests

    @Test
    fun pause_binds() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_PAUSE)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockServiceController, times(1)).bindToService(controls)
        verify(mockServiceController, times(1)).unbindFromService()
    }

    @Test
    fun pause_does_pause() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_PAUSE)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockservice, times(1)).pauseTracking(true)
        verify(mockservice, never()).resumeTracking(true)
        verify(mockservice, never()).stopTracking(true)
    }

    @Test
    fun pause_does_record_analytics() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_PAUSE)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.HUD_PAUSE_ID, AnalyticsConstants.HUD_PAUSE_NAME, AnalyticsConstants.HUD_TYPE)
    }

    // resume tests

    @Test
    fun resume_binds() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_RESUME)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockServiceController, times(1)).bindToService(controls)
        verify(mockServiceController, times(1)).unbindFromService()
    }

    @Test
    fun resume_does_resume() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_RESUME)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockservice, times(1)).resumeTracking(true)
        verify(mockservice, never()).pauseTracking(true)
        verify(mockservice, never()).stopTracking(true)
    }
    @Test
    fun resume_does_record_analytics() {
        // arrange
        controls.performAction(TrackRecordingControls.ACTION_RESUME)

        // act
        controls.connectService(mockbinder)                // the action is not performed until the service connects

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.HUD_RESUME_ID, AnalyticsConstants.HUD_RESUME_NAME, AnalyticsConstants.HUD_TYPE)
    }
}