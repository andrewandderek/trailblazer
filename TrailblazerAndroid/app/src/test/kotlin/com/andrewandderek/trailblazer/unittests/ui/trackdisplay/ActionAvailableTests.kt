/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class ActionAvailableTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
        presenter.start()
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }


    @Test
    fun action_available_start_false_by_default() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_start_true() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_start_false_if_no_permission() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(false)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_start_false_when_recording() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        Mockito.`when`(mockservice.isTracking).thenReturn(true)

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_start_false_when_paused() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        Mockito.`when`(mockservice.isPaused).thenReturn(true)

        // act
        val result = presenter.isActionAvailable(R.id.fab_start)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_stop_false() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_stop)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_stop_false_if_no_permission() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(false)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_stop)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_stop_true_when_recording() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        Mockito.`when`(mockservice.isTracking).thenReturn(true)

        // act
        val result = presenter.isActionAvailable(R.id.fab_stop)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_pause_false() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_pause)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_pause_false_if_no_permission() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(false)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_pause)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_pause_true_when_recording() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        Mockito.`when`(mockservice.isTracking).thenReturn(true)

        // act
        val result = presenter.isActionAvailable(R.id.fab_pause)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_resume_false() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_resume)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_resume_false_if_no_permission() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(false)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.isActionAvailable(R.id.fab_resume)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_resume_false_when_recording_and_not_paused() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        Mockito.`when`(mockservice.isTracking).thenReturn(true)
        Mockito.`when`(mockservice.isPaused).thenReturn(false)

        // act
        val result = presenter.isActionAvailable(R.id.fab_resume)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_resume_true_when_paused() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        Mockito.`when`(mockservice.isPaused).thenReturn(true)

        // act
        val result = presenter.isActionAvailable(R.id.fab_resume)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_statistics_false_by_default() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_statistics)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_statistics_true_when_recording() {
        // arrange
        startRecording()

        // act
        val result = presenter.isActionAvailable(R.id.action_statistics)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_statistics_true_when_displaying_track() {
        // arrange
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        val result = presenter.isActionAvailable(R.id.action_statistics)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_export_false_by_default() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_export_track)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_export_true_when_recording() {
        // arrange
        startRecording()

        // act
        val result = presenter.isActionAvailable(R.id.action_export_track)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_export_true_when_displaying_track() {
        // arrange
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        val result = presenter.isActionAvailable(R.id.action_export_track)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_share_false_by_default() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_share_track)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_share_true_when_recording() {
        // arrange
        startRecording()

        // act
        val result = presenter.isActionAvailable(R.id.action_share_track)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_share_true_when_displaying_track() {
        // arrange
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        val result = presenter.isActionAvailable(R.id.action_share_track)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_rename_false_by_default() {
        // arrange

        // act
        val result = presenter.isActionAvailable(R.id.action_rename_track)

        // assert
        Assert.assertEquals( false, result)
    }

    @Test
    fun action_available_rename_true_when_recording() {
        // arrange
        startRecording()

        // act
        val result = presenter.isActionAvailable(R.id.action_rename_track)

        // assert
        Assert.assertEquals( true, result)
    }

    @Test
    fun action_available_rename_true_when_displaying_track() {
        // arrange
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        val result = presenter.isActionAvailable(R.id.action_rename_track)

        // assert
        Assert.assertEquals( true, result)
    }



}