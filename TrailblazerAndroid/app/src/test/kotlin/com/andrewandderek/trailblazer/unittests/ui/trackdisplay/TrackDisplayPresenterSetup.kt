/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import android.content.Intent
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.RxEventBus
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.SystemTimeHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.settings.IZoomSettings
import com.andrewandderek.trailblazer.sharing.importexport.IExporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.IImporter
import com.andrewandderek.trailblazer.sharing.importexport.ITrackExporter
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.ui.trackdisplay.ITrackDisplayView
import com.andrewandderek.trailblazer.ui.trackdisplay.TrackDisplayPresenter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.schedulers.TestScheduler
import io.reactivex.rxjava3.subjects.PublishSubject
import org.mockito.Mock
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.`when`
import java.util.Calendar

open class TrackDisplayPresenterSetup : MockingAnnotationSetup() {
    protected val RECORDING_TRACK_ID = 123L
    protected val DISPLAY_TRACK_ID = 456L

    protected val IMPORT_FILENAME = "IMPORT FILENAME"
    private val EXPORT_FILENAME = "EXPORT FILENAME"
    private val SHARE_FILENAME = "SHARE FILENAME"

    protected val USER_ZOOM_LEVEL = 12.3F

    protected lateinit var presenter: TrackDisplayPresenter

    protected lateinit var eventBus: IEventBus
    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var mockScheduler: ISchedulerProvider

    @Mock protected lateinit var mockview: ITrackDisplayView
    @Mock protected lateinit var mockservice: IGpsLoggerService
    @Mock protected lateinit var mockbinder: GpsLoggerService.LocalBinder
    @Mock protected lateinit var mockServiceController: IGpsLoggerServiceController
    @Mock protected lateinit var mockPermissionsChecker: IPermissionChecker
    @Mock protected lateinit var mockTrackRepository: ITrackRepository
    @Mock protected lateinit var mockPositionRepository: IPositionRepository
    @Mock protected lateinit var mockExporterFactory: IExporterFactory
    @Mock protected lateinit var mockGpxExporter: ITrackExporter
    @Mock protected lateinit var mockKmlExporter: ITrackExporter
    @Mock protected lateinit var mockImporter: IImporter
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockAnalyticsEngine: IAnalyticsEngine
    @Mock protected lateinit var mockSharer: IIntentSharer
    @Mock protected lateinit var mockIntent: Intent
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockZoomSettings: IZoomSettings

    protected var observableTrack: PublishSubject<Track> = PublishSubject.create()
    protected lateinit var currentRecordingTrack: Track
    protected var observableCurrentPosition: PublishSubject<Position> = PublishSubject.create()

    protected lateinit var now: Calendar
    private lateinit var now2: Calendar

    protected lateinit var position1: Position
    protected lateinit var position2: Position
    protected lateinit var position3: Position
    protected lateinit var position4: Position
    protected lateinit var currentPosition: Position

    protected fun setupNowDateTimes() {
        now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now2 = Calendar.getInstance()
        now2.time = now.time
        now2.add(Calendar.SECOND, 10)
    }

    protected fun setupPoints() {
        position1 = Position(1.0, 2.0, 3.0, now, 10F, "POSITION_NOTES_1")
        position2 = Position(11.0, 12.0, 13.0, now2, 10F,"POSITION_NOTES_2")
        position3 = Position(51.0, 112.0, 113.0, now, 10F,"POSITION_NOTES_3")
        position4 = Position(61.0, 1112.0, 1113.0, now2, 10F,"POSITION_NOTES_4")
        currentPosition = Position(99.9, 88.8, 5555.5, now2, 10F)
    }

    protected fun setupPresenter(scheduler: TestScheduler? = null) {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockScheduler = if (scheduler == null) RxHelper.setupSchedulerProvider() else RxHelper.setupTestSchedulerProvider(scheduler)
        eventBus = RxEventBus(
                mockLoggerFactory,
                mockCrashReporter,
                mockScheduler
        )
        presenter = TrackDisplayPresenter(
                mockLoggerFactory,
                mockScheduler,
                mockServiceController,
                mockPermissionsChecker,
                mockTrackRepository,
                mockPositionRepository,
                mockExporterFactory,
                mockImporter,
                mockSharer,
                mockResourceProvider,
                mockAnalyticsEngine,
                mockCrashReporter,
                eventBus,
                mockZoomSettings
        )

        `when`(mockbinder.service).thenReturn(mockservice)

        // we use real observers so we can trigger the subscriptions in the tests
        currentRecordingTrack = Track()
        currentRecordingTrack.id = RECORDING_TRACK_ID
        currentRecordingTrack.name = "TEST TRACK"
        currentRecordingTrack.started = SystemTimeHelper.testDate()
        `when`(mockservice.currentTrack).thenReturn(observableTrack.toFlowable(BackpressureStrategy.ERROR))
        `when`(mockservice.currentTrackObject).thenReturn(currentRecordingTrack)
        `when`(mockservice.currentTrackPositions).thenReturn(currentRecordingTrack.positions)
        `when`(mockservice.currentPosition).thenReturn(observableCurrentPosition.toFlowable(BackpressureStrategy.ERROR))

        `when`(mockGpxExporter.getExportFilename(anyObject(), anyBoolean())).thenReturn(EXPORT_FILENAME)
        `when`(mockGpxExporter.getShareFilename(anyObject())).thenReturn(SHARE_FILENAME)
        `when`(mockExporterFactory.getExporter(NameLiterals.FILE_FORMAT_GPX)).thenReturn(mockGpxExporter)
        `when`(mockKmlExporter.getExportFilename(anyObject(), anyBoolean())).thenReturn(EXPORT_FILENAME)
        `when`(mockKmlExporter.getShareFilename(anyObject())).thenReturn(SHARE_FILENAME)
        `when`(mockExporterFactory.getExporter(NameLiterals.FILE_FORMAT_KML)).thenReturn(mockKmlExporter)

        `when`(mockSharer.getShareIntentWithMultipleFiles(anyObject(), anyObject())).thenReturn(mockIntent)

        `when`(mockResourceProvider.getString(R.string.main_title_recording_fmt)).thenReturn("TEST_TITLE_REC %s")
        `when`(mockResourceProvider.getString(R.string.main_title_paused_fmt)).thenReturn("TEST_TITLE_PAUSE %s")
        `when`(mockResourceProvider.getString(R.string.app_name)).thenReturn("TEST_APP_NAME")
        `when`(mockResourceProvider.getString(R.string.file_format_export_prompt)).thenReturn("TEST_EXPORT_PROMPT")

        `when`(mockZoomSettings.zoomLevel).thenReturn(USER_ZOOM_LEVEL)
    }

    protected fun startRecording() {
        presenter.connectService(mockbinder)                // attach the subscribers = effectively start the recording
        `when`(mockservice.isTracking).thenReturn(true)
        observableTrack.onNext(currentRecordingTrack)       // kick the subscribers
    }

    protected fun pauseRecording() {
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(true)
    }

    protected fun stopRecording() {
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(false)
    }

    protected fun unPauseRecording() {
        `when`(mockservice.isTracking).thenReturn(true)
        `when`(mockservice.isPaused).thenReturn(false)
    }

    protected fun setupDisplayTrack(): Track {
        val displayTrack = Track()
        displayTrack.id = DISPLAY_TRACK_ID
        displayTrack.name = "TEST DISPLAY TRACK"
        displayTrack.started = SystemTimeHelper.testDate()
        displayTrack.addNewPosition(Position(1.0, 2.0, 3.0, Calendar.getInstance(), 100F,"POSITION_NOTES_1"))

        `when`(mockTrackRepository.getWithPositionsAndAccuracy(displayTrack.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(displayTrack)

        return displayTrack
    }

    protected fun setupDisplayTrackWithTwoPoints(): Track {
        val displayTrack = Track()
        displayTrack.id = DISPLAY_TRACK_ID
        displayTrack.name = "TEST TRACK"
        displayTrack.started = SystemTimeHelper.testDate()
        displayTrack.addNewPosition(position1)
        displayTrack.addNewPosition(position2)

        `when`(mockTrackRepository.getWithPositionsAndAccuracy(displayTrack.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(displayTrack)
        return displayTrack
    }

    protected fun setupDisplayRecordingTrack() {
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(currentRecordingTrack.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(currentRecordingTrack)
    }
}