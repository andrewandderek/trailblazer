/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.settings.usersettings

import com.andrewandderek.trailblazer.helpers.UserSettingsHelper
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.units.AltitudeUnit
import com.andrewandderek.trailblazer.units.DistanceUnit
import com.andrewandderek.trailblazer.units.PaceUnit
import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.units.SpeedUnit
import org.junit.Assert.assertEquals
import org.junit.Test

class UserSettingsTests {
    private lateinit var userSettings: IUserSettings

    @Test
    fun prefer_metric_units_altitude() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())

        // act
        val unit = userSettings.altitudeUnit
        val label = userSettings.altitudeUnitLabel

        // assert
        assertEquals(AltitudeUnit.Metres, unit)
        assertEquals("ALTITUDE_LABEL_METRIC", label)
    }

    @Test
    fun prefer_metric_units_distance() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())

        // act
        val unit = userSettings.distanceUnit
        val label = userSettings.distanceUnitLabel

        // assert
        assertEquals(DistanceUnit.Kilometres, unit)
        assertEquals("DISTANCE_LABEL_METRIC", label)
    }

    @Test
    fun prefer_metric_units_pace() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())

        // act
        val unit = userSettings.paceUnit
        val label = userSettings.paceUnitLabel

        // assert
        assertEquals(PaceUnit.MinutesPerKilometre, unit)
        assertEquals("PACE_LABEL_METRIC", label)
    }

    @Test
    fun prefer_metric_units_speed() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())

        // act
        val unit = userSettings.speedUnit
        val label = userSettings.speedUnitLabel

        // assert
        assertEquals(SpeedUnit.KilometresPerHour, unit)
        assertEquals("SPEED_LABEL_METRIC", label)
    }

    @Test
    fun prefer_imperial_units_altitude() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.IMPERIAL.toString())

        // act
        val unit = userSettings.altitudeUnit
        val label = userSettings.altitudeUnitLabel

        // assert
        assertEquals(AltitudeUnit.Feet, unit)
        assertEquals("ALTITUDE_LABEL_IMPERIAL", label)
    }

    @Test
    fun prefer_imperial_units_distance() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.IMPERIAL.toString())

        // act
        val unit = userSettings.distanceUnit
        val label = userSettings.distanceUnitLabel

        // assert
        assertEquals(DistanceUnit.Miles, unit)
        assertEquals("DISTANCE_LABEL_IMPERIAL", label)
    }

    @Test
    fun prefer_imperial_units_pace() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.IMPERIAL.toString())

        // act
        val unit = userSettings.paceUnit
        val label = userSettings.paceUnitLabel

        // assert
        assertEquals(PaceUnit.MinutesPerMile, unit)
        assertEquals("PACE_LABEL_IMPERIAL", label)
    }

    @Test
    fun prefer_imperial_units_speed() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.IMPERIAL.toString())

        // act
        val unit = userSettings.speedUnit
        val label = userSettings.speedUnitLabel

        // assert
        assertEquals(SpeedUnit.MilesPerHour, unit)
        assertEquals("SPEED_LABEL_IMPERIAL", label)
    }

    @Test
    fun prefer_no_state() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())

        // act
        val state = userSettings.recordingState

        // assert
        assertEquals("track id", -1, state.trackId)
        assertEquals("current segment", -1, state.currentSegment)
        assertEquals("paused", false, state.isPaused)
        assertEquals("recording", false, state.isRecording)
    }

    @Test
    fun prefer_state() {
        // arrange
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString(), 123, 5, true, true)

        // act
        val state = userSettings.recordingState

        // assert
        assertEquals("track id", 123, state.trackId)
        assertEquals("current segment", 5, state.currentSegment)
        assertEquals("paused", true, state.isPaused)
        assertEquals("recording", true, state.isRecording)
    }


}