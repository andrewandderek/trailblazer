/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.ExportException
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Captor
import org.mockito.Mockito
import org.mockito.Mockito.*

class MultiselectExportTests : TrackListPresenterSetup() {
    @Captor
    private lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
        setupTrackListItems(3)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun not_in_select_mode_and_exported() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.onActionItemClicked(R.id.action_export_track)

        // assert
        verify(mockview, never()).promptForFileFormat(anyString(), anyString())
    }

    @Test
    fun nothing_selected_and_exported() {
        // arrange
        presenter.loadTracks()
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(1, 0)

        // act
        presenter.onActionItemClicked(R.id.action_export_track)

        // assert
        verify(mockview, never()).promptForFileFormat(anyString(), anyString())
    }

    @Test
    fun item_1_and_2_selected_and_exported_without_permission() {
        // arrange
        `when`(mockPermissionChecker.hasWriteStoragePermission()).thenReturn(false)
        loadTracksAndSelect1and2()

        // act
        presenter.onActionItemClicked(R.id.action_export_track)

        // assert
        verify(mockview, times(1)).requestWriteExternalStoragePermission()
        verify(mockview, never()).promptForFileFormat(anyString(), anyString())
    }

    @Test
    fun item_1_and_2_selected_and_exported() {
        // arrange
        `when`(mockPermissionChecker.hasWriteStoragePermission()).thenReturn(true)
        loadTracksAndSelect1and2()

        // act
        presenter.onActionItemClicked(R.id.action_export_track)

        // assert
        verify(mockview, times(1)).promptForFileFormat("TEST_FILE_FORMAT_EXPORT", "GPX")
    }

    @Test
    fun item_1_and_2_selected_and_exported_and_confirmed() {
        // arrange
        `when`(mockPermissionChecker.hasWriteStoragePermission()).thenReturn(true)
        `when`(mockGpxExporter.exportTrack(MockitoHelper.anyObject(), Mockito.anyString())).thenReturn(true)
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_export_track)

        // act
        reset(mockview)
        presenter.confirmedMultipleExportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        // tracks exported
        // there were 2
        verify(mockGpxExporter, times(2)).exportTrack(MockitoHelper.anyObject(), Mockito.anyString())
        // the correct 2
        verify(mockGpxExporter, times(1)).getExportFilename("2017-12-30-Track With points", true)
        verify(mockGpxExporter, times(1)).exportTrack(trackWithPositions, "EXPORT FILENAME 1")
        verify(mockGpxExporter, times(1)).getExportFilename("2017-12-30-Track Without points", true)
        verify(mockGpxExporter, times(1)).exportTrack(trackWithoutPositions, "EXPORT FILENAME 2")
        // and nothing else
        verify(mockGpxExporter, never()).getShareFilename(MockitoHelper.anyObject())
        verify(mockKmlExporter, never()).exportTrack(MockitoHelper.anyObject(), Mockito.anyString())
        // UI updated
        verify(mockview, times(1)).startProgress(R.string.track_multi_exporting_progress, 2)
        verify(mockview, times(1)).updateProgress(1)
        verify(mockview, times(1)).updateProgress(2)
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).showMessage("TEST_MULTI_EXPORT 2")
    }

    @Test
    fun item_1_and_2_selected_and_exported_and_confirmed_throws() {
        // arrange
        `when`(mockPermissionChecker.hasWriteStoragePermission()).thenReturn(true)
        `when`(mockGpxExporter.exportTrack(MockitoHelper.anyObject(), Mockito.anyString())).thenThrow(NullPointerException("ERROR"))
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_export_track)

        // act
        reset(mockview)
        presenter.confirmedMultipleExportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        // UI updated
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).showMessage(R.string.track_export_error)
    }

    @Test
    fun item_1_and_2_selected_and_exported_and_confirmed_throws_and_logs() {
        // arrange
        `when`(mockPermissionChecker.hasWriteStoragePermission()).thenReturn(true)
        `when`(mockGpxExporter.exportTrack(MockitoHelper.anyObject(), Mockito.anyString())).thenThrow(NullPointerException("ERROR"))
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_export_track)

        // act
        reset(mockview)
        presenter.confirmedMultipleExportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(MockitoHelper.capture(loggingCaptor))
        val exception = loggingCaptor.value
        Assert.assertEquals("exception type incorrect", true, exception is ExportException)
        Assert.assertEquals("inner exception type incorrect", true, exception.cause is NullPointerException)
    }
}