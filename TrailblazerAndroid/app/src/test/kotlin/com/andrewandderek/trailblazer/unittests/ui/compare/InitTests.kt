/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.compare

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.ui.compare.SortedTrackList
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class InitTests : CompareViewModelSetup() {

    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun init_no_ids_title_is_set() {
        // arrange

        // act
        viewModel.init(longArrayOf())

        // assert
        verify(mockTitleObserver, times(1)).onChanged("TEST_TITLE 0")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockTitleObserver);
    }

    @Test
    fun init_title_is_set() {
        // arrange

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockTitleObserver, times(1)).onChanged("TEST_TITLE 3")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockTitleObserver);
    }

    @Test
    fun init_progress() {
        // arrange
        setupAllTracks()

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockProgressStartObserver, times(1)).onChanged(3)
        verify(mockProgressPositionObserver, times(1)).onChanged(1)
        verify(mockProgressPositionObserver, times(1)).onChanged(2)
        verify(mockProgressPositionObserver, times(1)).onChanged(3)
        verify(mockProgressEndObserver, times(1)).onChanged(null)
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockProgressStartObserver);
        verifyNoMoreInteractions(mockProgressPositionObserver);
        verifyNoMoreInteractions(mockProgressEndObserver);
    }

    @Test
    fun init_overall_summary_data() {
        // arrange
        setupAllTracks()

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockTrackGroupSummaryStatisticsProvider, times(1)).getSummaryStatistics(MockitoHelper.capture(trackStatsCaptor))
        assertEquals(3, trackStatsCaptor.value.size)
        // after the stats are calculated we remove all the positions as we dont need them - so the tracks should all be empty
        assertEquals(true, trackStatsCaptor.value.all { trackStats -> trackStats.track.isEmpty })
    }

    @Test
    fun init_overall_summary() {
        // arrange
        setupAllTracks()

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockSummaryStats, times(1)).onChanged(MockitoHelper.eq(formattedSummaryStats))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSummaryStats);
    }

    @Test
    fun init_sorted_list() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_MOVING_TIME",
                "TEST_ORDER_ASC",
                sortedTracks
        )

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun init_twice_does_not_load() {
        // arrange
        setupAllTracks()
        viewModel.init(longArrayOf(1,2,3))
        reset(mockTrackRepository)
        reset(mockProgressStartObserver);
        reset(mockProgressPositionObserver);
        reset(mockProgressEndObserver);

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verifyNoMoreInteractions(mockTrackRepository);
        verifyNoMoreInteractions(mockProgressStartObserver);
        verifyNoMoreInteractions(mockProgressPositionObserver);
        verifyNoMoreInteractions(mockProgressEndObserver);
    }

    @Test
    fun init_twice_sets_title() {
        // arrange
        setupAllTracks()
        viewModel.init(longArrayOf(1,2,3))
        reset(mockTitleObserver)

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockTitleObserver, times(1)).onChanged("TEST_TITLE 3")
    }

    @Test
    fun init_twice_sorted_list() {
        // arrange
        setupAllTracks()
        val expected = SortedTrackList(
                "TEST_SORT_MOVING_TIME",
                "TEST_ORDER_DESC",
                sortedTracks
        )
        viewModel.init(longArrayOf(1,2,3))
        viewModel.setSortDirection(1)
        reset(mockSortedList);

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        // check the sort order is not reset
        verify(mockSortedList, times(1)).onChanged(MockitoHelper.eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSortedList);
    }

    @Test
    fun init_twice_overall_summary() {
        // arrange
        setupAllTracks()
        viewModel.init(longArrayOf(1,2,3))
        reset(mockSummaryStats);

        // act
        viewModel.init(longArrayOf(1,2,3))

        // assert
        verify(mockSummaryStats, times(1)).onChanged(MockitoHelper.eq(formattedSummaryStats))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockSummaryStats);
    }

}