/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import android.os.Bundle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.anyFloat
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ConnectServiceTests : TrackDisplayPresenterSetup() {
    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()
    }

    @Test
    fun connect_reads_current_track() {
        // arrange
        presenter.bind(mockview, null)
        presenter.start()

        // act
        presenter.connectService(mockbinder)        // attach the subscribers

        // assert
        verify(mockservice, times(1)).currentTrackObject
        verify(mockview, times(1)).updateAvailableActions()
    }

    @Test
    fun connect_without_state_without_paused_track() {
        // arrange
        presenter.bind(mockview, null)
        presenter.start()

        // act
        presenter.connectService(mockbinder)        // attach the subscribers

        // assert
        // we dont load anything
        verify(mockTrackRepository, never()).getWithPositionsAndAccuracy(ArgumentMatchers.anyLong(), ArgumentMatchers.anyFloat(), MockitoHelper.anyObject())
        verify(mockview, never()).completeProgress()
        verify(mockview, never()).startProgress(anyInt())
    }

    @Test
    fun connect_without_state_without_paused_track_subscribes_to_current_position() {
        // arrange
        presenter.bind(mockview, null)
        presenter.start()
        currentRecordingTrack.id = -1

        // act
        presenter.connectService(mockbinder)        // attach the subscribers
        observableCurrentPosition.onNext(currentPosition)   // trigger current position so we can test subscription
        observableCurrentPosition.onNext(currentPosition)   // trigger again so we can test that we've unsubscribed

        // assert
        verify(mockservice, times(1)).currentPosition
        verify(mockview, times(1)).centreMapAndZoom(currentPosition, USER_ZOOM_LEVEL)
    }

    @Test
    fun connect_without_state_with_recording_track() {
        // arrange
        presenter.bind(mockview, null)
        presenter.start()
        startRecording()

        // act
        presenter.connectService(mockbinder)        // attach the subscribers

        // assert
        // we dont load anything - it will start displaying when an update is received
        verify(mockTrackRepository, never()).getWithPositionsAndAccuracy(ArgumentMatchers.anyLong(), ArgumentMatchers.anyFloat(), MockitoHelper.anyObject())
        verify(mockview, never()).completeProgress()
        verify(mockview, never()).startProgress(anyInt())
    }

    @Test
    fun connect_without_state_with_recording_track_does_not_move_to_current_position() {
        // arrange
        presenter.bind(mockview, null)
        presenter.start()
        startRecording()

        // act
        presenter.connectService(mockbinder)        // attach the subscribers
        observableCurrentPosition.onNext(currentPosition)   // trigger current position so we can test subscription

        // assert
        verify(mockview, never()).centreMapAndZoom(MockitoHelper.anyObject(), anyFloat())
    }

    @Test
    fun connect_with_state_and_paused_track() {
        // arrange
        setupDisplayTrack()
        val bundle = mock(Bundle::class.java)
        `when`(bundle.getLong(ArgumentMatchers.anyString(), ArgumentMatchers.anyLong())).thenReturn(DISPLAY_TRACK_ID)
        presenter.bind(mockview, bundle)
        presenter.start()
        pauseRecording()

        // act
        presenter.connectService(mockbinder)        // attach the subscribers

        // assert
        // we load the track from the state
        verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(
                ArgumentMatchers.eq(DISPLAY_TRACK_ID),
                ArgumentMatchers.anyFloat(),
                MockitoHelper.anyObject())
        verify(mockview, times(1)).setTitle("TEST DISPLAY TRACK")
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).startProgress(R.string.track_loading_progress)
    }

    @Test
    fun connect_with_state_and_recording_track() {
        // arrange
        setupDisplayTrack()
        val bundle = mock(Bundle::class.java)
        `when`(bundle.getLong(ArgumentMatchers.anyString(), ArgumentMatchers.anyLong())).thenReturn(DISPLAY_TRACK_ID)
        presenter.bind(mockview, bundle)
        presenter.start()
        startRecording()

        // act
        presenter.connectService(mockbinder)        // attach the subscribers

        // assert
        // we load the track from the state
        verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(
                ArgumentMatchers.eq(DISPLAY_TRACK_ID),
                ArgumentMatchers.anyFloat(),
                MockitoHelper.anyObject())
        verify(mockview, times(1)).setTitle("TEST DISPLAY TRACK")
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).startProgress(R.string.track_loading_progress)
    }

}