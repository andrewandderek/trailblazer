/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.never
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ActionSelectedTests : TrackDisplayPresenterSetup() {
    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun action_start_selected() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.actionSelected(R.id.fab_start)
        startRecording()

        // assert
        assertEquals( true, result)
        verify(mockServiceController, times(1)).startService()
        verify(mockServiceController, never()).stopService()
        verify(mockservice, times(1)).startTracking()
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.START_ID, AnalyticsConstants.START_NAME, AnalyticsConstants.FAB_TYPE)
    }

    @Test
    fun action_start_selected_should_clear_displayed_track() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        presenter.actionSelected(R.id.fab_start)

        // assert
        assertEquals( false, presenter.isDisplayingPreviouslyRecordedTrack)
        verify(mockview, times(1)).clearAllPositions()
    }

    @Test
    fun action_resume_selected_when_paused() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        `when`(mockservice.isPaused).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        val result = presenter.actionSelected(R.id.fab_resume)

        // assert
        assertEquals( true, result)
        verify(mockServiceController, never()).startService()
        verify(mockServiceController, never()).stopService()
        verify(mockservice, times(1)).resumeTracking(false)
    }

    @Test
    fun action_resume_selected_when_paused_should_not_clear_displayed_track() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        `when`(mockservice.isPaused).thenReturn(true)
        presenter.connectService(mockbinder)
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        presenter.actionSelected(R.id.fab_resume)

        // assert
        assertEquals( true, presenter.isDisplayingPreviouslyRecordedTrack)
        verify(mockview, never()).clearAllPositions()
    }

    @Test
    fun action_pause_selected() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        startRecording()
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(true)

        // act
        val result = presenter.actionSelected(R.id.fab_pause)

        // assert
        assertEquals( true, result)
        verify(mockServiceController, never()).startService()
        verify(mockServiceController, never()).stopService()
        verify(mockservice, times(1)).pauseTracking(false)
        verify(mockview, times(1)).setTitle("TEST_TITLE_PAUSE TEST TRACK")
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.PAUSE_ID, AnalyticsConstants.PAUSE_NAME, AnalyticsConstants.FAB_TYPE)
    }

    @Test
    fun action_stop_selected() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        startRecording()
        currentRecordingTrack.addNewPosition(position3)

        // act
        reset(mockview)     // we only want to test the view interactions from this point
        val result = presenter.actionSelected(R.id.fab_stop)

        // assert
        assertEquals( true, result)
        verify(mockServiceController, never()).startService()
        verify(mockServiceController, times(1)).stopService()
        verify(mockservice, times(1)).stopTracking(false)
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.STOP_ID, AnalyticsConstants.STOP_NAME, AnalyticsConstants.FAB_TYPE)
    }

    @Test
    fun action_stop_selected_reloads_track_and_zooms_to_fit() {
        // arrange
        `when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        startRecording()
        currentRecordingTrack.addNewPosition(position3)
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(false)
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(RECORDING_TRACK_ID, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(currentRecordingTrack)

        // act
        reset(mockview)     // we only want to test the view interactions from this point
        val result = presenter.actionSelected(R.id.fab_stop)

        // assert
        // also check that we then display the track we just recorded
        assertEquals( true, result)
        verify(mockview, times(1)).setTitle("TEST TRACK")
        verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(RECORDING_TRACK_ID, Position.ACCURACY_THRESHOLD, mockPositionRepository)
        verify(mockview, times(1)).centreMapAndZoomToFit(currentRecordingTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(listOf(position3))
    }

    @Test
    fun action_statistics_selected_when_displaying_track() {
        // arrange
        val displayTrack = setupDisplayTrack()
        presenter.displayTrack(displayTrack.id)

        // act
        val result = presenter.actionSelected(R.id.action_statistics)

        // assert
        assertEquals( true, result)
        verify(mockview, times(1)).navigateToStatistics(DISPLAY_TRACK_ID)
    }

    @Test
    fun action_statistics_selected_when_recording() {
        // arrange
        startRecording()

        // act
        val result = presenter.actionSelected(R.id.action_statistics)

        // assert
        assertEquals( true, result)
        verify(mockview, times(1)).navigateToStatistics(RECORDING_TRACK_ID)
    }

    @Test
    fun action_statistics_selected_when_no_current_track() {
        // arrange

        // act
        val result = presenter.actionSelected(R.id.action_statistics)

        // assert
        assertEquals( true, result)
        verify(mockview, never()).navigateToStatistics(ArgumentMatchers.anyLong())
    }

}