/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

class TrackItemRenderTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        `when`(mockResourceProvider.getString(R.string.track_list_item_unknown)).thenReturn("TEST_1")
        `when`(mockResourceProvider.getString(R.string.track_list_item_title_fmt)).thenReturn("TEST_TITLE %s")
        `when`(mockResourceProvider.getString(R.string.track_list_item_subtitle_1_fmt)).thenReturn("TEST_SUBTITLE_1 %1\$s")
        `when`(mockResourceProvider.getString(R.string.track_list_item_subtitle_2_fmt)).thenReturn(", TEST_SUBTITLE_2 %1\$s")
        `when`(mockResourceProvider.getString(R.string.track_list_item_subtitle_3_fmt)).thenReturn(", TEST_SUBTITLE_3 %1\$s %2\$s %3\$s %4\$s")
        `when`(mockResourceProvider.getString(R.string.track_list_item_title_recording_fmt)).thenReturn("TEST_TITLE_RECORDING %s")
        `when`(mockResourceProvider.getString(R.string.track_list_item_title_paused_fmt)).thenReturn("TEST_TITLE_PAUSED %s")
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun trackList_itemCount() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        val result = presenter.itemCount

        // assert
        assertEquals(2, result)
    }

    @Test
    fun trackList_itemOutOfRange() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(123)
        val resultSubLabel = presenter.getTrackSubLabel(123)

        // assert
        assertEquals("TEST_1", resultLabel)
        assertEquals("TEST_1", resultSubLabel)
    }

    @Test
    fun trackList_itemWithPositions() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(0)
        val resultSubLabel = presenter.getTrackSubLabel(0)

        // assert
        assertEquals("TEST_TITLE Track With points", resultLabel)
        // we have the last position but there is no metadata
        assertEquals("TEST_SUBTITLE_1 Sat 30, Dec, 2017 15:37:22", resultSubLabel)
    }

    @Test
    fun trackList_itemWithoutPositions() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(1)
        val resultSubLabel = presenter.getTrackSubLabel(1)

        // assert
        assertEquals("TEST_TITLE Track Without points", resultLabel)
        assertEquals("TEST_SUBTITLE_1 Sat 30, Dec, 2017 15:37:32", resultSubLabel)
    }

    @Test
    fun trackList_itemWithPositionsAndMetadataWithoutEndTime() {
        // arrange
        setupTrackListItems(3)
        anotherTrackWithPositionsAndMetadata.metadata!!.ended = null
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(2)
        val resultSubLabel = presenter.getTrackSubLabel(2)

        // assert
        assertEquals("TEST_TITLE Another Track With points", resultLabel)
        assertEquals("TEST_SUBTITLE_1 Sat 30, Dec, 2017 15:37:37, TEST_SUBTITLE_3 1235 02:15:12 2 2", resultSubLabel)
    }

    @Test
    fun trackList_itemWithPositionsAndMetadataWithEndTime() {
        // arrange
        setupTrackListItems(3)
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(2)
        val resultSubLabel = presenter.getTrackSubLabel(2)

        // assert
        assertEquals("TEST_TITLE Another Track With points", resultLabel)
        assertEquals("TEST_SUBTITLE_1 Sat 30, Dec, 2017 15:37:37, TEST_SUBTITLE_2 15:37:47, TEST_SUBTITLE_3 1235 02:15:12 2 2", resultSubLabel)
    }

    @Test
    fun trackList_itemBeingRecorded() {
        // arrange
        setupTrackListItems(2)
        trackIsBeingRecorded(trackWithPositions)
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(0)
        val resultSubLabel = presenter.getTrackSubLabel(0)

        // assert
        assertEquals("TEST_TITLE_RECORDING Track With points", resultLabel)
        assertEquals("TEST_SUBTITLE_1 Sat 30, Dec, 2017 15:37:22", resultSubLabel)
    }

    @Test
    fun trackList_itemBeingPaused() {
        // arrange
        setupTrackListItems(2)
        trackIsBeingPaused(trackWithPositions)
        presenter.loadTracks()

        // act
        val resultLabel = presenter.getTrackLabel(0)
        val resultSubLabel = presenter.getTrackSubLabel(0)

        // assert
        assertEquals("TEST_TITLE_PAUSED Track With points", resultLabel)
        assertEquals("TEST_SUBTITLE_1 Sat 30, Dec, 2017 15:37:22", resultSubLabel)
    }
}