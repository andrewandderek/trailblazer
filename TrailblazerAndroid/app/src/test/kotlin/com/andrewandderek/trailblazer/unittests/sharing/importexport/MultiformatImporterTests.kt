/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.sharing.importexport

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.sharing.importexport.ImportAndResult
import com.andrewandderek.trailblazer.sharing.importexport.ImportResult
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.*

class MultiformatImporterTests : ImportSetup() {
    @Before
    fun before_each_test() {
        setupImporter()
    }

    @Test
    fun gpx_import() {
        // arrange
        val track: Track = Track()
        `when`(mockGpxImporter.importTrack(anyString())).thenReturn(ImportAndResult(track, ImportResult.OK))
        `when`(mockKmlImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))
        `when`(mockKmzImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))

        // act
        val result = importer.importTrack("")

        // assert
        Assert.assertNotNull(result.track)
        verify(mockAnalytics, times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_GPX_ID, AnalyticsConstants.ACTION_IMPORT_GPX_NAME, AnalyticsConstants.INTERNAL_TYPE)
        verify(mockAnalytics, never()).recordClick(AnalyticsConstants.ACTION_IMPORT_KML_ID, AnalyticsConstants.ACTION_IMPORT_KML_NAME, AnalyticsConstants.INTERNAL_TYPE)
        verify(mockAnalytics, never()).recordClick(AnalyticsConstants.ACTION_IMPORT_KMZ_ID, AnalyticsConstants.ACTION_IMPORT_KMZ_NAME, AnalyticsConstants.INTERNAL_TYPE)
    }

    @Test
    fun kml_import() {
        // arrange
        val track: Track = Track()
        `when`(mockGpxImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))
        `when`(mockKmlImporter.importTrack(anyString())).thenReturn(ImportAndResult(track, ImportResult.OK))
        `when`(mockKmzImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))

        // act
        val result = importer.importTrack("")

        // assert
        Assert.assertNotNull(result.track)
        verify(mockAnalytics, never()).recordClick(AnalyticsConstants.ACTION_IMPORT_GPX_ID, AnalyticsConstants.ACTION_IMPORT_GPX_NAME, AnalyticsConstants.INTERNAL_TYPE)
        verify(mockAnalytics, times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_KML_ID, AnalyticsConstants.ACTION_IMPORT_KML_NAME, AnalyticsConstants.INTERNAL_TYPE)
        verify(mockAnalytics, never()).recordClick(AnalyticsConstants.ACTION_IMPORT_KMZ_ID, AnalyticsConstants.ACTION_IMPORT_KMZ_NAME, AnalyticsConstants.INTERNAL_TYPE)
    }

    @Test
    fun kmz_import() {
        // arrange
        val track: Track = Track()
        `when`(mockGpxImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))
        `when`(mockKmlImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))
        `when`(mockKmzImporter.importTrack(anyString())).thenReturn(ImportAndResult(track, ImportResult.OK))

        // act
        val result = importer.importTrack("")

        // assert
        Assert.assertNotNull(result.track)
        verify(mockAnalytics, never()).recordClick(AnalyticsConstants.ACTION_IMPORT_GPX_ID, AnalyticsConstants.ACTION_IMPORT_GPX_NAME, AnalyticsConstants.INTERNAL_TYPE)
        verify(mockAnalytics, never()).recordClick(AnalyticsConstants.ACTION_IMPORT_KML_ID, AnalyticsConstants.ACTION_IMPORT_KML_NAME, AnalyticsConstants.INTERNAL_TYPE)
        verify(mockAnalytics, times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_KMZ_ID, AnalyticsConstants.ACTION_IMPORT_KMZ_NAME, AnalyticsConstants.INTERNAL_TYPE)
    }

    @Test
    fun unknown_import() {
        // arrange
        `when`(mockGpxImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))
        `when`(mockKmlImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))
        `when`(mockKmzImporter.importTrack(anyString())).thenReturn(ImportAndResult(null, ImportResult.UNKNOWN_FORMAT))

        // act
        val result = importer.importTrack("")

        // assert
        Assert.assertEquals(ImportResult.UNKNOWN_FORMAT, result.result)
        Assert.assertNull(result.track)
        verify(mockAnalytics, never()).recordClick(anyString(), anyString(), anyString())
    }

}