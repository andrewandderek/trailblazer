/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics.elevation

import com.andrewandderek.trailblazer.statistics.elevation.RawElevationAlgorithm
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class RawElevationAlgorithmTests : ElevationAlgorithmTestBase() {

    private lateinit var elevationAlgorithm: RawElevationAlgorithm

    @Before
    fun setupTest() {
        `when`(mockElevationSettings.elevationThreshold).thenReturn(3.0)

        elevationAlgorithm = RawElevationAlgorithm(mockAltitudeProcessor, mockElevationSettings)
    }

    @Test
    fun reset_resetsProcessor() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5))

        // act
        elevationAlgorithm.reset(positions[0])

        // assert
        verify(mockAltitudeProcessor).reset(positions[0])
    }

    @Test
    fun process_sendsPositionToProcessor() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5))
        elevationAlgorithm.reset(positions[0])

        // act
        elevationAlgorithm.process(positions[1])

        // assert
        verify(mockAltitudeProcessor).reset(positions[0])
        verify(mockAltitudeProcessor).process(positions[1])
    }

    @Test
    fun process_secondPositionIsLower_returnsZeroGainAndPositiveLoss() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5))
        elevationAlgorithm.reset(positions[0])

        // act
        val result = elevationAlgorithm.process(positions[1])

        // assert
        assertEquals(0.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.5, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_eachPositionIsLowerThanPrevious_returnsZeroGainAndPositiveLoss() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5, 8.4, 7.3))
        elevationAlgorithm.reset(positions[0])

        // act
        val result1 = elevationAlgorithm.process(positions[1])
        val result2 = elevationAlgorithm.process(positions[2])
        val result3 = elevationAlgorithm.process(positions[3])

        // assert
        assertEquals(0.0, result1.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.5, result1.loss, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result2.gain, TEST_ELEVATION_DELTA)
        assertEquals(1.1, result2.loss, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result3.gain, TEST_ELEVATION_DELTA)
        assertEquals(1.1, result3.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_higherPositionThanPrevious_returnsAltitudeDifferenceFromPreviousAsGain() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5, 8.4, 7.3, 11.7))
        elevationAlgorithm.reset(positions[0])
        elevationAlgorithm.process(positions[1])
        elevationAlgorithm.process(positions[2])
        elevationAlgorithm.process(positions[3])

        // act
        val result = elevationAlgorithm.process(positions[4])

        // assert
        assertEquals(4.4, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_negativeAltitudesTreatedAsZeroForGainsAndLosses() {
        // arrange
        `when`(mockElevationSettings.treatNegativeElevationsAsZero).thenReturn(true)
        val positions = createPositions(doubleArrayOf(-1.0, 1.0, -4.0, -2.0, 2.0))
        elevationAlgorithm.reset(positions[0])

        // act
        val result1 = elevationAlgorithm.process(positions[1])
        val result2 = elevationAlgorithm.process(positions[2])
        val result3 = elevationAlgorithm.process(positions[3])
        val result4 = elevationAlgorithm.process(positions[4])

        // assert
        assertEquals(1.0, result1.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result1.loss, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result2.gain, TEST_ELEVATION_DELTA)
        assertEquals(1.0, result2.loss, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result3.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result3.loss, TEST_ELEVATION_DELTA)
        assertEquals(2.0, result4.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result4.loss, TEST_ELEVATION_DELTA)
    }
}