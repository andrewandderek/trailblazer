/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.statistics

import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class LoadTrackTests : TrackStatisticsPresenterSetup() {
    @Captor
    protected lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    @Before
    fun setupTests() {
        setupPresenter()
        presenter.bind(mockView)
    }

    @After
    fun afterTests() {
        presenter.unbind(mockView)
    }

    @Test
    fun loadTrack_does_not_hide_rows() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, never()).hideTimeBasedRows()
        verify(mockView, never()).hideAltitudeBasedRows()
        verify(mockView, never()).hideNotes()
    }

    @Test
    fun loadTrack_hides_altitude_rows() {
        // arrange
        setupTrack(false, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, never()).hideTimeBasedRows()
        verify(mockView, times(1)).hideAltitudeBasedRows()
        verify(mockView, never()).hideNotes()
    }

    @Test
    fun loadTrack_hides_timestamp_rows() {
        // arrange
        setupTrack(true, false)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).hideTimeBasedRows()
        verify(mockView, never()).hideAltitudeBasedRows()
        verify(mockView, never()).hideNotes()
    }

    @Test
    fun loadTrack_hides_notes_rows() {
        // arrange
        setupTrack(true, true)
        setupStatistics()
        track.notes = ""

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, never()).hideTimeBasedRows()
        verify(mockView, never()).hideAltitudeBasedRows()
        verify(mockView, times(1)).hideNotes()
    }

    @Test
    fun loadTrack_hides_segments_rows() {
        // arrange
        setupTrack(true, true)
        setupStatistics()
        track.resetPositions()      // so there are no segments

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).hideSegmentsRow()
    }

    @Test
    fun loadTrack_sets_title() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setTrackTitle(TRACK_NAME)
    }

    @Test
    fun loadTrack_sets_distance() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setDistance("1.23", "DISTANCE_LABEL_METRIC")
    }

    @Test
    fun loadTrack_sets_recordedTime() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setRecordedTime("02:15:23")
    }

    @Test
    fun loadTrack_sets_movingTime() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setMovingTime("02:15:12")
    }

    @Test
    fun loadTrack_sets_elevationValues() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setElevationValues("778", "889", "2", "333", "331", "ALTITUDE_LABEL_METRIC")
    }

    @Test
    fun loadTrack_sets_speedValues() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setSpeedValues("3.60", "7.20", "SPEED_LABEL_METRIC")
    }

    @Test
    fun loadTrack_sets_paceValues() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setPaceValues("16:40", "8:20", "PACE_LABEL_METRIC")
    }

    @Test
    fun loadTrack_sets_elevationChartPositions() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setElevationByTimeChartPositions(track.currentPositionsSnapshot)
        verify(mockView, never()).hideTimeBasedGraph()
        verify(mockView, times(1)).setElevationByDistanceChartPositions(track.currentPositionsSnapshot)
    }

    @Test
    fun loadTrack_sets_elevationChartPositions_multiday_segmented() {
        // arrange
        setupMultidaySegmentedTrack(true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, never()).setElevationByTimeChartPositions(MockitoHelper.anyObject())
        verify(mockView, times(1)).hideTimeBasedGraph()
        verify(mockView, times(1)).setElevationByDistanceChartPositions(track.currentPositionsSnapshot)
    }
    @Test
    fun loadTrack_sets_notes() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setNotes(TRACK_NOTES)
    }

    @Test
    fun loadTrack_sets_segment_text() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID)

        // assert
        verify(mockView, times(1)).setSegmentsText("0, Thu 1, Jan, 1970 12:00:00 - 12:00:00, 00:00:00")
    }

    @Test
    fun loadTrack_whenTrackNotFound_setsTitleToTrackNotFound() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID + 1)

        // assert
        verify(mockView, times(1)).setTrackTitle(TRACK_LOAD_ERROR)
    }

    @Test
    fun loadTrack_whenTrackNotFound_logs() {
        // arrange
        setupTrack(true, true)
        setupStatistics()

        // act
        presenter.loadTrack(TRACK_ID + 1)

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect",true, exception is TrackLoadException)
        assertEquals("inner exception type incorrect",true, exception.cause is NullPointerException)
    }

}