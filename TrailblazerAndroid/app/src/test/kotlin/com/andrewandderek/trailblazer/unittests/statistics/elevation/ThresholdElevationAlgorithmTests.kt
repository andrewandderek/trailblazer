/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics.elevation

import com.andrewandderek.trailblazer.statistics.elevation.ElevationGainAndLoss
import com.andrewandderek.trailblazer.statistics.elevation.ThresholdElevationAlgorithm
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ThresholdElevationAlgorithmTests : ElevationAlgorithmTestBase() {

    private lateinit var elevationAlgorithm: ThresholdElevationAlgorithm

    @Before
    fun setupTest() {
        `when`(mockElevationSettings.elevationThreshold)
                .thenReturn(3.0)

        elevationAlgorithm = ThresholdElevationAlgorithm(mockAltitudeProcessor, mockElevationSettings)
    }

    @Test
    fun reset_resetsProcessor() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5))

        // act
        elevationAlgorithm.reset(positions[0])

        // assert
        verify(mockAltitudeProcessor).reset(positions[0])
    }

    @Test
    fun process_sendsPositionToProcessor() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.5))
        elevationAlgorithm.reset(positions[0])

        // act
        elevationAlgorithm.process(positions[1])

        // assert
        verify(mockAltitudeProcessor).reset(positions[0])
        verify(mockAltitudeProcessor).process(positions[1])
    }

    @Test
    fun process_secondPosition_smallerGainThanThreshold_returnsZeroGainAndLoss() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 12.9))
        elevationAlgorithm.reset(positions[0])

        // act
        val result = elevationAlgorithm.process(positions[1])

        // assert
        assertEquals(0.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_secondPosition_smallerLossThanThreshold_returnsZeroGainAndLoss() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 7.1))
        elevationAlgorithm.reset(positions[0])

        // act
        val result = elevationAlgorithm.process(positions[1])

        // assert
        assertEquals(0.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_secondPosition_largerGainThanThreshold_returnsTheGainAndZeroLoss() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 13.1))
        elevationAlgorithm.reset(positions[0])

        // act
        val result = elevationAlgorithm.process(positions[1])

        // assert
        assertEquals(3.1, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_secondPosition_largerLossThanThreshold_returnsTheLossAndZeroGain() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 6.9))
        elevationAlgorithm.reset(positions[0])

        // act
        val result = elevationAlgorithm.process(positions[1])

        // assert
        assertEquals(0.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(3.1, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_largerGainThanThreshold_resetsLastValidPosition() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 13.1, 16.0))
        elevationAlgorithm.reset(positions[0])
        elevationAlgorithm.process(positions[1])

        // act
        val result = elevationAlgorithm.process(positions[2])

        // assert
        assertEquals(0.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_largerLossThanThreshold_resetsLastValidPosition() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 6.9, 16.9))
        elevationAlgorithm.reset(positions[0])
        elevationAlgorithm.process(positions[1])

        // act
        val result = elevationAlgorithm.process(positions[2])

        // assert
        assertEquals(10.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_multipleSmallMovementsStayingWithinThreshold_doNotChangeLastValidPosition() {
        // arrange
        val positions = createPositions(doubleArrayOf(10.0, 9.0, 11.0, 8.1, 7.1, 7.5, 15.0))
        elevationAlgorithm.reset(positions[0])
        elevationAlgorithm.process(positions[1])
        elevationAlgorithm.process(positions[2])
        elevationAlgorithm.process(positions[3])
        elevationAlgorithm.process(positions[4])
        elevationAlgorithm.process(positions[5])

        // act
        val result = elevationAlgorithm.process(positions[6])

        // assert
        assertEquals(5.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_multipleMovements_mostlyUp_onlyAddsGainsAndLossesLargerThanThreshold() {
        // arrange
        // These are from the example at http://www.gpsvisualizer.com/tutorials/elevation_gain.html
        val positions = createPositions(doubleArrayOf(5.0, 10.0, 12.0, 8.0, 13.0, 11.0, 14.0))
        elevationAlgorithm.reset(positions[0])

        // act
        val gainsAndLosses = ArrayList<ElevationGainAndLoss>()
        gainsAndLosses.add(elevationAlgorithm.process(positions[1]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[2]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[3]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[4]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[5]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[6]))
        val totalGain = gainsAndLosses.sumOf { item -> item.gain }
        val totalLoss = gainsAndLosses.sumOf { item -> item.loss }

        // assert
        assertEquals(9.0, totalGain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, totalLoss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_multipleMovements_mostlyDown_onlyAddsGainsAndLossesLargerThanThreshold() {
        // arrange
        val positions = createPositions(doubleArrayOf(15.0, 11.0, 13.0, 8.0, 12.0, 10.0, 5.0))
        elevationAlgorithm.reset(positions[0])

        // act
        val gainsAndLosses = ArrayList<ElevationGainAndLoss>()
        gainsAndLosses.add(elevationAlgorithm.process(positions[1]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[2]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[3]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[4]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[5]))
        gainsAndLosses.add(elevationAlgorithm.process(positions[6]))
        val totalGain = gainsAndLosses.sumOf { item -> item.gain }
        val totalLoss = gainsAndLosses.sumOf { item -> item.loss }

        // assert
        assertEquals(0.0, totalGain, TEST_ELEVATION_DELTA)
        assertEquals(10.0, totalLoss, TEST_ELEVATION_DELTA)
    }

    @Test
    fun process_usesProcessedAltitudeIfPresentToCalculateGainsAndLosses() {
        // arrange
        setupFakeProcessor()
        val positions = createPositions(doubleArrayOf(10.0, 20.0))
        elevationAlgorithm.reset(positions[0])

        // act
        val result = elevationAlgorithm.process(positions[1])

        // assert
        assertEquals(5.0, result.gain, TEST_ELEVATION_DELTA)
        assertEquals(0.0, result.loss, TEST_ELEVATION_DELTA)
    }
}