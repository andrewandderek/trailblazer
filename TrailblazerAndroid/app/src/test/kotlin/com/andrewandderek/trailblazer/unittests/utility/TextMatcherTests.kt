/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.utility

import com.andrewandderek.trailblazer.model.TextSpan
import com.andrewandderek.trailblazer.utility.TextMatcher
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class TextMatcherTests {

    private lateinit var textMatcher: TextMatcher

    @Before
    fun before_each_test() {
        textMatcher = TextMatcher()
    }

    @Test
    fun isMatch_non_matching_name() {
        assertFalse(textMatcher.isMatch("Test track", "blah"))
    }

    @Test
    fun isMatch_matches_empty_string() {
        assertTrue(textMatcher.isMatch("Test track", ""))
    }

    @Test
    fun isMatch_exact_match_name() {
        assertTrue(textMatcher.isMatch("Test track", "Trac"))
    }

    @Test
    fun isMatch_case_insensitive_match_name() {
        assertTrue(textMatcher.isMatch("Test track", "track"))
    }

    @Test
    fun getMatches_returns_empty_list_if_no_match() {
        // act
        val result = textMatcher.getMatches("some text", "blah")

        // assert
        assertEquals(emptyList<TextSpan>(), result)
    }

    @Test
    fun getMatches_returns_empty_list_if_empty_search_text() {
        // act
        val result = textMatcher.getMatches("some text", "")

        // assert
        assertEquals(emptyList<TextSpan>(), result)
    }

    @Test
    fun getMatches_returns_single_match() {
        // act
        val result = textMatcher.getMatches("some Text here", "text")

        // assert
        assertEquals(listOf(TextSpan(5, 4)), result)
    }

    @Test
    fun getMatches_returns_all_matches() {
        // act
        val result = textMatcher.getMatches(" Blah blah and more BLAH.", "blah")

        // assert
        val expectedMatches = listOf(TextSpan(1, 4), TextSpan(6, 4), TextSpan(20, 4))
        assertEquals(expectedMatches, result)
    }
}