/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.chart

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.chart.ElevationChartDataMapper
import com.andrewandderek.trailblazer.ui.chart.ILineDataSetFactory
import com.andrewandderek.trailblazer.ui.chart.IPositionToEntryMapper
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.AdditionalMatchers.or
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

class ElevationChartDataMapperTests : MockingAnnotationSetup() {

    @Mock
    private lateinit var mockLineDataSetFactory: ILineDataSetFactory

    @Mock
    private lateinit var mockPositionToEntryMapper: IPositionToEntryMapper

    private lateinit var mapper: ElevationChartDataMapper

    @Before
    fun setupTests() {
        setupLineDataSetFactory()
        setupPositionToEntryMapper()

        mapper = ElevationChartDataMapper(mockLineDataSetFactory, mockPositionToEntryMapper)
    }

    private fun setupLineDataSetFactory() {
        `when`(mockLineDataSetFactory.createLineDataSet(or(eq(0L), eq(1L)), MockitoHelper.anyObject()))
                .then { invocation ->
                    createMockLineDataSet(invocation.getArgument(1))
                }
    }

    private fun setupPositionToEntryMapper() {
        `when`(mockPositionToEntryMapper.mapToEntry(MockitoHelper.anyObject(), MockitoHelper.anyObject()))
                .then { invocation ->
                    createEntry(invocation.getArgument(0))
                }
    }

    private fun createMockLineDataSet(entries: List<Entry>) : ILineDataSet {
        val lineDataSet = mock(ILineDataSet::class.java)
        `when`(lineDataSet.getEntryForIndex(anyInt()))
                .then { invocation -> entries[invocation.getArgument(0)] }
        `when`(lineDataSet.entryCount)
                .thenReturn(entries.size)
        return lineDataSet
    }

    private fun createEntry(position: Position): Entry {
        return Entry(0.0F, position.altitude!!.toFloat())
    }

    private fun createPositionsWithoutPause(): ArrayList<Position> {
        val positions = ArrayList<Position>()
        positions.add(Position(1.0, 2.0, 3.0, 4))
        positions.add(Position(5.0, 6.0, 7.0, 8))
        return positions
    }

    private fun createPositionsWithPause(): ArrayList<Position> {
        val positions = createPositionsWithoutPause()
        positions.add(Position(9.0, 10.0, 11.0, 12, 0F, "", 1))
        positions.add(Position(13.0, 14.0, 15.0, 16, 0F, "", 1))

        return positions
    }

    @Test
    fun mapToLineData_emptyTrack_returnsNullData() {
        // arrange
        val positions = ArrayList<Position>()

        // act
        val lineData = mapper.mapToLineData(positions)

        // assert
        assertNull(lineData)
    }

    @Test
    fun mapToLineData_trackWithoutPauses_returnsSingleDataSetWithMappedEntries() {
        // arrange
        val positions = createPositionsWithoutPause()

        // act
        val lineData = mapper.mapToLineData(positions)

        // assert
        assertNotNull(lineData)
        assertEquals(1, lineData!!.dataSetCount)
        val lineDataSet = lineData.getDataSetByIndex(0)
        assertEquals(2, lineDataSet.entryCount)
        assertEquals(3.0F, lineDataSet.getEntryForIndex(0).y)
        assertEquals(7.0F, lineDataSet.getEntryForIndex(1).y)
    }

    @Test
    fun mapToLineData_trackWithPauses_returnsDataSetPerSegmentWithMappedEntries() {
        // arrange
        val positions = createPositionsWithPause()

        // act
        val lineData = mapper.mapToLineData(positions)

        // assert
        assertNotNull(lineData)
        assertEquals(2, lineData!!.dataSetCount)
        val lineDataSet1 = lineData.getDataSetByIndex(0)
        assertEquals(2, lineDataSet1.entryCount)
        assertEquals(3.0F, lineDataSet1.getEntryForIndex(0).y)
        assertEquals(7.0F, lineDataSet1.getEntryForIndex(1).y)
        val lineDataSet2 = lineData.getDataSetByIndex(1)
        assertEquals(2, lineDataSet2.entryCount)
        assertEquals(11.0F, lineDataSet2.getEntryForIndex(0).y)
        assertEquals(15.0F, lineDataSet2.getEntryForIndex(1).y)
    }
}
