/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.receiver.trackrecordingcontrols

import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class TrackRecordingControlsSetup : MockingAnnotationSetup() {
    protected lateinit var controls: TrackRecordingControls

    protected lateinit var mockLoggerFactory: ILoggerFactory

    @Mock protected lateinit var mockAnalyticsEngine: IAnalyticsEngine
    @Mock protected lateinit var mockServiceController: IGpsLoggerServiceController
    @Mock protected lateinit var mockservice: IGpsLoggerService
    @Mock protected lateinit var mockbinder: GpsLoggerService.LocalBinder

    protected fun setupTrackRecordingControls() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()

        controls = TrackRecordingControls(mockLoggerFactory, mockAnalyticsEngine, mockServiceController)

        `when`(mockbinder.service).thenReturn(mockservice)
    }
}