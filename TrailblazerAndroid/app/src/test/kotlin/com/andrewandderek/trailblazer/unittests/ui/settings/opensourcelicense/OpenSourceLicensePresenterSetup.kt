/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.settings.opensourcelicense

import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.ui.settings.IOpenSourceLicensesView
import com.andrewandderek.trailblazer.ui.settings.OpenSourceLicensesPresenter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class OpenSourceLicensePresenterSetup : MockingAnnotationSetup() {
    protected lateinit var presenter: OpenSourceLicensesPresenter

    private lateinit var mockLoggerFactory: ILoggerFactory

    @Mock protected lateinit var mockFileSystemHelper: IFileSystemHelper
    @Mock protected lateinit var mockview: IOpenSourceLicensesView

    private var fileList: MutableList<String> = mutableListOf("TEST_FILE")
    private var fileContents: String = "title\nline2\n"

    protected fun setupPresenter() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()

        presenter = OpenSourceLicensesPresenter(mockLoggerFactory, mockFileSystemHelper)

        `when`(mockFileSystemHelper.getFolderFiles(MockitoHelper.anyObject())).thenReturn(fileList)
        `when`(mockFileSystemHelper.getFileContents("TEST_FILE", true)).thenReturn(fileContents)
    }
}