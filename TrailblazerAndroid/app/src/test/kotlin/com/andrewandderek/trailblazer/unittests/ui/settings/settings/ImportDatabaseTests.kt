/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.settings.settings

import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class ImportDatabaseTests : SettingsFragmentViewModelSetup() {
    @Captor
    private lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    @Before
    fun before_each_test() {
        setupViewModel()
        setupEventSubscriber()
    }

    @Test
    fun import_starts_progress() {
        // arrange

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockStartProgressObserver, times(1)).onChanged("IMPORT_PROGRESS")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockStartProgressObserver);
    }

    @Test
    fun import_copies_from_resolver_to_import_folder() {
        // arrange

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockFileSystemHelper, times(1)).copyFileFromResolver(mockUri, "IMPORT_PATHNAME")
    }

    @Test
    fun import_copies_from_import_folder_to_active() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenReturn(true)

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockDatabaseCopier, times(1)).copyDb("IMPORT_PATHNAME", "ACTIVE_PATHNAME", true)
    }

    @Test
    fun import_first_copy_fails_aborts() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenReturn(false)

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockDatabaseCopier, never()).copyDb(anyString(), anyString(), anyBoolean())
        verify(mockEndProgressObserver, times(1)).onChanged("COPY_ERROR")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun import_second_copy_fails_aborts() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenReturn(true)
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenReturn(false)

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockEndProgressObserver, times(1)).onChanged("COPY_ERROR")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun import_succeeds_ends_progress() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenReturn(true)
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenReturn(true)

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockEndProgressObserver, times(1)).onChanged("IMPORT_OK")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun import_succeeds_loads_metadata() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenReturn(true)
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenReturn(true)

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockResetMetadataSummarySuffixObserver, times(1)).onChanged("TEST_SUFFIX_0")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockResetMetadataSummarySuffixObserver);
    }

    @Test
    fun exception_thrown_progress_is_cancelled() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenThrow(NullPointerException("UNIT TESTS"))

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockEndProgressObserver, times(1)).onChanged("COPY_ERROR")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun exception_thrown_is_logged() {
        // arrange
        `when`(mockFileSystemHelper.copyFileFromResolver(anyObject(), anyString())).thenThrow(NullPointerException("UNIT TESTS"))

        // act
        viewModel.importDatabase(mockUri)

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect", true, exception is NullPointerException)
    }
}
