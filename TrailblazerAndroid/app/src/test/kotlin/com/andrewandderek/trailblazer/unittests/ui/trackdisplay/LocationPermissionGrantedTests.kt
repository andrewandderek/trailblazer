/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.times

class LocationPermissionGrantedTests : TrackDisplayPresenterSetup() {
    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun location_permission_granted_restarts_current_position_updates() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(true)
        presenter.connectService(mockbinder)

        // act
        presenter.locationPermissionGranted()

        // assert
        Mockito.verify(mockservice, times(1)).restartCurrentPositionUpdates()
    }

    @Test
    fun location_permission_granted_but_permission_checker_says_no() {
        // arrange
        Mockito.`when`(mockPermissionsChecker.hasLocationPermission()).thenReturn(false)
        presenter.connectService(mockbinder)

        // act
        presenter.locationPermissionGranted()

        // assert
        // This really shouldn't happen - we're told the user has granted permission but the checker
        // contradicts that - but it's a belt-and-braces check
        Mockito.verify(mockservice, never()).restartCurrentPositionUpdates()
    }

}