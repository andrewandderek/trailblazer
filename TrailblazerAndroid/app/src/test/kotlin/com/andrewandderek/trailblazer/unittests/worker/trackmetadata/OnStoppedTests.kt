/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.worker.trackmetadata

import com.andrewandderek.trailblazer.exception.WorkerException
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class OnStoppedTests : WorkerSetup() {
    @Before
    fun before_each_test() {
        setupWorker()
    }

    @Test
    fun stopping_logs() {
        // arrange

        // act
        worker.onStopped()

        // assert
        verify(mockCrashReporter, Mockito.times(1)).logNonFatalException(MockitoHelper.capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect", true, exception is WorkerException)
        verifyNoMoreInteractions(mockCrashReporter)
    }

}