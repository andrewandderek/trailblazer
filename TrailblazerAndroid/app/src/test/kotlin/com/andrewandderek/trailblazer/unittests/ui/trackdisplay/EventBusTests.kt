/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.event.TrackRenamedEvent
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class EventBusTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()

        presenter.bind(mockview, null)
        presenter.start()
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun unknown_event_is_ignored() {
        // arrange

        // act
        eventBus.publish("UNKNOWN")

        // assert
        verify(mockview, never()).updateAvailableActions()
        verify(mockview, never()).setTitle(anyString())
    }

    @Test
    fun status_change_updates_ui() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        reset(mockview)

        // act
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, DISPLAY_TRACK_ID, false))

        // assert
        verify(mockview, times(1)).updateAvailableActions()
        verify(mockview, times(1)).setTitle("TEST TRACK")
    }

    @Test
    fun status_change_triggers_rename() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        reset(mockview)

        // act
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, DISPLAY_TRACK_ID, false))

        // assert
        verify(mockview, times(1)).promptForTrackNameAfterStopping(DISPLAY_TRACK_ID.toString(),"TEST TRACK")
    }

    @Test
    fun status_change_from_notification_updates_ui() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        reset(mockview)

        // act
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, DISPLAY_TRACK_ID, true))

        // assert
        verify(mockview, times(1)).updateAvailableActions()
        verify(mockview, times(1)).setTitle("TEST TRACK")
    }

    @Test
    fun status_change_from_notification_does_not_triggers_rename() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        reset(mockview)

        // act
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, DISPLAY_TRACK_ID, true))

        // assert
        verify(mockview, never()).promptForTrackNameAfterStopping(anyString(), anyString())
    }

    @Test
    fun rename_updates_ui() {
        // arrange

        // act
        eventBus.publish(TrackRenamedEvent(1L, "old", "new"))

        // assert
        verify(mockview, times(1)).setTitle("TEST_APP_NAME")
    }

}