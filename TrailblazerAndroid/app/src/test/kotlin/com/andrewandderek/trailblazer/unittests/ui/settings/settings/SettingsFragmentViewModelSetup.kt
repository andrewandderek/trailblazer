/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.settings.settings

import android.app.Application
import android.content.Intent
import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.utility.IDatabaseConfigProvider
import com.andrewandderek.trailblazer.data.utility.IDatabaseCopier
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.RxEventBus
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.helpers.UserSettingsHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.settings.IThemeSetter
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.ui.settings.SettingsFragmentViewModel
import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.worker.trackmetadata.ITrackMetadataScheduler
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.anyString
import org.mockito.Mockito.eq
import org.mockito.Mockito.`when`

open class SettingsFragmentViewModelSetup : MockingAnnotationSetup() {
    protected lateinit var viewModel: SettingsFragmentViewModel

    private lateinit var mockLoggerFactory: ILoggerFactory
    private lateinit var mockSchedulerProvider: ISchedulerProvider
    protected lateinit var eventBus: IEventBus
    protected var tracksUpdatedEventSubscriber: IEventBusSubscription? = null
    protected var publishedTracksUpdatedEvent: TrackMetadataUpdatedEvent? = null
    private lateinit var userSettings: IUserSettings

    @Mock protected lateinit var mockApplication: Application
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockTrackMetadataRepository: ITrackMetadataRepository
    @Mock protected lateinit var mockDatabaseConfigProvider: IDatabaseConfigProvider
    @Mock protected lateinit var mockDatabaseCopier: IDatabaseCopier
    @Mock protected lateinit var mockThemeSetter: IThemeSetter
    @Mock protected lateinit var mockMetadataScheduler: ITrackMetadataScheduler
    @Mock protected lateinit var mockSharer: IIntentSharer
    @Mock protected lateinit var mockIntent: Intent
    @Mock protected lateinit var mockServiceController: IGpsLoggerServiceController
    @Mock protected lateinit var mockFileSystemHelper: IFileSystemHelper
    @Mock protected lateinit var mockservice: IGpsLoggerService
    @Mock protected lateinit var mockbinder: GpsLoggerService.LocalBinder
    @Mock protected lateinit var mockUri: Uri

    // Rule for help testing livedata
    @Rule @JvmField public var rule = InstantTaskExecutorRule()
    @Mock protected lateinit var mockMessageObserver: Observer<String>
    @Mock protected lateinit var mockResetMetadataSummarySuffixObserver: Observer<String>
    @Mock protected lateinit var mockStartProgressObserver: Observer<String>
    @Mock protected lateinit var mockEndProgressObserver: Observer<String>
    @Mock protected lateinit var mockDisplayChooserObserver: Observer<Pair<String, Intent>>
    @Mock protected lateinit var mockTrackRecordingObserver: Observer<Boolean>

    protected lateinit var trackMetadataList: MutableList<TrackMetadata>

    protected fun setupViewModel() {
        trackMetadataList = ArrayList(10)

        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockSchedulerProvider = RxHelper.setupSchedulerProvider()
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())

        eventBus = RxEventBus(
            mockLoggerFactory,
            mockCrashReporter,
            mockSchedulerProvider
        )

        viewModel = SettingsFragmentViewModel(
            mockApplication,
            mockLoggerFactory,
            mockResourceProvider,
            mockCrashReporter,
            mockSchedulerProvider,
            mockTrackMetadataRepository,
            mockDatabaseConfigProvider,
            mockDatabaseCopier,
            userSettings,
            mockThemeSetter,
            eventBus,
            mockMetadataScheduler,
            mockSharer,
            mockServiceController,
            mockFileSystemHelper,
        )

        `when`(mockbinder.service).thenReturn(mockservice)
        `when`(mockDatabaseConfigProvider.getActiveApplicationDbPathname()).thenReturn("ACTIVE_PATHNAME")
        `when`(mockDatabaseConfigProvider.getExportDbPathname(anyBoolean())).thenReturn("EXPORT_PATHNAME")
        `when`(mockDatabaseConfigProvider.getImportDbPathname(anyBoolean())).thenReturn("IMPORT_PATHNAME")
        `when`(mockSharer.getShareIntentWithSingleFile(anyString(), anyString())).thenReturn(mockIntent)

        viewModel.observables.message.observeForever(mockMessageObserver)
        viewModel.observables.resetMetadataSummarySuffix.observeForever(mockResetMetadataSummarySuffixObserver)
        viewModel.observables.startProgress.observeForever(mockStartProgressObserver)
        viewModel.observables.endProgress.observeForever(mockEndProgressObserver)
        viewModel.observables.displayChooser.observeForever(mockDisplayChooserObserver)
        viewModel.observables.trackRecording.observeForever(mockTrackRecordingObserver)

        `when`(mockResourceProvider.getString(eq(R.string.settings_reset_metadata_summary_suffix), anyString())).thenAnswer( {
            invocation ->
            val args = invocation.arguments
            var numberStr = args[1] as String
            "TEST_SUFFIX_$numberStr"
        })

        `when`(mockResourceProvider.getString(R.string.settings_exportdb_progress)).thenReturn("EXPORT_PROGRESS")
        `when`(mockResourceProvider.getString(R.string.settings_exportdb_copy_ok)).thenReturn("EXPORT_OK")
        `when`(mockResourceProvider.getString(R.string.settings_exportdb_chooser_title)).thenReturn("EXPORT_CHOOSER")
        `when`(mockResourceProvider.getString(R.string.settings_copydb_error)).thenReturn("COPY_ERROR")

        `when`(mockResourceProvider.getString(R.string.settings_copydb_progress)).thenReturn("IMPORT_PROGRESS")
        `when`(mockResourceProvider.getString(R.string.settings_copydb_import_ok)).thenReturn("IMPORT_OK")
    }

    protected fun setupEventSubscriber() {
        // we dont mock the event bus because it will run quite happily in the unit tests so lets test as much as we can
        publishedTracksUpdatedEvent = null   // reset the event we captured
        tracksUpdatedEventSubscriber = eventBus.subscribe(TrackMetadataUpdatedEvent::class.java)
        { event ->
            publishedTracksUpdatedEvent = event
        }
    }

    // metadata track ids are 1..n
    protected fun setupTrackMetadataRepo(numberOfMetadataItems: Int) {
        trackMetadataList = ArrayList(10)
        for (index in 1.. numberOfMetadataItems) {
            val metadata = TrackHelper.setupTrackMetadata(index)
            trackMetadataList.add(metadata)
        }
        `when`(mockTrackMetadataRepository.getAll()).thenReturn(trackMetadataList)
    }
}