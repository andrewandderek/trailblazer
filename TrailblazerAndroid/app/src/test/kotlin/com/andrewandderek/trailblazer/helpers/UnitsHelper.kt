/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.units.conversion.AltitudeConverter
import com.andrewandderek.trailblazer.units.conversion.DistanceConverter
import com.andrewandderek.trailblazer.units.conversion.PaceConverter
import com.andrewandderek.trailblazer.units.conversion.SpeedConverter
import com.andrewandderek.trailblazer.units.formatting.*
import com.andrewandderek.trailblazer.utility.IResourceProvider
import org.mockito.Mockito

object UnitsHelper {
    fun setupFormatStrings(): IResourceProvider {
        var mockResources: IResourceProvider = Mockito.mock(IResourceProvider::class.java)

        // make sure these match the ones in the production code or the tests lose meaning
        Mockito.`when`(mockResources.getString(R.string.altitude_fmt)).thenReturn("%1\$.0f%2\$s")
        Mockito.`when`(mockResources.getString(R.string.distance_fmt)).thenReturn("%1\$.2f%2\$s")
        Mockito.`when`(mockResources.getString(R.string.pace_fmt)).thenReturn("%1\$d:%2\$02d%3\$s")
        Mockito.`when`(mockResources.getString(R.string.speed_fmt)).thenReturn("%1\$.2f%2\$s")

        return mockResources
    }

    /**
     * setup a real formatter - backed by a real UserSettings - backed by mock resources and preferences
     */
    fun setupAltitudeFormatter(units: String): IAltitudeFormatter {
        return AltitudeFormatter(setupFormatStrings(), UserSettingsHelper.setupUserSettings(units), AltitudeConverter())
    }

    /**
     * setup a real formatter - backed by a real UserSettings - backed by mock resources and preferences
     */
    fun setupDistanceFormatter(units: String): IDistanceFormatter {
        return DistanceFormatter(setupFormatStrings(), UserSettingsHelper.setupUserSettings(units), DistanceConverter())
    }

    /**
     * setup a real formatter - backed by a real UserSettings - backed by mock resources and preferences
     */
    fun setupPaceFormatter(units: String): IPaceFormatter {
        return PaceFormatter(setupFormatStrings(), UserSettingsHelper.setupUserSettings(units), PaceConverter())
    }

    /**
     * setup a real formatter - backed by a real UserSettings - backed by mock resources and preferences
     */
    fun setupSpeedFormatter(units: String): ISpeedFormatter {
        return SpeedFormatter(setupFormatStrings(), UserSettingsHelper.setupUserSettings(units), SpeedConverter())
    }
}