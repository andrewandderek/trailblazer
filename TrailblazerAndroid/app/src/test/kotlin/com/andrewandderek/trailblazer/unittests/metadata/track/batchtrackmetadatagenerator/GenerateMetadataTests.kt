/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.metadata.track.batchtrackmetadatagenerator

import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import com.andrewandderek.trailblazer.metadata.track.BatchTrackMetadataGenerator
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.unittests.metadata.track.GeneratorSetup
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyFloat
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.anyLong
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class GenerateMetadataTests : GeneratorSetup() {

    companion object {
        private const val NUMBER_OF_TRACKS_TO_PROCESS = 5
    }

    @Before
    fun before_each_test() {
        setupGenerator()
        setupEventSubscriber()
    }

    private fun verifyTracksLoaded(idRange: LongRange) {
        for (id in idRange) {
            verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(id, Position.ACCURACY_THRESHOLD, mockPositionRepository)
        }
    }

    @Test
    fun empty_does_nothing() {
        // arrange

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(0, processItems)
        assertNull(publishedTracksUpdatedEvent)
        // the generator never gets a track to work on
        verify(mockTrackRepository, never()).getWithPositionsAndAccuracy(anyLong(), anyFloat(), anyObject())
    }

    @Test
    fun load_track_error_logs() {
        // arrange
        setupTrackRepo(1)
        // make it look like the track cannot be loaded
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(1, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(null)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(1, processItems)
        verify(mockTrackMetadataRepository, never()).update(anyObject())
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect", true, exception is TrackLoadException)
    }

    @Test
    fun lots_of_tracks_without_metadata_get_generated() {
        // arrange
        setupTrackRepo(10)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(5, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(1L,2L,3L,4L,5L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verifyTracksLoaded(1L..5L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(5)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

    @Test
    fun some_tracks_without_metadata_get_generated() {
        // arrange
        setupTrackRepo(3)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(3, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(1L,2L,3L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verify(mockTrackMetadataRepository, times(1)).getWhereVersionCodeBefore(BatchTrackMetadataGenerator.LAST_VERSION_CODE_WITH_TRUSTED_METADATA)
        verifyTracksLoaded(1L..3L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(3)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

    @Test
    fun lots_of_tracks_with_old_metadata_get_generated() {
        // arrange
        setupTrackMetadataRepo(10)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(5, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(11L,12L,13L,14L,15L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verify(mockTrackMetadataRepository, times(1)).getWhereVersionCodeBefore(BatchTrackMetadataGenerator.LAST_VERSION_CODE_WITH_TRUSTED_METADATA)
        verifyTracksLoaded(11L..15L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(5)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

    @Test
    fun some_of_tracks_with_old_metadata_get_generated() {
        // arrange
        setupTrackMetadataRepo(3)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(3, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(11L,12L,13L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verify(mockTrackMetadataRepository, times(1)).getWhereVersionCodeBefore(BatchTrackMetadataGenerator.LAST_VERSION_CODE_WITH_TRUSTED_METADATA)
        verifyTracksLoaded(11L..13L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(3)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

    @Test
    fun tracks_with_old_metadata_and_no_metadata_1_get_generated() {
        // arrange
        setupTrackRepo(10)
        setupTrackMetadataRepo(10)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(5, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(1L,2L,3L,4L,5L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verify(mockTrackMetadataRepository, never()).getWhereVersionCodeBefore(anyInt())
        verifyTracksLoaded(1L..5L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(5)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

    @Test
    fun tracks_with_old_metadata_and_no_metadata_2_get_generated() {
        // arrange
        setupTrackRepo(2)
        setupTrackMetadataRepo(10)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(5, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(1L,2L,11L,12L,13L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verify(mockTrackMetadataRepository, times(1)).getWhereVersionCodeBefore(BatchTrackMetadataGenerator.LAST_VERSION_CODE_WITH_TRUSTED_METADATA)
        verifyTracksLoaded(1L..2L)      // the generator loads a track to work on it
        verifyTracksLoaded(11L..13L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(5)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

    @Test
    fun tracks_with_old_metadata_and_no_metadata_3_get_generated() {
        // arrange
        setupTrackRepo(2)
        setupTrackMetadataRepo(2)

        // act
        var processItems = batchGenerator.generateMetadata(NUMBER_OF_TRACKS_TO_PROCESS)

        // assert
        assertEquals(4, processItems)
        assertNotNull(publishedTracksUpdatedEvent)
        assertEquals(listOf(1L,2L,11L,12L), publishedTracksUpdatedEvent!!.trackIds)
        verify(mockTrackRepository, times(1)).getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        verify(mockTrackMetadataRepository, times(1)).getWhereVersionCodeBefore(BatchTrackMetadataGenerator.LAST_VERSION_CODE_WITH_TRUSTED_METADATA)
        verifyTracksLoaded(1L..2L)      // the generator loads a track to work on it
        verifyTracksLoaded(11L..12L)      // the generator loads a track to work on it
        verifyNoMoreInteractions(mockTrackRepository)
        verify(mockTrackMetadataRepository, times(4)).update(anyObject())
        verifyNoMoreInteractions(mockTrackMetadataRepository)
    }

}