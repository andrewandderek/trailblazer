/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

class LoadTracksTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun trackList_loading_sets_progress() {
        // arrange
        setupTrackListItems(2)

        // act
        presenter.loadTracks()

        // assert
        verify(mockview, Mockito.times(1)).startProgress()
        verify(mockview, Mockito.times(1)).completeProgress()
        verify(mockview, Mockito.times(1)).setTitle("TEST_TITLE 2")
    }

    @Test
    fun trackList_loading_sets_multiselect() {
        // arrange
        setupTrackListItems(2)

        // act
        presenter.loadTracks()

        // assert
        Assert.assertEquals("multiselect mode incorrectly set",false, presenter.inMultiSelectMode)
    }

    @Test
    fun trackList_loading_fails_is_handled() {
        // arrange
        setupTrackListItems(2)
        `when`(mockTrackRepository.getTrackListWithoutAnyPositions()).thenReturn(null)

        // act
        presenter.loadTracks()

        // assert
        verify(mockview, Mockito.times(1)).startProgress()
        verify(mockview, Mockito.times(1)).completeProgress()
        verify(mockview, Mockito.times(1)).showMessage(R.string.load_track_error)
    }

}