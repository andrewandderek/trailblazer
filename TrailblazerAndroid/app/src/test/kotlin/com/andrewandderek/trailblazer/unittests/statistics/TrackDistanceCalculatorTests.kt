/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.statistics.TrackDistanceCalculator
import com.andrewandderek.trailblazer.statistics.IDistanceProvider
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito


class TrackDistanceCalculatorTests {
    private val TEST_DISTANCE_DELTA = 0.001

    private lateinit var mockDistanceProvider: IDistanceProvider

    private lateinit var calculator: TrackDistanceCalculator

    @Before
    fun setupTest() {
        mockDistanceProvider = Mockito.mock(IDistanceProvider::class.java)

        Mockito.`when`(mockDistanceProvider.getDistanceInMetres(1.1, 2.2, 3.3, 4.4))
                .thenReturn(111.1)

        Mockito.`when`(mockDistanceProvider.getDistanceInMetres(3.3, 4.4, 5.5, 6.6))
                .thenReturn(222.2)

        Mockito.`when`(mockDistanceProvider.getDistanceInMetres(5.5, 6.6, 7.7, 8.8))
                .thenReturn(444.4)

        calculator = TrackDistanceCalculator(mockDistanceProvider)
    }

    private fun createPosition(latitude: Double, longitude: Double): Position {
        return createPositionWithSegment(latitude, longitude, 0)
    }

    private fun createPositionWithSegment(latitude: Double, longitude: Double, segment: Long): Position {
        val position = Position(latitude, longitude, 0.0, 0)
        position.segment = segment

        return position
    }

    private fun createTrack(): Track {
        val track = Track()
        track.addNewPosition(createPosition(1.1, 2.2))
        track.addNewPosition(createPosition(3.3, 4.4))
        track.addNewPosition(createPosition(5.5, 6.6))
        track.addNewPosition(createPosition(7.7, 8.8))
        return track
    }

    private fun createTrackWithPause(): Track {
        val track = Track()
        track.addNewPosition(createPositionWithSegment(1.1, 2.2, 0))
        track.addNewPosition(createPositionWithSegment(3.3, 4.4, 0))
        track.addNewPosition(createPositionWithSegment(5.5, 6.6, 1))
        track.addNewPosition(createPositionWithSegment(7.7, 8.8, 1))
        return track
    }

    @Test
    fun addPosition_forFirstPosition_shouldNotChangeDistance() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)

        // assert
        Assert.assertEquals(0.0, statistics.distanceInMetres, TEST_DISTANCE_DELTA)
    }

    @Test
    fun addPosition_forSecondPosition_shouldGetDistanceBetweenFirstAndSecondFromProviderUsingLatitudesAndLongitudes() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 1, statistics)

        // assert
        Assert.assertEquals(111.1, statistics.distanceInMetres, TEST_DISTANCE_DELTA)
    }

    @Test
    fun addPosition_forMultiplePositions_shouldAddAllDistancesFromProvider() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 1, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 2, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 3, statistics)

        // assert
        Assert.assertEquals(777.7, statistics.distanceInMetres, TEST_DISTANCE_DELTA)
    }

    @Test
    fun addPosition_forMultiplePositionsWithPause_shouldOmitDistanceWhilePaused() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrackWithPause()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 1, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 2, statistics)
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 3, statistics)

        // assert
        Assert.assertEquals(555.5, statistics.distanceInMetres, TEST_DISTANCE_DELTA)
    }

    @Test
    fun addPosition_shouldSetIndividualPositionDistances() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrack()
        val positions = track.currentPositionsSnapshot

        // act
        calculator.addPosition(track.started, positions, 0, statistics)
        calculator.addPosition(track.started, positions, 1, statistics)
        calculator.addPosition(track.started, positions, 2, statistics)
        calculator.addPosition(track.started, positions, 3, statistics)

        // assert
        Assert.assertEquals(0.0, positions[0].distanceFromStart!!, TEST_DISTANCE_DELTA)
        Assert.assertEquals(111.1, positions[1].distanceFromStart!!, TEST_DISTANCE_DELTA)
        Assert.assertEquals(333.3, positions[2].distanceFromStart!!, TEST_DISTANCE_DELTA)
        Assert.assertEquals(777.7, positions[3].distanceFromStart!!, TEST_DISTANCE_DELTA)
    }

    @Test
    fun addPosition_withPause_shouldSetIndividualPositionDistances() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTrackWithPause()
        val positions = track.currentPositionsSnapshot

        // act
        calculator.addPosition(track.started, positions, 0, statistics)
        calculator.addPosition(track.started, positions, 1, statistics)
        calculator.addPosition(track.started, positions, 2, statistics)
        calculator.addPosition(track.started, positions, 3, statistics)

        // assert
        Assert.assertEquals(0.0, positions[0].distanceFromStart!!, TEST_DISTANCE_DELTA)
        Assert.assertEquals(111.1, positions[1].distanceFromStart!!, TEST_DISTANCE_DELTA)
        Assert.assertEquals(111.1, positions[2].distanceFromStart!!, TEST_DISTANCE_DELTA)
        Assert.assertEquals(555.5, positions[3].distanceFromStart!!, TEST_DISTANCE_DELTA)
    }
}
