/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.statistics.TrackElevationCalculator
import com.andrewandderek.trailblazer.statistics.elevation.ElevationGainAndLoss
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithm
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithmFactory
import com.andrewandderek.trailblazer.unittests.statistics.elevation.ElevationAlgorithmTestBase
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class TrackElevationCalculatorTests : ElevationAlgorithmTestBase() {

    @Mock private lateinit var mockElevationAlgorithm: IElevationAlgorithm
    @Mock private lateinit var mockElevationAlgorithmFactory: IElevationAlgorithmFactory

    private lateinit var calculator: TrackElevationCalculator

    @Before
    fun setupTest() {
        `when`(mockElevationAlgorithmFactory.getAlgorithm())
                .thenReturn(mockElevationAlgorithm)

        `when`(mockElevationAlgorithm.process(MockitoHelper.anyObject()))
                .thenReturn(ElevationGainAndLoss(1.1, 2.2))

        calculator = TrackElevationCalculator(mockElevationAlgorithmFactory, mockElevationSettings)
    }

    private fun createTestTrack(): Track {
        return createTrack(doubleArrayOf(11.1, 22.2, 33.3, 5.5, 15.5, 67.8, 44.4))
    }

    private fun createTestTrackWithPauses(): Track {
        val track = createTestTrack()
        val positions = track.currentPositionsSnapshot
        positions[2].segment = 1
        positions[3].segment = 1
        positions[4].segment = 2
        positions[5].segment = 2
        positions[6].segment = 2
        return track
    }

    private fun calculateForTrack(track: Track, statistics: TrackStatistics) {
        val positions = track.currentPositionsSnapshot
        positions.indices.forEach { index ->
            calculator.addPosition(track.started, track.currentPositionsSnapshot, index, statistics)
        }
    }

    private fun treatNegativeElevationsAsZero() {
        `when`(mockElevationSettings.treatNegativeElevationsAsZero)
                .thenReturn(true)
    }

    @Test
    fun addPosition_forFirstPosition_shouldSetMinAndMaxElevations() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)

        // assert
        assertEquals(11.1, statistics.minElevationInMetres, TEST_ELEVATION_DELTA)
        assertEquals(11.1, statistics.maxElevationInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_forFirstPosition_shouldLeaveElevationDifferenceAndGainAndLossUnchanged() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()

        // act
        calculator.addPosition(track.started, track.currentPositionsSnapshot, 0, statistics)

        // assert
        assertEquals(0.0, statistics.elevationDifferenceInMetres, TEST_ELEVATION_DELTA)
        assertEquals(0.0, statistics.elevationGainInMetres, TEST_ELEVATION_DELTA)
        assertEquals(0.0, statistics.elevationLossInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_forFirstPosition_shouldResetElevationAlgorithm() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()
        val positions = track.currentPositionsSnapshot

        // act
        calculator.addPosition(track.started, positions, 0, statistics)

        // assert
        verify(mockElevationAlgorithm).reset(positions[0])
    }

    @Test
    fun addPosition_forAllPositions_shouldSetMinAndMaxElevationsCorrectly() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()

        // act
        calculateForTrack(track, statistics)

        // assert
        assertEquals(5.5, statistics.minElevationInMetres, TEST_ELEVATION_DELTA)
        assertEquals(67.8, statistics.maxElevationInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_forAllPositions_shouldUseProcessedAltitudeToSetMinAndMaxElevations() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()
        // Make sure processing the positions gives different processedAltitude values
        `when`(mockElevationAlgorithm.process(MockitoHelper.anyObject()))
                .then { invocation ->
                    val position = invocation.getArgument<Position>(0)
                    position.processedAltitude = position.altitude!! * 2
                    ElevationGainAndLoss(1.1, 2.2)
                }

        // act
        calculateForTrack(track, statistics)

        // assert
        assertEquals(11.0, statistics.minElevationInMetres, TEST_ELEVATION_DELTA)
        assertEquals(135.6, statistics.maxElevationInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_forAllPositions_shouldSetElevationDifferenceCorrectly() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()

        // act
        calculateForTrack(track, statistics)

        // assert
        assertEquals((67.8 - 5.5), statistics.elevationDifferenceInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_forAllPositions_shouldSetElevationGainAndLossCorrectly() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrack()

        // act
        calculateForTrack(track, statistics)

        // assert
        // sum of all the uphill and downhill bits, provided by the elevation algorithm
        assertEquals(6.6, statistics.elevationGainInMetres, TEST_ELEVATION_DELTA)
        assertEquals(13.2, statistics.elevationLossInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_minElevationIsNegative_shouldSetMinElevationToZero() {
        // arrange
        treatNegativeElevationsAsZero()
        val statistics = TrackStatistics()
        val track = createTrack(doubleArrayOf(11.1, -5.0, 33.3, 5.5, 15.5, 67.8, 44.4))

        // act
        calculateForTrack(track, statistics)

        // assert
        assertEquals(0.0, statistics.minElevationInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_maxElevationIsNegative_shouldSetMaxElevationToZero() {
        // arrange
        treatNegativeElevationsAsZero()
        val statistics = TrackStatistics()
        val track = createTrack(doubleArrayOf(-10.0, -5.0, -33.3, -0.4, -15.5, -67.8, -44.4))

        // act
        calculateForTrack(track, statistics)

        // assert
        assertEquals(0.0, statistics.maxElevationInMetres, TEST_ELEVATION_DELTA)
    }

    @Test
    fun addPosition_forAllPositionsInTrackWithPauses_shouldResetAtTheStartOfEachSegment() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrackWithPauses()
        val positions = track.currentPositionsSnapshot

        // act
        calculateForTrack(track, statistics)

        // assert
        verify(mockElevationAlgorithm).reset(positions[0])
        verify(mockElevationAlgorithm).reset(positions[2])
        verify(mockElevationAlgorithm).reset(positions[4])
    }

    @Test
    fun addPosition_forAllPositionsInTrackWithPauses_shouldOmitGainsAndLossesWhilePaused() {
        // arrange
        val statistics = TrackStatistics()
        val track = createTestTrackWithPauses()

        // act
        calculateForTrack(track, statistics)

        // assert
        // sum of all the uphill and downhill bits, provided by the elevation algorithm, without the pauses
        assertEquals(4.4, statistics.elevationGainInMetres, TEST_ELEVATION_DELTA)
        assertEquals(8.8, statistics.elevationLossInMetres, TEST_ELEVATION_DELTA)
    }
}