/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.model.TrackStatistics
import java.util.Calendar

object TrackHelper {

    fun setupTrack(trackNo: Int, addPosition: Boolean = false): Track {
        val track = Track()
        track.id = trackNo.toLong()
        track.name = "NAME_$trackNo"
        track.notes = "NOTES_$trackNo"
        // generate tracks with different start times and with poitions slightly later
        track.started = CalendarHelper.getTimeOffsetBy(SystemTimeHelper.testDate(), Calendar.MINUTE, trackNo)
        if (addPosition) {
            val position1 = Position(1.0, 2.0, 3.0,
                CalendarHelper.getTimeOffsetBy(track.started, Calendar.SECOND, 15),
                100F,"POSITION_NOTES_${trackNo}_1")
            track.addNewPosition(position1)
        }
        return track
    }

    fun setupTrackMetadata(trackNo: Int): TrackMetadata {
        val metadata = TrackMetadata()
        metadata.trackId = trackNo.toLong()
        metadata.versionName = "VERSION_NAME_$trackNo"
        metadata.versionCode = 100 + trackNo
        metadata.ended = CalendarHelper.getTimeOffsetBy(SystemTimeHelper.testDate(), Calendar.SECOND, trackNo * 60 + 30)
        // note: all the tracks will have the same statistics
        metadata.statistics = testTrackStatistics1()
        return metadata
    }

    fun testTrackStatistics1() : TrackStatistics {
        // 2:15:23
        val recordedTime = ((2 * 60 * 60) + (15 * 60) + 23) * 1000L
        // 2:15:12
        val movingTime = recordedTime - 11000
        val statistics1 = TrackStatistics(
                1234.56,
                999,
                recordedTime,
                movingTime,
                1.0,
                2.0,
                2.0,
                333.45,
                331.45,
                777.56,
                888.99)
        return statistics1
    }

    fun testTrackStatistics2() : TrackStatistics {
        // 1:39:55
        val recordedTime = ((1 * 60 * 60) + (39 * 60) + 55) * 1000L
        // 1:39:44
        val movingTime = recordedTime - 11000
        val statistics2 = TrackStatistics(
                54321.98,
                888,
                recordedTime,
                movingTime,
                2.0,
                3.0,
                4.0,
                444.45,
                440.45,
                510.01,
                200.99)
        return statistics2
    }

    fun testTrackStatistics3() : TrackStatistics {
        // 12:34
        val recordedTime = ((12 * 60) + 34) * 1000L
        // 11:50
        val movingTime = recordedTime - 44000
        val statistics3 = TrackStatistics(
                111.29,
                777,
                recordedTime,
                movingTime,
                3.0,
                4.0,
                5.0,
                555.45,
                550.45,
                520.01,
                210.99)
        return statistics3
    }
}