/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.utility

import android.content.Context
import com.andrewandderek.trailblazer.settings.IMapThemeSettings
import com.andrewandderek.trailblazer.settings.MapTheme
import com.andrewandderek.trailblazer.ui.utility.IUiModeProvider
import com.andrewandderek.trailblazer.ui.utility.MapColourSchemeProvider
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.google.android.gms.maps.model.MapColorScheme
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`

class MapColourSchemeProviderTests : MockingAnnotationSetup() {

    @Mock
    protected lateinit var mockMapThemeSettings: IMapThemeSettings

    @Mock
    protected lateinit var mockUiModeProvider: IUiModeProvider

    @Mock
    protected lateinit var mockContext: Context

    private lateinit var mapColourSchemeProvider: MapColourSchemeProvider

    @Before
    fun before_each_test() {
        mapColourSchemeProvider = MapColourSchemeProvider(
            mockMapThemeSettings,
            mockUiModeProvider
        )
    }

    @Test
    fun when_map_theme_setting_is_light_then_colour_scheme_is_light() {
        `when`(mockMapThemeSettings.mapTheme).thenReturn(MapTheme.LIGHT)

        val result = mapColourSchemeProvider.getMapColourScheme(mockContext)

        assertEquals(MapColorScheme.LIGHT, result)
    }

    @Test
    fun when_map_theme_setting_is_dark_then_colour_scheme_is_dark() {
        `when`(mockMapThemeSettings.mapTheme).thenReturn(MapTheme.DARK)

        val result = mapColourSchemeProvider.getMapColourScheme(mockContext)

        assertEquals(MapColorScheme.DARK, result)
    }

    @Test
    fun when_map_theme_setting_is_follow_app_and_app_not_in_dark_mode_then_colour_scheme_is_light() {
        `when`(mockMapThemeSettings.mapTheme).thenReturn(MapTheme.FOLLOW_APP)
        `when`(mockUiModeProvider.isInDarkMode(mockContext)).thenReturn(false)

        val result = mapColourSchemeProvider.getMapColourScheme(mockContext)

        assertEquals(MapColorScheme.LIGHT, result)
    }

    @Test
    fun when_map_theme_setting_is_follow_app_and_app_is_in_dark_mode_then_colour_scheme_is_dark() {
        `when`(mockMapThemeSettings.mapTheme).thenReturn(MapTheme.FOLLOW_APP)
        `when`(mockUiModeProvider.isInDarkMode(mockContext)).thenReturn(true)

        val result = mapColourSchemeProvider.getMapColourScheme(mockContext)

        assertEquals(MapColorScheme.DARK, result)
    }
}