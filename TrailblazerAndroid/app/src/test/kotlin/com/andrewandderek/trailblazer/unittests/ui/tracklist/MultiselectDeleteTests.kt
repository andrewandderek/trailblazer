/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.exception.TrackDeleteException
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class MultiselectDeleteTests : TrackListPresenterSetup() {
    @Captor
    private lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
        setupTrackListItems(3)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun not_in_select_mode_and_deleted() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.onActionItemClicked(R.id.action_track_delete)

        // assert
        verify(mockview, never()).promptToDeleteMultiple(anyString())
    }

    @Test
    fun nothing_selected_and_deleted() {
        // arrange
        presenter.loadTracks()
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(1, 0)

        // act
        presenter.onActionItemClicked(R.id.action_track_delete)

        // assert
        verify(mockview, never()).promptToDeleteMultiple(anyString())
    }

    @Test
    fun item_1_and_2_selected_and_deleted() {
        // arrange
        loadTracksAndSelect1and2()

        // act
        presenter.onActionItemClicked(R.id.action_track_delete)

        // assert
        verify(mockview, times(1)).promptToDeleteMultiple("TEST_SELECTION_DELETE 2")
    }

    @Test
    fun recording_track_selected_and_deleted() {
        // arrange
        trackIsBeingRecorded(trackWithPositions)
        loadTracksAndSelect1and2()

        // act
        presenter.onActionItemClicked(R.id.action_track_delete)

        // assert
        verify(mockview, times(1)).promptToDeleteMultiple("TEST_SELECTION_DELETE_REC 2")
    }

    @Test
    fun paused_track_selected_and_deleted() {
        // arrange
        trackIsBeingPaused(trackWithPositions)
        loadTracksAndSelect1and2()

        // act
        presenter.onActionItemClicked(R.id.action_track_delete)

        // assert
        verify(mockview, times(1)).promptToDeleteMultiple("TEST_SELECTION_DELETE_REC 2")
    }

    @Test
    fun item_1_and_2_selected_and_deleted_and_confirmed() {
        // arrange
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_track_delete)

        // act
        reset(mockview)
        presenter.confirmedMultipleDeleteTrack()

        // assert
        // tracks deleted
        assertEquals("incorrect number of items", 1, presenter.itemCount)
        verify(mockTrackRepository, times(1)).deleteById(1)
        verify(mockTrackRepository, times(1)).deleteById(2)
        // UI updated
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).setTitle("TEST_TITLE")
        verify(mockview, times(1)).showMessage("TEST_MULTI_DELETE 2")
        verify(mockservice, never()).stopTracking(false)
    }

    @Test
    fun recording_item_selected_and_deleted_and_confirmed() {
        // arrange
        trackIsBeingRecorded(trackWithPositions)
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_track_delete)

        // act
        presenter.confirmedMultipleDeleteTrack()

        // assert
        // tracks deleted
        verify(mockservice, times(1)).stopTracking(false)
    }

    @Test
    fun item_1_and_2_selected_and_deleted_and_confirmed_throws() {
        // arrange
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_track_delete)
        `when`(mockTrackRepository.deleteById(2)).thenThrow(NullPointerException("ERROR"))

        // act
        reset(mockview)
        presenter.confirmedMultipleDeleteTrack()

        // assert
        // UI updated
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).showMessage(R.string.delete_multi_track_error)
    }

    @Test
    fun item_1_and_2_selected_and_deleted_and_confirmed_throws_and_logs() {
        // arrange
        loadTracksAndSelect1and2()
        presenter.onActionItemClicked(R.id.action_track_delete)
        `when`(mockTrackRepository.deleteById(2)).thenThrow(NullPointerException("ERROR"))

        // act
        reset(mockview)
        presenter.confirmedMultipleDeleteTrack()

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect",true, exception is TrackDeleteException)
        assertEquals("inner exception type incorrect",true, exception.cause is NullPointerException)
    }
}


