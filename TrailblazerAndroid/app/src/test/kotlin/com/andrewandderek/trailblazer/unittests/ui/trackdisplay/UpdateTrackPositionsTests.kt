/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.SystemTimeHelper
import com.andrewandderek.trailblazer.model.Position
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyFloat
import org.mockito.Mockito

class UpdateTrackPositionsTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()

        position1 = Position(1.0, 2.0, 3.0, SystemTimeHelper.testDate(), 10F)
        position1.trackId = currentRecordingTrack.id
        position2 = Position(11.0, 12.0, 13.0, SystemTimeHelper.testDate(), 10F)
        position2.trackId = currentRecordingTrack.id
        position3 = Position(51.0, 112.0, 113.0, SystemTimeHelper.testDate(), 10F)
        position3.trackId = currentRecordingTrack.id
        position4 = Position(52.0, 152.0, 113.0, SystemTimeHelper.testDate(), 100F)
        position4.trackId = currentRecordingTrack.id

        presenter.bind(mockview, null)
        presenter.start()
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun draw_no_points() {
        // arrange
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        currentRecordingTrack.reset()

        // assert
        Mockito.verify(mockview, Mockito.never()).centreMapAndZoom(anyObject(), anyFloat())
        Mockito.verify(mockview, Mockito.never()).drawAllPositions(anyObject())
    }

    @Test
    fun draw_first_point() {
        // arrange
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        currentRecordingTrack.addNewPosition(position1)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).centreMapAndZoom(position1, USER_ZOOM_LEVEL)
        Mockito.verify(mockview, Mockito.times(1)).drawAllPositions(currentRecordingTrack.currentPositionsSnapshot)
    }

    @Test
    fun draw_first_two_points() {
        // arrange
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).centreMapAndZoom(position1, USER_ZOOM_LEVEL)
        Mockito.verify(mockview, Mockito.times(1)).drawAllPositions(listOf(position1))
        Mockito.verify(mockview, Mockito.times(1)).addPosition(position2)
    }

    @Test
    fun draw_first_three_points() {
        // arrange
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)
        currentRecordingTrack.addNewPosition(position3)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).centreMapAndZoom(position1, USER_ZOOM_LEVEL)
        Mockito.verify(mockview, Mockito.times(1)).drawAllPositions(listOf(position1))
        Mockito.verify(mockview, Mockito.times(1)).addPosition(position2)
        Mockito.verify(mockview, Mockito.times(1)).addPosition(position3)
    }

    @Test
    fun draw_first_three_points_and_discard_inaccurate() {
        // arrange
        presenter.connectService(mockbinder)        // attach the subscribers

        // act
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position4)     // this one should be discarded
        currentRecordingTrack.addNewPosition(position2)
        currentRecordingTrack.addNewPosition(position3)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).centreMapAndZoom(position1, USER_ZOOM_LEVEL)
        Mockito.verify(mockview, Mockito.times(1)).drawAllPositions(listOf(position1))
        Mockito.verify(mockview, Mockito.times(1)).addPosition(position2)
        Mockito.verify(mockview, Mockito.times(1)).addPosition(position3)
        Mockito.verify(mockview, Mockito.never()).addPosition(position4)
    }

    @Test
    fun draw_first_two_points_in_one_go() {
        // arrange
        currentRecordingTrack.addNewPosition(position1)

        // act
        presenter.connectService(mockbinder)        // attach the subscribers
        currentRecordingTrack.addNewPosition(position2)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).centreMapAndZoom(position2, USER_ZOOM_LEVEL)
        Mockito.verify(mockview, Mockito.times(1)).drawAllPositions(currentRecordingTrack.currentPositionsSnapshot)
        Mockito.verify(mockview, Mockito.never()).addPosition(anyObject())
    }

    @Test
    fun draw_first_four_points_in_one_go_discarding_inaccurate() {
        // arrange
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position4)
        currentRecordingTrack.addNewPosition(position2)

        // act
        presenter.connectService(mockbinder)        // attach the subscribers
        currentRecordingTrack.addNewPosition(position3)

        // assert
        Mockito.verify(mockview, Mockito.times(1)).centreMapAndZoom(position3, USER_ZOOM_LEVEL)
        Mockito.verify(mockview, Mockito.times(1)).drawAllPositions(listOf(position1, position2, position3))
        Mockito.verify(mockview, Mockito.never()).addPosition(anyObject())
    }

}