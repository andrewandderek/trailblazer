/*
 *  Copyright 2017-2022 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify

class ActionSelectedTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun action_settings_selected() {
        // arrange

        // act
        val result = presenter.actionSelected(R.id.action_settings)

        // assert
        Assert.assertEquals(true, result)
        verify(mockview, Mockito.times(1)).navigateToSettings()
    }

    @Test
    fun action_import_selected_requests_read_storage_permission_when_required() {
        // arrange
        setupWithoutReadStoragePermission()

        // act
        val result = presenter.actionSelected(R.id.action_import_track)

        // assert
        Assert.assertEquals(true, result)
        verify(mockview, Mockito.times(1)).requestReadExternalStoragePermission()
    }

    @Test
    fun action_import_selected_does_not_request_read_storage_permission_when_not_required() {
        // arrange
        setupWithReadStoragePermission()

        // act
        val result = presenter.actionSelected(R.id.action_import_track)

        // assert
        Assert.assertEquals(true, result)
        verify(mockview, Mockito.never()).requestReadExternalStoragePermission()
    }
}