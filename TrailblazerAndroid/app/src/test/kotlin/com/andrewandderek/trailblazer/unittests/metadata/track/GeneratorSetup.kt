/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.metadata.track

import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.RxEventBus
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.metadata.track.BatchTrackMetadataGenerator
import com.andrewandderek.trailblazer.metadata.track.SingleTrackMetadataGenerator
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.`when`

open class GeneratorSetup : MockingAnnotationSetup() {
    protected lateinit var batchGenerator: BatchTrackMetadataGenerator
    protected lateinit var singleGenerator: SingleTrackMetadataGenerator

    protected lateinit var eventBus: IEventBus

    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var mockScheduler: ISchedulerProvider
    protected var tracksUpdatedEventSubscriber: IEventBusSubscription? = null
    protected var publishedTracksUpdatedEvent: TrackMetadataUpdatedEvent? = null

    @Mock protected lateinit var mockTrackRepository: ITrackRepository
    @Mock protected lateinit var mockTrackMetadataRepository: ITrackMetadataRepository
    @Mock protected lateinit var mockPositionRepository: IPositionRepository
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockStatisticsProvider: ITrackStatisticsProvider
    @Captor protected lateinit var loggingCaptor: ArgumentCaptor<Throwable>
    @Captor protected lateinit var metadataCaptor: ArgumentCaptor<TrackMetadata>

    protected lateinit var trackList: MutableList<Track>
    protected lateinit var trackMetadataList: MutableList<TrackMetadata>
    protected lateinit var trackStatistics: TrackStatistics

    protected fun setupTrackStatistics() : TrackStatistics {
        val retval = TrackHelper.testTrackStatistics1()
        `when`(mockStatisticsProvider.getStatistics(anyObject())).thenReturn(retval)
        return retval
    }

    protected fun setupGenerator() {
        trackList = ArrayList(10)
        trackMetadataList = ArrayList(10)
        trackStatistics = setupTrackStatistics()

        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockScheduler = RxHelper.setupSchedulerProvider()

        eventBus = RxEventBus(
            mockLoggerFactory,
            mockCrashReporter,
            mockScheduler
        )

        singleGenerator = SingleTrackMetadataGenerator(
            mockLoggerFactory,
            mockTrackMetadataRepository,
            mockStatisticsProvider,
        )

        batchGenerator = BatchTrackMetadataGenerator(
            mockLoggerFactory,
            mockTrackRepository,
            mockTrackMetadataRepository,
            mockPositionRepository,
            eventBus,
            mockCrashReporter,
            singleGenerator
        )
    }

    protected fun setupEventSubscriber() {
        // we dont mock the event bus because it will run quite happily in the unit tests so lets test as much as we can
        publishedTracksUpdatedEvent = null   // reset the event we captured
        tracksUpdatedEventSubscriber = eventBus.subscribe(TrackMetadataUpdatedEvent::class.java)
        { event ->
            publishedTracksUpdatedEvent = event
        }
    }

    // tracks have ids 1..n
    protected fun setupTrackRepo(numberOfTracksNeeded: Int) {
        trackList = ArrayList(10)
        for (index in 1.. numberOfTracksNeeded) {
            val thisTrack = TrackHelper.setupTrack(index)
            trackList.add(thisTrack)
            `when`(mockTrackRepository.getWithPositionsAndAccuracy(thisTrack.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(thisTrack)
        }
        `when`(mockTrackRepository.getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()).thenReturn(trackList)
    }

    // metadata track ids are 11..n
    protected fun setupTrackMetadataRepo(numberOfMetadataItems: Int) {
        trackMetadataList = ArrayList(10)
        for (index in 11.. numberOfMetadataItems + 10) {
            val metadata = TrackHelper.setupTrackMetadata(index)
            trackMetadataList.add(metadata)
            // we also need to add the track
            val thisTrack = TrackHelper.setupTrack(index)
            `when`(mockTrackRepository.getWithPositionsAndAccuracy(thisTrack.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(thisTrack)
        }
        `when`(mockTrackMetadataRepository.getWhereVersionCodeBefore(anyInt())).thenReturn(trackMetadataList)
    }

}