/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.sharing.importexport

import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.NotSupportedException
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetExporterTests : ExportSetup() {
    @Before
    fun before_each_test() {
        setupExporterFactory()
    }

    @Test
    fun gpx_exporter() {
        // arrange

        // act
        val exporter = factory.getExporter(NameLiterals.FILE_FORMAT_GPX)

        // assert
        Assert.assertEquals(mockGpxExporter, exporter)
    }

    @Test
    fun kml_exporter() {
        // arrange

        // act
        val exporter = factory.getExporter(NameLiterals.FILE_FORMAT_KML)

        // assert
        Assert.assertEquals(mockKmlExporter, exporter)
    }

    @Test(expected=NotSupportedException::class)
    fun unknown_exporter() {
        // arrange

        // act
        factory.getExporter("UNKNOWN")

        // assert
        // we are asserting that the expected exception is thrown
    }
}