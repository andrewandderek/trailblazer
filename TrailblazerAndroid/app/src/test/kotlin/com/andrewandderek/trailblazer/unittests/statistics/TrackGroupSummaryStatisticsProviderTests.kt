/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.helpers.UnitsHelper
import com.andrewandderek.trailblazer.helpers.UserSettingsHelper
import com.andrewandderek.trailblazer.model.FormattedTrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.model.TrackAndStatistics
import com.andrewandderek.trailblazer.model.TrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.statistics.TrackGroupSummaryStatisticsProvider
import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class TrackGroupSummaryStatisticsProviderTests : MockingAnnotationSetup() {
    private lateinit var provider: TrackGroupSummaryStatisticsProvider

    private lateinit var userSettings: IUserSettings
    private lateinit var distanceFormatter: IDistanceFormatter
    private lateinit var speedFormatter: ISpeedFormatter
    private lateinit var paceFormatter: IPaceFormatter

    private var allTracks: ArrayList<TrackAndStatistics> = ArrayList()
    private lateinit var expectedSummaryStats: TrackGroupSummaryStatistics
    private lateinit var expectedFormattedSummaryStats: FormattedTrackGroupSummaryStatistics

    @Before
    fun setupTest() {
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())
        speedFormatter = UnitsHelper.setupSpeedFormatter(PreferredUnits.METRIC.toString())
        paceFormatter = UnitsHelper.setupPaceFormatter(PreferredUnits.METRIC.toString())
        distanceFormatter = UnitsHelper.setupDistanceFormatter(PreferredUnits.METRIC.toString())

        provider = TrackGroupSummaryStatisticsProvider(userSettings, distanceFormatter, speedFormatter, paceFormatter)
        setupAllTracks()
    }

    private fun setupAllTracks() {
        allTracks.add(TrackAndStatistics(TrackHelper.setupTrack(1), TrackHelper.testTrackStatistics1()))
        allTracks.add(TrackAndStatistics(TrackHelper.setupTrack(2), TrackHelper.testTrackStatistics2()))
        allTracks.add(TrackAndStatistics(TrackHelper.setupTrack(3), TrackHelper.testTrackStatistics3()))

        expectedSummaryStats = TrackGroupSummaryStatistics(
                3,
                55667.83,               // 1234.56 + 54321.98 + 111.29 = 55,667.83
                14806000,
                14872000,
                3.759815615291098
        )

        expectedFormattedSummaryStats = FormattedTrackGroupSummaryStatistics(
                "DISTANCE_LABEL_METRIC",
                "SPEED_LABEL_METRIC",
                "PACE_LABEL_METRIC",
                "55.67",            // 1234.56 + 54321.98 + 111.29 = 55,667.83
                "18.56",            // 55,667.83 / 3 = 18,555.94
                "04:07:52",         // 2:15:23 + 1:39:55 + 12:34 = 247 mins + 52 secs = 4 hours 7 mins 52 secs
                "01:22:37",         // 82 mins + 37.3 secs = 1:22:37
                "04:06:46",         // 2:15:12 + 1:39:44 + 11:50 = 246 mins + 46 secs = 4 hours 6 mins 46 secs
                "01:22:15",         // 82 mins + 15.3 secs = 1:22:15
                "13.54",            // 55,667.83 / ((246 * 60) + 46)) = 55,667.83 / 14,806 = 3.75981 m/s = 3.75 * 3,600 / 1,000 = 13.54 KM/H
                "4:25"              // 1000 / 3.75981 m/s = 265.9704 s/KM = 4 min  25 secs (we truncate not round)
        )
    }

    @Test
    fun summary_stats_correct() {
        // arrange

        // act
        val summary = provider.getSummaryStatistics(allTracks)

        // assert
        assertEquals(expectedSummaryStats, summary)
    }

    @Test
    fun formatted_summary_stats_correct() {
        // arrange
        val summary = provider.getSummaryStatistics(allTracks)

        // act
        val formattedSummary = provider.getFormattedSummaryStatistics(summary)

        // assert
        assertEquals(expectedFormattedSummaryStats, formattedSummary)
    }
}
