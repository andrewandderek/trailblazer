/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.TrackImportedEvent
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.sharing.importexport.ImportAndResult
import com.andrewandderek.trailblazer.sharing.importexport.ImportResult
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ImportTrackTests : TrackDisplayPresenterSetup() {
    private val SAVED_TRACK_ID = 789L

    private lateinit var importedTrack: Track
    private lateinit var savedTrack: Track

    private var trackImportedEventSubscriber: IEventBusSubscription? = null
    private var publishedTrackImportedEvent: TrackImportedEvent? = null

    private fun setupImportedTrackWithTwoPoints() {
        importedTrack = Track()
        importedTrack.name = "TEST IMPORTED TRACK"
        importedTrack.addNewPosition(position1)
        importedTrack.addNewPosition(position2)

        savedTrack = Track()
        savedTrack.id = SAVED_TRACK_ID
        savedTrack.name = "TEST SAVED TRACK"

        `when`(mockImporter.importTrack(IMPORT_FILENAME)).thenReturn(ImportAndResult(importedTrack, ImportResult.OK))
        `when`(mockTrackRepository.create(anyObject())).thenReturn(savedTrack)
    }

    private fun setup(scheduler: TestScheduler? = null) {
        setupPresenter(scheduler)
        setupNowDateTimes()
        setupPoints()
        setupTrackImportedEventSubscriber()

        `when`(mockResourceProvider.getString(R.string.import_track_prompt_fmt)).thenReturn("TEST_IMPORT %s")

        presenter.bind(mockview, null)
        presenter.start()
    }

    private fun setupTrackImportedEventSubscriber() {
        trackImportedEventSubscriber = eventBus.subscribe(TrackImportedEvent::class.java)
        { event ->
            publishedTrackImportedEvent = event
        }
    }

    @After
    fun after_each_test() {
        trackImportedEventSubscriber?.unsubscribe()
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun import_track_analytics() {
        // arrange
        setup()
        setupImportedTrackWithTwoPoints()

        // act
        presenter.importTrack(IMPORT_FILENAME)

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_ID, AnalyticsConstants.ACTION_IMPORT_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun import_named_track() {
        // arrange
        setup()
        setupImportedTrackWithTwoPoints()

        // act
        presenter.importTrack(IMPORT_FILENAME)

        // assert
        verify(mockview, times(1)).centreMapAndZoomToFit(savedTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).drawAllPositions(savedTrack.currentPositionsSnapshot)
        verify(mockview, times(1)).updateAvailableActions()
        verify(mockview, times(1)).showMessage(R.string.track_imported)
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
        verify(mockview, never()).exit()
        Assert.assertEquals(SAVED_TRACK_ID, publishedTrackImportedEvent?.trackId)
    }

    @Test
    fun import_track_fails() {
        // arrange
        setup()
        setupImportedTrackWithTwoPoints()
        `when`(mockImporter.importTrack(IMPORT_FILENAME)).thenReturn(ImportAndResult(importedTrack, ImportResult.UNKNOWN_FORMAT))

        // act
        presenter.importTrack(IMPORT_FILENAME)

        // assert
        verify(mockview, times(1)).showMessage(R.string.error_loading_track)
        verify(mockview, times(1)).exit()
    }

    @Test
    fun import_track_fails_no_timestamp() {
        // arrange
        setup()
        setupImportedTrackWithTwoPoints()
        `when`(mockImporter.importTrack(IMPORT_FILENAME)).thenReturn(ImportAndResult(importedTrack, ImportResult.NO_TIMESTAMP))

        // act
        presenter.importTrack(IMPORT_FILENAME)

        // assert
        verify(mockview, times(1)).showMessage(R.string.no_timestamps_in_track)
        verify(mockview, times(1)).exit()
    }

    @Test
    fun import_track_not_found() {
        // arrange
        setup()
        `when`(mockImporter.importTrack(IMPORT_FILENAME)).thenReturn(null)

        // act
        presenter.importTrack(IMPORT_FILENAME)

        // assert
        verify(mockview, never()).centreMapAndZoomToFit(anyObject())
        verify(mockview, never()).drawAllPositions(anyObject())
        verify(mockview, times(1)).showMessage(R.string.error_loading_track)
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
        verify(mockview, times(1)).exit()
    }

    @Test
    fun import_track_save_failed() {
        // arrange
        setup()
        setupImportedTrackWithTwoPoints()
        `when`(mockTrackRepository.create(anyObject())).thenReturn(null)

        // act
        presenter.importTrack(IMPORT_FILENAME)

        // assert
        verify(mockview, never()).centreMapAndZoomToFit(anyObject())
        verify(mockview, never()).drawAllPositions(anyObject())
        verify(mockview, times(1)).showMessage(R.string.error_loading_track)
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
    }


}