/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.compare

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.helpers.UnitsHelper
import com.andrewandderek.trailblazer.helpers.UserSettingsHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.FormattedTrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.model.ITrackAndStatistics
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.statistics.ITrackGroupSorter
import com.andrewandderek.trailblazer.statistics.ITrackGroupSummaryStatisticsProvider
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.ui.compare.CompareViewModel
import com.andrewandderek.trailblazer.ui.compare.SortedTrackList
import com.andrewandderek.trailblazer.ui.compare.TrackAndStatisticsWithLabels
import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import org.junit.Rule
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.anyList
import org.mockito.Mockito.`when`


open class CompareViewModelSetup : MockingAnnotationSetup() {
    protected lateinit var viewModel: CompareViewModel
    private lateinit var mockLoggerFactory: ILoggerFactory
    private lateinit var mockSchedulerProvider: ISchedulerProvider
    private lateinit var userSettings: IUserSettings
    private lateinit var distanceFormatter: IDistanceFormatter
    private lateinit var speedFormatter: ISpeedFormatter
    private lateinit var paceFormatter: IPaceFormatter
    private lateinit var altitudeFormatter: IAltitudeFormatter

    @Mock protected lateinit var mockApplication: Application
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockTrackRepository: ITrackRepository
    @Mock protected lateinit var mockPositionRepository: IPositionRepository
    @Mock protected lateinit var mockStatisticsProvider: ITrackStatisticsProvider
    @Mock protected lateinit var mockTrackGroupSummaryStatisticsProvider: ITrackGroupSummaryStatisticsProvider
    @Mock protected lateinit var mockTrackGroupSorter: ITrackGroupSorter

    // Rule for help testing livedata
    @Rule @JvmField public var rule = InstantTaskExecutorRule()
    @Mock protected lateinit var mockTitleObserver: Observer<String>
    @Mock protected lateinit var mockMessageObserver: Observer<String>
    @Mock protected lateinit var mockProgressStartObserver: Observer<Int>
    @Mock protected lateinit var mockProgressPositionObserver: Observer<Int>
    @Mock protected lateinit var mockProgressEndObserver: Observer<Void?>
    @Mock protected lateinit var mockSummaryStats: Observer<FormattedTrackGroupSummaryStatistics>
    @Mock protected lateinit var mockSortedList: Observer<SortedTrackList>

    @Captor protected lateinit var trackStatsCaptor: ArgumentCaptor<List<ITrackAndStatistics>>

    protected fun setupViewModel() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockSchedulerProvider = RxHelper.setupSchedulerProvider()
        userSettings = UserSettingsHelper.setupUserSettings(PreferredUnits.METRIC.toString())
        altitudeFormatter = UnitsHelper.setupAltitudeFormatter(PreferredUnits.METRIC.toString())
        speedFormatter = UnitsHelper.setupSpeedFormatter(PreferredUnits.METRIC.toString())
        paceFormatter = UnitsHelper.setupPaceFormatter(PreferredUnits.METRIC.toString())
        distanceFormatter = UnitsHelper.setupDistanceFormatter(PreferredUnits.METRIC.toString())

        viewModel = CompareViewModel(
                mockApplication,
                mockLoggerFactory,
                mockResourceProvider,
                mockCrashReporter,
                mockSchedulerProvider,
                mockTrackRepository,
                mockPositionRepository,
                mockStatisticsProvider,
                mockTrackGroupSummaryStatisticsProvider,
                mockTrackGroupSorter,
                distanceFormatter,
                speedFormatter,
                paceFormatter
        )

        viewModel.observables.title.observeForever(mockTitleObserver)
        viewModel.observables.message.observeForever(mockMessageObserver)
        viewModel.observables.progressStart.observeForever(mockProgressStartObserver)
        viewModel.observables.progressPosition.observeForever(mockProgressPositionObserver)
        viewModel.observables.progressEnd.observeForever(mockProgressEndObserver)
        viewModel.observables.summaryStatistics.observeForever(mockSummaryStats)
        viewModel.observables.sortedTrackList.observeForever(mockSortedList)

        `when`(mockResourceProvider.getString(R.string.compare_title_fmt)).thenReturn("TEST_TITLE %s")
        `when`(mockResourceProvider.getString(R.string.compare_track_list_sort_moving_time)).thenReturn("TEST_SORT_MOVING_TIME")
        `when`(mockResourceProvider.getString(R.string.compare_track_list_sort_distance)).thenReturn("TEST_SORT_DISTANCE")
        `when`(mockResourceProvider.getString(R.string.compare_track_list_sort_speed)).thenReturn("TEST_SORT_SPEED")
        `when`(mockResourceProvider.getString(R.string.compare_track_list_ascending)).thenReturn("TEST_ORDER_ASC")
        `when`(mockResourceProvider.getString(R.string.compare_track_list_descending)).thenReturn("TEST_ORDER_DESC")
    }

    private lateinit var track1: Track
    private lateinit var statistics1: TrackStatistics
    private lateinit var trackAndStatistics1: TrackAndStatisticsWithLabels
    private fun setupTrack1() {
        track1 = TrackHelper.setupTrack(1)
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(1, Position.ACCURACY_THRESHOLD, mockPositionRepository))
                .thenReturn(track1)

        statistics1 = TrackHelper.testTrackStatistics1()
        `when`(mockStatisticsProvider.getStatistics(track1))
                .thenReturn(statistics1)

        trackAndStatistics1 = TrackAndStatisticsWithLabels(
                track1,
                statistics1,
                "Sat 13, Jan, 2018 20:13:14",
                "1.23 DISTANCE_LABEL_METRIC, 02:15:12, 7.20 SPEED_LABEL_METRIC, 8:20 PACE_LABEL_METRIC"
        )
    }

    private lateinit var track2: Track
    private lateinit var statistics2: TrackStatistics
    private lateinit var trackAndStatistics2: TrackAndStatisticsWithLabels
    private fun setupTrack2() {
        track2 = TrackHelper.setupTrack(2)
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(2, Position.ACCURACY_THRESHOLD, mockPositionRepository))
                .thenReturn(track2)

        statistics2 = TrackHelper.testTrackStatistics2()
        `when`(mockStatisticsProvider.getStatistics(track2))
                .thenReturn(statistics2)

        trackAndStatistics2 = TrackAndStatisticsWithLabels(
                track2,
                statistics2,
                "Sat 13, Jan, 2018 20:13:14",
                "54.32 DISTANCE_LABEL_METRIC, 01:39:44, 10.80 SPEED_LABEL_METRIC, 5:33 PACE_LABEL_METRIC"
        )
    }

    private lateinit var track3: Track
    private lateinit var statistics3: TrackStatistics
    private lateinit var trackAndStatistics3: TrackAndStatisticsWithLabels
    private fun setupTrack3() {
        track3 = TrackHelper.setupTrack(3)
        `when`(mockTrackRepository.getWithPositionsAndAccuracy(3, Position.ACCURACY_THRESHOLD, mockPositionRepository))
                .thenReturn(track3)

        statistics3 = TrackHelper.testTrackStatistics3()
        `when`(mockStatisticsProvider.getStatistics(track3))
                .thenReturn(statistics3)

        trackAndStatistics3 = TrackAndStatisticsWithLabels(
                track3,
                statistics3,
                "Sat 13, Jan, 2018 20:13:14",
                "0.11 DISTANCE_LABEL_METRIC, 00:11:50, 14.40 SPEED_LABEL_METRIC, 4:10 PACE_LABEL_METRIC"
        )
    }

    protected lateinit var sortedTracks: List<TrackAndStatisticsWithLabels>
    protected lateinit var formattedSummaryStats: FormattedTrackGroupSummaryStatistics
    protected fun setupAllTracks() {
        setupTrack1()
        setupTrack2()
        setupTrack3()

        val summary = TrackGroupSummaryStatistics(1, 2.2, 3, 4, 5.5)
        formattedSummaryStats = FormattedTrackGroupSummaryStatistics(
                "DISTANCE_LABEL_METRIC",
                "SPEED_LABEL_METRIC",
                "PACE_LABEL_METRIC",
                "TOT_DISTANCE",
                "AVE_DISTANCE",
                "TOT_TIME",
                "AVE_TIME",
                "TOT_MOV_TIME",
                "AVE_MOV_TIME",
                "AVE_MOV_SPEED",
                "AVE_MOV_PACE"
        )
        `when`(mockTrackGroupSummaryStatisticsProvider.getSummaryStatistics(MockitoHelper.anyObject())).thenReturn(summary)
        `when`(mockTrackGroupSummaryStatisticsProvider.getFormattedSummaryStatistics(summary)).thenReturn(formattedSummaryStats)

        sortedTracks = listOf(
                trackAndStatistics2,
                trackAndStatistics3,
                trackAndStatistics1
        )
        // we are not testing the sorting of the tracks here - that is tested in the TrackGroupSorterTests
        // we just need to test that the returned value is used in the UI
        `when`(mockTrackGroupSorter.sortTracks(anyList(), MockitoHelper.anyObject(), anyBoolean())).thenReturn(sortedTracks)
    }

}