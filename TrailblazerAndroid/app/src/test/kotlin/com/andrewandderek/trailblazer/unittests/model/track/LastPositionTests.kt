/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import com.andrewandderek.trailblazer.model.Position
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

class LastPositionTests : TrackSetup() {
    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun isNull_whenEmpty() {
        // arrange
        val track = setupEmptyTrack()

        // act
        var result = track.lastPositionInSnapshot

        // assert
        assertNull(result)
    }

    @Test
    fun isLastPosition_whenNotEmpty() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)

        // act
        var result = track.lastPositionInSnapshot

        // assert
        assertEquals("POSITION_NOTES_2", result?.notes)
    }

    @Test
    fun isLastPosition_whenReplaced() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)
        position3 = Position(1.0, 2.0, 3.0, now, 100F, "POSITION_NOTES_3")
        track.replaceTrack(position3)

        // act
        var result = track.lastPositionInSnapshot

        // assert
        assertEquals("POSITION_NOTES_3", result?.notes)
    }

}