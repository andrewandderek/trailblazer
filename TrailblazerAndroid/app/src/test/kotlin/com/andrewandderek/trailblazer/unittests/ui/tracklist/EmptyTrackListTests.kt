/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class EmptyTrackListTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        Mockito.`when`(mockResourceProvider.getString(R.string.track_list_item_unknown)).thenReturn("TEST_1")
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun empty_list_itemCount() {
        // arrange

        // act
        val result = presenter.itemCount

        // assert
        Assert.assertEquals(0, result)
    }

    @Test
    fun empty_list_trackLabel() {
        // arrange

        // act
        val result = presenter.getTrackLabel(0)

        // assert
        Assert.assertEquals("TEST_1", result)
    }

    @Test
    fun empty_list_trackSubLabel() {
        // arrange

        // act
        val result = presenter.getTrackSubLabel(0)

        // assert
        Assert.assertEquals("TEST_1", result)
    }

    @Test
    fun empty_list_trackSelected() {
        // arrange

        // act
        presenter.trackSelected(-1, 0)

        // assert
        Mockito.verify(mockview, Mockito.never()).navigateToTrack(Mockito.anyLong())
    }
}