/*
 *  Copyright 2017-2022 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import android.content.ActivityNotFoundException
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

class ImportTrackTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        setupWithReadStoragePermission()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun import_track_records_analytics() {
        // arrange

        // act
        presenter.importTrack()

        // assert
        verify(mockAnalyticsEngine, Mockito.times(1)).recordClick(AnalyticsConstants.ACTION_IMPORT_SELECTED_ID, AnalyticsConstants.ACTION_IMPORT_SELECTED_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun import_track_prompts_for_file() {
        // arrange

        // act
        presenter.importTrack()

        // assert
        verify(mockview, Mockito.times(1)).promptForFile()
    }

    @Test
    fun import_track_imports_fixed_filename_when_no_storage_access_framework() {
        // arrange
        `when`(mockview.promptForFile()).thenThrow(ActivityNotFoundException())

        // act
        presenter.importTrack()

        // assert
        verify(mockview, Mockito.times(1)).navigateToImportTrack("file://IMPORT APP FOLDERimport.GPX", true)
    }
}