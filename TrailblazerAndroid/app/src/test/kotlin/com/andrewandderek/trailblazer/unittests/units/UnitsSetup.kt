/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.units

import com.andrewandderek.trailblazer.helpers.UnitsHelper
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter

open class UnitsSetup {
    protected lateinit var altitudeFormatter: IAltitudeFormatter
    protected lateinit var distanceFormatter: IDistanceFormatter
    protected lateinit var paceFormatter: IPaceFormatter
    protected lateinit var speedFormatter: ISpeedFormatter

    protected fun setupUnits(units: String) {
        altitudeFormatter = UnitsHelper.setupAltitudeFormatter(units)
        distanceFormatter = UnitsHelper.setupDistanceFormatter(units)
        paceFormatter = UnitsHelper.setupPaceFormatter(units)
        speedFormatter = UnitsHelper.setupSpeedFormatter(units)
    }
}