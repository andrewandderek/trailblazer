/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.worker.trackmetadata

import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class DoWorkTests : WorkerSetup() {
    @Before
    fun before_each_test() {
        setupWorker()
    }

    @Test
    fun nothing_to_generate() {
        // arrange
        `when`(mockGenerator.generateMetadata(anyInt())).thenReturn(0)

        // act
        worker.doWork()

        // assert
        verify(mockGenerator, times(1)).generateMetadata(anyInt())
        verifyNoMoreInteractions(mockCrashReporter)
    }

    @Test
    fun generate_1_batch() {
        // arrange
        `when`(mockGenerator.generateMetadata(anyInt()))
            .thenReturn(5)
            .thenReturn(0)

        // act
        worker.doWork()

        // assert
        verify(mockGenerator, times(1)).generateMetadata(anyInt())
        verifyNoMoreInteractions(mockCrashReporter)
    }

    @Test
    fun generate_2_batches() {
        // arrange
        `when`(mockGenerator.generateMetadata(anyInt()))
            .thenReturn(25)
            .thenReturn(5)

        // act
        worker.doWork()

        // assert
        verify(mockGenerator, times(2)).generateMetadata(anyInt())
        verifyNoMoreInteractions(mockCrashReporter)
    }

    @Test
    fun generate_times_out() {
        // arrange
        `when`(mockGenerator.generateMetadata(anyInt())).thenReturn(25)
        setupTime(1000, 60)

        // act
        worker.doWork()

        // assert
        // after 5 mins the worker should time out
        verify(mockGenerator, times(5)).generateMetadata(anyInt())
        verifyNoMoreInteractions(mockCrashReporter)
    }

    @Test
    fun generate_throws() {
        // arrange
        `when`(mockGenerator.generateMetadata(anyInt())).thenAnswer {
            throw TrackLoadException("UNIT TESTS", null)
        }

        // act
        worker.doWork()

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(MockitoHelper.capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect", true, exception is TrackLoadException)
        verifyNoMoreInteractions(mockCrashReporter)
    }
}