/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.TrackRenamedEvent
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class RenameTrackTests : TrackDisplayPresenterSetup() {

    private var trackRenamedEventSubscriber: IEventBusSubscription? = null
    private var publishedTrackRenamedEvent: TrackRenamedEvent? = null

    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()
        setupEventSubscriber()

        presenter.bind(mockview, null)
        presenter.start()
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    private fun setupEventSubscriber() {
        // we dont mock the event bus because it will run quite happily in the unit tests so lets test as much as we can
        publishedTrackRenamedEvent = null   // reset the event we captured
        trackRenamedEventSubscriber = eventBus.subscribe(TrackRenamedEvent::class.java)
        { event ->
            publishedTrackRenamedEvent = event
        }
    }

    @Test
    fun displayed_track_rename_prompts() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)

        // act
        presenter.actionSelected(R.id.action_rename_track)

        // assert
        verify(mockview, times(1)).promptForTrackName("456", "TEST TRACK")
    }

    @Test
    fun displayed_track_rename_confirmed_with_mismatched_id_ignored() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        presenter.actionSelected(R.id.action_rename_track)

        // act
        presenter.confirmedRename("999", "ID INCORRECT")

        // assert
        verify(mockTrackRepository, never()).update(anyObject())
        assertNull(publishedTrackRenamedEvent)
    }

    @Test
    fun displayed_track_rename_confirmed_updates_db() {
        // arrange
        val displayedTrack = setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        presenter.actionSelected(R.id.action_rename_track)

        // act
        presenter.confirmedRename("456", "NAME EDITED")

        // assert
        verify(mockTrackRepository, times(1)).update(displayedTrack)
        assertEquals("NAME EDITED", displayedTrack.name)
        // the UI will update because the event fires
        verify(mockview, times(1)).setTitle("NAME EDITED")
    }

    @Test
    fun displayed_track_rename_confirmed_fires_event() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        presenter.actionSelected(R.id.action_rename_track)

        // act
        presenter.confirmedRename("456", "NAME EDITED")

        // assert
        assertEquals(456L, publishedTrackRenamedEvent?.trackId)
        assertEquals("TEST TRACK", publishedTrackRenamedEvent?.oldName)
        assertEquals("NAME EDITED", publishedTrackRenamedEvent?.newName)
    }

    @Test
    fun recording_track_rename_prompts() {
        // arrange
        startRecording()
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)
        setupDisplayRecordingTrack()
        presenter.displayTrack(RECORDING_TRACK_ID)

        // act
        presenter.actionSelected(R.id.action_rename_track)

        // assert
        verify(mockview, times(1)).promptForTrackName("123", "TEST TRACK")
    }

    @Test
    fun recording_track_rename_confirmed_updates_db() {
        // arrange
        startRecording()
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)
        setupDisplayRecordingTrack()
        presenter.displayTrack(RECORDING_TRACK_ID)
        presenter.actionSelected(R.id.action_rename_track)

        // act
        presenter.confirmedRename("123", "NAME EDITED")

        // assert
        verify(mockTrackRepository, times(1)).update(currentRecordingTrack)
        assertEquals("NAME EDITED", currentRecordingTrack.name)
        // the UI will update because the event fires
        verify(mockview, times(1)).setTitle("TEST_TITLE_REC TEST TRACK")
    }

    @Test
    fun recording_track_rename_confirmed_fires_event() {
        // arrange
        startRecording()
        currentRecordingTrack.addNewPosition(position1)
        currentRecordingTrack.addNewPosition(position2)
        setupDisplayRecordingTrack()
        presenter.displayTrack(RECORDING_TRACK_ID)
        presenter.actionSelected(R.id.action_rename_track)

        // act
        presenter.confirmedRename("123", "NAME EDITED")

        // assert
        assertEquals(123L, publishedTrackRenamedEvent?.trackId)
        assertEquals("TEST TRACK", publishedTrackRenamedEvent?.oldName)
        assertEquals("NAME EDITED", publishedTrackRenamedEvent?.newName)
    }
}