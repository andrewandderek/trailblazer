/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.worker.trackmetadata

import android.app.Application
import androidx.work.WorkerParameters
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.SystemTimeHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.metadata.track.IBatchTrackMetadataGenerator
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.ISystemTime
import com.andrewandderek.trailblazer.worker.trackmetadata.TrackMetadataWorker
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class WorkerSetup : MockingAnnotationSetup() {
    protected lateinit var worker: TrackMetadataWorker

    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var mockSystemTime: ISystemTime

    @Mock protected lateinit var mockApplication: Application
    @Mock protected lateinit var mockParameters: WorkerParameters
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockGenerator: IBatchTrackMetadataGenerator

    @Captor protected lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    protected fun setupWorker() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockSystemTime = SystemTimeHelper.setupMockSystemTime()

        worker = TrackMetadataWorker(
            mockApplication,
            mockParameters,
            mockLoggerFactory,
            mockCrashReporter,
            mockSystemTime,
            mockGenerator
        )
    }

    protected fun setupTime(initial: Long, gapInSeconds: Long) {
        val gapInNanoseconds = gapInSeconds * 1_000_000_000
        `when`(mockSystemTime.getNanoTime())
            .thenReturn(initial)
            .thenReturn(initial + (gapInNanoseconds * 1))
            .thenReturn(initial + (gapInNanoseconds * 2))
            .thenReturn(initial + (gapInNanoseconds * 3))
            .thenReturn(initial + (gapInNanoseconds * 4))
            .thenReturn(initial + (gapInNanoseconds * 5))
            .thenReturn(initial + (gapInNanoseconds * 6))
            .thenReturn(initial + (gapInNanoseconds * 7))
            .thenReturn(initial + (gapInNanoseconds * 8))
            .thenReturn(initial + (gapInNanoseconds * 9))
            .thenReturn(initial + (gapInNanoseconds * 10))
    }
}