/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.units.formatting

import com.andrewandderek.trailblazer.units.PreferredUnits
import com.andrewandderek.trailblazer.units.formatting.PaceFormatter
import com.andrewandderek.trailblazer.unittests.units.UnitsSetup
import org.junit.Assert
import org.junit.Test

class PaceTests : UnitsSetup() {

    @Test
    fun formatPace_zero_metric() {
        // arrange
        setupUnits(PreferredUnits.METRIC.toString())

        // act
        val result = paceFormatter.formatPace(0.0, true)

        // assert
        Assert.assertEquals("0:00 PACE_LABEL_METRIC", result)
    }

    @Test
    fun formatPace_zero_imperial() {
        // arrange
        setupUnits(PreferredUnits.IMPERIAL.toString())

        // act
        val result = paceFormatter.formatPace(0.0, true)

        // assert
        Assert.assertEquals("0:00 PACE_LABEL_IMPERIAL", result)
    }

    @Test
    fun formatPace_metric() {
        // arrange
        setupUnits(PreferredUnits.METRIC.toString())

        // act
        val result = paceFormatter.formatPace(4.0, true)

        // assert
        Assert.assertEquals("4:10 PACE_LABEL_METRIC", result)
    }

    @Test
    fun formatPace_imperial() {
        // arrange
        setupUnits(PreferredUnits.IMPERIAL.toString())

        // act
        val result = paceFormatter.formatPace(4.0, true)

        // assert
        Assert.assertEquals("6:42 PACE_LABEL_IMPERIAL", result)
    }
}