/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.helpers

import com.andrewandderek.trailblazer.utility.ISystemTime
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import java.util.Calendar

object SystemTimeHelper {
    fun testDate(): Calendar {
        var randomDate1 = Calendar.getInstance()
        randomDate1.set(2017,12,13, 20, 13, 14)
        return randomDate1
    }

    fun setupMockSystemTime(): ISystemTime {
        return setupMockSystemTime(testDate())
    }

    fun setupMockSystemTime(date: Calendar): ISystemTime {
        var mockSystemTime = mock(ISystemTime::class.java)
        `when`(mockSystemTime.getCurrentTime()).thenReturn(date)
        return mockSystemTime
    }
}