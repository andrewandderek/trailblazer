/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class MultiselectTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
        setupTrackListItems(3)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun items_not_selected_after_Loading() {
        // arrange
        presenter.loadTracks()

        // act

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(1))
    }

    @Test
    fun select_item_starts_multiselect() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.trackLongSelected(1, 0)

        // assert
        verify(mockview, times(1)).startMultiSelectMode()
    }


    @Test
    fun item_1_selected_after_Loading() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(1))
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 1")
    }

    @Test
    fun item_2_selected_after_Loading() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.trackLongSelected(2, 1)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(1))
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 1")
    }

    @Test
    fun item_1_and_2_selected_after_Loading() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(2, 1)

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(1))
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 1")
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 2")
    }

    @Test
    fun item_1_toggled_and_2_selected_after_Loading() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(2, 1)
        presenter.trackSelected(1, 0)

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(1))
        verify(mockview, times(2)).updateList("TEST_SELECTION_TITLE 1")
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 2")
    }

    @Test
    fun item_1_and_2_toggled_after_Loading() {
        // arrange
        presenter.loadTracks()

        // act
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(2, 1)
        presenter.trackSelected(1, 0)
        presenter.trackSelected(2, 1)

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(1))
        verify(mockview, times(2)).updateList("TEST_SELECTION_TITLE 1")
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 2")
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 0")
    }

    @Test
    fun multiselect_ended() {
        // arrange
        presenter.loadTracks()
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
        presenter.trackSelected(2, 1)

        // act
        reset(mockview)
        presenter.inMultiSelectMode = false

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",false, presenter.isItemSelected(1))
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 0")
    }

    @Test
    fun select_all_selects_all_visible_tracks() {
        // arrange
        loadTracksAndSelect1()

        // act
        presenter.onActionItemClicked(R.id.action_select_all)

        // assert
        assertEquals("incorrect number of items",3, presenter.itemCount)
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(0))
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(1))
        assertEquals("item incorrectly selected",true, presenter.isItemSelected(2))
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 1")
        verify(mockview, times(1)).updateList("TEST_SELECTION_TITLE 3")
    }

}