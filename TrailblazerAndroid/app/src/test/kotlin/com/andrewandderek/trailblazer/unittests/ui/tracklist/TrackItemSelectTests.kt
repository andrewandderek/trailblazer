/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class TrackItemSelectTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        presenter.bind(mockview, null)
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun trackList_track_out_of_range_Selected() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        presenter.trackSelected(-1, -1)

        // assert
        verify(mockview, never()).navigateToTrack(anyLong())
    }

    @Test
    fun trackList_track_1_Selected() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        presenter.trackSelected(1, 0)

        // assert
        verify(mockview, times(1)).navigateToTrack(1L)
    }

    @Test
    fun trackList_track_2_Selected() {
        // arrange
        setupTrackListItems(2)
        presenter.loadTracks()

        // act
        presenter.trackSelected(2, 1)

        // assert
        verify(mockview, times(1)).navigateToTrack(2L)
    }
}