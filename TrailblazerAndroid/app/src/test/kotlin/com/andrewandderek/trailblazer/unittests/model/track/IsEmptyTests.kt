/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class IsEmptyTests : TrackSetup() {

    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun isEmpty_whenEmpty() {
        // arrange
        val track = setupEmptyTrack()

        // act
        var result = track.isEmpty

        // assert
        assertEquals(true, result)
    }

    @Test
    fun isNotEmpty_whenNotEmpty() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)

        // act
        var result = track.isEmpty

        // assert
        assertEquals(false, result)
        assertEquals("double check the test data", 2, track.currentPositionsSnapshot.count())
    }

    @Test
    fun isEmpty_whenReset() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)
        track.reset()

        // act
        var result = track.isEmpty

        // assert
        assertEquals(true, result)
    }

}


