/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class HasNoAltitudeDataTests : TrackSetup() {

    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun HasNoAltitudeData_whenEmpty() {
        // arrange
        val track = setupEmptyTrack()

        // act
        var result = track.hasNoAltitudeData

        // assert
        Assert.assertEquals(true, result)
    }

    @Test
    fun HasNoAltitudeData_not_whenHas() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)

        // act
        var result = track.hasNoAltitudeData

        // assert
        Assert.assertEquals(false, result)
        Assert.assertEquals("double check the test data", 2, track.currentPositionsSnapshot.count())
    }

    @Test
    fun HasNoAltitudeData_whenNoTimestamps() {
        // arrange
        val track = setupTrackWithTwoPoints(false, true)

        // act
        var result = track.hasNoAltitudeData

        // assert
        Assert.assertEquals(true, result)
        Assert.assertEquals("double check the test data", 2, track.currentPositionsSnapshot.count())
    }

    @Test
    fun HasNoAltitudeData_not_whenReset() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)
        track.reset()

        // act
        var result = track.hasNoAltitudeData

        // assert
        Assert.assertEquals(true, result)
    }

}