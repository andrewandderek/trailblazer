/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.service.gpslogger.trackrecorder

import com.andrewandderek.trailblazer.helpers.MockitoHelper
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class NotificationUpdateTests : TrackRecorderSetup() {
    @Before
    fun before_each_test() {
        setupTrackRecorder()
        createTrackInRepository()
    }

    @Test
    fun paused_has_correct_content() {
        // arrange
        Mockito.`when`(mockSystemTime.getCurrentTime()).thenReturn(getTestNow())
        Mockito.`when`(mockStatisticsProvider.getStatistics(MockitoHelper.anyObject())).thenReturn(getStatistics(123.45, 65 * 1000))
        trackRecorder.createNewTrack()

        // act
        trackRecorder.pause(false)

        // assert
        Assert.assertEquals("TEST_NOTIFICATION_TITLE_PAUSED", trackRecorder.getNotificationTitle())
        Assert.assertEquals("TEST_NOTIFICATION_CONTENT 00:01:05, 0.12 DISTANCE_LABEL_METRIC, 0:00 PACE_LABEL_METRIC", trackRecorder.getNotificationText())
        Assert.assertEquals("TEST_NOTIFICATION_ACCESSIBILITY 15:37", trackRecorder.getNotificationAccessibilityText())
        Assert.assertEquals(getTestNow().timeInMillis, trackRecorder.getNotificationWhenTimeInMilliseconds())
    }

    @Test
    fun recording_has_correct_content() {
        // arrange
        Mockito.`when`(mockSystemTime.getCurrentTime())
                .thenReturn(getTestNow())   // create track
                .thenReturn(getTestNow())   // first update
                .thenReturn(getTestNow(60))
        Mockito.`when`(mockStatisticsProvider.getStatistics(MockitoHelper.anyObject()))
                .thenReturn(getStatistics(123.45, 65 * 1000))
                .thenReturn(getStatistics(678.90, 125 * 1000))
        trackRecorder.createNewTrack()
        // when the track is started we call all the updates
        trackRecorder.getNotificationWhenTimeInMilliseconds()
        trackRecorder.getNotificationTitle()
        trackRecorder.getNotificationText()
        trackRecorder.getNotificationAccessibilityText()

        // act
        trackRecorder.resume(false)

        // assert
        Assert.assertEquals("TEST_NOTIFICATION_TITLE 15:37", trackRecorder.getNotificationTitle())
        Assert.assertEquals("TEST_NOTIFICATION_CONTENT 00:02:05, 0.68 DISTANCE_LABEL_METRIC, 0:00 PACE_LABEL_METRIC", trackRecorder.getNotificationText())
        Assert.assertEquals("TEST_NOTIFICATION_ACCESSIBILITY 15:37", trackRecorder.getNotificationAccessibilityText())
        Assert.assertEquals(getTestNow(60).timeInMillis, trackRecorder.getNotificationWhenTimeInMilliseconds())
    }

}