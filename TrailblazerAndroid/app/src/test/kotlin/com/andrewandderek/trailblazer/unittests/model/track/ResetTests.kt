/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

class ResetTests : TrackSetup() {
    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun reset_whenNotEmpty() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)

        // act
        track.reset()

        // assert
        var result = track.lastPositionInSnapshot
        assertNull(result)
        assertEquals(true, track.isEmpty)
        assertEquals(-1L, track.id)
        assertEquals(true, track.isNew)
    }

    @Test
    fun resetPositions_whenNotEmpty() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)

        // act
        track.resetPositions()

        // assert
        var result = track.lastPositionInSnapshot
        assertNull(result)
        assertEquals(true, track.isEmpty)
        // the id is not reset
        assertEquals(1L, track.id)
        assertEquals(false, track.isNew)
    }
}

