/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.settings.settings

import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import com.andrewandderek.trailblazer.helpers.MockitoHelper.eq
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.anyString
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class ExportDatabaseTests : SettingsFragmentViewModelSetup() {
    @Captor
    private lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    @Before
    fun before_each_test() {
        setupViewModel()
        setupEventSubscriber()
    }

    @Test
    fun export_starts_progress() {
        // arrange

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockStartProgressObserver, times(1)).onChanged("EXPORT_PROGRESS")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockStartProgressObserver);
    }

    @Test
    fun export_copies_active_to_export_folder() {
        // arrange

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockDatabaseCopier, times(1)).copyDb("ACTIVE_PATHNAME", "EXPORT_PATHNAME", false)
    }

    @Test
    fun export_fails_cancels_progress() {
        // arrange
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenReturn(false)

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockEndProgressObserver, times(1)).onChanged("COPY_ERROR")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun export_succeeds_cancels_progress() {
        // arrange
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenReturn(true)

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockEndProgressObserver, times(1)).onChanged("")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun export_succeeds_displays_chooser() {
        // arrange
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenReturn(true)
        val expected = Pair("EXPORT_CHOOSER", mockIntent)

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockDisplayChooserObserver, times(1)).onChanged(eq(expected))
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockDisplayChooserObserver);
    }

    @Test
    fun exception_thrown_progress_is_cancelled() {
        // arrange
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenThrow(NullPointerException("UNIT TESTS"))

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockEndProgressObserver, times(1)).onChanged("COPY_ERROR")
        // check that there were no more changes of this observer:
        verifyNoMoreInteractions(mockEndProgressObserver);
    }

    @Test
    fun exception_thrown_is_logged() {
        // arrange
        `when`(mockDatabaseCopier.copyDb(anyString(), anyString(), anyBoolean())).thenThrow(NullPointerException("UNIT TESTS"))

        // act
        viewModel.exportDatabase()

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        assertEquals("exception type incorrect", true, exception is NullPointerException)
    }
}

