/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.statistics

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import com.andrewandderek.trailblazer.sharing.importexport.ExportResult
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class ShareScreenshotTests : TrackStatisticsPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
        setupTrack(true, true)
        setupStatistics()
        presenter.bind(mockView)

        Mockito.`when`(mockResourceProvider.getString(R.string.stats_image_share_title)).thenReturn("TEST SHARE TITLE")
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockView)
    }

    @Test
    fun share_analytics_written() {
        // arrange
        presenter.loadTrack(TRACK_ID)

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        Mockito.verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.ACTION_SHARE_STATS_SCREEN_ID, AnalyticsConstants.ACTION_SHARE_STATS_SCREEN_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun share_the_displayed_screen() {
        // arrange
        presenter.loadTrack(TRACK_ID)
        `when`(mockExporter.exportBitmap(MockitoHelper.anyObject(), MockitoHelper.anyObject())).thenReturn(ExportResult.OK)

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockExporter, times(1)).getShareFilename("Track name")
        verify(mockExporter, never()).getExportFilename(MockitoHelper.anyObject(), ArgumentMatchers.anyBoolean())
        verify(mockView, never()).showMessage(anyInt())
        verify(mockView, times(1)).startIntent(mockIntent, "TEST SHARE TITLE")
    }

    @Test
    fun share_failed() {
        // arrange
        presenter.loadTrack(TRACK_ID)
        `when`(mockExporter.exportBitmap(MockitoHelper.anyObject(), MockitoHelper.anyObject())).thenReturn(ExportResult.IO_ERROR)

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockView, times(1)).showMessage(R.string.track_export_error)
    }

    @Test
    fun share_throws() {
        // arrange
        presenter.loadTrack(TRACK_ID)
        `when`(mockExporter.exportBitmap(MockitoHelper.anyObject(), MockitoHelper.anyObject())).thenThrow(NullPointerException())

        // act
        presenter.actionSelected(R.id.action_share_track)

        // assert
        verify(mockView, times(1)).showMessage(R.string.track_export_error)
    }
}