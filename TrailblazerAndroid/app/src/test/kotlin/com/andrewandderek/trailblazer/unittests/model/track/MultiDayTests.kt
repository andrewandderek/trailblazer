/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class MultiDayTests : TrackSetup() {
    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun multiday_when_0_segments() {
        // arrange

        // act
        val track = setupEmptyTrack()

        // assert
        assertFalse(track.isMultiDayTrack)
        assertFalse(track.isMultiDaySegmentedTrack)
    }

    @Test
    fun multiday_when_1_segment() {
        // arrange

        // act
        val track = setupTrackWithTwoPoints(true, true)

        // assert
        assertFalse(track.isMultiDayTrack)
        assertFalse(track.isMultiDaySegmentedTrack)
    }

    @Test
    fun multiday_when_2_segments() {
        // arrange

        // act
        val track = setupSegmentedTrack(true, true)

        // assert
        assertFalse(track.isMultiDayTrack)
        assertFalse(track.isMultiDaySegmentedTrack)
    }

    @Test
    fun multiday_when_2_segments_multiday() {
        // arrange

        // act
        val track = setupMultiDaySegmentedTrack(true)

        // assert
        assertTrue(track.isMultiDayTrack)
        assertTrue(track.isMultiDaySegmentedTrack)
    }
}