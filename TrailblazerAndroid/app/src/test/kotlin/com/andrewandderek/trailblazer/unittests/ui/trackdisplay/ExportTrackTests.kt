/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.ShareException
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.MockitoHelper.capture
import com.andrewandderek.trailblazer.helpers.SystemTimeHelper
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.*

class ExportTrackTests : TrackDisplayPresenterSetup() {
    @Captor protected lateinit var loggingCaptor: ArgumentCaptor<Throwable>

    @Before
    fun before_each_test() {
        setupPresenter()
        setupNowDateTimes()
        setupPoints()

        presenter.bind(mockview, null)
        presenter.start()

        `when`(mockResourceProvider.getString(R.string.track_exported_fmt)).thenReturn("TEST MESSAGE %s")
    }

    @After
    fun after_each_test() {
        presenter.stop()
        presenter.unbind(mockview)
    }

    @Test
    fun export_track_prompts() {
        // arrange

        // act
        presenter.exportTrack()

        // assert
        verify(mockview, times(1)).promptForFileFormat("TEST_EXPORT_PROMPT",NameLiterals.FILE_FORMAT_GPX)
    }

    @Test
    fun nothing_to_export() {
        // arrange

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockview, times(1)).showMessage(R.string.track_not_exported)
        verify(mockview, never()).showMessage(anyString())
    }

    @Test
    fun export_analytics_written() {
        // arrange
        startRecording()

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.ACTION_EXPORT_ID, AnalyticsConstants.ACTION_EXPORT_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun export_kml_analytics_written() {
        // arrange
        startRecording()

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_KML)

        // assert
        verify(mockAnalyticsEngine, times(1)).recordClick(AnalyticsConstants.ACTION_EXPORT_KML_ID, AnalyticsConstants.ACTION_EXPORT_KML_NAME, AnalyticsConstants.ACTION_TYPE)
    }

    @Test
    fun empty_track_to_export() {
        // arrange
        startRecording()

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockview, times(1)).showMessage(R.string.track_not_exported)
        verify(mockview, never()).showMessage(anyString())
    }

    @Test
    fun export_failed() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(),anyString())).thenReturn(false)

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockview, times(1)).showMessage("TEST MESSAGE EXPORT FILENAME")
    }

    @Test
    fun export_throws() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(),anyString())).thenThrow(NullPointerException())

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockview, times(1)).showMessage(R.string.track_export_error)
        verify(mockview, never()).showMessage(anyString())
    }

    @Test
    fun export_throws_and_logs() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(),anyString())).thenThrow(NullPointerException())

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockCrashReporter, times(1)).logNonFatalException(capture(loggingCaptor))
        val exception = loggingCaptor.value
        Assert.assertEquals("exception type incorrect",true, exception is ShareException)
        Assert.assertEquals("inner exception type incorrect",true, exception.cause is NullPointerException)
    }

    @Test
    fun export_throws_works_second_time() {
        // arrange
        setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(),anyString()))
                .thenThrow(NullPointerException())
                .thenReturn(true)
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        // first time fails
        verify(mockview, times(1)).showMessage(R.string.track_export_error)
        // second time succeeds
        verify(mockview, times(1)).showMessage("TEST MESSAGE EXPORT FILENAME")
    }

    @Test
    fun export_the_displayed_track() {
        // arrange
        val displayTrack = setupDisplayTrackWithTwoPoints()
        presenter.displayTrack(DISPLAY_TRACK_ID)
        `when`(mockGpxExporter.exportTrack(anyObject(),anyString())).thenReturn(true)

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockGpxExporter, times(1)).getExportFilename("2018-01-13-TEST TRACK", true)
        verify(mockGpxExporter, times(1)).exportTrack(displayTrack, "EXPORT FILENAME")
        verify(mockGpxExporter, never()).getShareFilename(anyObject())
        verify(mockKmlExporter, never()).exportTrack(anyObject(), anyString())
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).showMessage("TEST MESSAGE EXPORT FILENAME")
        verify(mockview, times(2)).startProgress(anyInt())
        verify(mockview, times(2)).completeProgress()
    }

    @Test
    fun export_the_recording_track() {
        // arrange
        startRecording()
        position1 = Position(1.0, 2.0, 3.0, SystemTimeHelper.testDate(), 100F)
        position1.trackId = currentRecordingTrack.id
        currentRecordingTrack.addNewPosition(position1)

        `when`(mockGpxExporter.exportTrack(anyObject(),anyString())).thenReturn(true)

        // act
        presenter.exportTrack(NameLiterals.FILE_FORMAT_GPX)

        // assert
        verify(mockGpxExporter, times(1)).getExportFilename("2018-01-13-TEST TRACK", true)
        verify(mockGpxExporter, times(1)).exportTrack(currentRecordingTrack, "EXPORT FILENAME")
        verify(mockGpxExporter, never()).getShareFilename(anyObject())
        verify(mockKmlExporter, never()).exportTrack(anyObject(), anyString())
        verify(mockview, never()).showMessage(anyInt())
        verify(mockview, times(1)).showMessage("TEST MESSAGE EXPORT FILENAME")
        verify(mockview, times(1)).startProgress(anyInt())
        verify(mockview, times(1)).completeProgress()
    }
}