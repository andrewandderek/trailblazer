/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

class TrackItemDeleteTests : TrackListPresenterSetup() {

    @Before
    fun before_each_test() {
        `when`(mockResourceProvider.getString(R.string.track_list_delete_prompt_fmt)).thenReturn("TEST_DELETE %s")
        `when`(mockResourceProvider.getString(R.string.track_list_delete_recording_prompt_fmt)).thenReturn("TEST_DELETE_RECORDING %s")
    }

    @After
    fun after_each_test() {
        presenter.unbind(mockview)
    }

    @Test
    fun trackList_track_out_of_range_deleted() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        presenter.loadTracks()

        // act
        presenter.deleteTrack(-1)

        // assert
        verify(mockview, never()).promptToDelete(anyString(), anyString())
    }

    @Test
    fun trackList_track_1_deleted() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        presenter.loadTracks()

        // act
        presenter.deleteTrack(1)

        // assert
        verify(mockview, times(1)).promptToDelete("TEST_DELETE Track With points", "1")
    }

    @Test
    fun trackList_track_1_recording_deleted() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        trackIsBeingRecorded(trackWithPositions)
        presenter.loadTracks()

        // act
        presenter.deleteTrack(1)

        // assert
        verify(mockview, times(1)).promptToDelete("TEST_DELETE_RECORDING Track With points", "1")
    }

    @Test
    fun trackList_track_1_paused_deleted() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        trackIsBeingPaused(trackWithPositions)
        presenter.loadTracks()

        // act
        presenter.deleteTrack(1)

        // assert
        verify(mockview, times(1)).promptToDelete("TEST_DELETE_RECORDING Track With points", "1")
    }

    @Test
    fun trackList_track_1_delete_confirmed() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        presenter.loadTracks()
        presenter.deleteTrack(1)

        // act
        presenter.confirmedDeleteTrack("1")

        // assert
        // we load up 2 tracks
        verify(mockview, times(1)).setTitle("TEST_TITLE 2")
        // the title is updated after we delete
        verify(mockview, times(1)).setTitle("TEST_TITLE")
        verify(mockview, times(1)).showMessage(R.string.track_deleted)
        verify(mockTrackRepository, times(1)).deleteById(1)
        verify(mockview, times(2)).tracksLoaded()
        // we do not stop the service
        verify(mockservice, never()).stopTracking(false)
    }

    @Test
    fun trackList_track_1_delete_confirmed_delete_again() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        presenter.loadTracks()
        presenter.deleteTrack(1)
        presenter.confirmedDeleteTrack("1")

        // act
        presenter.deleteTrack(2)

        // assert
        verify(mockview, times(1)).promptToDelete("TEST_DELETE Track With points", "1")
        verify(mockview, times(1)).promptToDelete("TEST_DELETE Track Without points", "2")
    }

    @Test
    fun trackList_track_1_delete_confirmed_delete_again_before_complete() {
        // arrange
        val scheduler = TestScheduler()
        setupPresenter(scheduler)
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        presenter.loadTracks()
        scheduler.triggerActions()
        scheduler.shutdown()    // this will stop the first delete from completing
        presenter.deleteTrack(1)
        presenter.confirmedDeleteTrack("1")

        // act
        presenter.deleteTrack(2)

        // assert
        // the first track is loaded
        verify(mockview, times(1)).promptToDelete("TEST_DELETE Track With points", "1")
        // the second is rejected
        verify(mockview, times(1)).showMessage(R.string.track_being_deleted)
    }

    @Test
    fun trackList_track_1_recording_delete_confirmed() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        trackIsBeingRecorded(trackWithPositions)
        presenter.loadTracks()
        presenter.deleteTrack(1)

        // act
        presenter.confirmedDeleteTrack("1")

        // assert
        verify(mockservice, times(1)).stopTracking(false)
    }

    @Test
    fun trackList_track_1_paused_delete_confirmed() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        trackIsBeingPaused(trackWithPositions)
        presenter.loadTracks()
        presenter.deleteTrack(1)

        // act
        presenter.confirmedDeleteTrack("1")

        // assert
        verify(mockservice, times(1)).stopTracking(false)
    }

    @Test
    fun trackList_track_1_delete_confirmed_track_2_recording() {
        // arrange
        setupPresenter()
        setupTrackListItems(2)
        presenter.bind(mockview, null)
        trackIsBeingRecorded(trackWithoutPositions)
        presenter.loadTracks()
        presenter.deleteTrack(1)

        // act
        presenter.confirmedDeleteTrack("1")

        // assert
        verify(mockservice, never()).stopTracking(false)
    }

}