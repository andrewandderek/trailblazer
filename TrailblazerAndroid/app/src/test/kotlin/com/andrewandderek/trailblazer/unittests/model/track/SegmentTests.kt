/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.model.track

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class SegmentTests : TrackSetup() {
    @Before
    fun before_each_test() {
        setupNowDateTimes()
    }

    @Test
    fun segments_when_0_segments() {
        // arrange
        val track = setupEmptyTrack()

        // act
        var result = track.currentPositionsSnapshotSegments

        // assert
        assertNull(result)
        assertNull(track.lastPositionInSnapshotSegment)
        assertFalse(track.isSegmentedTrack)
        assertFalse(track.isMultiDaySegmentedTrack)
    }

    @Test
    fun segments_when_1_segment() {
        // arrange
        val track = setupTrackWithTwoPoints(true, true)

        // act
        var result = track.currentPositionsSnapshotSegments

        // assert
        assertNotNull(result)
        assertEquals(0L, track.lastPositionInSnapshotSegment)
        assertEquals(1, result!!.size)
        assertEquals(2, result.get(0).size)
        assertFalse(track.isSegmentedTrack)
        assertFalse(track.isMultiDaySegmentedTrack)
    }

    @Test
    fun segments_when_2_segments() {
        // arrange
        val track = setupSegmentedTrack(true, true)

        // act
        var result = track.currentPositionsSnapshotSegments

        // assert
        assertNotNull(result)
        assertEquals(2L, track.lastPositionInSnapshotSegment)
        assertEquals(3, result!!.size)
        assertEquals(2, result.get(0).size)
        assertEquals(0, result.get(1).size)
        assertEquals(1, result.get(2).size)
        assertTrue(track.isSegmentedTrack)
        assertFalse(track.isMultiDaySegmentedTrack)
    }

    @Test
    fun segments_when_2_segments_multiday() {
        // arrange
        val track = setupMultiDaySegmentedTrack(true)

        // act
        var result = track.currentPositionsSnapshotSegments

        // assert
        assertNotNull(result)
        assertEquals(2L, track.lastPositionInSnapshotSegment)
        assertEquals(3, result!!.size)
        assertEquals(2, result.get(0).size)
        assertEquals(0, result.get(1).size)
        assertEquals(1, result.get(2).size)
        assertTrue(track.isSegmentedTrack)
        assertTrue(track.isMultiDaySegmentedTrack)
    }
}

