/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.statistics

import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.model.TrackAndStatistics
import com.andrewandderek.trailblazer.statistics.ITrackGroupSorter.SortBy
import com.andrewandderek.trailblazer.statistics.TrackGroupSorter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class TrackGroupSorterTests : MockingAnnotationSetup() {
    private lateinit var sorter: TrackGroupSorter

    private var allTracks: ArrayList<TrackAndStatistics> = ArrayList()
    private lateinit var trackAndStatistics1: TrackAndStatistics
    private lateinit var trackAndStatistics2: TrackAndStatistics
    private lateinit var trackAndStatistics3: TrackAndStatistics

    @Before
    fun setupTest() {
        sorter = TrackGroupSorter()
        setupAllTracks()
    }

    private fun setupAllTracks() {
        trackAndStatistics1 = TrackAndStatistics(TrackHelper.setupTrack(1), TrackHelper.testTrackStatistics1())
        trackAndStatistics2 = TrackAndStatistics(TrackHelper.setupTrack(2), TrackHelper.testTrackStatistics2())
        trackAndStatistics3 = TrackAndStatistics(TrackHelper.setupTrack(3), TrackHelper.testTrackStatistics3())
        allTracks.add(trackAndStatistics1)
        allTracks.add(trackAndStatistics2)
        allTracks.add(trackAndStatistics3)
    }

    @Test
    fun sort_moving_time_asc() {
        // arrange
        val expected =
                listOf(
                        trackAndStatistics3,
                        trackAndStatistics2,
                        trackAndStatistics1
                )

        // act
        val result = sorter.sortTracks(allTracks, SortBy.MOVING_TIME, true)

        //assert
        assertEquals(expected, result)
    }

    @Test
    fun sort_moving_time_desc() {
        val expected =
                listOf(
                        trackAndStatistics1,
                        trackAndStatistics2,
                        trackAndStatistics3
                )

        // act
        val result = sorter.sortTracks(allTracks, SortBy.MOVING_TIME, false)

        //assert
        assertEquals(expected, result)
    }

    @Test
    fun sort_distance_asc() {
        val expected =
                listOf(
                        trackAndStatistics3,
                        trackAndStatistics1,
                        trackAndStatistics2
                )

        // act
        val result = sorter.sortTracks(allTracks, SortBy.DISTANCE, true)

        //assert
        assertEquals(expected, result)
    }

    @Test
    fun sort_distance_desc() {
        val expected =
                listOf(
                        trackAndStatistics2,
                        trackAndStatistics1,
                        trackAndStatistics3
                )

        // act
        val result = sorter.sortTracks(allTracks, SortBy.DISTANCE, false)

        //assert
        Assert.assertEquals(expected, result)
    }

    @Test
    fun sort_speed_asc() {
        val expected =
                listOf(
                        trackAndStatistics1,
                        trackAndStatistics2,
                        trackAndStatistics3
                )

        // act
        val result = sorter.sortTracks(allTracks, SortBy.SPEED, true)

        //assert
        assertEquals(expected, result)
    }

    @Test
    fun sort_speed_desc() {
        val expected =
                listOf(
                        trackAndStatistics3,
                        trackAndStatistics2,
                        trackAndStatistics1
                )

        // act
        val result = sorter.sortTracks(allTracks, SortBy.SPEED, false)

        //assert
        assertEquals(expected, result)
    }

}