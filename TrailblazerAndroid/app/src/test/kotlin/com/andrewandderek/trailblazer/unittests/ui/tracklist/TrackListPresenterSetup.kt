/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.tracklist

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.RxEventBus
import com.andrewandderek.trailblazer.helpers.LoggingHelper
import com.andrewandderek.trailblazer.helpers.MockitoHelper.anyObject
import com.andrewandderek.trailblazer.helpers.RxHelper
import com.andrewandderek.trailblazer.helpers.TrackHelper
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.sharing.importexport.IExporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.ITrackExporter
import com.andrewandderek.trailblazer.ui.tracklist.ITrackListView
import com.andrewandderek.trailblazer.ui.tracklist.TrackListPresenter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.unittests.MockingAnnotationSetup
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.utility.ISystemTime
import com.andrewandderek.trailblazer.utility.ITextMatcher
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.schedulers.TestScheduler
import io.reactivex.rxjava3.subjects.PublishSubject
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.anyBoolean
import org.mockito.Mockito.anyDouble
import org.mockito.Mockito.`when`
import org.mockito.stubbing.Answer
import java.util.Calendar

open class TrackListPresenterSetup : MockingAnnotationSetup() {
    companion object {
        const val EXPORT_FILENAME = "EXPORT FILENAME"
        const val SHARE_FILENAME = "SHARE FILENAME"
        const val EXPORT_FILENAME_1 = "EXPORT FILENAME 1"
        const val EXPORT_FILENAME_2 = "EXPORT FILENAME 2"
        const val EXPORT_FILENAME_3 = "EXPORT FILENAME 3"
        const val IMPORT_APP_FOLDER = "IMPORT APP FOLDER"

        const val COMBINED_NAME = "COMBINED NAME"
    }

    protected lateinit var presenter: TrackListPresenter

    protected lateinit var eventBus: IEventBus
    protected lateinit var mockLoggerFactory: ILoggerFactory
    private lateinit var mockScheduler: ISchedulerProvider

    @Mock protected lateinit var mockview: ITrackListView
    @Mock protected lateinit var mockTrackRepository: ITrackRepository
    @Mock protected lateinit var mockPositionRepository: IPositionRepository
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockservice: IGpsLoggerService
    @Mock protected lateinit var mockbinder: GpsLoggerService.LocalBinder
    @Mock protected lateinit var mockServiceController: IGpsLoggerServiceController
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockExporterFactory: IExporterFactory
    @Mock protected lateinit var mockAnalyticsEngine: IAnalyticsEngine
    @Mock protected lateinit var mockPermissionChecker: IPermissionChecker
    @Mock protected lateinit var mockGpxExporter: ITrackExporter
    @Mock protected lateinit var mockKmlExporter: ITrackExporter
    @Mock protected lateinit var mockFilesystemHelper: IFileSystemHelper
    @Mock protected lateinit var mockTrackMatcher: ITextMatcher
    @Mock protected lateinit var mockSystemTime: ISystemTime
    @Mock protected lateinit var mockDistanceFormatter: IDistanceFormatter
    @Mock protected lateinit var mockPaceFormatter: IPaceFormatter
    @Mock protected lateinit var mockSpeedFormatter: ISpeedFormatter

    private var observableTrack: PublishSubject<Track> = PublishSubject.create()

    protected fun setupPresenter(scheduler: TestScheduler? = null) {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        mockScheduler = if (scheduler == null) RxHelper.setupSchedulerProvider() else RxHelper.setupTestSchedulerProvider(scheduler)
        `when`(mockSystemTime.getCurrentTime()).then(Answer {
            var now = Calendar.getInstance()
            now.set(2020, 1, 1, 1, 2, 3)
            return@Answer now
        })
        eventBus = RxEventBus(
            mockLoggerFactory,
            mockCrashReporter,
            mockScheduler
        )
        presenter = TrackListPresenter(
            mockLoggerFactory,
            mockScheduler,
            mockTrackRepository,
            mockPositionRepository,
            mockResourceProvider,
            mockServiceController,
            mockCrashReporter,
            eventBus,
            mockExporterFactory,
            mockAnalyticsEngine,
            mockPermissionChecker,
            mockFilesystemHelper,
            mockTrackMatcher,
            mockSystemTime,
            mockDistanceFormatter,
            mockPaceFormatter,
            mockSpeedFormatter,
        )

        `when`(mockbinder.service).thenReturn(mockservice)

        `when`(mockservice.currentTrack).thenReturn(observableTrack.toFlowable(BackpressureStrategy.ERROR))
        `when`(mockGpxExporter.getExportFilename(anyObject(), Mockito.anyBoolean()))
                .thenReturn(EXPORT_FILENAME_1)
                .thenReturn(EXPORT_FILENAME_2)
                .thenReturn(EXPORT_FILENAME_3)
        `when`(mockGpxExporter.getShareFilename(anyObject())).thenReturn(SHARE_FILENAME)
        `when`(mockExporterFactory.getExporter(NameLiterals.FILE_FORMAT_GPX)).thenReturn(mockGpxExporter)
        `when`(mockKmlExporter.getExportFilename(anyObject(), Mockito.anyBoolean())).thenReturn(EXPORT_FILENAME)
        `when`(mockKmlExporter.getShareFilename(anyObject())).thenReturn(SHARE_FILENAME)
        `when`(mockExporterFactory.getExporter(NameLiterals.FILE_FORMAT_KML)).thenReturn(mockKmlExporter)
        `when`(mockFilesystemHelper.getApplicationFolderOnSdCard()).thenReturn(IMPORT_APP_FOLDER)

        `when`(mockResourceProvider.getString(R.string.track_list_title)).thenReturn("TEST_TITLE")
        `when`(mockResourceProvider.getString(R.string.track_list_title_fmt)).thenReturn("TEST_TITLE %s")
        `when`(mockResourceProvider.getString(R.string.track_list_multi_select_action_mode_title_fmt)).thenReturn("TEST_SELECTION_TITLE %s")

        `when`(mockResourceProvider.getString(R.string.track_list_multi_delete_prompt_fmt)).thenReturn("TEST_SELECTION_DELETE %s")
        `when`(mockResourceProvider.getString(R.string.track_list_multi_delete_recording_prompt_fmt)).thenReturn("TEST_SELECTION_DELETE_REC %s")
        `when`(mockResourceProvider.getString(R.string.track_multi_deleted_fmt)).thenReturn("TEST_MULTI_DELETE %s")

        `when`(mockResourceProvider.getString(R.string.file_format_export_prompt)).thenReturn("TEST_FILE_FORMAT_EXPORT")
        `when`(mockResourceProvider.getString(R.string.track_multi_exported_fmt)).thenReturn("TEST_MULTI_EXPORT %s")

        `when`(mockResourceProvider.getString(R.string.combine_track_name_fmt)).thenReturn("COMBINE_TRACK_NAME %s")
        `when`(mockResourceProvider.getString(R.string.track_multi_combined_fmt)).thenReturn("TEST_MULTI_COMBINED %s")

        // By default set track matcher to match - we can override in specific tests
        `when`(mockTrackMatcher.isMatch(anyObject(), anyObject())).thenReturn(true)

        `when`(mockDistanceFormatter.formatDistance(anyDouble(), anyBoolean())).thenAnswer {
            invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }

        `when`(mockPaceFormatter.formatPace(anyDouble(), anyBoolean())).thenAnswer {
                invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }

        `when`(mockSpeedFormatter.formatSpeed(anyDouble(), anyBoolean())).thenAnswer {
                invocation ->
            val args = invocation.arguments
            val number = args[0] as Double
            return@thenAnswer String.format("%.0f", number)
        }
    }

    private lateinit var trackList: MutableList<Track>
    protected lateinit var trackWithPositions: Track
    protected lateinit var trackWithoutPositions: Track
    protected lateinit var anotherTrackWithPositionsAndMetadata: Track

    private lateinit var now: Calendar
    private lateinit var now2: Calendar
    private lateinit var now3: Calendar
    private lateinit var now4: Calendar

    private lateinit var position1: Position
    private lateinit var position2: Position

    protected fun setupNowDateTimes() {
        now = Calendar.getInstance()
        now.set(2017, 11, 30, 15, 37, 22)
        now2 = Calendar.getInstance()
        now2.time = now.time
        now2.add(Calendar.SECOND, 10)
        now3 = Calendar.getInstance()
        now3.time = now.time
        now3.add(Calendar.SECOND, 15)
        now4 = Calendar.getInstance()
        now4.time = now.time
        now4.add(Calendar.SECOND, 25)
    }

    private fun setupTrackWithPositions() {
        trackWithPositions = Track()
        trackWithPositions.id = 1
        trackWithPositions.name = "Track With points"
        trackWithPositions.notes = "NOTES_TRACK_WITH_POSITIONS"
        trackWithPositions.started = now
        position1 = Position(1.0, 2.0, 3.0, now2, 100F,"POSITION_NOTES_1")
        trackWithPositions.addNewPosition(position1)
    }

    private fun setupTrackWithoutPositions() {
        trackWithoutPositions = Track()
        trackWithoutPositions.id = 2
        trackWithoutPositions.name = "Track Without points"
        trackWithoutPositions.notes = "NOTES_TRACK_WITHOUT_POSITIONS"
        trackWithoutPositions.started = now2
    }

    private fun setupAnotherTrackWithPositionsAndMetadata() {
        anotherTrackWithPositionsAndMetadata = Track()
        anotherTrackWithPositionsAndMetadata.id = 3
        anotherTrackWithPositionsAndMetadata.name = "Another Track With points"
        anotherTrackWithPositionsAndMetadata.notes = "NOTES_ANOTHER_TRACK_WITH_POSITIONS"
        anotherTrackWithPositionsAndMetadata.started = now3
        position2 = Position(11.0, 12.0, 13.0, now4, 100F,"POSITION_NOTES_2")
        anotherTrackWithPositionsAndMetadata.addNewPosition(position2)
        val metadata = TrackMetadata()
        metadata.trackId = 3
        metadata.versionName = "VERSION_NAME"
        metadata.versionCode = 99
        metadata.ended = now4
        metadata.statistics = TrackHelper.testTrackStatistics1()
        anotherTrackWithPositionsAndMetadata.metadata = metadata
    }

    protected fun setupTrackListItems(tracksToAdd: Int) {
        setupNowDateTimes()
        setupTrackWithPositions()
        setupTrackWithoutPositions()
        setupAnotherTrackWithPositionsAndMetadata()
        trackList = ArrayList<Track>(10)
        if (tracksToAdd > 0) {
            trackList.add(trackWithPositions)
            `when`(mockTrackRepository.getWithPositionsAndAccuracy(trackWithPositions.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(trackWithPositions)
        }
        if (tracksToAdd > 1) {
            trackList.add(trackWithoutPositions)
            `when`(mockTrackRepository.getWithPositionsAndAccuracy(trackWithoutPositions.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(trackWithoutPositions)
        }
        if (tracksToAdd > 2) {
            trackList.add(anotherTrackWithPositionsAndMetadata)
            `when`(mockTrackRepository.getWithPositionsAndAccuracy(anotherTrackWithPositionsAndMetadata.id, Position.ACCURACY_THRESHOLD, mockPositionRepository)).thenReturn(anotherTrackWithPositionsAndMetadata)
        }
        `when`(mockTrackRepository.getTrackListWithoutAnyPositions()).thenReturn(trackList)
    }

    private fun startRecording(track: Track) {
        `when`(mockservice.currentTrack).thenReturn(observableTrack.toFlowable(BackpressureStrategy.ERROR))
        `when`(mockservice.currentTrackPositions).thenReturn(track.positions)

        presenter.connectService(mockbinder)                // attach the subscribers = effectively start the recording
        `when`(mockservice.isTracking).thenReturn(true)
        observableTrack.onNext(track)       // kick the subscribers
    }

    protected fun trackIsBeingRecorded(track: Track) {
        startRecording(track)
    }

    protected fun trackIsBeingPaused(track: Track) {
        startRecording(track)
        `when`(mockservice.isTracking).thenReturn(false)
        `when`(mockservice.isPaused).thenReturn(true)
    }

    protected fun loadTracksAndSelect1() {
        presenter.loadTracks()
        presenter.trackLongSelected(1, 0)
        presenter.inMultiSelectMode = true  // this would/should be set from the activity when startMultiSelectMode() is called
    }

    protected fun loadTracksAndSelect1and2() {
        loadTracksAndSelect1()
        presenter.trackSelected(2, 1)
    }

    protected fun loadTracksAndSelect1and3() {
        loadTracksAndSelect1()
        presenter.trackSelected(3, 2)
    }

    protected fun setupWithoutReadStoragePermission(){
        `when`(mockPermissionChecker.hasReadStoragePermission())
            .thenReturn(false)
    }

    protected fun setupWithReadStoragePermission(){
        `when`(mockPermissionChecker.hasReadStoragePermission())
            .thenReturn(true)
    }
}