/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.unittests.ui.trackdisplay

import android.os.Bundle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.helpers.MockitoHelper
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.never

class BindTests : TrackDisplayPresenterSetup() {

    @Before
    fun before_each_test() {
        setupPresenter()
    }

    @Test
    fun presenter_binds_to_controller() {
        // arrange

        // act
        presenter.bind(mockview, null)
        presenter.start()

        // assert
        Mockito.verify(mockServiceController, times(1)).bindToService(presenter)
    }

    @Test
    fun presenter_binds_to_controller_without_state() {
        // arrange

        // act
        presenter.bind(mockview, null)
        presenter.start()

        // assert
        Mockito.verify(mockTrackRepository, never()).getWithPositionsAndAccuracy(anyLong(), anyFloat(), MockitoHelper.anyObject())
        Mockito.verify(mockview, never()).completeProgress()
        Mockito.verify(mockview, never()).startProgress(anyInt())
    }

    @Test
    fun presenter_binds_to_controller_with_state() {
        // arrange
        setupDisplayTrack()
        var bundle = mock(Bundle::class.java)
        Mockito.`when`(bundle.getLong(anyString(), anyLong())).thenReturn(DISPLAY_TRACK_ID)

        // act
        presenter.bind(mockview, bundle)
        presenter.start()

        // assert
        Mockito.verify(mockTrackRepository, times(1)).getWithPositionsAndAccuracy(
                eq(DISPLAY_TRACK_ID),
                anyFloat(),
                MockitoHelper.anyObject())
        Mockito.verify(mockview, times(1)).setTitle("TEST DISPLAY TRACK")
        Mockito.verify(mockview, times(1)).completeProgress()
        Mockito.verify(mockview, times(1)).startProgress(R.string.track_loading_progress)
    }

    @Test
    fun presenter_unbinds_from_controller() {
        // arrange
        presenter.bind(mockview, null)

        // act
        presenter.stop()
        presenter.unbind(mockview)

        // assert
        Mockito.verify(mockServiceController, times(1)).unbindFromService()
    }

}