/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.logging

import android.util.Log
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.util.ContextInitializer
import com.andrewandderek.trailblazer.AndroidApplication
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class Slf4jLoggerFactory @Inject constructor() : ILoggerFactory
{
    private var cachedLogger: Logger? = null

    override val logger: Logger
        get() {
            cachedLogger?.let {
                return it;
            }
            synchronized(this) {
                cachedLogger = LoggerFactory.getLogger(AndroidApplication::class.java)
                return cachedLogger!!
            }
        }

    //
    // SLF4J has no mechanism for altering the log level or setting the file of a file appender
    // the following functions punch thru the facade and access the methods directly in the Logback backend to do this
    // if we ever use a different backend for SLF4J then these methods will need to be rewritten
    //

    private fun getLogbackRootLogger(): ch.qos.logback.classic.Logger {
        return LoggerFactory.getLogger("ROOT") as ch.qos.logback.classic.Logger
    }

    override fun setLoggingConfiguration(level: LoggingLevel, logFilename: String) {
        val root = getLogbackRootLogger()

        // this will set the level from the XML file so do this before we set the level
        reloadConfig(root.loggerContext, logFilename)

        setLoggingLevel(level)
    }

    override fun setLoggingLevel(level: LoggingLevel) {
        val root = getLogbackRootLogger()
        val currentLoggingLevel = getLoggingLevel()
        if (currentLoggingLevel == level) {
            // nothing to do
            Log.d("logging","logging currently set to be ${currentLoggingLevel}")
            return;
        }
        when (level) {
            LoggingLevel.PRODUCTION -> {
                cachedLogger?.debug("setLoggingLevel: logging is switching to production")
                root.level = ch.qos.logback.classic.Level.WARN
            }
            LoggingLevel.DEBUG ->  {
                root.level = ch.qos.logback.classic.Level.DEBUG
            }
        }
    }

    private fun reloadConfig(loggerContext: LoggerContext, logsHome: String) {
        loggerContext.reset()
        // the name of the property must match the one in the XML config file
        loggerContext.putProperty("LOG_HOME", logsHome)
        val initialiser = ContextInitializer(loggerContext)
        initialiser.autoConfig()
    }

    override fun getLoggingLevel(): LoggingLevel {
        val root = getLogbackRootLogger()
        when (root.level) {
            ch.qos.logback.classic.Level.DEBUG -> {
                return LoggingLevel.DEBUG
            }
            else -> {
                return LoggingLevel.PRODUCTION
            }
        }
    }

    private fun logConfig(logger: ch.qos.logback.classic.Logger) {
        Log.d("logging","name = ${logger.name}, level = ${logger.level}")
        val iterator = logger.iteratorForAppenders()
        for (appender in iterator) {
            Log.d("logging","appender name = ${appender.name}")
            val fileAppender = appender as? ch.qos.logback.core.rolling.RollingFileAppender
            if (fileAppender != null) {
                Log.d("logging","appender file = ${fileAppender.file}")
                val filePolicy = fileAppender.rollingPolicy as? ch.qos.logback.core.rolling.FixedWindowRollingPolicy
                if (filePolicy != null) {
                    Log.d("logging","appender rollover policy file = ${filePolicy.fileNamePattern}")
                }
            }
        }
    }
}