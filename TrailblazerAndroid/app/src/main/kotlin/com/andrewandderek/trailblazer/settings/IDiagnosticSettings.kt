package com.andrewandderek.trailblazer.settings

import com.andrewandderek.trailblazer.logging.LoggingLevel

interface IDiagnosticSettings {
    val loggingLevel: LoggingLevel
}