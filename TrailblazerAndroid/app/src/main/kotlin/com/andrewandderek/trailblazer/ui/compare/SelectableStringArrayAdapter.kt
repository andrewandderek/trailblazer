/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.compare

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView

class SelectableStringArrayAdapter constructor (
        context: Context,
        itemsList: List<SelectableString>
    )
    : BaseAdapter(){

    private var inflater: LayoutInflater
    // the items is an immutable copy of the items in the constructor list
    private val items: List<SelectableString> = itemsList.toList()

    init {
        inflater = LayoutInflater.from(context);
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): SelectableString {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        // as the list of items is immutable it can never change, the list is a private immutable copy of the constructor list
        // as it can never change we can use the position as an ID
        // if we ever allow items on the list to be changed then we will need to review this
        return position.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getView(position: Int, rowView: View?, parent: ViewGroup?): View? {
        var itemView: View? = rowView
        if (rowView == null) {
            itemView = inflater.inflate(R.layout.list_item_selectable_string, parent, false)
        }
        val holder = ViewHolder(itemView!!)
        holder.bind(items[position])
        return itemView
    }

    internal class ViewHolder(view: View) {
        @BindView(R.id.selectable_string_text)
        internal lateinit var txtLabel: TextView
        @BindView(R.id.selectable_string_check)
        internal lateinit var btnCheck: ImageView

        init {
            CheeseKnife.bind(this, view)
        }

        fun bind(item: SelectableString) {
            txtLabel.text = item.displayValue
            btnCheck.visibility = if (item.selected) View.VISIBLE else View.GONE
        }
    }
}
