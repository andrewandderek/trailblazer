/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import android.location.Location
import javax.inject.Inject

class AndroidLocationDistanceProvider
@Inject constructor()
    : IDistanceProvider {
    override fun getDistanceInMetres(latitude1: Double, longitude1: Double, latitude2: Double, longitude2: Double): Double {
        // Looking at the source code, Location.distanceBetween() could be quite expensive in
        // terms of cpu and therefore battery, so we may need to use a cheaper approximation
        // TODO: We're only using lat/long for calculating distance - should we be using altitude too?
        val results = FloatArray(1)
        Location.distanceBetween(latitude1, longitude1, latitude2, longitude2, results)
        return results[0].toDouble()
    }
}