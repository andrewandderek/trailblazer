/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.trackdisplay

import android.content.Intent
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.trackcontrol.ITrackControlView

interface ITrackDisplayView : ITrackControlView {
    fun drawAllPositions(points: List<Position>)
    fun addPosition(position: Position)
    fun centreMapAndZoom(position: Position, zoomLevel: Float)
    fun centreMapAndZoomToFit(positions: List<Position>)

    fun clearAllPositions()
    fun requestWriteExternalStoragePermission()
    fun navigateToStatistics(trackId: Long)
    fun promptToImport(prompt: String, uriString: String)
    fun promptForFileFormat(prompt: String, format: String)
    fun startIntent(intent: Intent, title: String)
    fun setTitle(title: String)
    fun startProgress(messageId: Int)
    fun completeProgress()
    fun exit()
    fun promptForTrackName(id: String, name: String)
    fun promptForTrackNameAfterStopping(trackId: String, name: String)
}