/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.repository

import android.content.ContentValues
import android.database.Cursor
import com.andrewandderek.trailblazer.data.DatabaseHelper
import com.andrewandderek.trailblazer.data.schema.PositionTable
import com.andrewandderek.trailblazer.data.utility.DatabaseFieldConverter
import com.andrewandderek.trailblazer.exception.OrphanPositionException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import javax.inject.Inject

interface IPositionRepository : IRepository<Position,Long> {
    // use this to create without the overhead of reading the data back
    fun createFast(newItem: Position): Long
    fun createTrack(positions: List<Position>)
    fun getByTrackId(id: Long): List<Position>
    fun getByTrackIdWithAccuracy(id: Long, accuracyThreshold: Float): List<Position>
    fun getLastPositionOnTrack(id: Long): Position?
}

class PositionRepository
@Inject constructor(
        internal var loggerFactory: ILoggerFactory,
        private var databaseHelper: DatabaseHelper
) : BaseRepository<Position>(), IPositionRepository {

    private val allColumns = arrayOf<String>(
            PositionTable.COLUMN_ID,
            PositionTable.COLUMN_TRACK_ID,
            PositionTable.COLUMN_LATITUDE,
            PositionTable.COLUMN_LONGITUDE,
            PositionTable.COLUMN_ALTITUDE,
            PositionTable.COLUMN_NOTES,
            DatabaseFieldConverter.getCalendarSelect(PositionTable.COLUMN_TIME_RECORDED),
            PositionTable.COLUMN_ACCURACY,
            PositionTable.COLUMN_FLAGS,
            PositionTable.COLUMN_SEGMENT
    )

    override fun create(newItem: Position): Position {
        val insertedId = createFast(newItem)
        return(getById(insertedId)!!)
    }

    override fun createFast(newItem: Position): Long {
        val values: ContentValues = positionToContentValues(newItem)
        val insertedId = databaseHelper.getDatabase().insert(PositionTable.TABLE_POSITION, null, values)
        loggerFactory.logger.trace("position created with id $insertedId, on track ${newItem.trackId} segment ${newItem.segment}")
        return insertedId
    }

    override fun createTrack(positions: List<Position>) {
        databaseHelper.getDatabase().beginTransaction()
        try {
            for (position in positions) {
                createFast(position)
            }
            databaseHelper.getDatabase().setTransactionSuccessful()
        } catch (e: Throwable) {
            loggerFactory.logger.error("error creating a track", e)
        } finally {
            databaseHelper.getDatabase().endTransaction()
        }
    }

    override fun update(item: Position): Position {
        val values: ContentValues = positionToContentValues(item)
        loggerFactory.logger.debug("updating position with id ${item.id}, on track ${item.trackId}")
        databaseHelper.getDatabase().update(PositionTable.TABLE_POSITION, values, "${PositionTable.COLUMN_ID} = ${item.id}", null)
        return(getById(item.id)!!);
    }

    override fun delete(item: Position) {
        deleteById(item.id)
    }

    override fun deleteById(id: Long) {
        loggerFactory.logger.debug("deleting position with id $id")
        databaseHelper.getDatabase().delete(PositionTable.TABLE_POSITION, "${PositionTable.COLUMN_ID} = ${id}", null)
    }

    override fun deleteAll() {
        throw NotImplementedError("dont do this - delete the items by deleting the track and use the cascade")
    }

    override fun getById(id: Long): Position? {
        loggerFactory.logger.debug("getting position with id $id")
        val cursor = databaseHelper.getDatabase().query(PositionTable.TABLE_POSITION, allColumns, "${PositionTable.COLUMN_ID} = ${id}", null, null, null, null)
        return getSingle(cursor, this::cursorToPosition)
    }

    override fun getByTrackId(id: Long): List<Position> {
        loggerFactory.logger.debug("getting positions for track with id $id")
        val cursor = databaseHelper.getDatabase().query(PositionTable.TABLE_POSITION, allColumns, "${PositionTable.COLUMN_TRACK_ID} = ${id}", null, null, null, "${PositionTable.COLUMN_TIME_RECORDED}")
        return getMultiple(cursor, this::cursorToPosition)
    }

    override fun getByTrackIdWithAccuracy(id: Long, accuracyThreshold: Float): List<Position> {
        loggerFactory.logger.debug("getting positions for track with id $id and an accuracyThreshold of $accuracyThreshold")
        val cursor = databaseHelper.getDatabase().query(PositionTable.TABLE_POSITION, allColumns, "${PositionTable.COLUMN_TRACK_ID} = ${id} and ${PositionTable.COLUMN_ACCURACY} < ${accuracyThreshold} ", null, null, null, "${PositionTable.COLUMN_TIME_RECORDED}")
        return getMultiple(cursor, this::cursorToPosition)
    }

    override fun getLastPositionOnTrack(id: Long): Position? {
        val cursor = databaseHelper.getDatabase().query(PositionTable.TABLE_POSITION, allColumns, "${PositionTable.COLUMN_TRACK_ID} = ${id}", null, null, null, "${PositionTable.COLUMN_TIME_RECORDED} DESC", "1")
        return getSingle(cursor, this::cursorToPosition)
    }

    override fun getAll(): List<Position> {
        throw NotImplementedError("this is a very bad idea - you are loading the whole DB - consider getByTrackId()")
    }

    private fun positionToContentValues(item: Position): ContentValues {
        if (item.isOrphan) {
            throw OrphanPositionException("Attempt to persist an orphan position")
        }
        val values = ContentValues()
        // the ID is allocated by the DB - do not put it in here
        values.put(PositionTable.COLUMN_LATITUDE, item.latitude)
        values.put(PositionTable.COLUMN_LONGITUDE, item.longitude)
        values.put(PositionTable.COLUMN_ALTITUDE, item.altitude)
        values.put(PositionTable.COLUMN_ACCURACY, item.accuracy)
        values.put(PositionTable.COLUMN_TIME_RECORDED, DatabaseFieldConverter.convertCalendarTimeToDbString(item.timeRecorded))
        values.put(PositionTable.COLUMN_NOTES, item.notes)
        values.put(PositionTable.COLUMN_TRACK_ID, item.trackId)
        values.put(PositionTable.COLUMN_FLAGS, item.flags)
        values.put(PositionTable.COLUMN_SEGMENT, item.segment)
        return values
    }

    private fun cursorToPosition(cursor: Cursor): Position {
        val lat = DatabaseFieldConverter.getDouble(cursor, PositionTable.COLUMN_LATITUDE)
        val long = DatabaseFieldConverter.getDouble(cursor, PositionTable.COLUMN_LONGITUDE)
        val altitude = DatabaseFieldConverter.getNullable(cursor, PositionTable.COLUMN_ALTITUDE, DatabaseFieldConverter::getDouble)
        val accuracy = DatabaseFieldConverter.getFloat(cursor, PositionTable.COLUMN_ACCURACY)
        val recorded = DatabaseFieldConverter.getNullable(cursor, PositionTable.COLUMN_TIME_RECORDED, DatabaseFieldConverter::getCalendar)
        val position = Position(lat, long, altitude, recorded, accuracy)
        position.id = DatabaseFieldConverter.getLong(cursor,PositionTable.COLUMN_ID)
        position.trackId = DatabaseFieldConverter.getLong(cursor, PositionTable.COLUMN_TRACK_ID)
        position.notes = DatabaseFieldConverter.getString(cursor, PositionTable.COLUMN_NOTES)
        position.flags = DatabaseFieldConverter.getLong(cursor, PositionTable.COLUMN_FLAGS)
        position.segment = DatabaseFieldConverter.getLong(cursor, PositionTable.COLUMN_SEGMENT)
        return position
    }
}