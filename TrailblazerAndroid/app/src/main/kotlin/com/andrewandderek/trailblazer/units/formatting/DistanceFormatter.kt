/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.units.formatting

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.units.conversion.IDistanceConverter
import com.andrewandderek.trailblazer.utility.IResourceProvider
import javax.inject.Inject

interface IDistanceFormatter {
    fun formatDistance(metres: Double, addLabel: Boolean): String
}

class DistanceFormatter @Inject constructor(
        private val resourceProvider: IResourceProvider,
        private val userSettings: IUserSettings,
        private val distanceConverter: IDistanceConverter
    )
    : IDistanceFormatter
{
    override fun formatDistance(metres: Double, addLabel: Boolean): String {
        return String.format(resourceProvider.getString(R.string.distance_fmt),
                distanceConverter.convertMetres(metres, userSettings.distanceUnit),
                if (addLabel) " " + userSettings.distanceUnitLabel else ""
        )
    }
}

