/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import java.io.InputStream
import javax.inject.Inject
import javax.inject.Named

class MultiformatImporter @Inject constructor(
        private val analyticsEngine: IAnalyticsEngine,
        private val importerFactory: IImporterFactory
) : IImporter {
    override fun importTrack(uriString: String): ImportAndResult {
        var importResult: ImportAndResult? = null
        for (definition in importerFactory.getImporters()) {
            importResult = import(
                    definition.analyticsId,
                    definition.analyticsName,
                    definition.analyticsType,
                    { definition.importer.importTrack(uriString) }
            )
            if (importResult.result != ImportResult.UNKNOWN_FORMAT) {
                return importResult
            }
        }
        // just return the last attempt
        return importResult!!
    }

    override fun importTrack(stream: InputStream): ImportAndResult {
        var importResult: ImportAndResult? = null
        for (definition in importerFactory.getImporters()) {
            importResult = import(
                    definition.analyticsId,
                    definition.analyticsName,
                    definition.analyticsType,
                    { definition.importer.importTrack(stream) }
            )
            if (importResult.result != ImportResult.UNKNOWN_FORMAT) {
                return importResult
            }
        }
        // just return the last attempt
        return importResult!!
    }

    private fun import(
            analyticsId: String,
            analyticsName: String,
            analyticsType: String,
            importTrack: () -> ImportAndResult
    ) : ImportAndResult {
        var loadedTrack = importTrack()
        if (loadedTrack.result == ImportResult.OK) {
            analyticsEngine.recordClick(analyticsId, analyticsName, analyticsType)
        }
        return loadedTrack
    }
}