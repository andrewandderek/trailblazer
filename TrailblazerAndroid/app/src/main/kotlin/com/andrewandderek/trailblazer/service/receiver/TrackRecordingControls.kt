/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.service.receiver

import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceConnectionListener
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import javax.inject.Inject

interface ITrackRecordingControls {
    fun performAction(action: String)
}

class TrackRecordingControls
@Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val analyticsEngine: IAnalyticsEngine,
    private val trackingServiceController: IGpsLoggerServiceController
)
    : ITrackRecordingControls, IGpsLoggerServiceConnectionListener
{
    override fun performAction(action: String) {
        loggerFactory.logger.debug("TrackRecordingControls: action ${action}")
        actionToPerform = action
        trackingServiceController.bindToService(this)
    }

    override fun connectService(serviceBinder: GpsLoggerService.LocalBinder) {
        loggerFactory.logger.debug("TrackRecordingControls: service connected")
        locationTrackingService = serviceBinder.service
        when (actionToPerform) {
            ACTION_STOP -> stop()
            ACTION_PAUSE -> pause()
            ACTION_RESUME -> resume()
            else -> {
                loggerFactory.logger.debug("TrackRecordingControls: unknown action ${actionToPerform}")
                trackingServiceController.unbindFromService()
            }
        }
    }

    override fun disconnectService() {
        loggerFactory.logger.debug("TrackRecordingControls: service disconnected")
    }

    private fun stop() {
        loggerFactory.logger.debug("TrackRecordingControls: stop")
        analyticsEngine.recordClick(AnalyticsConstants.HUD_STOP_ID, AnalyticsConstants.HUD_STOP_NAME, AnalyticsConstants.HUD_TYPE)
        locationTrackingService?.stopTracking(true)
        trackingServiceController.unbindFromService()
    }

    private fun pause() {
        loggerFactory.logger.debug("TrackRecordingControls: pause")
        analyticsEngine.recordClick(AnalyticsConstants.HUD_PAUSE_ID, AnalyticsConstants.HUD_PAUSE_NAME, AnalyticsConstants.HUD_TYPE)
        locationTrackingService?.pauseTracking(true)
        trackingServiceController.unbindFromService()
    }

    private fun resume() {
        loggerFactory.logger.debug("TrackRecordingControls: resume")
        analyticsEngine.recordClick(AnalyticsConstants.HUD_RESUME_ID, AnalyticsConstants.HUD_RESUME_NAME, AnalyticsConstants.HUD_TYPE)
        locationTrackingService?.resumeTracking(true)
        trackingServiceController.unbindFromService()
    }

    private var actionToPerform: String? = null
    private var locationTrackingService: IGpsLoggerService? = null

    companion object {
        public const val ACTION_STOP = "ACTION_STOP"
        public const val ACTION_PAUSE = "ACTION_PAUSE"
        public const val ACTION_RESUME = "ACTION_RESUME"
    }
}