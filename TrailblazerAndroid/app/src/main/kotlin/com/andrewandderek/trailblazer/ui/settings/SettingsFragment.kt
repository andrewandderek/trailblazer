/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.settings

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.ViewModelProvider
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import com.andrewandderek.trailblazer.BuildConfig
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.ui.view.OkCancelDialogFragment
import com.andrewandderek.trailblazer.ui.view.ProgressDialogFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class SettingsFragment :
    PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener,
    OkCancelDialogFragment.Listener
{
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    private lateinit var viewModel: SettingsFragmentViewModel

    private var progressDialog: ProgressDialogFragment? = null
    private lateinit var selectFileLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SettingsFragmentViewModel::class.java)
        lifecycle.addObserver(viewModel)
        super.onCreate(savedInstanceState)

        viewModel.observables.message.observe(this) {
            displayMessage(it)
        }
        viewModel.observables.resetMetadataSummarySuffix.observe(this) {
            setResetSummary(it)
        }
        viewModel.observables.startProgress.observe(this) {
            startProgress(it)
        }
        viewModel.observables.endProgress.observe(this) {
            endProgress(it)
        }
        viewModel.observables.displayChooser.observe(this) {
            displayChooser(it)
        }
        viewModel.observables.trackRecording.observe(this) {
            updateUi(it)
        }

        selectFileLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            handleSelectFileResult(result)
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.settings, rootKey)
        updateAllPreferenceSummaries()
        updateVersion()
        updateOsl()
        updatePrivacy()
        updateExportDb()
        updateImportDb()
        updateResetMetadata()
    }

    override fun onPause() {
        super.onPause()
        unregisterPreferenceChangeListener()
    }

    override fun onResume() {
        super.onResume()
        registerPreferenceChangeListener()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        key?.let {
            updatePreferenceSummaryByKey(it)
            when (it) {
                getString(R.string.settings_theme_key) -> handleThemeSettingChanged()
                getString(R.string.settings_diag_logging_key) -> handleLoggingSettingChanged()
            }
        }
    }

    private fun registerPreferenceChangeListener() {
        preferenceScreen.sharedPreferences?.registerOnSharedPreferenceChangeListener(this)
    }

    private fun unregisterPreferenceChangeListener() {
        preferenceScreen.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
    }

    private fun updateOsl() {
        // we cannot do this in the XML file as our package name changes between flavours
        val oslPreference: Preference? = findPreference(getString(R.string.settings_osl_key))
        if (oslPreference != null) {
            oslPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                val intent = Intent(activity, OpenSourceLicensesActivity::class.java)
                startActivity(intent)
                true
            }
        }
    }

    private fun updatePrivacy() {
        val privacyPreference: Preference? = findPreference(getString(R.string.settings_privacy_key))
        if (privacyPreference != null) {
            privacyPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.settings_privacy_url)))
                startActivity(browserIntent)
                true
            }
        }
    }

    private fun updateVersion() {
        val versionPreference: Preference? = findPreference(getString(R.string.settings_version_key))
        if (versionPreference != null) {
            setVersionSummary(versionPreference)
            if (isDebug()) {
                versionPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
                {
                    viewModel.testCrashReporting()
                    true
                }
            }
        }
    }

    private fun setVersionSummary(versionPreference: Preference) {
        var version = getString(R.string.settings_version_summary_fmt, BuildConfig.VERSION_NAME, BuildConfig.GIT_HASH)
        if (isDebug())
            version += " (debug)"

        versionPreference.summary = version
    }

    private fun updateExportDb() {
        val exportDbPreference: Preference? = findPreference(getString(R.string.settings_exportdb_key))
        if (exportDbPreference != null) {
            exportDbPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                viewModel.exportDatabase()
                true
            }
        }
    }

    private fun updateImportDb() {
        val importDbPreference: Preference? = findPreference(getString(R.string.settings_copydb_key))
        if (importDbPreference != null) {
            importDbPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                selectFile()
                true
            }
        }
    }

    private fun selectFile() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)

        // Filter using the MIME type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers, it would be "*/*".
        // as we know that other apps do not always report GPX MIME type correctly lets try for everything
        intent.setType("*/*")

        selectFileLauncher.launch(intent)
    }

    private fun handleSelectFileResult(result: ActivityResult) {
        if (result.resultCode == Activity.RESULT_OK) {
            result.data?.data?.let {
                loggerFactory.logger.debug("File selected for import: ${it}")
                promptDbImport(it)
            }
        }
    }

    private fun promptDbImport(src: Uri) {
        val newFragment = OkCancelDialogFragment.newInstance(
            getString(R.string.settings_copydb_import_prompt),
            getString(R.string.action_import),
            getString(R.string.action_cancel),
            src.toString()
        )
        newFragment.show(requireActivity().supportFragmentManager, TAG_IMPORT_PROMPT)
    }

    public fun dbImportConfirmed(uri: String) {
        viewModel.importDatabase(Uri.parse(uri))
    }

    private fun updateResetMetadata() {
        val category: PreferenceCategory? = findPreference(getString(R.string.settings_about_category_key)) as PreferenceCategory?
        val resetPreference: Preference? = findPreference(getString(R.string.settings_reset_metadata_key))
        if (resetPreference != null) {
            if (!isDebug()) {
                // option only available in debug builds
                category?.removePreference(resetPreference)
                return
            }

            setResetSummary("")
            resetPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                promptReset()
                true
            }
            // we are only doing this is if the menu item has not been removed
            viewModel.loadAllTrackMetadata()
        }
    }

    private fun startProgress(message: String) {
        progressDialog?.dismiss()
        progressDialog = ProgressDialogFragment.newInstance(message)
        progressDialog?.show(requireActivity().supportFragmentManager, TAG_PROGRESS)
    }

    private fun endProgress(message: String) {
        progressDialog?.dismiss()
        progressDialog = null
        if (message.isNotEmpty()) {
            displayMessage(message)
        }
    }

    private fun displayChooser(params: Pair<String, Intent>) {
        val (title, intent) = params
        startActivity(Intent.createChooser(intent, title));
    }

    private fun updateUi(trackRecording: Boolean) {
        val exportDbPreference: Preference? = findPreference(getString(R.string.settings_exportdb_key))
        exportDbPreference?.let { it.isEnabled = !trackRecording }
        val importDbPreference: Preference? = findPreference(getString(R.string.settings_copydb_key))
        importDbPreference?.let { it.isEnabled = !trackRecording }
    }

    private fun setResetSummary(suffix: String) {
        val resetPreference: Preference? = findPreference(getString(R.string.settings_reset_metadata_key))
        resetPreference?.let {
            it.summary = getString(R.string.settings_reset_metadata_summary_fmt, suffix)
        }
    }

    private fun promptReset() {
        val newFragment = OkCancelDialogFragment.newInstance(
            getString(R.string.settings_reset_metadata_prompt),
            getString(R.string.action_reset),
            getString(R.string.action_cancel),
            ""
        )
        newFragment.show(requireActivity().supportFragmentManager, TAG_RESET_PROMPT)
    }

    public fun resetConfirmed() {
        viewModel.resetAllTrackMetadata()
    }

    private fun displayMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    private fun isDebug(): Boolean {
        return BuildConfig.DEBUG
    }

    private fun updateAllPreferenceSummaries() {
        preferenceScreen.sharedPreferences?.all?.keys?.forEach {key ->
            updatePreferenceSummaryByKey(key)
        }
    }

    private fun updatePreferenceSummaryByKey(key: String) {
        updatePreferenceSummary(findPreference(key))
    }

    private fun updatePreferenceSummary(preference: Preference?) {
        (preference as? ListPreference)?.let {
            updateListPreferenceSummary(it)
        }
    }

    private fun updateListPreferenceSummary(preference: ListPreference) {
        preference.summary = preference.entry
    }

    private fun handleThemeSettingChanged() {
        viewModel.applyAppTheme()
    }

    private fun handleLoggingSettingChanged() {
        viewModel.applyloggingLevel()
    }

    override fun ok(tag: String, userData: String) {
        when (tag) {
            TAG_IMPORT_PROMPT -> {
                dbImportConfirmed(userData)
            }
            TAG_RESET_PROMPT -> {
                resetConfirmed()
            }
        }
    }

    override fun cancel(tag: String, userData: String) {
        // nothing to do yet
    }

    companion object {
        public const val TAG_IMPORT_PROMPT = "SettingsActivity:import_db_tag"
        public const val TAG_RESET_PROMPT = "SettingsActivity:reset_metadata_tag"
        public const val TAG_PROGRESS = "SettingsActivity:progress_tag"
    }
}
