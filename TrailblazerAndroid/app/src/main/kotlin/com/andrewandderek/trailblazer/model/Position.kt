/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.model

import android.location.Location
import java.util.Calendar
import java.util.Date

fun Position(latitude: Double, longitude: Double, altitude: Double?, time: Long, accuracy: Float = 0F, notes: String = "", segment: Long = 0L): Position {
    var calendar = Calendar.getInstance()
    calendar.time = Date(time)
    return Position(latitude, longitude, altitude, calendar, accuracy, notes, segment)
}

data class Position(
        val latitude: Double,
        val longitude: Double,
        val altitude: Double?,
        var timeRecorded: Calendar?,
        // small number == more accurate, large number == less accurate, zero if no accuracy information
        // see https://developer.android.com/reference/android/location/Location#getAccuracy()
        // and https://stackoverflow.com/questions/3052517/how-is-location-accuracy-measured-in-android
        var accuracy: Float = 0F,
        var notes: String = "",
        // the segment this position belongs to, zero based and incremented when the track is paused
        var segment: Long = 0L,
        var flags: Long = 0L,
        var trackId: Long = -1L,    // not sure I like this
        var id: Long = -1L,
        // null distanceFromStart == not yet calculated
        var distanceFromStart: Double? = null,
        // null processedAltitude == not yet processed
        var processedAltitude: Double? = null
)
{
    constructor(location: Location, time: Calendar) : this(location.latitude, location.longitude, if (location.hasAltitude()) location.altitude else null, time, location.accuracy)

    companion object {
        // we will discard any positions that have a higher value than the threshold
        // we will probably want to have some method of not having to hard code this value
        // at that point we will migrate it to a provider of some sort but for the moment hard coded is OK
        const val ACCURACY_THRESHOLD = 25.0F
    }

    val isOrphan: Boolean
        get() = trackId == -1L

    val isNew: Boolean
        get() = id == -1L

    fun isFromDifferentSegment(otherPosition: Position): Boolean {
        return otherPosition.segment != segment
    }
}
