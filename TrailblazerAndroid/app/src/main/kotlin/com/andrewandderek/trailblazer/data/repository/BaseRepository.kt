/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.repository

import android.database.Cursor
import com.andrewandderek.trailblazer.data.DatabaseHelper

abstract class BaseRepository<ENTITY_TYPE>
{
    protected fun getSingle(cursor: Cursor, convertToEntity: (cursor: Cursor) -> ENTITY_TYPE): ENTITY_TYPE? {
        var entity: ENTITY_TYPE? = null
        try {
            cursor.moveToFirst()
            if (!cursor.isAfterLast) {
                entity = convertToEntity(cursor)
            }
        } finally {
            cursor.close()
        }
        return entity
    }

    protected fun getMultiple(cursor: Cursor, convertToEntity: (cursor: Cursor) -> ENTITY_TYPE): List<ENTITY_TYPE> {
        var entities: MutableList<ENTITY_TYPE> = ArrayList<ENTITY_TYPE>()
        try {
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                entities.add(convertToEntity(cursor))
                cursor.moveToNext()
            }
        } finally {
            cursor.close()
        }
        return entities
    }

    /**
     * If the sql is a join then its too complex for 'query' we need to use 'rawQuery'
     */
    protected fun getSingleFromComplexQuery(databaseHelper: DatabaseHelper, sql: String, convertToEntity: (cursor: Cursor) -> ENTITY_TYPE): ENTITY_TYPE? {
        // we could use selectionArgs to protect against a sql injection if the sql contains a string obtained from outside the app
        val cursor = databaseHelper.getDatabase().rawQuery(sql, null)
        return getSingle(cursor, convertToEntity)
    }

    /**
     * If the sql is a join then its too complex for 'query' we need to use 'rawQuery'
     */
    protected fun getMultipleFromComplexQuery(databaseHelper: DatabaseHelper, sql: String, convertToEntity: (cursor: Cursor) -> ENTITY_TYPE): List<ENTITY_TYPE> {
        // we could use selectionArgs to protect against a sql injection if the sql contains a string obtained from outside the app
        val cursor = databaseHelper.getDatabase().rawQuery(sql, null)
        return getMultiple(cursor, convertToEntity)
    }

}