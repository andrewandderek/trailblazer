/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.utility

import android.content.Context
import android.view.Window
import android.view.WindowManager
import com.andrewandderek.trailblazer.ui.view.ProgressSpinnerView

object ProgressViewHelper {
    // specify a dynamic message to display in the progress view and use a stepped progress
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, messageId: Int, context: Context, max: Int) {
        progressBar.message = context.getString(messageId)
        progressBar.max = max
        startProgress(progressBar, window, false)
    }

    // specify a dynamic message to display in the progress view
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, messageId: Int, context: Context) {
        progressBar.message = context.getString(messageId)
        startProgress(progressBar, window)
    }

    // use the message specified in the layout XML and use a stepped progress
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, max: Int) {
        progressBar.max = max
        startProgress(progressBar, window, false)
    }

    // use the message specified in the layout XML
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, indeterminateProgress: Boolean = true) {
        progressBar.progress = 0
        progressBar.slideDown(indeterminateProgress)

        // disable the UI
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun completeProgress(progressBar: ProgressSpinnerView, window: Window) {
        progressBar.slideUp()

        // enable the UI
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
}