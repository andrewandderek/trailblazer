/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.gpx

object GpxLiterals {
    const val GPX_EXTENSION = "GPX"

    const val GPX_TAG = "gpx"
    const val TRACK_TAG = "trk"
    const val TRACK_NAME_TAG = "name"
    const val TRACK_DESC_TAG = "desc"
    const val TRACK_SEGEMENT_TAG = "trkseg"
    const val TRACK_POINT_TAG = "trkpt"
    const val LATITUDE_ATTR = "lat"
    const val LONGITUDE_ATTR = "lon"
    const val ELEVATION_TAG = "ele"
    const val TIME_TAG = "time"
}