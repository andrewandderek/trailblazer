/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.di

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.andrewandderek.trailblazer.worker.trackmetadata.TrackMetadataWorker
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class WorkerModule {
    @Binds
    @IntoMap
    @WorkerKey(TrackMetadataWorker::class)
    abstract fun bindWorkerFactory(worker: TrackMetadataWorker.Factory): ChildWorkerFactory
}

@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class WorkerKey(val value: KClass<out Worker>)

interface ChildWorkerFactory {
    fun create(appContext: Context, params: WorkerParameters): Worker
}