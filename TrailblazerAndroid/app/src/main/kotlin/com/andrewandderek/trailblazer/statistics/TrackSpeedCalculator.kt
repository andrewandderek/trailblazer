/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.TrackStatistics
import java.util.*

/**
 * Calculate track speeds.
 * NOTE: this calculator assumes distance and times have already been calculated, therefore it should be
 * called after the distance and time calculators.
 */
class TrackSpeedCalculator : ITrackCalculator {
    override fun addPosition(trackStartTime: Calendar, positions: List<Position>, index: Int, statistics: TrackStatistics) {
        statistics.averageSpeedInMetresPerSecond = getMetresPerSecond(statistics.distanceInMetres, statistics.recordedTimeInMilliseconds)
        statistics.averageMovingSpeedInMetresPerSecond = getMetresPerSecond(statistics.distanceInMetres, statistics.movingTimeInMilliseconds)
    }

    private fun getMetresPerSecond(distanceInMetres: Double, timeInMilliseconds: Long): Double {
        if (timeInMilliseconds == 0L)
            return 0.0

        return distanceInMetres * 1000.0 / timeInMilliseconds.toDouble()
    }
}