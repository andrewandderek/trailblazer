/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.utility

import android.database.sqlite.SQLiteDatabase

object DatabaseUtilities {

    fun isSchemaUpgradeThroughVersion(oldVersion: Int, newVersion: Int, versionToCheckFor: Int): Boolean {
        return oldVersion < versionToCheckFor && newVersion >= versionToCheckFor
    }

    // SQLite does not support ALTER TABLE ALTER COLUMN
    // see https://www.sqlite.org/lang_altertable.html
    // so we need to recreate the table and reload the data when we want to alter a column
    fun recreateAndCopyTable(db: SQLiteDatabase, tableName: String, tableBackupName: String, recreateTable: (db: SQLiteDatabase) -> Unit) {
        // This assumes that we are only making non-destructive changes and the existing table
        // can be copied to the new one, eg. a non-nullable column is becoming nullable

        db.beginTransaction()
        try {
            DatabaseUtilities.renameTable(db, tableName, tableBackupName)
            recreateTable(db)
            DatabaseUtilities.copyTable(db, tableBackupName, tableName)
            DatabaseUtilities.dropTable(db, tableBackupName)
            db.setTransactionSuccessful()
        } finally {
            // commit or rollback
            db.endTransaction()
        }
    }

    fun copyTable(db: SQLiteDatabase, sourceTable: String, destinationTable: String) {
        db.execSQL("insert into $destinationTable select * from $sourceTable")
    }

    fun dropTable(db: SQLiteDatabase, tableName: String) {
        db.execSQL("drop table if exists $tableName")
    }

    fun dropIndex(db: SQLiteDatabase, indexName: String) {
        db.execSQL("drop index if exists $indexName")
    }

    fun renameTable(db: SQLiteDatabase, oldName: String, newName: String) {
        db.execSQL("ALTER TABLE $oldName RENAME TO $newName")
    }

}

