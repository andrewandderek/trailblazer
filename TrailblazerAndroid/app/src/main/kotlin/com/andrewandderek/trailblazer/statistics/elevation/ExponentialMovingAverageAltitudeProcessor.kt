/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics.elevation

import com.andrewandderek.trailblazer.model.Position

class ExponentialMovingAverageAltitudeProcessor : IAltitudeProcessor {
    companion object {
        // new_average = alpha * new_value + (1 - alpha) * previous average
        // Lower alpha => smoother (but obviously potential for losing real peaks and troughs)
        // See https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
        // (We could maybe have this as a setting?)
        const val ALPHA_MULTIPLIER = 0.05
    }

    private var currentValue: Double? = null

    override fun reset(firstPosition: Position) {
        currentValue = null
        process(firstPosition)
    }

    override fun process(nextPosition: Position) {
        currentValue = if (currentValue == null)
            nextPosition.altitude
        else
            calculateNewAverage(currentValue!!, nextPosition)

        nextPosition.processedAltitude = currentValue
    }

    private fun calculateNewAverage(currentAverage: Double, nextPosition: Position): Double? {
        // We're assuming that if this position doesn't have an altitude (ie. it's null)
        // we can use the current average as an approximation
        return if (nextPosition.altitude != null)
            (ALPHA_MULTIPLIER * nextPosition.altitude) + ((1.0 - ALPHA_MULTIPLIER) * currentAverage)
        else
            currentAverage
    }
}