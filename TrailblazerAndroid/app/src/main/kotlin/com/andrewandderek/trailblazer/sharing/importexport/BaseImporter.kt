/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import android.content.ContentResolver
import android.content.ContentUris
import android.net.Uri
import android.provider.MediaStore
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import java.io.FileNotFoundException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.TimeZone

/**
 * this class does the legwork converting a URI string into a stream
 * derived classes deal with turning the stream into a Track
 */
abstract class BaseImporter constructor(
    private val loggerFactory: ILoggerFactory,
    private val fileSystemHelper: IFileSystemHelper,
    private val contentResolver: ContentResolver,
    private val crashReporter: ICrashReporter,
) : IImporter {

    // it turns out that Android does not support X as a format char (which is what we need for an ISO format date TZ)
    // so we will need to add our own decoding of the TZ
    // at the moment we are alright as all the KML files I have seen are in UTC
    val dateTimeFormatterUtc = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())

    init {
        dateTimeFormatterUtc.timeZone = TimeZone.getTimeZone("UTC")
    }

    override fun importTrack(uriString: String): ImportAndResult {
        loggerFactory.logger.debug("Imported track from: ${uriString}")
        val uri: Uri = Uri.parse(uriString)
        when (uri.scheme) {
            "content" -> {
                try {
                    return importTrack(uri, contentResolver)
                }
                catch (ex: FileNotFoundException) {
                    loggerFactory.logger.error("error reading content", ex)
                    crashReporter.logNonFatalException(ex)
                    return importTrack(getPath(uri))
                }
            }
            "file" -> {
                return importTrackFromFile(uri.path!!)
            }
        }
        return ImportAndResult(null, ImportResult.IO_ERROR)
    }

    // I got this from here
    // https://stackoverflow.com/questions/23773715/file-not-found-exception-while-reading-email-attachment-file-from-my-app-in-andr
    // looks like this is the replacement
    // https://stackoverflow.com/questions/67946670/android-what-is-mediastore-mediacolumns-data-replacement
    private fun getPath(uri: Uri): String {
        val projection = arrayOf(MediaStore.Images.Media._ID)
        val cursor = contentResolver.query(uri, projection, null, null, null)
        cursor?.let {
            val columnIndex = it.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            it.moveToFirst()
            val result = ContentUris.withAppendedId(uri, it.getLong(columnIndex))
            it.close()
            return result.toString()
        }
        loggerFactory.logger.error("cannot get cursor")
        return ""
    }

    private fun importTrack(uri: Uri, contentResolver: ContentResolver?): ImportAndResult {
        return contentResolver?.let {
            importTrackFromStream(it.openInputStream(uri))
        } ?: run {
            ImportAndResult(null, ImportResult.IO_ERROR)
        }
    }

    private fun importTrackFromFile(filename: String): ImportAndResult {
        return importTrackFromStream(fileSystemHelper.createInputStream(filename))
    }

    private fun importTrackFromStream(stream: InputStream?): ImportAndResult {
        return stream?.let { validStream ->
            validStream.use {
                // this method is implemented in the derived specialised importer
                importTrack(it)
            }
        } ?: run {
            loggerFactory.logger.debug("importTrackFromStream: NULL stream")
            ImportAndResult( null, ImportResult.IO_ERROR)
        }
    }

    /**
     * utility functions
     */

    protected fun nameEquals(name: String?, compareTo: String): Boolean {
        if (name == null) {
            return false
        }
        return name.equals(compareTo, ignoreCase = true)
    }

    protected fun nameEquals(namespace: String?, name: String?, compareToNamespace: String, compareToName: String): Boolean {
        if (name == null || namespace == null) {
            return false
        }
        return name.equals(compareToName, ignoreCase = true) && namespace.equals(compareToNamespace, ignoreCase = true)
    }


}