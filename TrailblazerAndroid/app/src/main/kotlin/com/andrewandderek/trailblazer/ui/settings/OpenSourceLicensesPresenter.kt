/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.settings

import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import javax.inject.Inject

class OpenSourceLicensesPresenter
@Inject constructor(
        private val loggerFactory: ILoggerFactory,
        private val fileSystemHelper: IFileSystemHelper
        )
    : IOpenSourceLicensesPresenter
{
    override fun unbind(theView: IOpenSourceLicensesView) {
        loggerFactory.logger.debug("OpenSourceLicensesPresenter.unbind")
        view = null
    }

    override fun bind(theView: IOpenSourceLicensesView) {
        loggerFactory.logger.debug("OpenSourceLicensesPresenter.bind")
        view = theView
        addAllLicenseFiles(view!!)
    }

    private fun addAllLicenseFiles(view: IOpenSourceLicensesView) {
        // to add new licenses to this page just place the new licenses text file in the license folder in the assets
        // each file should just be text, the first line is treated as the title the rest as the body text
        // subfolder are supported, files and subfolders are sorted alphabetically
        // to remove a license, delete the file from the license folder in the assets

        val files = fileSystemHelper.getFolderFiles("license") ?: return
        for (file in files) {
            loggerFactory.logger.debug("OpenSourceLicensesPresenter found file $file")
            addLicenseTextToView(view, file)
        }

        view.scrollToTopOfText()
    }

    private fun addLicenseTextToView(view: IOpenSourceLicensesView, filename: String) {
        val text = fileSystemHelper.getFileContents(filename, true)
        val lineSplit = text.indexOf("\n")
        if (lineSplit != -1) {
            view.addTextBlock(text.substring(0, lineSplit), text.substring(lineSplit+1))
        } else {
            view.addTextBlock("", text)
        }
    }

    private var view: IOpenSourceLicensesView? = null
}