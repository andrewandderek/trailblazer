/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.statistics

import android.annotation.SuppressLint
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.ShareException
import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackAndStatistics
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.sharing.importexport.ExportResult
import com.andrewandderek.trailblazer.sharing.importexport.IBitmapExporter
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import com.andrewandderek.trailblazer.utility.DurationFormatter
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.Disposable
import java.util.Calendar
import javax.inject.Inject
import javax.inject.Named

class TrackStatisticsPresenter
@Inject constructor(
        private val loggerFactory: ILoggerFactory,
        private val schedulerProvider: ISchedulerProvider,
        private val trackRepository: ITrackRepository,
        private val positionRepository: IPositionRepository,
        @Named(NameLiterals.STATISTICS_FULL) private val statisticsProvider: ITrackStatisticsProvider,
        private val resourceProvider: IResourceProvider,
        private val crashReporter: ICrashReporter,
        private val userSettings: IUserSettings,
        private val altitudeFormatter: IAltitudeFormatter,
        private val distanceFormatter: IDistanceFormatter,
        private val speedFormatter: ISpeedFormatter,
        private val paceFormatter: IPaceFormatter,
        private val analyticsEngine: IAnalyticsEngine,
        @Named(NameLiterals.SHARE_IMAGE) private val sharer: IIntentSharer,
        private val bitmapExporter: IBitmapExporter
)
    : ITrackStatisticsPresenter
{
    override fun bind(view: ITrackStatisticsView) {
        loggerFactory.logger.debug("TrackStatisticsPresenter.bind")
        this.view = view
    }

    override fun unbind(view: ITrackStatisticsView) {
        loggerFactory.logger.debug("TrackStatisticsPresenter.unbind")
        screenExportSubscriber?.dispose()
        screenExportSubscriber = null
        this.view = null
    }

    // We don't need to store the result of the subscribe
    @SuppressLint("CheckResult")
    override fun loadTrack(trackId: Long) {
        // although the track metadata contains the statistics we need to rerun the stats provider as it updates some values in the loaded positions
        // for example distanceFromStart and processedAltitude
        Flowable.fromCallable { trackRepository.getWithPositionsAndAccuracy(trackId, Position.ACCURACY_THRESHOLD, positionRepository)!! }
                .map { track -> TrackAndStatistics(track, statisticsProvider.getStatistics(track)) }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe (
                        { trackAndStatistics -> onTrackLoaded(trackAndStatistics) },
                        { throwable -> onTrackLoadError(trackId, throwable) })
    }

    private fun onTrackLoaded(trackAndStatistics: TrackAndStatistics) {
        setStatisticsVisibility(trackAndStatistics.track)
        trackName = trackAndStatistics.track.name
        setTrackTitle(trackAndStatistics.track.name)
        setDistance(trackAndStatistics.statistics.distanceInMetres)
        setRecordedTime(trackAndStatistics.statistics.recordedTimeInMilliseconds)
        setMovingTime(trackAndStatistics.statistics.movingTimeInMilliseconds)
        setElevationStatistics(
                trackAndStatistics.statistics.elevationGainInMetres,
                trackAndStatistics.statistics.elevationLossInMetres,
                trackAndStatistics.statistics.minElevationInMetres,
                trackAndStatistics.statistics.maxElevationInMetres,
                trackAndStatistics.statistics.elevationDifferenceInMetres)
        setSpeedStatistics(trackAndStatistics.statistics.averageSpeedInMetresPerSecond, trackAndStatistics.statistics.averageMovingSpeedInMetresPerSecond)
        setPaceStatistics(trackAndStatistics.statistics.averageSpeedInMetresPerSecond, trackAndStatistics.statistics.averageMovingSpeedInMetresPerSecond)
        setupGraphs(trackAndStatistics.track)
        setupNotes(trackAndStatistics.track)
        setupSegmentsText(trackAndStatistics.track)
    }

    private fun onTrackLoadError(trackId: Long, throwable: Throwable?) {
        loggerFactory.logger.error("TrackStatisticsPresenter.loadTrack: Error loading track $trackId; ", throwable)
        if (throwable != null) {
            crashReporter.logNonFatalException(TrackLoadException("ID $trackId", throwable))
        }
        setTrackTitle(resourceProvider.getString(R.string.error_loading_track))
    }

    private fun setStatisticsVisibility(track: Track) {
        if (track.hasNoAltitudeData) {
            view?.hideAltitudeBasedRows()
        }
        if (track.hasMissingTimestamps) {
            view?.hideTimeBasedRows()
        }
    }

    private fun setTrackTitle(trackName: String) {
        view?.setTrackTitle(trackName)
    }

    private fun setDistance(distance: Double) {
        view?.setDistance(distanceFormatter.formatDistance(distance, false), userSettings.distanceUnitLabel)
    }

    private fun setRecordedTime(recordedTime: Long) {
        view?.setRecordedTime(DurationFormatter.formatMilliseconds(recordedTime))
    }

    private fun setMovingTime(movingTime: Long) {
        view?.setMovingTime(DurationFormatter.formatMilliseconds(movingTime))
    }

    private fun setElevationStatistics(elevationGain: Double, elevationLoss: Double, minElevation: Double, maxElevation: Double, elevationDifference: Double) {
        view?.setElevationValues(
                altitudeFormatter.formatAltitude(elevationGain, false),
                altitudeFormatter.formatAltitude(elevationLoss, false),
                altitudeFormatter.formatAltitude(minElevation, false),
                altitudeFormatter.formatAltitude(maxElevation, false),
                altitudeFormatter.formatAltitude(elevationDifference, false),
                userSettings.altitudeUnitLabel)
    }

    private fun setSpeedStatistics(averageSpeed: Double, averageMovingSpeed: Double) {
        view?.setSpeedValues(
                speedFormatter.formatSpeed(averageSpeed, false),
                speedFormatter.formatSpeed(averageMovingSpeed, false),
                userSettings.speedUnitLabel)
    }

    private fun setPaceStatistics(averageSpeed: Double, averageMovingSpeed: Double) {
        view?.setPaceValues(
                paceFormatter.formatPace(averageSpeed, false),
                paceFormatter.formatPace(averageMovingSpeed, false),
                userSettings.paceUnitLabel)
    }

    private fun setupGraphs(track: Track) {
        loggerFactory.logger.debug("TrackStatisticsPresenter.setupGraphs: Adding positions ID ${track.id}")
        // getting the positions can be expensive
        val positions = track.currentPositionsSnapshot
        view?.setElevationByDistanceChartPositions(positions)
        if (track.isMultiDaySegmentedTrack) {
            // the time based track is pretty meaningless for a multi day track
            // so lets try and speed it up
            view?.hideTimeBasedGraph()
        } else {
            view?.setElevationByTimeChartPositions(positions)
        }
        loggerFactory.logger.debug("TrackStatisticsPresenter.setupGraphs: Adding positions complete ID ${track.id} positions ${positions.size}")
    }

    private fun setupNotes(track: Track) {
        if (track.notes.isBlank()) {
            view?.hideNotes()
            return
        }
        view?.setNotes(track.notes)
    }

    private fun setupSegmentsText(track: Track) {
        val allSegments = track.currentPositionsSnapshotSegments
        if (allSegments == null) {
            loggerFactory.logger.debug("TrackStatisticsPresenter.setupSegmentsText: no segments in track")
            view?.hideSegmentsRow()
            return
        }
        loggerFactory.logger.debug("TrackStatisticsPresenter.setupSegmentsText: segments = ${allSegments.size}")
        var allSegmentText = StringBuilder(100)
        for (segmentPositions in allSegments) {
            segmentPositions.firstOrNull()?.let { start ->
                segmentPositions.lastOrNull()?.let { end ->
                    val duration = start.timeRecorded?.timeInMillis?.let {
                        end.timeRecorded?.timeInMillis?.minus(
                            it
                        )
                    } ?: run {
                        0L
                    }
                    val segmentText = String.format(
                        resourceProvider.getString(R.string.stats_segment_line_fmt),
                        start.segment,
                        getRecordedTime(start.timeRecorded, true),
                        getRecordedTime(end.timeRecorded, false),
                        DurationFormatter.formatMilliseconds(duration)
                    )
                    loggerFactory.logger.debug("TrackStatisticsPresenter.setupSegmentsText: text = ${segmentText}")
                    allSegmentText.append(segmentText)
                } ?: run {
                    loggerFactory.logger.debug("TrackStatisticsPresenter.setupSegmentsText: missing end")
                }
            } ?: run {
                loggerFactory.logger.debug("TrackStatisticsPresenter.setupSegmentsText: empty segment")
            }
        }
        view?.setSegmentsText(allSegmentText.toString())
    }

    private fun getRecordedTime(timeRecorded: Calendar?, includeDate: Boolean): String {
        if (timeRecorded == null) {
            return resourceProvider.getString(R.string.stats_segment_no_time)
        }
        if (includeDate) {
            return CalendarFormatter.convertCalendarToDateTimeString(timeRecorded)
        }
        return CalendarFormatter.convertCalendarToTimeString(timeRecorded)
    }

    override fun actionSelected(itemId: Int): Boolean {
        loggerFactory.logger.debug("TrackStatisticsPresenter: actionSelected ${itemId}")
        when (itemId) {
            R.id.action_share_track -> {
                shareScreen()
                return true
            }
        }
        return false
    }

    private fun shareScreen() {
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_SHARE_STATS_SCREEN_ID, AnalyticsConstants.ACTION_SHARE_STATS_SCREEN_NAME, AnalyticsConstants.ACTION_TYPE)
        view?.startProgress(R.string.screen_exporting_progress)
        screenExportSubscriber?.dispose()
        screenExportSubscriber = Flowable.fromCallable { writeScreenshot(trackName) }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        { fileName: String ->
                            view?.completeProgress()
                            if (fileName.isNotEmpty()) {
                                val intent = sharer.getShareIntentWithSingleFile(trackName, fileName)
                                view?.startIntent(intent, resourceProvider.getString(R.string.stats_image_share_title))
                            } else {
                                view?.showMessage(R.string.track_export_error)
                            }
                        },
                        { error: Throwable ->
                            view?.completeProgress()
                            view?.showMessage(R.string.track_export_error)
                            loggerFactory.logger.error("exportScreen", error)
                            crashReporter.logNonFatalException(ShareException("File ${trackName}", error))
                        }
                )
    }

    private fun writeScreenshot(trackName: String): String {
        val bitmap = view!!.getScreenshotBitmap()
        val filename = bitmapExporter.getShareFilename(trackName)
        val result = bitmapExporter.exportBitmap(bitmap, filename)
        if (result != ExportResult.OK) {
            throw ShareException("Error exporting ${trackName}", null)
        }
        return filename
    }

    private var trackName:String = resourceProvider.getString(R.string.untitled_track)

    private var screenExportSubscriber: Disposable? = null

    private var view: ITrackStatisticsView? = null
}