/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView

class FileFormatPromptDialogFragment : DialogFragment() {
    companion object {
        const val OK = "ok_key"
        const val CANCEL = "cancel_key"
        const val PROMPT = "prompt_key"
        const val VALUE = "value_key"

        fun newInstance(ok: String, cancel: String, prompt: String, value: String): FileFormatPromptDialogFragment {
            val fragment = FileFormatPromptDialogFragment()
            val args = Bundle()
            args.putString(OK, ok)
            args.putString(CANCEL, cancel)
            args.putString(PROMPT, prompt)
            args.putString(VALUE, value)
            fragment.arguments = args
            return fragment
        }
    }

    interface Listener {
        fun ok(tag: String, userData: String)
        fun cancel(tag: String, userData: String)
    }

    private var listener: Listener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        val view = inflater.inflate(R.layout.dialog_fragment_file_format, container)
        CheeseKnife.bind(this, view)
        return view
    }

    override fun onResume() {
        // make the layout 90% of the width of the screen
        // because it looks good - and the OS default varies but is often very narrow
        val params = dialog?.window!!.attributes
        params.width = (resources.displayMetrics.widthPixels * 90) / 100
        dialog?.window!!.attributes = params as android.view.WindowManager.LayoutParams
        super.onResume()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            listener = activity as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement Listener")
        }

        val args = requireArguments()
        val tag = tag!!
        val ok = args.getString(OK)
        val cancel = args.getString(CANCEL)
        val prompt = args.getString(PROMPT)
        val value = args.getString(VALUE)

        promptView.text = prompt
        if (NameLiterals.FILE_FORMAT_GPX.equals(value)) {
            radioGroup.check(R.id.radioGpx)
        }
        if (NameLiterals.FILE_FORMAT_KML.equals(value)) {
            radioGroup.check(R.id.radioKml)
        }

        okButton.text = ok
        cancelButton.text = cancel

        okButton.setOnClickListener {
            listener!!.ok(tag, if (radioGpx.isChecked) NameLiterals.FILE_FORMAT_GPX else NameLiterals.FILE_FORMAT_KML)
            dismiss()
        }
        cancelButton.setOnClickListener {
            listener!!.cancel(tag, if (radioGpx.isChecked) NameLiterals.FILE_FORMAT_GPX else NameLiterals.FILE_FORMAT_KML)
            dismiss()
        }
    }

    @BindView(R.id.fileFormatPrompt)
    internal lateinit var promptView: AppCompatTextView

    @BindView(R.id.radioFileFormatGroup)
    internal lateinit var radioGroup: RadioGroup

    @BindView(R.id.radioGpx)
    internal lateinit var radioGpx: AppCompatRadioButton

    @BindView(R.id.radioKml)
    internal lateinit var radioKml: AppCompatRadioButton

    @BindView(R.id.ok)
    internal lateinit var okButton: Button

    @BindView(R.id.cancel)
    internal lateinit var cancelButton: Button
}

