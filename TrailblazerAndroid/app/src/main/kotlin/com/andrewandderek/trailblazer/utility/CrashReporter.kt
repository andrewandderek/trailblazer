/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.google.firebase.crashlytics.FirebaseCrashlytics
import javax.inject.Inject

interface ICrashReporter {
    fun testReporting()
    fun logNonFatalException(e: Throwable)
}

class CrashlyticsReporter
    @Inject constructor(
        private val loggerFactory: ILoggerFactory,
    )
    : ICrashReporter {
    override fun logNonFatalException(e: Throwable) {
        FirebaseCrashlytics.getInstance().recordException(e)
    }

    override fun testReporting() {
        // log an error as the exception does not go into the on-device logging
        loggerFactory.logger.error("CrashlyticsReporter: testReporting")
        throw RuntimeException("Test Crash Reporting");
    }
}