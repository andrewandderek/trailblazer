/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.cheeseknife

import android.app.Activity
import android.view.View

object CheeseKnife {
    private val binderCache = mutableMapOf<Class<Any>, ClassBinder>()

    fun bind(target: Activity){
        val sourceView = target.window.decorView
        bind(target, sourceView)
    }

    fun bind(target: View){
        bind(target, target)
    }

    fun bind(targetObject: Any, sourceView: View) {
        val binder = getBinder(targetObject)
        binder.bind(targetObject, sourceView)
    }

    private fun getBinder(targetObject: Any): ClassBinder {
        return binderCache.getOrPut(targetObject.javaClass) {
            ClassBinder(targetObject.javaClass)
        }
    }
}