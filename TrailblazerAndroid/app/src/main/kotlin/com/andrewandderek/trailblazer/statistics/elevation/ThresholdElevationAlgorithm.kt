/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics.elevation

import com.andrewandderek.trailblazer.model.Position

// Elevation changes less than the threshold are treated as noise and ignored
class ThresholdElevationAlgorithm(
        altitudeProcessor: IAltitudeProcessor,
        elevationSettings: IElevationSettings
)
    : BaseElevationAlgorithm(altitudeProcessor, elevationSettings), IElevationAlgorithm {

    private lateinit var lastValidPosition: Position

    override fun reset(firstPosition: Position) {
        resetProcessor(firstPosition)
        lastValidPosition = firstPosition
    }

    override fun process(nextPosition: Position): ElevationGainAndLoss {
        processPosition(nextPosition)

        var gain = getAltitude(nextPosition) - getAltitude(lastValidPosition)
        var loss = 0.0

        if (Math.abs(gain) <= elevationSettings.elevationThreshold)
            return ElevationGainAndLoss(0.0, 0.0)

        lastValidPosition = nextPosition

        if (gain < 0.0) {
            loss = -gain
            gain = 0.0
        }

        return ElevationGainAndLoss(gain, loss)
    }
}