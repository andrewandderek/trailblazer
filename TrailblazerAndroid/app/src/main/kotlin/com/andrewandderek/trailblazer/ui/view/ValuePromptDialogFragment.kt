/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.DialogFragment
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.OnClick
import com.google.android.material.textfield.TextInputLayout


class ValuePromptDialogFragment : DialogFragment() {
    companion object {
        const val LAYOUT = "layout_key"
        const val OK = "ok_key"
        const val CANCEL = "cancel_key"
        const val CUSTOM = "custom_key"
        const val PROMPT = "prompt_key"
        const val VALUE = "value_key"

        fun newInstance(layout: Int, ok: String, cancel: String?, data: String, prompt: String, value: String): ValuePromptDialogFragment {
            val fragment = ValuePromptDialogFragment()
            val args = Bundle()
            args.putInt(LAYOUT, layout)
            args.putString(OK, ok)
            args.putString(CANCEL, cancel)
            args.putString(CUSTOM, data)
            args.putString(PROMPT, prompt)
            args.putString(VALUE, value)
            fragment.arguments = args
            return fragment
        }
    }

    interface Listener {
        fun ok(tag: String, userData: String, value: String)
        fun cancel(tag: String, userData: String, value: String)
    }

    private var listener: Listener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        val args = requireArguments()
        val layout = args.getInt(LAYOUT)
        val view = inflater.inflate(layout, container)
        CheeseKnife.bind(this, view)
        return view
    }

    override fun onResume() {
        // make the layout 90% of the width of the screen
        // because it looks good - and the OS default varies but is often very narrow
        val params = dialog?.window!!.attributes
        params.width = (resources.displayMetrics.widthPixels * 90) / 100
        dialog?.window!!.attributes = params as android.view.WindowManager.LayoutParams

        super.onResume()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            listener = activity as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement Listener")
        }

        val args = requireArguments()
        val tag = tag!!
        val ok = args.getString(OK)
        val cancel = args.getString(CANCEL)
        val customData = args.getString(CUSTOM)
        val prompt = args.getString(PROMPT)
        val value = args.getString(VALUE)

        layValue.hint = prompt
        txtValue.setText(value)
        txtValue.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                doOkAction(tag, customData)
            }
            false
        })

        okButton.text = ok
        okButton.setOnClickListener {
            doOkAction(tag, customData)
        }

        // the cancel button is optional
        if (cancel.isNullOrBlank()) {
            cancelButton.visibility = View.GONE
        } else {
            cancelButton.visibility = View.VISIBLE
            cancelButton.text = cancel
            cancelButton.setOnClickListener {
                doCancelAction(tag, customData)
            }
        }

        autoDisplaySoftKeyboard()
    }

    @OnClick(R.id.txt_clear)
    fun clearText() {
        txtValue.setText("")
    }

    private fun doOkAction(tag: String, customData: String?) {
        listener!!.ok(tag, customData ?: "", txtValue.text.toString())
        dismiss()
    }

    private fun doCancelAction(tag: String, customData: String?) {
        listener!!.cancel(tag, customData ?: "", txtValue.text.toString())
        dismiss()
    }

    private fun autoDisplaySoftKeyboard() {
        // Show soft keyboard automatically and request focus to field
        // I'm not very proud of this but it does have the virtue of working
        // see https://stackoverflow.com/questions/13694995/android-softkeyboard-showsoftinput-vs-togglesoftinput
        // see https://stackoverflow.com/questions/41725817/hide-to-show-and-hide-keyboard-in-dialogfragment

        val imm = txtValue.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        txtValue.postDelayed(
            {
                txtValue.requestFocus()
                txtValue.setSelection(txtValue.text.length)
                imm.showSoftInput(txtValue, InputMethodManager.SHOW_IMPLICIT)
            }, 100
        )
    }

    @BindView(R.id.value_layout)
    internal lateinit var layValue: TextInputLayout

    @BindView(R.id.value)
    internal lateinit var txtValue: EditText

    @BindView(R.id.ok)
    internal lateinit var okButton: Button

    @BindView(R.id.cancel)
    internal lateinit var cancelButton: Button
}