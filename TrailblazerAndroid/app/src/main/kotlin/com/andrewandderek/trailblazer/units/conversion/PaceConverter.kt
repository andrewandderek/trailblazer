/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.units.conversion

import com.andrewandderek.trailblazer.units.conversion.Constants.METRES_PER_KILOMETRE
import com.andrewandderek.trailblazer.units.conversion.Constants.METRES_PER_MILE
import com.andrewandderek.trailblazer.units.PaceUnit
import javax.inject.Inject

interface IPaceConverter {
    /**
     * returns the total number of seconds needed to complete one unit of measure
     */
    fun convertMetresPerSecond(metresPerSecond: Double, units: PaceUnit): Int
}

class PaceConverter @Inject constructor()
    : IPaceConverter
{
    /**
     * returns the total number of seconds needed to complete one unit of measure
     */
    override fun convertMetresPerSecond(metresPerSecond: Double, units: PaceUnit): Int {
        return when (units) {
            PaceUnit.MinutesPerKilometre -> convertMetresPerSecondToSecondsPerKilometre(metresPerSecond)
            PaceUnit.MinutesPerMile -> convertMetresPerSecondToSecondsPerMile(metresPerSecond)
        }
    }

    private fun convertMetresPerSecondToSecondsPerKilometre(metresPerSecond: Double): Int {
        if (metresPerSecond < 0.1) {
            return 0
        }
        return (METRES_PER_KILOMETRE / metresPerSecond).toInt()
    }

    private fun convertMetresPerSecondToSecondsPerMile(metresPerSecond: Double): Int {
        if (metresPerSecond < 0.1) {
            return 0
        }
        return (METRES_PER_MILE / metresPerSecond).toInt()
    }
}

