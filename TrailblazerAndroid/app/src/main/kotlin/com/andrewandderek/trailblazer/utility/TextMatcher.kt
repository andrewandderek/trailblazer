/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import com.andrewandderek.trailblazer.model.TextSpan
import javax.inject.Inject

interface ITextMatcher {
    fun isMatch(text: String, searchText: String) : Boolean
    fun getMatches(text: String, searchText: String) : List<TextSpan>
}

class TextMatcher
    @Inject constructor()
    : ITextMatcher {

    override fun isMatch(text: String, searchText: String) : Boolean {
        return text.contains(searchText, true)
    }

    override fun getMatches(text: String, searchText: String): List<TextSpan> {
        var startIndex = 0
        val searchTextLength = searchText.length
        if (searchTextLength == 0) {
            return emptyList()
        }
        val matches = mutableListOf<TextSpan>()
        while (startIndex + searchTextLength <= text.length) {
            val foundIndex = text.indexOf(searchText, startIndex, true)
            if (foundIndex < 0)
                break
            matches.add(TextSpan(foundIndex, searchTextLength))
            startIndex = foundIndex + searchTextLength
        }
        return matches
    }
}