/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.content.Context
import java.util.Calendar
import javax.inject.Inject

interface ISystemTime {
    fun getCurrentTime(): Calendar
    fun getNanoTime(): Long
}

class SystemTime
@Inject constructor(
    internal var context: Context
) : ISystemTime {
    override fun getCurrentTime(): Calendar {
        return Calendar.getInstance()
    }

    override fun getNanoTime(): Long {
        return System.nanoTime()
    }
}
