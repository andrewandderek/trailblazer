/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.chart

import com.andrewandderek.trailblazer.model.Position
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import javax.inject.Inject

class ElevationChartDataMapper
@Inject constructor(
        private val lineDataSetFactory: ILineDataSetFactory,
        private val positionToEntryMapper: IPositionToEntryMapper
) : IChartDataMapper {

    override fun mapToLineData(positions: List<Position>): LineData? {
        if (positions.isEmpty())
            return null

        return LineData(mapToLineDataSets(positions))
    }

    private fun mapToLineDataSets(positions: List<Position>): List<ILineDataSet> {
        val firstPosition = positions[0]    // this is the first position in the track, not the first position in the segment
        return positions
                .groupBy { position -> position.segment }
                .map { segment -> lineDataSetFactory.createLineDataSet(segment.key, mapToEntries(segment.value, firstPosition)) }
    }

    private fun mapToEntries(positions: List<Position>, firstPosition: Position): List<Entry> {
        if (positions.isEmpty())
            return emptyList()

        return (positions.map { position -> createEntryForPosition(position, firstPosition) })
    }

    private fun createEntryForPosition(position: Position, firstPosition: Position): Entry {
        return positionToEntryMapper.mapToEntry(position, firstPosition)
    }
}