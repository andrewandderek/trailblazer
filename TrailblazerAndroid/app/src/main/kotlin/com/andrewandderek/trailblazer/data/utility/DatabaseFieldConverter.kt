/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.utility

import android.database.Cursor
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone


object DatabaseFieldConverter {
    val BOOLEAN_FALSE = 0
    val BOOLEAN_TRUE = 1

    fun isColumnPresentAndNotNull(cursor: Cursor, columnName: String): Boolean {
        val index = cursor.getColumnIndex(columnName)
        if (index  == -1) {
            return false
        }
        if (cursor.isNull(index)) {
            return false
        }
        return true
    }

    fun getString(cursor: Cursor, columnName: String): String {
        return cursor.getString(cursor.getColumnIndexOrThrow(columnName))
    }

    fun getBoolean(cursor: Cursor, columnName: String): Boolean {
        return getInt(cursor, columnName) == BOOLEAN_TRUE
    }

    fun getLong(cursor: Cursor, columnName: String): Long {
        return cursor.getLong(cursor.getColumnIndexOrThrow(columnName))
    }

    fun getInt(cursor: Cursor, columnName: String): Int {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName))
    }

    fun getDouble(cursor: Cursor, columnName: String): Double {
        return cursor.getDouble(cursor.getColumnIndexOrThrow(columnName))
    }

    fun getFloat(cursor: Cursor, columnName: String): Float {
        return cursor.getFloat(cursor.getColumnIndexOrThrow(columnName))
    }

    fun <FIELD_TYPE> getNullable(
            cursor: Cursor,
            columnName: String,
            reader: (c: Cursor, n: String) -> FIELD_TYPE): FIELD_TYPE? {

        val columnIndex = cursor.getColumnIndexOrThrow(columnName)
        if (cursor.isNull(columnIndex))
            return null
        return reader(cursor, columnName)
    }

    fun getCalendarSelect(columnName: String): String {
        // need to do it like this so we can convert it to be a Calendar in getCalendar()
        return "(strftime('%s', ${columnName}) * 1000) AS ${columnName}"
    }

    fun getCalendar(cursor: Cursor, columnName: String): Calendar {
        // SQL must use (strftime('%s', column_name) * 1000) AS column_name
        val milliseconds = cursor.getLong(cursor.getColumnIndexOrThrow(columnName))
        val cal = Calendar.getInstance()
        cal.setTime(Date(milliseconds))
        return cal
    }

    fun convertCalendarTimeToDbString(date: Calendar?): String? {
        if (date == null) {
            return null
        }
        val dateFormat = SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("UTC");
        return dateFormat.format(date.time)
    }

}