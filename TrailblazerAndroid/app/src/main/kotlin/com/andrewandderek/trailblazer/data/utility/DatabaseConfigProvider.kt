/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.utility

import android.content.Context
import com.andrewandderek.trailblazer.data.DATABASE_FILE_NAME_ROOT
import java.io.File
import javax.inject.Inject

interface IDatabaseConfigProvider {
    // pathname to the DB being used by the application
    fun getActiveApplicationDbPathname(): String
    // pathname to the DB file to use for import
    fun getImportDbPathname(ensureFolderExists: Boolean): String
    // pathname to the DB file to use for export
    fun getExportDbPathname(ensureFolderExists: Boolean): String
    // true if the import DB exists
    fun doesImportDbExist(): Boolean
}

class DatabaseConfigProvider @Inject constructor(
    private val context: Context,
) : IDatabaseConfigProvider  {

    private fun getDbFolderPathname(subFolderName: String, ensureFolderExists: Boolean): String {
        val filename = "$DATABASE_FILE_NAME_ROOT.db"
        val externalRootFolder = context.applicationContext.getExternalFilesDir(null)?.absolutePath
        val folder = "${externalRootFolder}/${subFolderName}"
        if (ensureFolderExists) {
            ensureFolderExists(folder)
        }
        return "${folder}/${filename}"
    }

    private fun ensureFolderExists(folder: String) {
        val sDir = File(folder)
        if (!sDir.exists()) {
            sDir.mkdirs()
        }
    }

    override fun getImportDbPathname(ensureFolderExists: Boolean): String {
        return getDbFolderPathname("import", ensureFolderExists)
    }

    override fun getExportDbPathname(ensureFolderExists: Boolean): String {
        return getDbFolderPathname("export", ensureFolderExists)
    }

    override fun doesImportDbExist(): Boolean {
        val db = File(getImportDbPathname(false));
        return db.exists()
    }

    override fun getActiveApplicationDbPathname(): String {
        // this is the pathname where we keep the real application DB
        val filename = "$DATABASE_FILE_NAME_ROOT.db"
        val pathname = context.getDatabasePath(filename)
        return "${pathname}"
    }
}