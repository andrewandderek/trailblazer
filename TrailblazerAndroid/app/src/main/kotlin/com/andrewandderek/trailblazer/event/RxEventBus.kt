/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.event

import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

/**
 * this is the event bus for the app
 * it could be implemented using any library for example otto or eventbus
 * but for now we are using a light weight hand rolled RxAndroid implementation
 * inspired by this article
 * http://nerds.weddingpartyapp.com/tech/2014/12/24/implementing-an-event-bus-with-rxjava-rxbus/
 * and this one for the kotlin conversion
 * https://jayrambhia.com/notes/eventbus-rxkotlin
 */

class RxEventBusSubscription constructor(private val rxSubscription: Disposable) : IEventBusSubscription {
    override fun unsubscribe() = rxSubscription.dispose()
}

class RxEventBus
    @Inject constructor(
            private val loggerFactory: ILoggerFactory,
            private val crashReporter: ICrashReporter,
            private val schedulerProvider: ISchedulerProvider
    )
    : IEventBus {
    private val publisher: PublishSubject<Any> = PublishSubject.create()

    override fun publish(event: Any) {
        loggerFactory.logger.debug("rx event bus publish ${event.toString()}")
        if (publisher.hasObservers()) {
            publisher.onNext(event)
        }
    }

    override fun <EVENT_TYPE> subscribe(eventType: Class<EVENT_TYPE>, receiver: (EVENT_TYPE) -> Unit): IEventBusSubscription {
        return subscribe(eventType, receiver)
            { error: Throwable ->
                loggerFactory.logger.error("rx event bus subscriber", error)
                crashReporter.logNonFatalException(error)
            }
    }

    override fun <EVENT_TYPE> subscribe(eventType: Class<EVENT_TYPE>, receiver: (EVENT_TYPE) -> Unit, errorReceiver: (Throwable) -> Unit): IEventBusSubscription {
        val subscriber = publisher
                .toFlowable(BackpressureStrategy.BUFFER)
                .observeOn(schedulerProvider.androidMainThread())
                .ofType(eventType)
                .subscribe(
                        { event ->
                            loggerFactory.logger.debug("rx event bus subscriber ${eventType.canonicalName}")
                            receiver(event)
                        },
                        { error: Throwable ->
                            errorReceiver(error)
                        }
                )
        return RxEventBusSubscription(subscriber)
    }
}



