/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.FileProvider
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.utility.IResourceProvider
import java.io.File

abstract class BaseSharer constructor(
            private val loggerFactory: ILoggerFactory,
            private val context: Context,
            private val resourceProvider: IResourceProvider
    ) : IIntentSharer
{
    override fun getShareIntentWithSingleFile(trackName: String, fileName: String): Intent {
        return getShareIntentWithMultipleFiles(trackName, listOf(fileName))
    }

    protected fun getAdvertisingUrl(): String {
        return resourceProvider.getString(R.string.share_advert_app_url)
    }

    protected fun getAttachmentUri(filename: String): Uri {
        loggerFactory.logger.debug("Share file ${filename}")
        val shareFile = File(filename)
        val shareableUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", shareFile)
        loggerFactory.logger.debug("Share uri ${shareableUri}")
        return shareableUri
    }
}