/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.di

import androidx.lifecycle.ViewModel
import com.andrewandderek.trailblazer.ui.compare.CompareActivity
import com.andrewandderek.trailblazer.ui.compare.CompareViewModel
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [
    ICompareActivitySubcomponent::class
])
abstract class CompareActivityModule {
    @Binds
    @IntoMap
    @ClassKey(CompareActivity::class)
    abstract fun bindActivityInjectorFactory(builder: ICompareActivitySubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ViewModelKey(CompareViewModel::class)
    abstract fun bindCompareViewModel(model: CompareViewModel): ViewModel
}

