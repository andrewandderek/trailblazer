/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.trackdisplay

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.MenuCompat
import androidx.lifecycle.Lifecycle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.statistics.TrackStatisticsActivity
import com.andrewandderek.trailblazer.ui.trackcontrol.TrackControlActivity
import com.andrewandderek.trailblazer.ui.utility.IMapColourSchemeProvider
import com.andrewandderek.trailblazer.ui.utility.PermissionRequester
import com.andrewandderek.trailblazer.ui.utility.ProgressViewHelper
import com.andrewandderek.trailblazer.ui.view.FileFormatPromptDialogFragment
import com.andrewandderek.trailblazer.ui.view.OkCancelDialogFragment
import com.andrewandderek.trailblazer.ui.view.ProgressSpinnerView
import com.andrewandderek.trailblazer.ui.view.TrackMapView
import com.andrewandderek.trailblazer.ui.view.ValuePromptDialogFragment
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import dagger.android.AndroidInjection
import javax.inject.Inject


class TrackDisplayActivity :
    TrackControlActivity(),
    ITrackDisplayView,
    OkCancelDialogFragment.Listener,
    FileFormatPromptDialogFragment.Listener,
    ValuePromptDialogFragment.Listener,
    TrackMapView.IZoomLevelObserver
{
    companion object {
        private const val TAG_RENAME_PROMPT = "TrackDisplayActivity:rename_tag"
        private const val TAG_IMPORT_PROMPT = "import_track_tag"
        private const val TAG_EXPORT_FORMAT_PROMPT = "export_track_format_tag"

        fun createIntent(context: Context, trackId: Long): Intent {
            val intent = Intent(context,  TrackDisplayActivity::class.java)
            intent.putExtra(TrackDisplayPresenter.PARAM_TRACK_ID, trackId)
            return intent
        }
        fun createIntent(context: Context, fileUri: String, promptForConfirmation: Boolean): Intent {
            val intent = Intent(context,  TrackDisplayActivity::class.java)
            intent.data = Uri.parse(fileUri)
            intent.putExtra(TrackDisplayPresenter.PARAM_PROMPT_FOR_IMPORT_CONFIRMATION, promptForConfirmation)
            return intent
        }
    }

    @Inject lateinit var presenter: ITrackDisplayPresenter
    @Inject lateinit var permissionChecker: IPermissionChecker
    @Inject lateinit var mapColourSchemeProvider: IMapColourSchemeProvider

    @BindView(R.id.progressBar)
    protected lateinit var progressBar: ProgressSpinnerView

    @BindView(R.id.track_map)
    protected lateinit var trackMap: TrackMapView

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("TrackDisplayActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trackdisplay)
        CheeseKnife.bind(this)

        trackMap.onCreate(savedInstanceState, mapColourSchemeProvider)
        requestLocationPermission()
        presenter.bind(this, savedInstanceState)
        if (intent != null) {
            // only unpack if we are being started rather than restored
            presenter.unpackIntent(intent)
        }
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("TrackDisplayActivity.onDestroy()")
        super.onDestroy()
        trackMap.onDestroy()
        presenter.unbind(this)
    }

    override fun onStart() {
        loggerFactory.logger.debug("TrackDisplayActivity.onStart()")
        super.onStart()
        trackMap.onStart()
        presenter.start()
    }

    override fun onStop() {
        loggerFactory.logger.debug("TrackDisplayActivity.onStop()")
        super.onStop()
        trackMap.onStop()
        presenter.stop()
    }

    override fun onResume() {
        super.onResume()
        trackMap.onResume()
        trackMap.zoomLevelObserver = this
    }

    override fun onPause() {
        super.onPause()
        trackMap.onPause()
        trackMap.zoomLevelObserver = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        // we handle restoring the state in onCreate
        loggerFactory.logger.debug("TrackDisplayActivity.onSaveInstanceState()")
        super.onSaveInstanceState(outState)
        trackMap.onSaveInstanceState(outState)
        presenter.saveState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        trackMap.onLowMemory()
    }

    override fun onNewIntent(intent: Intent?) {
        loggerFactory.logger.debug("TrackDisplayActivity.onNewIntent()")
        super.onNewIntent(intent)
        if (intent != null) {
            presenter.unpackIntent(intent)
        }
    }

    override fun promptToImport(prompt: String, uriString: String) {
        val newFragment = OkCancelDialogFragment.newInstance(
                prompt,
                getString(R.string.action_import),
                getString(R.string.action_cancel),
                uriString
        )
        newFragment.show(supportFragmentManager, TAG_IMPORT_PROMPT)
    }

    override fun promptForFileFormat(prompt: String, format: String) {
        val newFragment = FileFormatPromptDialogFragment.newInstance(
                getString(R.string.action_export),
                getString(R.string.action_cancel),
                prompt,
                format
        )
        newFragment.show(supportFragmentManager, TAG_EXPORT_FORMAT_PROMPT)
    }

    override fun ok(tag: String, userData: String) {
        loggerFactory.logger.debug("TrackDisplayActivity.OK ${tag}, ${userData}")
        when (tag) {
            TAG_IMPORT_PROMPT -> {
                presenter.importTrack(userData)
            }
            TAG_EXPORT_FORMAT_PROMPT -> {
                presenter.exportTrack(userData)
            }
        }
    }

    override fun cancel(tag: String, userData: String) {
        loggerFactory.logger.debug("TrackDisplayActivity.cancel ${tag}, ${userData}")
    }

    override fun promptForTrackName(id: String, name: String) {
        val newFragment = ValuePromptDialogFragment.newInstance(
            R.layout.dialog_fragment_track_name,
            getString(R.string.action_save),
            getString(R.string.action_cancel),
            id,
            getString(R.string.edit_track_name_prompt),
            name
        )
        newFragment.show(supportFragmentManager, TAG_RENAME_PROMPT)
    }

    override fun promptForTrackNameAfterStopping(trackId: String, name: String) {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            loggerFactory.logger.debug("TrackDisplayActivity.promptForTrackNameAfterStopping() - ignored as we are not on top")
            return
        }
        val newFragment = ValuePromptDialogFragment.newInstance(
            R.layout.dialog_fragment_track_name,
            getString(R.string.action_set_name),
            null,
            trackId,
            getString(R.string.edit_track_name_prompt),
            name
        )
        newFragment.show(supportFragmentManager, TAG_RENAME_PROMPT)
    }

    override fun ok(tag: String, userData: String, value: String) {
        loggerFactory.logger.debug("TrackListActivity.ok value ${tag}, ${userData}")
        when (tag) {
            TAG_RENAME_PROMPT -> {
                presenter.confirmedRename(userData, value)
            }
        }
    }

    override fun cancel(tag: String, userData: String, value: String) {
        loggerFactory.logger.debug("TrackListActivity.Cancel value ${tag}, ${userData}")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.track_display_activity, menu)
        // we want a separator on the menu
        MenuCompat.setGroupDividerEnabled(menu, true);
        return true
    }

    private fun enableMenuItemIfAvailable(menu: Menu, itemId: Int) {
        menu.findItem(itemId)?.isEnabled = presenter.isActionAvailable(itemId)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        enableMenuItemIfAvailable(menu, R.id.action_statistics)
        enableMenuItemIfAvailable(menu, R.id.action_rename_track)
        enableMenuItemIfAvailable(menu, R.id.action_share_track)
        enableMenuItemIfAvailable(menu, R.id.action_export_track)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (presenter.actionSelected(item.itemId)) {
            return true
        }
        if (item.itemId == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun addPosition(position: Position) {
        trackMap.addPosition(position)
    }

    override fun drawAllPositions(points: List<Position>) {
        trackMap.drawAllPositions(points)
    }

    override fun centreMapAndZoom(position: Position, zoomLevel: Float) {
        trackMap.centreMapAndZoom(position, zoomLevel)
    }

    override fun centreMapAndZoomToFit(positions: List<Position>) {
        trackMap.centreMapAndZoomToFit(positions)
    }

    override fun clearAllPositions() {
        trackMap.clearAllPositions()
    }

    override fun navigateToStatistics(trackId: Long) {
        val intent = TrackStatisticsActivity.createIntent(this, trackId)
        startActivity(intent)
    }

    override fun startIntent(intent: Intent, title: String) {
        startActivity(Intent.createChooser(intent, title))
    }

    override fun startProgress(messageId: Int) {
        ProgressViewHelper.startProgress(progressBar, window, messageId, this)
    }

    override fun completeProgress() {
        ProgressViewHelper.completeProgress(progressBar, window)
    }

    override fun exit() {
        finish()
    }

    override fun showMessage(messageId: Int) {
        Toast.makeText(this, messageId, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun requestLocationPermission() {
        if (!permissionChecker.hasLocationPermission()) {
            PermissionRequester.requestLocationPermission(this)
        }
    }

    override fun requestWriteExternalStoragePermission() {
        if (!permissionChecker.hasWriteStoragePermission()) {
            PermissionRequester.requestWriteExternalStoragePermission(this)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PermissionRequester.MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    if (permissionChecker.hasLocationPermission()) {
                        // set the map control up again now we have the permission
                        trackMap.setupMap()
                        // make sure the FABs are being displayed
                        updateAvailableActions()
                        presenter.locationPermissionGranted()
                    }
                } else {
                    // permission denied
                    showMessage(R.string.location_permission_denied)
                }
                return
            }
            PermissionRequester.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    // the way the code works we just enable and disable the menu item if the permission is not available so there is nothing to do
                    presenter.exportTrack()
                } else {
                    // permission denied
                    showMessage(R.string.write_storage_permission_denied)
                }
                return
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun setTitle(title: String) {
        this.title = title
    }

    override fun onMapZoomLevelChanged(newZoomLevel: Float) {
        presenter.mapZoomLevelChanged(newZoomLevel)
    }

    protected override fun isActionAvailable(actionId: Int): Boolean {
        return presenter.isActionAvailable(actionId)
    }

    protected override fun actionSelected(itemId: Int): Boolean {
        return presenter.actionSelected(itemId)
    }
}
