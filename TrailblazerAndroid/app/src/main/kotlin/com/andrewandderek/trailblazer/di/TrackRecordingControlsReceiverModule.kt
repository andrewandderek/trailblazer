/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.di

import com.andrewandderek.trailblazer.service.receiver.ITrackRecordingControls
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControlsReceiver
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [ITrackRecordingControlsReceiverSubcomponent::class])
abstract class TrackRecordingControlsReceiverModule {
    @Binds
    @IntoMap
    @ClassKey(TrackRecordingControlsReceiver::class)
    abstract fun bindInjectorFactory(builder: ITrackRecordingControlsReceiverSubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    abstract fun bindControls(controls: TrackRecordingControls): ITrackRecordingControls
}