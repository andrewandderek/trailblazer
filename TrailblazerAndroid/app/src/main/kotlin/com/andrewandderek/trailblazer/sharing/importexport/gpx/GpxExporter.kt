/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.gpx

import android.util.Xml
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.sharing.importexport.BaseExporter
import com.andrewandderek.trailblazer.sharing.importexport.ITrackExporter
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import com.andrewandderek.trailblazer.utility.IEnvironmentInformationProvider
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import org.xmlpull.v1.XmlSerializer
import java.io.Writer
import javax.inject.Inject


class GpxExporter @Inject constructor(
        private val loggerFactory: ILoggerFactory,
        private var fileSystemHelper: IFileSystemHelper,
        private var environmentInformationProvider: IEnvironmentInformationProvider
) : BaseExporter(fileSystemHelper), ITrackExporter
{
    override val fileExtension: String
        get() = GpxLiterals.GPX_EXTENSION

    override fun exportTrack(track: Track, filename: String): Boolean {
        loggerFactory.logger.debug("Export track to: ${filename}")
        val writer = fileSystemHelper.createWriter(filename)
        val retval =  exportTrack(track, writer)
        writer.flush()
        writer.close()
        return retval
    }

    private fun exportTrack(track: Track, writer: Writer): Boolean {
        val xmlSerializer = Xml.newSerializer()

        xmlSerializer.setOutput(writer)
        // start DOCUMENT
        xmlSerializer.startDocument("UTF-8", false)

        xmlSerializer.startTag("", GpxLiterals.GPX_TAG)
        xmlSerializer.attribute("", "xmlns", "http://www.topografix.com/GPX/1/1")
        xmlSerializer.attribute("", "creator", "Trailblazer v${environmentInformationProvider.getApplicationVersion()}")
        xmlSerializer.attribute("", "version", "1.1")
        xmlSerializer.attribute("", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        xmlSerializer.attribute("", "xsi:schemaLocation", "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd")

        writeMetaData(xmlSerializer)
        writeTrack(xmlSerializer, track)

        xmlSerializer.endTag("", GpxLiterals.GPX_TAG)

        // end DOCUMENT
        xmlSerializer.endDocument()

        loggerFactory.logger.debug("Track exported")
        return true
    }

    private fun writeMetaData(xmlSerializer: XmlSerializer) {
        xmlSerializer.startTag("", "metadata")
        xmlSerializer.endTag("", "metadata")
    }

    private fun writeTrack(xmlSerializer: XmlSerializer, track: Track) {
        xmlSerializer.startTag("", GpxLiterals.TRACK_TAG)
        xmlSerializer.startTag("", GpxLiterals.TRACK_NAME_TAG)
        xmlSerializer.text(track.name)
        xmlSerializer.endTag("", GpxLiterals.TRACK_NAME_TAG)
        xmlSerializer.startTag("", GpxLiterals.TRACK_DESC_TAG)
        xmlSerializer.text(track.notes)
        xmlSerializer.endTag("", GpxLiterals.TRACK_DESC_TAG)

        writeAllPoints(xmlSerializer, track)

        xmlSerializer.endTag("", GpxLiterals.TRACK_TAG)
    }

    private fun writeAllPoints(xmlSerializer: XmlSerializer, track: Track) {
        xmlSerializer.startTag("", GpxLiterals.TRACK_SEGEMENT_TAG)
        var lastPoint: Position? = null
        for (point in track.currentPositionsSnapshot) {
            if (lastPoint != null && lastPoint.isFromDifferentSegment(point)) {
                xmlSerializer.endTag("", GpxLiterals.TRACK_SEGEMENT_TAG)
                xmlSerializer.startTag("", GpxLiterals.TRACK_SEGEMENT_TAG)
            }
            writePoint(xmlSerializer, point)
            lastPoint = point
        }
        xmlSerializer.endTag("", GpxLiterals.TRACK_SEGEMENT_TAG)
    }

    private fun writePoint(xmlSerializer: XmlSerializer, point: Position) {
        xmlSerializer.startTag("", GpxLiterals.TRACK_POINT_TAG)
        xmlSerializer.attribute("", GpxLiterals.LATITUDE_ATTR, point.latitude.toString())
        xmlSerializer.attribute("", GpxLiterals.LONGITUDE_ATTR, point.longitude.toString())
        writeElevation(xmlSerializer, point)
        writeTimestamp(xmlSerializer, point)
        xmlSerializer.endTag("", GpxLiterals.TRACK_POINT_TAG)
    }

    private fun writeTimestamp(xmlSerializer: XmlSerializer, point: Position) {
        // the time element is optional
        // see https://www.topografix.com/GPX/1/1/#type_ptType
        point.timeRecorded?.let {
            xmlSerializer.startTag("", GpxLiterals.TIME_TAG)
            xmlSerializer.text(CalendarFormatter.convertCalendarToXmlSchemaDateTimeString(it))
            xmlSerializer.endTag("", GpxLiterals.TIME_TAG)
        }
    }

    private fun writeElevation(xmlSerializer: XmlSerializer, point: Position) {
        // the ele element is optional
        // see https://www.topografix.com/GPX/1/1/#type_ptType
        if (point.altitude == null)
            return
        xmlSerializer.startTag("", GpxLiterals.ELEVATION_TAG)
        xmlSerializer.text(point.altitude.toString())
        xmlSerializer.endTag("", GpxLiterals.ELEVATION_TAG)
    }
}

