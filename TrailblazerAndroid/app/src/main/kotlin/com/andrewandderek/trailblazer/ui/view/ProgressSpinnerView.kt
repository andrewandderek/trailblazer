/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView

class ProgressSpinnerView : LinearLayout {

    @BindView(R.id.progress_bar_message)
    lateinit var messageView: TextView

    @BindView(R.id.indeterminateBar)
    lateinit var indeterminateBar: ProgressBar

    @BindView(R.id.steppedBar)
    lateinit var steppedBar: ProgressBar

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    var message: String? = null
        set(value) {
            field = value
            messageView.text = value
        }

    var max: Int = 1
        set(value) {
            field = value
            steppedBar.max = value
        }

    var progress: Int = 0
        set(value) {
            field = value
            steppedBar.progress = value
        }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        val view = inflateView(context)
        CheeseKnife.bind(this, view)

        loadAttributes(attrs, defStyle)
    }

    private fun loadAttributes(attrs: AttributeSet?, defStyle: Int) {
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.ProgressSpinnerView, defStyle, 0)

        message = a.getString(R.styleable.ProgressSpinnerView_message)

        a.recycle()
    }

    private fun inflateView(context: Context): View {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.view_progress_spinner, this, true)
    }

    fun slideDown(indeterminateProgress: Boolean) {
        indeterminateBar.visibility = if (indeterminateProgress) View.VISIBLE else View.GONE
        steppedBar.visibility = if (indeterminateProgress) View.GONE else View.VISIBLE
        visibility = View.VISIBLE
        val animate = TranslateAnimation(
                0f,       // fromXDelta
                0f,         // toXDelta
                -height.toFloat(),   // fromYDelta
                0f)         // toYDelta
        animate.duration = 500
        clearAnimation()
        startAnimation(animate)
    }

    fun slideUp() {
        visibility = View.GONE
        val animate = TranslateAnimation(
                0f,        // fromXDelta
                0f,          // toXDelta
                0f,        // fromYDelta
                -height.toFloat())   // toYDelta
        animate.duration = 500
        clearAnimation()
        startAnimation(animate)
    }
}