/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.compare

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView

class CompareTrackListRecyclerAdapter
constructor (
        private val context: Context
)
    : RecyclerView.Adapter<CompareTrackListRecyclerAdapter.RecyclerViewHolder>()
{
    private var allTracks: ArrayList<TrackAndStatisticsWithLabels> = ArrayList()

    init {
        setHasStableIds(true)
    }

    fun setTracks(tracks: List<TrackAndStatisticsWithLabels>) {
        allTracks = ArrayList(tracks)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return allTracks.size
    }

    override fun getItemId(position: Int): Long {
        return allTracks[position].track.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_compare_track, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val label = allTracks[position].track.name
        val sublabel = allTracks[position].label
        val sublabel2 = allTracks[position].label2
        holder.bind(label, sublabel, sublabel2)
    }

    inner class RecyclerViewHolder(internal var view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.track_row_label_container)
        internal lateinit var rowLabelContainer: View
        @BindView(R.id.track_row_label)
        internal lateinit var txtLabel: TextView
        @BindView(R.id.track_row_sub_label)
        internal lateinit var txtSubLabel: TextView
        @BindView(R.id.track_row_sub_label_2)
        internal lateinit var txtSubLabel2: TextView

        init {
            CheeseKnife.bind(this, view)
        }

        fun bind(label: String, sublabel: String, sublabel2: String) {
            txtLabel.text = label
            txtSubLabel.text = sublabel
            txtSubLabel2.text = sublabel2
        }
    }
}

