/*
 *  Copyright 2017-2022 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.trackcontrol

import android.os.Bundle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.exception.TrackRenderException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceConnectionListener
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import io.reactivex.rxjava3.disposables.Disposable

abstract class TrackControlPresenter<TView : ITrackControlView>
constructor(
    private val loggerFactory: ILoggerFactory,
    private val schedulerProvider: ISchedulerProvider,
    private val trackingServiceController: IGpsLoggerServiceController,
    private val permissionChecker: IPermissionChecker,
    private val analyticsEngine: IAnalyticsEngine,
    private val crashReporter: ICrashReporter,
    protected val eventBus: IEventBus
)
    : IGpsLoggerServiceConnectionListener, ITrackControlPresenter<TView> {

    override fun bind(theView: TView, savedInstanceState: Bundle?) {
        loggerFactory.logger.debug("TrackControlPresenter.bind")
        view = theView
        eventBusSubscriber = eventBus.subscribe(RecordingStatusChangedEvent::class.java)
        { event ->
            onRecordingStatusChanged(event)
        }
    }

    override fun unbind(theView: TView) {
        loggerFactory.logger.debug("TrackControlPresenter.unbind")
        eventBusSubscriber?.unsubscribe()
        eventBusSubscriber = null
        view = null
    }

    override fun start() {
        loggerFactory.logger.debug("TrackControlPresenter.start")
        trackingServiceController.bindToService(this)
        // we will attach the subscriptions after the service has connected
    }

    override fun stop() {
        loggerFactory.logger.debug("TrackControlPresenter.stop")
        resetSubscriptions()
        trackingServiceController.unbindFromService()
    }

    override fun connectService(serviceBinder: GpsLoggerService.LocalBinder) {
        loggerFactory.logger.debug("TrackControlPresenter: service connected")
        locationTrackingService = serviceBinder.service
        onLocationTrackingServiceConnected()
    }

    override fun disconnectService() {
        locationTrackingService = null
    }

    protected open fun onRecordingStatusChanged(event: RecordingStatusChangedEvent) {
        // we dont actually care what the new status is we just need to update the UI
        loggerFactory.logger.debug("TrackControlPresenter: eventbus: recording status changed ${event.statusChangedTo}")
        view?.updateAvailableActions()

    }

    protected open fun resetSubscriptions() {
        loggerFactory.logger.debug("TrackControlPresenter: resetSubscriptions")
        trackSubscriber?.dispose()
        trackSubscriber = null
    }

    protected open fun onLocationTrackingServiceConnected() {
        loggerFactory.logger.debug("TrackControlPresenter: onLocationTrackingServiceConnected")
        trackBeingRecorded = locationTrackingService?.currentTrackObject
        trackBeingRecorded?.let {
            loggerFactory.logger.debug("TrackControlPresenter: onLocationTrackingServiceConnected track being recorded id = ${it.id}")
        }
        subscribeToTrack()
        view?.updateAvailableActions()
    }

    private fun subscribeToTrack() {
        loggerFactory.logger.debug("TrackControlPresenter: subscribeToTrack")

        trackSubscriber?.dispose()     // discard any existing subscription

        val observable = locationTrackingService!!.currentTrack
        // whenever the track object reference changes we need to subscribe the new track's positions
        trackSubscriber = observable
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe (
                { track ->
                    onTrackSubscribed(track)
                },
                { error: Throwable ->
                    view?.showMessage(R.string.error_updating_track)
                    loggerFactory.logger.error("TrackControlPresenter: subscribeToTrack", error)
                    crashReporter.logNonFatalException(TrackRenderException("track update", error))
                }
            )
    }

    protected open fun onTrackSubscribed(track: Track) {
        loggerFactory.logger.debug("TrackControlPresenter: subscribeToTrack - ID ${track.id}")
        trackBeingRecorded = track
    }

    protected fun isThisTrackCurrentlyBeingRecorded(trackId: Long): Boolean {
        if (locationTrackingService?.isTracking == true) {
            return (trackBeingRecorded?.id == trackId)
        }
        return false
    }

    protected fun isThisTrackCurrentlyPaused(trackId: Long): Boolean {
        if (locationTrackingService?.isPaused == true) {
            return (trackBeingRecorded?.id == trackId)
        }
        return false
    }

    protected fun isTrackControlAction(actionId: Int) : Boolean {
        val trackControlActions = listOf(R.id.fab_start, R.id.fab_stop, R.id.fab_pause, R.id.fab_resume)
        return trackControlActions.contains(actionId)
    }

    override fun isActionAvailable(actionId: Int): Boolean {
        when (actionId) {
            R.id.fab_start -> {
                return permissionChecker.hasLocationPermission() && locationTrackingService != null
                        && !locationTrackingService!!.isTracking && !locationTrackingService!!.isPaused
            }
            R.id.fab_resume -> {
                return permissionChecker.hasLocationPermission() && locationTrackingService != null
                        && locationTrackingService!!.isPaused
            }
            R.id.fab_pause -> {
                return permissionChecker.hasLocationPermission() && locationTrackingService != null
                        && locationTrackingService!!.isTracking
            }
            R.id.fab_stop -> {
                return permissionChecker.hasLocationPermission() && locationTrackingService != null
                        && (locationTrackingService!!.isTracking || locationTrackingService!!.isPaused)
            }
        }
        return false
    }

    override fun actionSelected(itemId: Int): Boolean {
        loggerFactory.logger.debug("TrackControlPresenter: actionSelected ${itemId}")
        when (itemId) {
            R.id.fab_start -> {
                if (!permissionChecker.hasLocationPermission()) {
                    // we should never get here as we disable the menu - but you never know
                    view?.showMessage(R.string.location_permission_denied)
                    return true
                }
                if (!permissionChecker.hasPushNotificationPermission()) {
                    // the app will work without this permission but we should alert the user that the HUD buttons will not be there
                    loggerFactory.logger.warn("TrackControlPresenter: notifications disabled")
                    view?.showMessage(R.string.notification_permission_denied)
                }
                analyticsEngine.recordClick(AnalyticsConstants.START_ID, AnalyticsConstants.START_NAME, AnalyticsConstants.FAB_TYPE)
                trackingServiceController.startService()    // this will start the service if its not already started - and makes sure it does not go away
                locationTrackingService?.startTracking()
                onTrackStarted(locationTrackingService?.currentTrackObject)
                view?.updateAvailableActions()
                return true
            }
            R.id.fab_resume -> {
                if (!permissionChecker.hasLocationPermission()) {
                    // we should never get here as we disable the menu - but you never know
                    view?.showMessage(R.string.location_permission_denied)
                    return true
                }
                analyticsEngine.recordClick(AnalyticsConstants.RESUME_ID, AnalyticsConstants.RESUME_NAME, AnalyticsConstants.FAB_TYPE)
                locationTrackingService?.resumeTracking(false)
                onTrackResumed()
                view?.updateAvailableActions()
                return true
            }
            R.id.fab_pause -> {
                // don't stop the service as we may need to restart the track
                // if we restart tracking it will use the current track
                analyticsEngine.recordClick(AnalyticsConstants.PAUSE_ID, AnalyticsConstants.PAUSE_NAME, AnalyticsConstants.FAB_TYPE)
                locationTrackingService?.pauseTracking(false)
                view?.updateAvailableActions()
                onTrackPaused()
                return true
            }
            R.id.fab_stop -> {
                analyticsEngine.recordClick(AnalyticsConstants.STOP_ID, AnalyticsConstants.STOP_NAME, AnalyticsConstants.FAB_TYPE)
                locationTrackingService?.stopTracking(false)
                trackingServiceController.stopService()      // the service will not stop yet - we are still bound to it
                view?.updateAvailableActions()
                onTrackStopped()
                return true
            }
        }
        return false
    }

    protected open fun onTrackStarted(track: Track?) {
    }

    protected open fun onTrackPaused() {
    }

    protected open fun onTrackResumed() {
    }

    protected open fun onTrackStopped() {
    }


    // there is no state in these as all of these are reinitialised when the presenter gets started
    protected var view: TView? = null

    protected var locationTrackingService: IGpsLoggerService? = null
    private var trackSubscriber: Disposable? = null
    protected var trackBeingRecorded: Track? = null

    private var eventBusSubscriber: IEventBusSubscription? = null
}