/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.settings

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.andrewandderek.trailblazer.ui.view.OkCancelDialogFragment

class SettingsActivity :
    OkCancelDialogFragment.Listener,
    AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, SettingsFragment(), TAG_SETTINGS_FRAGMENT)
                .commit()

        initActionBar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun ok(tag: String, userData: String) {
        val fragment = supportFragmentManager.findFragmentByTag(TAG_SETTINGS_FRAGMENT) as OkCancelDialogFragment.Listener
        fragment.ok(tag, userData)
    }

    override fun cancel(tag: String, userData: String) {
        val fragment = supportFragmentManager.findFragmentByTag(TAG_SETTINGS_FRAGMENT) as OkCancelDialogFragment.Listener
        fragment.cancel(tag, userData)
    }

    companion object {
        public const val TAG_SETTINGS_FRAGMENT = "SettingsActivity:settings_fragment_tag"
    }
}
