/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.cheeseknife

import android.view.View
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.OnClick
import com.andrewandderek.trailblazer.ui.cheeseknife.binders.BinderFunction
import com.andrewandderek.trailblazer.ui.cheeseknife.binders.FieldBinders
import com.andrewandderek.trailblazer.ui.cheeseknife.binders.MethodBinders
import java.lang.reflect.AccessibleObject

class ClassBinder(private val boundClass: Class<Any>) {

    private val binders: List<BinderFunction>

    init {
        binders = getFieldBinders()
            .plus(getMethodBinders())
    }

    fun bind(targetObject: Any, sourceView: View) {
        binders.forEach { binder ->
            binder.invoke(targetObject, sourceView)
        }
    }

    private fun getFieldBinders(): List<BinderFunction> {
        val fieldBinders = mutableListOf<BinderFunction>()
        boundClass.declaredFields.forEach { field ->
            field.getAnnotation(BindView::class.java)?.let {
                makeAccessible(field)
                fieldBinders.add(FieldBinders.viewBinder(field, it.id))
            }
            // Add new field bindings here
        }
        return fieldBinders.toList()
    }

    private fun getMethodBinders(): List<BinderFunction> {
        val methodBinders = mutableListOf<BinderFunction>()
        boundClass.methods.forEach { method ->
            method.getAnnotation(OnClick::class.java)?.let {
                makeAccessible(method)
                methodBinders.add(MethodBinders.onClickBinder(method, it.id))
            }
            // Add new method bindings here
        }
        return methodBinders.toList()
    }

    private fun makeAccessible(accessibleObject: AccessibleObject) {
        if (!accessibleObject.isAccessible) {
            accessibleObject.isAccessible = true
        }
    }
}