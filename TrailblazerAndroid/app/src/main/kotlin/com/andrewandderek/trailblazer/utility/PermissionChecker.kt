/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import javax.inject.Inject

interface IPermissionChecker {
    fun hasLocationPermission(): Boolean
    fun hasReadStoragePermission(): Boolean
    fun hasWriteStoragePermission(): Boolean
    fun hasPushNotificationPermission(): Boolean
}

class PermissionChecker
@Inject constructor(
    internal var context: Context,
    internal var fileSystemHelper: IFileSystemHelper,
) : IPermissionChecker
{
    override fun hasReadStoragePermission(): Boolean {
        if (fileSystemHelper.usingLegacyFileAccess()) {
            return hasPermissionBeenGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        // if we are not using legacy file access then we do not need any additional permissions to do our work
        // anyway they will not be granted at Android 13 and above
        // so lets just say we have all the permissions we are going to need
        return true
    }

    override fun hasWriteStoragePermission(): Boolean {
        if (fileSystemHelper.usingLegacyFileAccess()) {
            return hasPermissionBeenGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        // if we are not using legacy file access then we do not need any additional permissions to do our work
        // anyway they will not be granted at Android 13 and above
        // so lets just say we have all the permissions we are going to need
        return true
    }

    override fun hasPushNotificationPermission(): Boolean {
        return hasPermissionBeenGranted(Manifest.permission.POST_NOTIFICATIONS)
    }

    override fun hasLocationPermission(): Boolean {
        return hasPermissionBeenGranted(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    private fun hasPermissionBeenGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }
}