/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.repository

import android.content.ContentValues
import android.database.Cursor
import com.andrewandderek.trailblazer.data.DatabaseHelper
import com.andrewandderek.trailblazer.data.schema.PositionTable
import com.andrewandderek.trailblazer.data.schema.TrackMetadataTable
import com.andrewandderek.trailblazer.data.schema.TrackTable
import com.andrewandderek.trailblazer.data.utility.DatabaseFieldConverter
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Track
import javax.inject.Inject

interface ITrackRepository : IRepository<Track, Long> {
    fun getWithPositions(id: Long, positionRepository: IPositionRepository): Track?
    fun getWithPositionsAndAccuracy(id: Long, accuracyThreshold: Float, positionRepository: IPositionRepository): Track?
    fun getMaxId(): Long?
    fun getTrackList(positionRepository: IPositionRepository): List<Track>     // in future we may return a List<TrackDetail> - a list of track objects with addition detail like distance etc
    fun getTrackListWithoutAnyPositions(): List<Track>
    fun getTracksWithoutAnyMetadata(): List<Track>
    fun getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated(): List<Track>
}

class TrackRepository
@Inject constructor(
    internal var loggerFactory: ILoggerFactory,
    private var databaseHelper: DatabaseHelper
) : BaseRepository<Track>(), ITrackRepository
{

    val QUERY_TRACK_AND_METADATA_SQL = """
        SELECT 
              t.${TrackTable.COLUMN_ID},
              t.${TrackTable.COLUMN_NAME},
              t.${TrackTable.COLUMN_NOTES},
              ${DatabaseFieldConverter.getCalendarSelect(TrackTable.COLUMN_TIME_STARTED)},
              meta.${TrackMetadataTable.COLUMN_TRACK_ID},
              meta.${TrackMetadataTable.COLUMN_VERSION_NAME},
              meta.${TrackMetadataTable.COLUMN_VERSION_CODE},
              ${DatabaseFieldConverter.getCalendarSelect(TrackMetadataTable.COLUMN_TIME_ENDED)},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_DISTANCE_METERS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_ELAPSED_MILLISECONDS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_RECORDED_MILLISECONDS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_MOVING_MILLISECONDS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_SPEED_METERS_PER_SECOND},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_MOVING_SPEED_METERS_PER_SECOND},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_MINIMUM_ELEVATION_METERS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_MAXIMUM_ELEVATION_METERS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_DIFFERENCE_METERS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_GAIN_METERS},
              meta.${TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_LOSS_METERS}
        FROM ${TrackTable.TABLE_TRACK} t
          LEFT JOIN ${TrackMetadataTable.TABLE_TRACK_METADATA} meta on t.${TrackTable.COLUMN_ID} = meta.${TrackMetadataTable.COLUMN_TRACK_ID}
        """

    override fun create(newItem: Track): Track {
        val values:ContentValues = trackToContentValues(newItem)
        val insertId = databaseHelper.getDatabase().insert(TrackTable.TABLE_TRACK, null, values)
        loggerFactory.logger.debug("track created with id $insertId")
        return(getById(insertId)!!)
    }

    override fun update(item: Track): Track {
        val values:ContentValues = trackToContentValues(item)
        loggerFactory.logger.debug("updating track with id ${item.id}")
        databaseHelper.getDatabase().update(TrackTable.TABLE_TRACK, values, "${TrackTable.COLUMN_ID} = ${item.id}", null)
        return(getById(item.id)!!)
    }

    override fun delete(item: Track) {
        deleteById(item.id)
    }

    override fun deleteById(id: Long) {
        loggerFactory.logger.debug("deleting track with id $id")

        // no need for a transaction as we use ON DELETE CASCADE
        databaseHelper.getDatabase().delete(TrackTable.TABLE_TRACK, "${TrackTable.COLUMN_ID} = ${id}", null)
    }

    override fun deleteAll() {
        loggerFactory.logger.debug("deleted ALL tracks")

        // no need for a transaction as we use ON DELETE CASCADE
        databaseHelper.getDatabase().delete(TrackTable.TABLE_TRACK, null, null)
    }

    override fun getMaxId(): Long? {
        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL +
        """
        ORDER BY t.${TrackTable.COLUMN_ID}  DESC LIMIT 1            
        """
        val track = getSingleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
        return track?.id
    }

    override fun getById(id: Long): Track? {
        loggerFactory.logger.debug("getting track with id $id")
        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL +
                """
        WHERE t.${TrackTable.COLUMN_ID} = ${id}
        """
        return getSingleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
    }

    override fun getAll(): List<Track> {
        loggerFactory.logger.debug("getting all tracks")
        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL
        return getMultipleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
    }

    override fun getTrackListWithoutAnyPositions(): List<Track> {
        loggerFactory.logger.debug("getting track list - quick")

        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL +
        """
        ORDER BY t.${TrackTable.COLUMN_TIME_STARTED} DESC            
        """
        return getMultipleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
    }

    override fun getTrackList(positionRepository: IPositionRepository): List<Track> {
        loggerFactory.logger.debug("getting track list")

        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL +
        """
        ORDER BY t.${TrackTable.COLUMN_TIME_STARTED} DESC            
        """
        val tracks = getMultipleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
        for (thisTrack in tracks) {
            val lastPosition = positionRepository.getLastPositionOnTrack(thisTrack.id)
            thisTrack.replaceTrack(lastPosition)
        }
        return tracks
    }

    /**
     * gets all the tracks that do not have any metadata
     */
    override fun getTracksWithoutAnyMetadata(): List<Track> {
        loggerFactory.logger.debug("getting tracks without metadata")

        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL +
                """
        WHERE meta.${TrackMetadataTable.COLUMN_TRACK_ID} is null
        """
        return getMultipleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
    }

    /**
     * like [getTracksWithoutAnyMetadata] but will also get any tracks that have points recorded since the last generation
     * Note: this is a very expensive query if you dont need the recording tracks then use [getTracksWithoutAnyMetadata]
     * Also: although the UI only allows for one track to record at once, the SQL will support multiple tracks being recorded
     */
    override fun getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated(): List<Track> {
        loggerFactory.logger.debug("getting tracks without metadata - including tracks being recorded")

        val QUERY_SQL = QUERY_TRACK_AND_METADATA_SQL +
                """
        WHERE meta.${TrackMetadataTable.COLUMN_TRACK_ID} is null
        or t.${TrackTable.COLUMN_ID} in (
            SELECT p.${PositionTable.COLUMN_TRACK_ID}
            FROM ${PositionTable.TABLE_POSITION} p
            WHERE p.${PositionTable.COLUMN_TIME_RECORDED} > (
                SELECT meta2.${TrackMetadataTable.COLUMN_TIME_ENDED}
                FROM ${TrackMetadataTable.TABLE_TRACK_METADATA} meta2
                ORDER BY meta2.${TrackMetadataTable.COLUMN_TIME_ENDED} desc
                LIMIT 1
            )
        )
        """
        return getMultipleFromComplexQuery(databaseHelper, QUERY_SQL, this::cursorToTrack)
    }

    override fun getWithPositions(id: Long, positionRepository: IPositionRepository): Track? {
        loggerFactory.logger.debug("getting track and positions with id $id")
        val track: Track? = getById(id)
        track?.replaceTrack(positionRepository.getByTrackId(track.id))
        return track
    }

    override fun getWithPositionsAndAccuracy(id: Long, accuracyThreshold: Float, positionRepository: IPositionRepository): Track? {
        loggerFactory.logger.debug("getting track and positions with id $id and accuracy $accuracyThreshold")
        val track: Track? = getById(id)
        track?.replaceTrack(positionRepository.getByTrackIdWithAccuracy(track.id, accuracyThreshold))
        return track
    }

    private fun trackToContentValues(item: Track): ContentValues {
        // we only save the track table data - the metadata is saved using the metadata repo
        val values = ContentValues()
        // the ID is allocated by the DB - do not put it in here
        values.put(TrackTable.COLUMN_NAME, item.name)
        values.put(TrackTable.COLUMN_NOTES, item.notes)
        values.put(TrackTable.COLUMN_TIME_STARTED, DatabaseFieldConverter.convertCalendarTimeToDbString(item.started))
        return values
    }

    private fun cursorToTrack(cursor: Cursor): Track {
        val track = Track()
        track.id = DatabaseFieldConverter.getLong(cursor, TrackTable.COLUMN_ID)
        track.name = DatabaseFieldConverter.getString(cursor, TrackTable.COLUMN_NAME)
        track.notes = DatabaseFieldConverter.getString(cursor, TrackTable.COLUMN_NOTES)
        track.started = DatabaseFieldConverter.getCalendar(cursor, TrackTable.COLUMN_TIME_STARTED)
        if (DatabaseFieldConverter.isColumnPresentAndNotNull(cursor, TrackMetadataTable.COLUMN_TRACK_ID)) {
            // if we return the metadata track id column we assume the whole table is in the select
            // if its not in the select then the track object will not have any metadata
            track.metadata = TrackMetadataRepository.cursorToTrackMetadata(cursor)
        }
        return track
    }
}