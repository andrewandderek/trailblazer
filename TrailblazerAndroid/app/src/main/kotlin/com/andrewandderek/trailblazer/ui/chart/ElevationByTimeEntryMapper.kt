/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.chart

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.statistics.elevation.ElevationHelper
import com.andrewandderek.trailblazer.statistics.elevation.IElevationSettings
import com.andrewandderek.trailblazer.units.conversion.IAltitudeConverter
import com.github.mikephil.charting.data.Entry
import javax.inject.Inject

class ElevationByTimeEntryMapper
@Inject constructor(
        private val userSettings: IUserSettings,
        private val elevationSettings: IElevationSettings,
        private val altitudeConverter: IAltitudeConverter
)
    : IPositionToEntryMapper {
    override fun mapToEntry(position: Position, firstPosition: Position): Entry {
        val timeOffsetInMilliseconds = (getPositionTime(position) - getPositionTime(firstPosition))
        val altitudeInMetres = ElevationHelper.getAltitude(position, elevationSettings)
        val altitude = altitudeConverter.convertMetres(altitudeInMetres, userSettings.altitudeUnit)
        return Entry(timeOffsetInMilliseconds.toFloat(), altitude.toFloat())
    }

    private fun getPositionTime(position: Position):Long =
            position.timeRecorded?.timeInMillis ?: 0L
}