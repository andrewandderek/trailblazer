/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.schema

import android.database.sqlite.SQLiteDatabase
import com.andrewandderek.trailblazer.data.utility.DatabaseUtilities
import com.andrewandderek.trailblazer.logging.ILoggerFactory

object TrackMetadataTable {
    const val TABLE_TRACK_METADATA = "track_metadata"
    const val TABLE_TRACK_METADATA_BACKUP = "track_metadata_backup"
    const val INDEX_TRACK_METADATA_TRACK_ID = "index_track_metadata_track_id"

    const val COLUMN_TRACK_ID = "track_id"
    const val COLUMN_VERSION_NAME = "version_name"
    const val COLUMN_VERSION_CODE = "version_code"
    const val COLUMN_TIME_ENDED = "time_ended_utc"        // UTC milliseconds
    const val COLUMN_STATISTICS_DISTANCE_METERS = "stats_distance_meters"
    const val COLUMN_STATISTICS_ELAPSED_MILLISECONDS = "stats_elapsed_milliseconds"
    const val COLUMN_STATISTICS_RECORDED_MILLISECONDS = "stats_recorded_milliseconds"
    const val COLUMN_STATISTICS_MOVING_MILLISECONDS = "stats_moving_milliseconds"
    const val COLUMN_STATISTICS_AVERAGE_SPEED_METERS_PER_SECOND = "stats_ave_speed_meters_per_second"
    const val COLUMN_STATISTICS_AVERAGE_MOVING_SPEED_METERS_PER_SECOND = "stats_ave_moving_speed_meters_per_second"
    const val COLUMN_STATISTICS_MINIMUM_ELEVATION_METERS = "stats_min_elevation_meters"
    const val COLUMN_STATISTICS_MAXIMUM_ELEVATION_METERS = "stats_max_elevation_meters"
    const val COLUMN_STATISTICS_ELEVATION_DIFFERENCE_METERS = "stats_elevation_difference_meters"
    const val COLUMN_STATISTICS_ELEVATION_GAIN_METERS = "stats_elevation_gain_meters"
    const val COLUMN_STATISTICS_ELEVATION_LOSS_METERS = "stats_elevation_loss_meters"

    // Database creation SQL statement
    private const val DATABASE_CREATE = """
    create table $TABLE_TRACK_METADATA(
     $COLUMN_TRACK_ID integer not null UNIQUE CONSTRAINT fk_track_metadata_track REFERENCES ${TrackTable.TABLE_TRACK}(${TrackTable.COLUMN_ID}) ON DELETE CASCADE,
     $COLUMN_VERSION_NAME varchar(255) not null default '',
     $COLUMN_VERSION_CODE integer not null default 0,
     $COLUMN_TIME_ENDED DATETIME null,
     $COLUMN_STATISTICS_DISTANCE_METERS REAL not null default 0,
     $COLUMN_STATISTICS_ELAPSED_MILLISECONDS integer not null default 0,
     $COLUMN_STATISTICS_RECORDED_MILLISECONDS integer not null default 0,
     $COLUMN_STATISTICS_MOVING_MILLISECONDS integer not null default 0,
     $COLUMN_STATISTICS_AVERAGE_SPEED_METERS_PER_SECOND REAL not null default 0,
     $COLUMN_STATISTICS_AVERAGE_MOVING_SPEED_METERS_PER_SECOND REAL not null default 0,
     $COLUMN_STATISTICS_MINIMUM_ELEVATION_METERS REAL not null default 0,
     $COLUMN_STATISTICS_MAXIMUM_ELEVATION_METERS REAL not null default 0,
     $COLUMN_STATISTICS_ELEVATION_DIFFERENCE_METERS REAL not null default 0,
     $COLUMN_STATISTICS_ELEVATION_GAIN_METERS REAL not null default 0,
     $COLUMN_STATISTICS_ELEVATION_LOSS_METERS REAL not null default 0
    );"""

    private const val INDEX_CREATE = """
    create index $INDEX_TRACK_METADATA_TRACK_ID  ON $TABLE_TRACK_METADATA (
        $COLUMN_TRACK_ID
    );"""

    fun onCreate(database: SQLiteDatabase) {
        database.execSQL(DATABASE_CREATE)
        database.execSQL(INDEX_CREATE)
    }

    @Suppress("UNUSED_PARAMETER")
    fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int, loggerFactory: ILoggerFactory) {
        // make sure that any modification statements match the create statement above

        if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 7)) {
            // this will be the only version that we can just create the table
            loggerFactory.logger.debug("creating track metadata table")
            onCreate(database)
        }

        if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 8)) {
            loggerFactory.logger.debug("regenerating track metadata table to add statistics")
            // we could alter the existing table and add each column one by one
            // but all the data on the table needs to be regenerated anyway so we will just drop the table
            DatabaseUtilities.dropTable(database, TABLE_TRACK_METADATA)
            // and recreate it with the new columns
            recreateTable(database)
        }

        // if you need to recreate the table without loosing the data, for example you need to alter a column (Sqlite does not support alter column)
        // then uncover the call to DatabaseUtilities.recreateAndCopyTable
        // DatabaseUtilities.recreateAndCopyTable(database, TABLE_TRACK_METADATA, TABLE_TRACK_METADATA_BACKUP, { db -> recreateTable(db) })

        // make sure that any modification statements match the create statements above
    }

    private fun recreateTable(db: SQLiteDatabase) {
        DatabaseUtilities.dropIndex(db, INDEX_TRACK_METADATA_TRACK_ID)
        onCreate(db)
    }

    fun onInitialiseData(@Suppress("UNUSED_PARAMETER") database: SQLiteDatabase) {
    }
}


