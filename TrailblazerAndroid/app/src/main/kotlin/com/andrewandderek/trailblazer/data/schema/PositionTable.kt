/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.schema

import android.database.sqlite.SQLiteDatabase
import com.andrewandderek.trailblazer.data.utility.DatabaseUtilities
import com.andrewandderek.trailblazer.logging.ILoggerFactory

// Database table
object PositionTable {
    const val TABLE_POSITION = "position"
    const val TABLE_POSITION_BACKUP = "position_backup"
    const val INDEX_POSITION_TRACK_ID = "index_position_track_id"

    const val COLUMN_ID = "_id"
    const val COLUMN_TRACK_ID = "track_id"
    const val COLUMN_LONGITUDE = "longitude"
    const val COLUMN_LATITUDE = "latitude"
    const val COLUMN_ALTITUDE = "altitude"
    const val COLUMN_TIME_RECORDED = "time_recorded_utc"
    const val COLUMN_NOTES = "notes"
    const val COLUMN_ACCURACY = "accuracy"
    const val COLUMN_FLAGS = "flags"
    const val COLUMN_SEGMENT = "segment"

    // Database creation SQL statement
    // Note that we cascade the delete of any track to remove its points - you need to enable the foreign key constraint using a PRAGMA when connecting
    private const val TABLE_CREATE = """
    create table $TABLE_POSITION(
     $COLUMN_ID integer primary key autoincrement,
     $COLUMN_TRACK_ID integer not null CONSTRAINT fk_position_track REFERENCES ${TrackTable.TABLE_TRACK}(${TrackTable.COLUMN_ID}) ON DELETE CASCADE,
     $COLUMN_LONGITUDE REAL not null,
     $COLUMN_LATITUDE REAL not null,
     $COLUMN_ALTITUDE REAL null,
     $COLUMN_TIME_RECORDED DATETIME null,
     $COLUMN_NOTES varchar(255) not null default '',
     $COLUMN_ACCURACY REAL not null default 0,
     $COLUMN_FLAGS integer not null default 0,
     $COLUMN_SEGMENT integer not null default 0
    );"""

    private const val INDEX_CREATE = """
    create index $INDEX_POSITION_TRACK_ID  ON $TABLE_POSITION (
        $COLUMN_TRACK_ID
    );"""

    fun onCreate(database: SQLiteDatabase) {
        database.execSQL(TABLE_CREATE)
        database.execSQL(INDEX_CREATE)
    }

    fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int, loggerFactory: ILoggerFactory) {
        // make sure that any modification statements match the create statement above
        if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 2)) {
            loggerFactory.logger.debug("adding accuracy column to position table")
            database.execSQL("""
                    ALTER TABLE $TABLE_POSITION  ADD COLUMN $COLUMN_ACCURACY REAL not null default 0
            """)
            loggerFactory.logger.debug("adding flags column to position table")
            database.execSQL("""
                    ALTER TABLE $TABLE_POSITION  ADD COLUMN $COLUMN_FLAGS integer not null default 0
            """)
        }
        if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 3)) {
            loggerFactory.logger.debug("adding segment column to position table")
            database.execSQL("""
                    ALTER TABLE $TABLE_POSITION  ADD COLUMN $COLUMN_SEGMENT integer not null default 0
            """)
        }
        if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 4)) {
            loggerFactory.logger.debug("recreating/copying position table to make altitude nullable")
            DatabaseUtilities.recreateAndCopyTable(database, TABLE_POSITION, TABLE_POSITION_BACKUP, { db -> recreateTable(db) })
        }
        else if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 5)) {
            // We don't do this if we're upgrading from something older than v4 as the code above recreates the table with the index
            loggerFactory.logger.debug("adding track_id index to position table")
            database.execSQL(INDEX_CREATE)
        }
        if (DatabaseUtilities.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 6)) {
            loggerFactory.logger.debug("recreating/copying position table to make timestamp nullable")
            DatabaseUtilities.recreateAndCopyTable(database, TABLE_POSITION, TABLE_POSITION_BACKUP, { db -> recreateTable(db) })
        }
        // make sure that any modification statements match the create statements above
    }

    private fun recreateTable(db: SQLiteDatabase) {
        DatabaseUtilities.dropIndex(db, INDEX_POSITION_TRACK_ID)
        onCreate(db)
    }

    fun onInitialiseData(@Suppress("UNUSED_PARAMETER") database: SQLiteDatabase) {
    }
}