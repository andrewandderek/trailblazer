/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.utility.IMapColourSchemeProvider
import com.andrewandderek.trailblazer.utility.PositionBoundsCalculator
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import kotlin.math.max
import kotlin.math.min


class TrackMapView : LinearLayout, OnMapReadyCallback {

    public interface IZoomLevelObserver {
        fun onMapZoomLevelChanged(newZoomLevel: Float)
    }

    private val positionBoundsCalculator = PositionBoundsCalculator()
    private lateinit var mapView: MapView
    private var googleMap: GoogleMap? = null
    private var segments: MutableMap<Long, Polyline> = mutableMapOf()
    private var isPotentiallyZooming = false
    private var currentZoomLevel = Float.MIN_VALUE

    public var zoomLevelObserver: IZoomLevelObserver? = null

    lateinit var mapColourSchemeProvider: IMapColourSchemeProvider

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        mapView = if (attrs == null)
            MapView(context, null as AttributeSet?)
        else
            MapView(context, attrs, defStyle)
        addView(mapView)
        mapView.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        setupMap()
    }

    // this may be called multiple times but really we are waiting for the call after we have the location permission granted
    // it does not really matter that it is called multiple times
    fun setupMap() {
        if (ContextCompat.checkSelfPermission(this.context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap?.isMyLocationEnabled = true
        }
        googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap?.mapColorScheme = mapColourSchemeProvider.getMapColourScheme(context)
        googleMap?.setOnCameraMoveStartedListener {
            cameraMoveStarted()
        }
        googleMap?.setOnCameraIdleListener {
            cameraIdle()
        }
        googleMap?.setOnCameraMoveCanceledListener {
            cameraMoveCancelled()
        }
        clearTrack()
    }

    private fun clearTrack() {
        if (googleMap == null) {
            // no map yet - nothing to clear
            return
        }
        googleMap?.clear()     // clear tracks and markers
        segments = mutableMapOf()
    }

    fun onCreate(savedInstanceState: Bundle?, mapColourSchemeProvider: IMapColourSchemeProvider) {
        this.mapColourSchemeProvider = mapColourSchemeProvider
        mapView.onCreate(savedInstanceState)
    }

    fun onDestroy() {
        mapView.onDestroy()
    }

    fun onStart() {
        mapView.onStart()
    }

    fun onStop() {
        mapView.onStop()
    }

    fun onResume() {
        mapView.onResume()
    }

    fun onPause() {
        mapView.onPause()
    }

    fun onSaveInstanceState(outState: Bundle) {
        mapView.onSaveInstanceState(outState)
    }

    fun onLowMemory() {
        mapView.onLowMemory()
    }

    fun centreMapAndZoom(position: Position, zoomLevel: Float) {
        val latLng = LatLng(position.latitude, position.longitude)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
    }

    fun centreMapAndZoomToFit(positions: List<Position>) {
        // Can't centre if nothing to centre on, and obviously we can't zoom to fit either
        if (positions.isEmpty())
            return

        val bounds = positionBoundsCalculator.calculateBounds(positions)
        val southWestLatLong = LatLng(bounds.southWest.latitude, bounds.southWest.longitude)
        val northEastLatLong = LatLng(bounds.northEast.latitude, bounds.northEast.longitude)
        val latLongBounds = LatLngBounds(southWestLatLong, northEastLatLong)

        googleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(latLongBounds, calculateZoomToFitPadding()))
    }

    private fun getSegmentPolyline(segment: Long): Polyline {
        synchronized(this) {
            if (segments.containsKey(segment)) {
                return segments[segment]!!
            }
            return createSegmentPolyline(segment)
        }
    }

    private fun createSegmentPolyline(segment: Long): Polyline {
        val colourResId = if (segment % 2L == 0L) { R.color.map_segment_even } else { R.color.map_segment_odd }
        // this could be null if the googleMap has not attached yet
        val polyLine = googleMap?.addPolyline(PolylineOptions().width(5f).color(ContextCompat.getColor(context, colourResId)))
        // we cannot complete the draw operation - we need to throw
        segments[segment] = polyLine!!
        return polyLine
    }

    fun drawAllPositions(trackPositions: List<Position>) {
        synchronized(this) {
            clearTrack()
            val points: MutableMap<Long, MutableList<LatLng>> = mutableMapOf()

            // add all the points
            for (thisPosition in trackPositions) {
                if (points.containsKey(thisPosition.segment)) {
                    points[thisPosition.segment]!!.add(LatLng(thisPosition.latitude, thisPosition.longitude))
                } else {
                    points[thisPosition.segment] = mutableListOf(LatLng(thisPosition.latitude, thisPosition.longitude))
                }
            }

            // draw all the segments
            for (thisKey in points.keys) {
                val thisSegmentPolyline = getSegmentPolyline(thisKey)
                val polylinePoints = thisSegmentPolyline.points
                polylinePoints.addAll(points[thisKey]!!.asIterable())
                thisSegmentPolyline.points = polylinePoints
            }
        }
    }

    fun addPosition(newPosition: Position) {
        val thisSegmentPolyline = getSegmentPolyline(newPosition.segment)
        val polylinePoints = thisSegmentPolyline.points
        polylinePoints.add(LatLng(newPosition.latitude, newPosition.longitude))
        thisSegmentPolyline.points = polylinePoints
    }

    fun clearAllPositions() {
        clearTrack()
    }

    private fun calculateZoomToFitPadding(): Int {
        val shortestDimension = min(width, height)
        val calculatedPadding = (shortestDimension * ZOOM_TO_FIT_PADDING_PERCENTAGE) / 100
        return max(calculatedPadding, MIN_ZOOM_TO_FIT_PADDING_PIXELS)
    }

    private fun cameraMoveStarted() {
        isPotentiallyZooming = true
    }

    private fun cameraIdle() {
        // Has there been a real move initiated or is this just the initial map idle state indication?
        if (!isPotentiallyZooming)
            return

        isPotentiallyZooming = false

        googleMap?.cameraPosition?.zoom?.let {
            updateZoomLevel(it)
        }
    }

    private fun cameraMoveCancelled() {
        isPotentiallyZooming = false
    }

    private fun updateZoomLevel(zoomLevel: Float) {
        if (zoomLevel == currentZoomLevel)
            return

        zoomLevelObserver?.onMapZoomLevelChanged(zoomLevel)
    }

    companion object {
        const val ZOOM_TO_FIT_PADDING_PERCENTAGE = 10
        const val MIN_ZOOM_TO_FIT_PADDING_PIXELS = 20
    }
}
