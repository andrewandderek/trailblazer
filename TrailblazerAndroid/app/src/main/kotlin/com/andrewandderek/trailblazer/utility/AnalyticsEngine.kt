/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

object AnalyticsConstants {
    // floating action buttons
    const val FAB_TYPE = "fab"

    const val START_ID = "1"
    const val START_NAME = "start"
    const val PAUSE_ID = "2"
    const val PAUSE_NAME = "pause"
    const val STOP_ID = "3"
    const val STOP_NAME = "stop"
    const val RESUME_ID = "16"
    const val RESUME_NAME = "resume"

    // the buttons on the notification
    const val HUD_TYPE = "hud"

    const val HUD_RESUME_ID = "4"
    const val HUD_RESUME_NAME = "resume_hud"
    const val HUD_PAUSE_ID = "5"
    const val HUD_PAUSE_NAME = "pause_hud"
    const val HUD_STOP_ID = "6"
    const val HUD_STOP_NAME = "stop_hud"

    // menu item
    const val ACTION_TYPE = "action"

    const val ACTION_EXPORT_ID = "7"
    const val ACTION_EXPORT_NAME = "export"
    const val ACTION_IMPORT_SELECTED_ID = "8"
    const val ACTION_IMPORT_SELECTED_NAME = "import_selected"
    const val ACTION_IMPORT_SHARED_ID = "9"
    const val ACTION_IMPORT_SHARED_NAME = "import_shared"
    const val ACTION_IMPORT_ID = "10"
    const val ACTION_IMPORT_NAME = "import"
    const val ACTION_SHARE_ID = "11"
    const val ACTION_SHARE_NAME = "share"
    const val ACTION_EXPORT_KML_ID = "12"
    const val ACTION_EXPORT_KML_NAME = "export_kml"
    const val ACTION_SHARE_STATS_SCREEN_ID = "20"
    const val ACTION_SHARE_STATS_SCREEN_NAME = "share_stats_screen"
    // from tracklist
    const val ACTION_ITEM_RENAME_ID = "21"
    const val ACTION_ITEM_RENAME_NAME = "rename_item"
    const val ACTION_ITEM_DELETE_ID = "22"
    const val ACTION_ITEM_DELETE_NAME = "delete_item"
    const val ACTION_COMPARE_ID = "23"
    const val ACTION_COMPARE_NAME = "compare"
    const val ACTION_RENAME_ID = "24"
    const val ACTION_RENAME_NAME = "rename"

    // internal events
    const val INTERNAL_TYPE = "internal"

    const val ACTION_IMPORT_GPX_ID = "13"
    const val ACTION_IMPORT_GPX_NAME = "import_gpx"
    const val ACTION_IMPORT_KML_ID = "14"
    const val ACTION_IMPORT_KML_NAME = "import_kml"
    const val ACTION_IMPORT_KMZ_ID = "15"
    const val ACTION_IMPORT_KMZ_NAME = "import_kmz"

}

interface IAnalyticsEngine {
    fun recordClick(id: String, name: String, type: String);
}

class FirebaseAnalyticsEngine
    @Inject constructor(
        internal var context: Context
    ) : IAnalyticsEngine {
    override fun recordClick(id: String, name: String, type: String) {
        val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name)
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, type)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }
}