/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer

import android.util.Log
import androidx.multidex.MultiDexApplication
import androidx.work.Configuration
import com.andrewandderek.trailblazer.data.DatabaseHelper
import com.andrewandderek.trailblazer.di.ApplicationModule
import com.andrewandderek.trailblazer.di.DaggerIApplicationComponent
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.settings.IDiagnosticSettings
import com.andrewandderek.trailblazer.settings.IThemeSetter
import com.andrewandderek.trailblazer.settings.IThemeSettings
import com.andrewandderek.trailblazer.ui.utility.IUiModeProvider
import com.andrewandderek.trailblazer.utility.IPreferencesProvider
import com.andrewandderek.trailblazer.worker.DaggerWorkerFactory
import com.andrewandderek.trailblazer.worker.trackmetadata.ITrackMetadataScheduler
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

// this is where we will export data to and copy the DB to
public const val APPLICATION_FOLDER = BuildConfig.APP_FOLDER

open class AndroidApplication :
        MultiDexApplication(),
        Configuration.Provider,
        HasAndroidInjector  {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var defaultDb: DatabaseHelper

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    @Inject
    lateinit var preferencesProvider: IPreferencesProvider

    @Inject
    lateinit var diagnosticSettings: IDiagnosticSettings

    @Inject
    lateinit var themeSettings: IThemeSettings

    @Inject
    lateinit var themeSetter: IThemeSetter

    @Inject
    lateinit var workerFactory: DaggerWorkerFactory

    @Inject
    lateinit var metadataScheduler: ITrackMetadataScheduler

    @Inject
    lateinit var uiModeProvider: IUiModeProvider

    override fun onCreate() {
        super.onCreate()

        DaggerIApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
                .inject(this)

        // if we do not set the logging config then the filename from the XML file will not be set correctly
        setupLoggingConfig()

        val version = getString(R.string.settings_version_summary_fmt, BuildConfig.VERSION_NAME, BuildConfig.GIT_HASH)
        val logLevel = loggerFactory.getLoggingLevel()
        Log.i("logging", "logging level = ${logLevel}")
        loggerFactory.logger.debug("app onCreate Version $version, Dark Mode ${uiModeProvider.isInDarkMode(this)}")

        preferencesProvider.setDefaultValues(R.xml.settings)

        themeSetter.setTheme(themeSettings.appTheme)

        initialiseDatabase()

        scheduleWorkerTasks()
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onTerminate() {
        defaultDb.getDatabase().close()   // time to go
        super.onTerminate()
    }

    protected fun setupLoggingConfig() {
        val level = diagnosticSettings.loggingLevel
        var dirs = this.getExternalFilesDirs(null);
        if (dirs != null && dirs[0] != null)
        {
            // use our external folder - dependes on package name
            Log.d("logging", "AndroidApplication:setupLoggingConfig Logs == ${dirs[0].absolutePath}");
            loggerFactory.setLoggingConfiguration(level, dirs[0].absolutePath)
        }
        else
        {
            // hard code and hope for the best
            loggerFactory.setLoggingConfiguration(level, "/sdcard/Android/data/${this.packageName}/files/")
        }
    }

    // override this in the unit tests to avoid creating a db
    protected open fun initialiseDatabase() {
        defaultDb.upgradeToLatestDatabaseSchema()
        loggerFactory.logger.debug("Database initialised")
        defaultDb.open()
    }

    // override this in the unit tests to avoid scheduling a task
    protected open fun scheduleWorkerTasks() {
        metadataScheduler.scheduleWorker()
    }

    override val workManagerConfiguration: Configuration
        get() {
            loggerFactory.logger.debug("Workmanager config supplied from application")
            return Configuration.Builder()
                .setMinimumLoggingLevel(android.util.Log.INFO)
                .setWorkerFactory(workerFactory)
                .build()
        }
}