/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Keep
import androidx.appcompat.widget.SearchView


// We need to exclude our specialised action view class from proguard renaming -
// see `android:actionViewClass` here: https://developer.android.com/guide/topics/resources/menu-resource#item-element
@Keep
class TrailblazerSearchView : SearchView {

    private var onCloseClickListener: OnClickListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle ) {
        init()
    }

    public fun setOnCloseButtonClickListener(listener: OnClickListener?) {
        onCloseClickListener = listener
    }

    override fun onActionViewExpanded() {
        // The base onActionViewExpanded clears the search text. However, it gets called when we
        // navigate back to the track list (eg. after we've searched then compared) so even
        // though the list reflects the previous search, the search text doesn't.
        // So here, we remember the text and reinstate it after calling the base.
        val originalQuery = query
        super.onActionViewExpanded()
        setQuery(originalQuery, false)
    }

    private fun init() {
        listenToCloseButton()
    }

    private fun listenToCloseButton() {
        // This is the only way to consistently hook the 'X' button in the search view
        findViewById<View>(androidx.appcompat.R.id.search_close_btn)?.setOnClickListener {
            onCloseClickListener?.onClick(it)
        }
    }
}