/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.bitmap

import android.graphics.Bitmap
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.sharing.importexport.BaseExporter
import com.andrewandderek.trailblazer.sharing.importexport.ExportResult
import com.andrewandderek.trailblazer.sharing.importexport.IBitmapExporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import java.io.FileOutputStream
import java.io.IOException

abstract class BaseBitmapExporter constructor(
        private val loggerFactory: ILoggerFactory,
        private var fileSystemHelper: IFileSystemHelper
) : BaseExporter(fileSystemHelper), IBitmapExporter {

    protected abstract val bitmapFormat: Bitmap.CompressFormat

    override fun exportBitmap(bitmap: Bitmap, filename: String): ExportResult {
        loggerFactory.logger.debug("Export stream to: ${filename}")
        var result: ExportResult = ExportResult.OK
        var outputStream: FileOutputStream? = null
        try {
            outputStream = fileSystemHelper.createFileOutputStream(filename)
            bitmap.compress(bitmapFormat, 100, outputStream)
        } catch (e: IOException) {
            loggerFactory.logger.error("error writing to ${filename} ", e)
            result = ExportResult.IO_ERROR
        } finally {
            outputStream?.flush()
            outputStream?.close()
        }
        return result
    }

}