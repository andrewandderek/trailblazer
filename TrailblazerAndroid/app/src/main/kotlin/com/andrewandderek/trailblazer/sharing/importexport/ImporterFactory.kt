/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import javax.inject.Inject
import javax.inject.Named

data class ImporterDefinition (
    val importer: IImporter,
    val analyticsId: String,
    val analyticsName: String,
    val analyticsType: String
)

interface IImporterFactory {
    fun getImporters(): List<ImporterDefinition>
}

class ImporterFactory @Inject constructor(
    @Named(NameLiterals.FILE_FORMAT_GPX) private val gpxImporter: IImporter,
    @Named(NameLiterals.FILE_FORMAT_KML) private val kmlImporter: IImporter,
    @Named(NameLiterals.FILE_FORMAT_KMZ) private val kmzImporter: IImporter
) : IImporterFactory {
    override fun getImporters(): List<ImporterDefinition> {
        // the order of these definitions is the order they will be tried in
        return listOf<ImporterDefinition>(
            ImporterDefinition(gpxImporter, AnalyticsConstants.ACTION_IMPORT_GPX_ID, AnalyticsConstants.ACTION_IMPORT_GPX_NAME, AnalyticsConstants.INTERNAL_TYPE),
            ImporterDefinition(kmlImporter, AnalyticsConstants.ACTION_IMPORT_KML_ID, AnalyticsConstants.ACTION_IMPORT_KML_NAME, AnalyticsConstants.INTERNAL_TYPE),
            ImporterDefinition(kmzImporter, AnalyticsConstants.ACTION_IMPORT_KMZ_ID, AnalyticsConstants.ACTION_IMPORT_KMZ_NAME, AnalyticsConstants.INTERNAL_TYPE)
        )
    }
}
