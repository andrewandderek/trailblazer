/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.model.TextSpan
import com.andrewandderek.trailblazer.utility.TextMatcher

class HighlightTextView : AppCompatTextView {

    private val textMatcher = TextMatcher()

    private var text: String? = null

    var highlightTextColour: Int = DefaultHighlightTextColour
        set(value) {
            field = value
            updateText()
        }

    var highlightBackgroundColour: Int = DefaultHighlightBackgroundColour
        set(value) {
            field = value
            updateText()
        }

    var highlightedText: String? = null
        set(value) {
            field = value
            updateText()
        }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        this.text = text?.toString()
        updateText()
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.HighlightTextView, defStyle, 0
        )

        highlightTextColour = a.getColor(
            R.styleable.HighlightTextView_highlightTextColour,
            highlightTextColour
        )
        highlightBackgroundColour = a.getColor(
            R.styleable.HighlightTextView_highlightBackgroundColour,
            highlightBackgroundColour
        )

        a.recycle()
    }

    private fun updateText() {
        val spannableText = SpannableString(text)
        val highlightedSpans = getHighlightedSpans()
        highlightedSpans.forEach {
            spannableText.setSpan(ForegroundColorSpan(highlightTextColour), it.start, it.end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
            spannableText.setSpan(BackgroundColorSpan(highlightBackgroundColour), it.start, it.end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        }
        super.setText(spannableText, BufferType.SPANNABLE)
    }

    private fun getHighlightedSpans(): List<TextSpan> {
        text?.let { allText ->
            highlightedText?.let { highlighted ->
                return textMatcher.getMatches(allText, highlighted)
            }
        }
        return emptyList()
    }

    companion object {
        const val DefaultHighlightTextColour = Color.RED
        const val DefaultHighlightBackgroundColour = Color.BLACK
    }
}