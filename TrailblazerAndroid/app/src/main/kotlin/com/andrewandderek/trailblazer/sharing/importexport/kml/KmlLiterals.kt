/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.kml

object KmlLiterals {
    const val KML_EXTENSION = "KML"
    const val TRACK_STYLE = "track"
    const val START_STYLE = "start"
    const val END_STYLE = "end"
    const val ABSOLUTE_ALTITUDE_MODE = "absolute"

    const val KML_DEFAULT_NAMESPACE = "http://www.opengis.net/kml/2.2"
    const val ATOM_NAMESPACE = "atom"
    const val ATOM_NAMESPACE_URI = "http://www.w3.org/2005/Atom"
    const val GX_NAMESPACE = "gx"
    const val GX_NAMESPACE_URI = "http://www.google.com/kml/ext/2.2"

    const val KML_TAG = "kml"
    const val DOCUMENT_TAG = "Document"
    const val OPEN_TAG = "open"
    const val VISIBILITY_TAG = "visibility"
    const val NAME_TAG = "name"
    const val SNIPPET_TAG = "snippet"
    const val DESCRIPTION_TAG = "description"
    const val AUTHOR_TAG = "author"
    const val ID_TAG = "id"

    const val STYLE_TAG = "Style"
    const val LINESTYLE_TAG = "LineStyle"
    const val COLOR_TAG = "color"
    const val WIDTH_TAG = "width"
    const val ICONSTYLE_TAG = "IconStyle"
    const val SCALE_TAG = "scale"
    const val ICON_TAG = "Icon"
    const val HREF_TAG = "href"
    const val HOTSPOT_TAG = "hotSpot"
    const val Y_TAG = "x"
    const val X_TAG = "y"
    const val XUNITS_TAG = "xunits"
    const val YUNITS_TAG = "yunits"

    const val NORMAL_TRACK_STYLE_SUFFIX = "normal"
    const val HIGHLIGHT_TRACK_STYLE_SUFFIX = "highlight"

    const val NORMAL_TRACK_STYLE_WIDTH_VALUE = "6"
    const val NORNAL_TRACK_STYLE_COLOUR_VALUE = "7f0000ff"
    const val NORMAL_TRACK_STYLE_ICONREF_VALUE = "http://earth.google.com/images/kml-icons/track-directional/track-0.png"
    const val NORMAL_TRACK_STYLE_SCALE_VALUE = "1"

    const val HIGHLIGHT_TRACK_STYLE_WIDTH_VALUE = "8"
    const val HIGHLIGHT_TRACK_STYLE_COLOUR_VALUE = "7f0000ff"
    const val HIGHLIGHT_TRACK_STYLE_ICONREF_VALUE = "http://earth.google.com/images/kml-icons/track-directional/track-0.png"
    const val HIGHLIGHT_TRACK_STYLE_SCALE_VALUE = "1.2"

    const val START_TRACK_STYLE_ICONREF_VALUE = "http://maps.google.com/mapfiles/kml/paddle/grn-circle.png"
    const val START_TRACK_STYLE_SCALE_VALUE = "1.3"
    const val START_TRACK_STYLE_HOTSPOT_X_VALUE = "32"
    const val START_TRACK_STYLE_HOTSPOT_Y_VALUE = "1"
    const val END_TRACK_STYLE_ICONREF_VALUE = "http://maps.google.com/mapfiles/kml/paddle/red-circle.png"
    const val END_TRACK_STYLE_SCALE_VALUE = "1.3"
    const val END_TRACK_STYLE_HOTSPOT_X_VALUE = "32"
    const val END_TRACK_STYLE_HOTSPOT_Y_VALUE = "1"

    const val STYLEMAP_TAG = "StyleMap"
    const val PAIR_TAG = "Pair"
    const val KEY_TAG = "key"

    const val PLACEMARK_TAG = "Placemark"
    const val STYLEURL_TAG = "styleUrl"
    const val LINESTRING_TAG = "LineString"
    const val TRACK_TAG = "Track"
    const val MULTITRACK_TAG = "MultiTrack"
    const val ALTITUDEMODE_TAG = "altitudeMode"
    const val INTERPOLATE_TAG = "interpolate"
    const val WHEN_TAG = "when"
    const val COORD_TAG = "coord"

    const val TIMESTAMP_TAG = "TimeStamp"
    const val POINT_TAG = "Point"

    const val COORDINATE_TAG = "coordinates"
    const val ALTITUDE_OFFSET_TAG = "altitudeOffset"
    const val TESSELLATE_TAG = "tessellate"
}