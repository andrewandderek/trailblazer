/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.compare

import android.app.Application
import androidx.lifecycle.*
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.ExportException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.*
import com.andrewandderek.trailblazer.statistics.ITrackGroupSorter
import com.andrewandderek.trailblazer.statistics.ITrackGroupSorter.SortBy
import com.andrewandderek.trailblazer.statistics.ITrackGroupSummaryStatisticsProvider
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.utility.*
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.FlowableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject
import javax.inject.Named

data class SortedTrackList(
        val listSortLabel: String = "",
        val listOrderLabel: String = "",
        val tracksAndStatistics: List<TrackAndStatisticsWithLabels>
)

data class TrackAndStatisticsWithLabels(
        override val track: Track,
        override val statistics: TrackStatistics,
        val label: String,
        val label2: String
) : ITrackAndStatistics

class CompareViewModel
    @Inject constructor(
            app: Application,
            private val loggerFactory: ILoggerFactory,
            private val resourceProvider: IResourceProvider,
            private val crashReporter: ICrashReporter,
            private val schedulerProvider: ISchedulerProvider,
            private val trackRepository: ITrackRepository,
            private val positionRepository: IPositionRepository,
            @Named(NameLiterals.STATISTICS_FULL) private val statisticsProvider: ITrackStatisticsProvider,
            private val trackGroupSummaryStatisticsProvider: ITrackGroupSummaryStatisticsProvider,
            private val trackGroupSorter: ITrackGroupSorter,
            private val distanceFormatter: IDistanceFormatter,
            private val speedFormatter: ISpeedFormatter,
            private val paceFormatter: IPaceFormatter
    )
    : AndroidViewModel(app), DefaultLifecycleObserver
{
    // observables
    data class Observables(
        val title: MutableLiveData<String> = SingleLiveEvent(),
        val message: MutableLiveData<String> = SingleLiveEvent(),
        val progressStart: MutableLiveData<Int> = SingleLiveEvent(),
        val progressPosition: MutableLiveData<Int> = SingleLiveEvent(),
        val progressEnd: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val summaryStatistics: MutableLiveData<FormattedTrackGroupSummaryStatistics> = SingleLiveEvent(),
        val sortedTrackList: MutableLiveData<SortedTrackList> = SingleLiveEvent()
    )
    val observables = Observables()
    // observables

    private var selectedSortByPosition: Int = 0
    private var selectSortBy = SortBy.MOVING_TIME

    fun getSortByOptions() : List<SelectableString> {
        return listOf(
                SelectableString(resourceProvider.getString(R.string.compare_track_list_sort_moving_time), selectedSortByPosition == 0),
                SelectableString(resourceProvider.getString(R.string.compare_track_list_sort_distance), selectedSortByPosition == 1),
                SelectableString(resourceProvider.getString(R.string.compare_track_list_sort_speed), selectedSortByPosition == 2)
        )
    }

    fun setSortBy(position: Int) {
        selectedSortByPosition = position
        when (position) {
            0 -> selectSortBy = SortBy.MOVING_TIME
            1 -> selectSortBy = SortBy.DISTANCE
            2 -> selectSortBy = SortBy.SPEED
        }
        displayTrackList(selectSortBy, selectedSortDirectionAscending)
    }

    private var selectedSortDirectionPosition: Int = 0
    private var selectedSortDirectionAscending = true

    fun getSortDirectionOptions(): List<SelectableString> {
        return listOf(
                SelectableString(resourceProvider.getString(R.string.compare_track_list_ascending), selectedSortDirectionPosition == 0),
                SelectableString(resourceProvider.getString(R.string.compare_track_list_descending), selectedSortDirectionPosition == 1)
        )
    }

    fun setSortDirection(position: Int) {
        selectedSortDirectionPosition = position
        selectedSortDirectionAscending = position == 0
        displayTrackList(selectSortBy, selectedSortDirectionAscending)
    }

    private var trackIds: ArrayList<Long> = ArrayList()
    private var trackLoaderSubscriber: Disposable? = null

    private var allTracks: ArrayList<TrackAndStatisticsWithLabels> = ArrayList()

    fun init(ids: LongArray) {
        if (trackIds.isNotEmpty()) {
            // we have already been initialised
            loggerFactory.logger.debug("CompareViewModel.init already init ${trackIds}")
            // but we need to refresh the UI
            observables.title.value = String.format(resourceProvider.getString(R.string.compare_title_fmt), trackIds.size)
            updateView()
            return
        }
        trackIds = ids.toCollection(ArrayList())
        loggerFactory.logger.debug("CompareViewModel.init ${trackIds}")
        observables.title.value = String.format(resourceProvider.getString(R.string.compare_title_fmt), trackIds.size)
        loadAllTracks(trackIds)
    }

    override fun onResume(owner: LifecycleOwner) {
        loggerFactory.logger.debug("CompareViewModel.onResume")
    }

    override fun onCreate(owner: LifecycleOwner) {
        loggerFactory.logger.debug("CompareViewModel.onCreate")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        loggerFactory.logger.debug("CompareViewModel.onDestroy")
        trackLoaderSubscriber?.dispose()
        trackLoaderSubscriber = null
    }

    private fun loadAllTracks(ids: List<Long>) {
        loggerFactory.logger.debug("CompareViewModel.loadAllTracks ${ids}")
        observables.progressStart.value = ids.size
        trackLoaderSubscriber?.dispose()
        trackLoaderSubscriber = Flowable.create<List<TrackAndStatisticsWithLabels>>(
                {
                    emitter -> loadTrackStatistics(ids, emitter)
                }, BackpressureStrategy.BUFFER)
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .doOnNext { trackAndStats: List<TrackAndStatisticsWithLabels> ->
                    loggerFactory.logger.debug("CompareViewModel.loadAllTracks ${trackAndStats.size} of ${ids.size}")
                    allTracks = ArrayList(trackAndStats)
                    observables.progressPosition.value = trackAndStats.size
                }
            .doOnComplete {
                    loggerFactory.logger.debug("CompareViewModel.loadAllTracks complete ${ids.size}")
                    updateView()
                    observables.progressEnd.call()
                }
            .subscribe(
                        {
                        },
                        { error: Throwable ->
                            observables.progressEnd.call()
                            observables.message.value = resourceProvider.getString(R.string.error_loading_track)
                            loggerFactory.logger.error("CompareViewModel.loadAllTracks", error)
                            crashReporter.logNonFatalException(ExportException("IDs ${ids}", error))
                        }
                )
    }

    private fun loadTrackStatistics(ids: List<Long>, emitter: FlowableEmitter<List<TrackAndStatisticsWithLabels>>) {
        val trackAndStats = ArrayList<TrackAndStatisticsWithLabels>()
        for (id in ids) {
            val track = trackRepository.getWithPositionsAndAccuracy(id, Position.ACCURACY_THRESHOLD, positionRepository)
            track?.let {
                val stats = statisticsProvider.getStatistics(it)
                val distance = distanceFormatter.formatDistance(stats.distanceInMetres, true)
                val movingTime =  DurationFormatter.formatMilliseconds(stats.movingTimeInMilliseconds)
                val movingSpeed = speedFormatter.formatSpeed(stats.averageMovingSpeedInMetresPerSecond, true)
                val movingPace = paceFormatter.formatPace(stats.averageMovingSpeedInMetresPerSecond, true)
                val label = CalendarFormatter.convertCalendarToDateTimeString(track.started)
                val label2 = "${distance}, ${movingTime}, ${movingSpeed}, ${movingPace}"
                // lets get rid of the actual positions as we have now calculated the stats
                // if we keep all the positions then we can run out of memory if we are comparing lots of tracks
                it.resetPositions()
                trackAndStats.add(TrackAndStatisticsWithLabels(it, stats, label, label2))
                emitter.onNext(trackAndStats)
            }
        }
        emitter.onComplete()
    }

    private fun updateView() {
        displayOverallSummary()
        displayTrackList(selectSortBy, selectedSortDirectionAscending)
    }

    private fun displayOverallSummary() {
        val summary = trackGroupSummaryStatisticsProvider.getSummaryStatistics(allTracks)
        val formattedSummary = trackGroupSummaryStatisticsProvider.getFormattedSummaryStatistics(summary)
        observables.summaryStatistics.value = formattedSummary
    }

    private fun displayTrackList(sortBy: SortBy, sortAscending: Boolean) {
        val sortedList = trackGroupSorter.sortTracks(allTracks, sortBy, sortAscending)
        val sortLabel = when (sortBy) {
            SortBy.MOVING_TIME -> resourceProvider.getString(R.string.compare_track_list_sort_moving_time)
            SortBy.DISTANCE -> resourceProvider.getString(R.string.compare_track_list_sort_distance)
            SortBy.SPEED -> resourceProvider.getString(R.string.compare_track_list_sort_speed)
        }
        val orderLabel = if (sortAscending)
            resourceProvider.getString(R.string.compare_track_list_ascending)
        else
            resourceProvider.getString(R.string.compare_track_list_descending)
        val sortedTrackList = SortedTrackList(sortLabel, orderLabel, sortedList)
        observables.sortedTrackList.value = sortedTrackList
    }

}