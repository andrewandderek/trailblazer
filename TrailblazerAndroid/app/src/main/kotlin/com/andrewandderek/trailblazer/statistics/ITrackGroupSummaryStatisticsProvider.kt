/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.FormattedTrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.model.ITrackAndStatistics
import com.andrewandderek.trailblazer.model.TrackGroupSummaryStatistics

interface ITrackGroupSummaryStatisticsProvider {
    fun getSummaryStatistics(trackGroup: List<ITrackAndStatistics>): TrackGroupSummaryStatistics
    fun getFormattedSummaryStatistics(summary: TrackGroupSummaryStatistics): FormattedTrackGroupSummaryStatistics
}

