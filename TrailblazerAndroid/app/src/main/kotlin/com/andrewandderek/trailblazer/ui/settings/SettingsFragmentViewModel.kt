/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.settings

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.utility.IDatabaseConfigProvider
import com.andrewandderek.trailblazer.data.utility.IDatabaseCopier
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.service.gpslogger.GpsLoggerService
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceConnectionListener
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.settings.IThemeSetter
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.utility.SingleLiveEvent
import com.andrewandderek.trailblazer.worker.trackmetadata.ITrackMetadataScheduler
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject
import javax.inject.Named

class SettingsFragmentViewModel
    @Inject constructor(
        app: Application,
        private val loggerFactory: ILoggerFactory,
        private val resourceProvider: IResourceProvider,
        private val crashReporter: ICrashReporter,
        private val schedulerProvider: ISchedulerProvider,
        private val trackMetadataRepository: ITrackMetadataRepository,
        private val dbConfigProvider: IDatabaseConfigProvider,
        private val dbCopier: IDatabaseCopier,
        private val userSettings: IUserSettings,
        private val themeSetter: IThemeSetter,
        private val eventBus: IEventBus,
        private val metadataScheduler: ITrackMetadataScheduler,
        @Named(NameLiterals.SHARE_DATABASE) private val sharer: IIntentSharer,
        private val trackingServiceController: IGpsLoggerServiceController,
        private val fileSystemHelper: IFileSystemHelper,
)
        : AndroidViewModel(app), DefaultLifecycleObserver, IGpsLoggerServiceConnectionListener
{
    data class Observables(
        val message: MutableLiveData<String> = SingleLiveEvent(),
        val resetMetadataSummarySuffix: MutableLiveData<String> = SingleLiveEvent(),
        val startProgress: MutableLiveData<String> = SingleLiveEvent(),
        val endProgress: MutableLiveData<String> = SingleLiveEvent(),
        val displayChooser: MutableLiveData<Pair<String, Intent>> = SingleLiveEvent(),
        val trackRecording: MutableLiveData<Boolean> = SingleLiveEvent(),
    )
    val observables = Observables()

    private var metadataSubscriber: Disposable? = null

    override fun connectService(serviceBinder: GpsLoggerService.LocalBinder) {
        loggerFactory.logger.debug("SettingsFragmentViewModel: service connected")
        val locationTrackingService = serviceBinder.service
        observables.trackRecording.value = locationTrackingService.isTracking || locationTrackingService.isPaused
    }

    override fun disconnectService() {
        loggerFactory.logger.debug("SettingsFragmentViewModel: service disconnected")
    }

    override fun onPause(owner: LifecycleOwner) {
        loggerFactory.logger.debug("SettingsFragmentViewModel.onPause")
        userSettings.resetCache()       // we should force the settings to be read again in case any values have changed
    }

    override fun onResume(owner: LifecycleOwner) {
        loggerFactory.logger.debug("SettingsFragmentViewModel.onResume")
    }

    override fun onCreate(owner: LifecycleOwner) {
        loggerFactory.logger.debug("SettingsFragmentViewModel.onCreate")
        trackingServiceController.bindToService(this)
    }

    override fun onDestroy(owner: LifecycleOwner) {
        loggerFactory.logger.debug("SettingsFragmentViewModel.onDestroy")
        trackingServiceController.unbindFromService()
        metadataSubscriber?.dispose()
        metadataSubscriber = null
    }

    fun loadAllTrackMetadata() {
        loggerFactory.logger.debug("SettingsFragmentViewModel.loadMetadata")
        metadataSubscriber?.dispose()
        metadataSubscriber = Flowable.fromCallable { trackMetadataRepository.getAll() }
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe(
                { loadedMetadata ->
                    updateUi(loadedMetadata.size)
                },
                { error: Throwable ->
                    loggerFactory.logger.error("loadMetadata", error)
                    crashReporter.logNonFatalException(TrackLoadException("load track metadata", error))
                }
            )
    }

    fun resetAllTrackMetadata() {
        loggerFactory.logger.debug("SettingsFragmentViewModel.resetAllTrackMetadata")
        metadataSubscriber?.dispose()
        metadataSubscriber = Flowable.fromCallable { resetAndLoadMetadata() }
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe(
                { loadedMetadata ->
                    // refresh the track list UI
                    eventBus.publish(TrackMetadataUpdatedEvent(listOf()))
                    // refresh the settings UI
                    updateUi(loadedMetadata.size)
                    // kick off the worker again
                    metadataScheduler.scheduleWorker()
                },
                { error: Throwable ->
                    loggerFactory.logger.error("resetAllTrackMetadata", error)
                    crashReporter.logNonFatalException(TrackLoadException("load track metadata", error))
                }
            )
    }

    private fun resetAndLoadMetadata(): List<TrackMetadata> {
        trackMetadataRepository.deleteAll()
        return trackMetadataRepository.getAll()
    }

    fun updateUi(numberOfMetadataItems: Int) {
        loggerFactory.logger.debug("SettingsFragmentViewModel metadata items ${numberOfMetadataItems}")
        val suffix = resourceProvider.getString(R.string.settings_reset_metadata_summary_suffix, numberOfMetadataItems.toString())
        observables.resetMetadataSummarySuffix.value = suffix
    }

    fun testCrashReporting() {
        crashReporter.testReporting()
    }

    fun applyAppTheme() {
        themeSetter.setTheme(userSettings.appTheme)
    }

    fun applyloggingLevel() {
        loggerFactory.setLoggingLevel(userSettings.loggingLevel)
    }

    @SuppressLint("CheckResult")
    fun exportDatabase() {
        observables.startProgress.value = resourceProvider.getString(R.string.settings_exportdb_progress)
        Flowable.fromCallable { copyDbToExternalFilesArea() }
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe (
                { exportedOk: Boolean ->
                    if (exportedOk) {
                        observables.endProgress.value = ""
                        observables.displayChooser.value = Pair(
                            resourceProvider.getString(R.string.settings_exportdb_chooser_title),
                            sharer.getShareIntentWithSingleFile("", dbConfigProvider.getExportDbPathname(false))
                        )
                    } else {
                        observables.endProgress.value = resourceProvider.getString(R.string.settings_copydb_error)
                    }
                },
                { error: Throwable ->
                    loggerFactory.logger.error("exportDatabase - error", error)
                    crashReporter.logNonFatalException(error)
                    observables.endProgress.value = resourceProvider.getString(R.string.settings_copydb_error)
                }
            )
    }

    private fun copyDbToExternalFilesArea(): Boolean {
        return dbCopier.copyDb(
            dbConfigProvider.getActiveApplicationDbPathname(),
            dbConfigProvider.getExportDbPathname(true),
            false)
    }

    @SuppressLint("CheckResult")
    fun importDatabase(src: Uri) {
        observables.startProgress.value = resourceProvider.getString(R.string.settings_copydb_progress)
        Flowable.fromCallable { copyFromResolver(src) }
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe (
                { importedOk: Boolean ->
                    if (importedOk) {
                        observables.endProgress.value = resourceProvider.getString(R.string.settings_copydb_import_ok)
                        // update settings UI
                        loadAllTrackMetadata()
                    } else {
                        observables.endProgress.value = resourceProvider.getString(R.string.settings_copydb_error)
                    }
                },
                { error: Throwable ->
                    loggerFactory.logger.error("importDatabase - error", error)
                    crashReporter.logNonFatalException(error)
                    observables.endProgress.value = resourceProvider.getString(R.string.settings_copydb_error)
                }
            )
    }

    private fun copyFromResolver(src: Uri) : Boolean {
        // we copy as a two stage operation so we have control over the final copy over the active DB
        // copy from selected file -> app external area
        if (fileSystemHelper.copyFileFromResolver(src, dbConfigProvider.getImportDbPathname(true))) {
            // copy from app external area -> active private DB
            if (copyDbFromExternalFileArea()) {
                return true
            }
        }
        return false
    }

    private fun copyDbFromExternalFileArea() : Boolean {
        return dbCopier.copyDb(
                dbConfigProvider.getImportDbPathname(false),
                dbConfigProvider.getActiveApplicationDbPathname(),
                true)
    }
}