/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.statistics

import android.content.Intent
import android.graphics.Bitmap
import com.andrewandderek.trailblazer.model.Position

interface ITrackStatisticsView {
    fun setTrackTitle(title: String)
    fun setDistance(distance: String, unit: String)
    fun setRecordedTime(recordedTime: String)
    fun setMovingTime(movingTime: String)
    fun setElevationValues(elevationGain: String, elevationLoss: String, minElevation: String, maxElevation: String, elevationDifference: String, unit: String)
    fun setSpeedValues(averageSpeed: String, averageMovingSpeed: String, unit: String)
    fun setElevationByDistanceChartPositions(positions: List<Position>)
    fun setElevationByTimeChartPositions(positions: List<Position>)
    fun setPaceValues(averagePace: String, averageMovingPace: String, paceUnitLabel: String)
    fun getScreenshotBitmap(): Bitmap
    fun showMessage(messageId: Int)
    fun startProgress(messageId: Int)
    fun completeProgress()
    fun startIntent(intent: Intent, title: String)
    fun hideAltitudeBasedRows()
    fun hideTimeBasedRows()
    fun hideTimeBasedGraph()
    fun hideNotes()
    fun setNotes(text: String)
    fun hideSegmentsRow()
    fun setSegmentsText(text: String)
}