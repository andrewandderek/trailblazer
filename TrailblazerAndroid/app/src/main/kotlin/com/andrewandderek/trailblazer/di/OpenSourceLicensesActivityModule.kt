/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.di

import com.andrewandderek.trailblazer.ui.settings.IOpenSourceLicensesPresenter
import com.andrewandderek.trailblazer.ui.settings.OpenSourceLicensesActivity
import com.andrewandderek.trailblazer.ui.settings.OpenSourceLicensesPresenter
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [
        IOpenSourceLicensesActivitySubcomponent::class
])
abstract class OpenSourceLicensesActivityModule {
    @Binds
    @IntoMap
    @ClassKey(OpenSourceLicensesActivity::class)
    abstract fun bindActivityInjectorFactory(builder: IOpenSourceLicensesActivitySubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    abstract fun bindPresenter(presenter: OpenSourceLicensesPresenter): IOpenSourceLicensesPresenter
}