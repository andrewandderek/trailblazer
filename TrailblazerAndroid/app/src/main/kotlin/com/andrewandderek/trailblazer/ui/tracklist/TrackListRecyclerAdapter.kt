/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.tracklist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.view.HighlightTextView


class TrackListRecyclerAdapter(
    private val context: Context,
    private val presenter: ITrackListPresenter,      // if we want to use this control on another activity then we will need to split this interface
    private val loggerFactory: ILoggerFactory,
)
    : RecyclerView.Adapter<TrackListRecyclerAdapter.RecyclerViewHolder>()
{
    private var popupBeingDisplayed: Boolean = false

    init {
        setHasStableIds(true)
        presenter.loadTracks()
    }

    override fun getItemCount(): Int {
        return presenter.itemCount
    }

    override fun getItemId(position: Int): Long {
        return presenter.getTrackId(position)
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_track, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val id = presenter.getTrackId(position)
        val label = presenter.getTrackLabel(position)
        val sublabel = presenter.getTrackSubLabel(position)
        holder.bind(presenter.inMultiSelectMode, label, sublabel, presenter.trackNameSearch)
        holder.view.isSelected = presenter.isItemSelected(position)
        holder.rowLabelContainer.setOnClickListener {
            presenter.trackSelected(id, position)
        }
        holder.view.isLongClickable = true
        holder.rowLabelContainer.setOnLongClickListener {
            presenter.trackLongSelected(id, position)
        }
        holder.txtOverflowMenu.setOnClickListener {
            if (popupBeingDisplayed) {
                loggerFactory.logger.debug("TrackListRecyclerAdapter: onClickListener: already displaying popup")
                return@setOnClickListener
            }
            popupBeingDisplayed = true
            loggerFactory.logger.debug("TrackListRecyclerAdapter: onClickListener: popup")
            val popup = PopupMenu(context, holder.txtOverflowMenu)
            popup.inflate(R.menu.track_list_item)
            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_track_edit_name -> {
                        presenter.editTrackName(id)
                    }
                    R.id.action_track_delete -> {
                        presenter.deleteTrack(id)
                    }
                    else -> {
                        false
                    }
                }
            }
            popup.setOnDismissListener {
                loggerFactory.logger.debug("TrackListRecyclerAdapter: onClickListener: popup dismissed")
                popupBeingDisplayed = false
            }
            popup.show()
        }
    }

    inner class RecyclerViewHolder(internal var view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.track_row_label_container)
        internal lateinit var rowLabelContainer: View
        @BindView(R.id.track_row_label)
        internal lateinit var txtLabel: HighlightTextView
        @BindView(R.id.track_row_sub_label)
        internal lateinit var txtSubLabel: TextView
        @BindView(R.id.track_row_options)
        internal lateinit var txtOverflowMenu: TextView      // this is the overflow menu ... char container

        init {
            CheeseKnife.bind(this, view)
        }

        fun bind(isSelectable: Boolean, label: String, sublabel: String, highlightText: String?) {
            view.isClickable = true
            if (isSelectable) {
                // we need to do this programmatically as using ?attr/selectableItemBackground in a drawable causes non nexus device to crash
                view.setBackgroundResource(R.drawable.selectable_list_item)
                txtOverflowMenu.visibility = View.GONE
            } else {
                txtOverflowMenu.visibility = View.VISIBLE
            }
            txtLabel.text = label
            txtLabel.highlightedText = highlightText
            txtSubLabel.text = sublabel
        }
    }
}
