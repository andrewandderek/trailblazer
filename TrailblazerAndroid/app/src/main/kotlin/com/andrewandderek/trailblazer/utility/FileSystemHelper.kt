/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.os.Environment
import com.andrewandderek.trailblazer.APPLICATION_FOLDER
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.Writer
import javax.inject.Inject

interface IFileSystemHelper {
    fun usingLegacyFileAccess(): Boolean
    fun getApplicationFolderOnSdCard(): String
    fun ensureApplicationFolderExists(): Boolean
    fun createWriter(filename: String): Writer
    fun createFileOutputStream(filename: String): FileOutputStream
    fun createInputStream(filename: String): InputStream
    fun getCacheFolder(): String
    fun getFolderFiles(foldername: String): List<String>?
    fun getFileContents(filename: String, addLineEndings: Boolean): String
    fun copyFileFromResolver(srcUri: Uri, destPathname: String): Boolean
}

class FileSystemHelper @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val crashReporter: ICrashReporter,
    private val context: Context,
    private val contentResolver: ContentResolver,
) : IFileSystemHelper
{
    override fun usingLegacyFileAccess(): Boolean {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R) {
            // we can write to the root of the SD cards in android up to 10
            return true
        }
        return false
    }

    // We're only calling getExternalStorageDirectory on OS versions in which it isn't deprecated
    @Suppress("DEPRECATION")
    override fun getApplicationFolderOnSdCard(): String {
        if (usingLegacyFileAccess()) {
            // we can write to the root of the SD cards in android up to 10 - lets do that its easier to find
            return "${Environment.getExternalStorageDirectory()}/${APPLICATION_FOLDER}/"
        }
        // we are limited where we can write unless we ask for extra permissions in android 11+
        val dir = context.getExternalFilesDir(null)
        // throw if we cannot get the path
        return "${dir!!.absolutePath}/export/"
    }

    override fun getCacheFolder(): String {
        val outputDir = context.cacheDir
        return "${outputDir.canonicalPath}/"
    }

    override fun ensureApplicationFolderExists(): Boolean {
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            val sDir = File(getApplicationFolderOnSdCard())
            if (!sDir.exists()) {
                sDir.mkdirs()
            }
        } else {
            loggerFactory.logger.error("There is no memory card available. This application requires a memory card to store the files on.")
            return false
        }
        return true
    }

    override fun createWriter(filename: String): Writer {
        return FileWriter(filename)
    }

    override fun createFileOutputStream(filename: String): FileOutputStream {
        val file = File(filename)
        return FileOutputStream(file)
    }

    override fun createInputStream(filename: String): InputStream {
        return FileInputStream(filename)
    }

    override fun getFolderFiles(foldername: String): List<String>? {
        val allFiles: MutableList<String> = mutableListOf()
        try {
            val files = context.assets.list(foldername) ?: return null
            for (file in files) {
                val filename = "$foldername/$file"
                val subFolderFiles = getFolderFiles(filename)
                if (subFolderFiles != null && subFolderFiles.size > 0) {
                    // the file is a folder, add its files
                    allFiles.addAll(subFolderFiles)
                } else {
                    // the file is a file
                    allFiles.add(filename)
                }
            }
        } catch (e: Throwable) {
            loggerFactory.logger.error("Error getting files in folder {}", foldername, e)
            crashReporter.logNonFatalException(e)
        }
        return allFiles
    }

    override fun getFileContents(filename: String, addLineEndings: Boolean): String {
        var inputStream: InputStream? = null
        var reader: BufferedReader? = null
        val buf = StringBuffer()
        try {
            inputStream = context.assets.open(filename)
            loggerFactory.logger.debug("Opened {} Available {}", filename, inputStream.available())
            reader = BufferedReader(InputStreamReader(inputStream))

            var str: String?
            while (reader.readLine().let {str = it; it != null }) {
                buf.append(str)
                if (addLineEndings) {
                    buf.append("\n")
                }
            }
        } catch (e: Throwable) {
            loggerFactory.logger.error("Error reading {}", filename, e)
            crashReporter.logNonFatalException(e)
        } finally {
            try {
                reader?.close()
                inputStream?.close()
            } catch (e: Throwable) {
                loggerFactory.logger.error("Error closing {}", filename, e)
                crashReporter.logNonFatalException(e)
            }
        }
        return buf.toString()
    }

    override fun copyFileFromResolver(srcUri: Uri, destPathname: String): Boolean {
        var src: InputStream? = null
        var dest: OutputStream? = null
        try {
            loggerFactory.logger.debug("copyFileFromResolver {} -> {}", srcUri.toString(), destPathname)
            src = contentResolver.openInputStream(srcUri)
            dest = FileOutputStream(destPathname)
            src!!.copyTo(dest)
            dest.flush()
            return true
        } catch (e: Throwable) {
            loggerFactory.logger.error("Error copyFileFromResolver {}", destPathname, e)
            crashReporter.logNonFatalException(e)
        } finally {
            try {
                src?.close()
                dest?.close()
            } catch (e: Throwable) {
                loggerFactory.logger.error("Error copyFileFromResolver closing {}", destPathname, e)
                crashReporter.logNonFatalException(e)
            }
        }
        return false
    }
}