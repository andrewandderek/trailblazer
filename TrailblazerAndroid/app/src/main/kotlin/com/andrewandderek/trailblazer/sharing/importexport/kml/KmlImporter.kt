/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.kml

import android.content.ContentResolver
import com.andrewandderek.trailblazer.exception.NotSupportedException
import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.sharing.importexport.BaseImporter
import com.andrewandderek.trailblazer.sharing.importexport.ImportAndResult
import com.andrewandderek.trailblazer.sharing.importexport.ImportResult
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.ISystemTime
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.IOException
import java.io.InputStream
import java.text.ParseException
import java.util.Calendar
import javax.inject.Inject


class KmlImporter @Inject constructor(
        private val loggerFactory: ILoggerFactory,
        fileSystemHelper: IFileSystemHelper,
        private val systemTime: ISystemTime,
        contentResolver: ContentResolver,
        private val crashReporter: ICrashReporter,
) : BaseImporter(loggerFactory, fileSystemHelper, contentResolver, crashReporter) {

    override fun importTrack(stream: InputStream): ImportAndResult {
        var result: ImportResult = ImportResult.OK
        var track: Track? = null
        var segment: Long = -1

        try {
            val factory = XmlPullParserFactory.newInstance()
            factory.isNamespaceAware = true;
            factory.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            val parser = factory.newPullParser()
            parser.setInput(stream, null)
            var eventType = parser.eventType
            var rootElementFound = false
            while (eventType != XmlPullParser.END_DOCUMENT) {
                when (eventType) {
                    XmlPullParser.START_DOCUMENT -> {
                        track = Track()
                    }

                    XmlPullParser.START_TAG -> {
                        var tagName = parser.name
                        if (!rootElementFound) {
                            if (nameEquals(tagName, KmlLiterals.KML_TAG)) {
                                rootElementFound = true
                            } else {
                                loggerFactory.logger.warn("Unexpected root node ${tagName}")
                                return ImportAndResult(null, ImportResult.UNKNOWN_FORMAT)
                            }
                        }
                        if (nameEquals(tagName,KmlLiterals.DOCUMENT_TAG)) {
                            parseMetadata(parser, track!!)
                            tagName = parser.name
                        }

                        if (nameEquals(parser.namespace, tagName, KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)) {
                            // google earth and MyTracks use gx:track
                            segment = segment + 1
                            loggerFactory.logger.debug("KML importTrack: <gx:Track>")
                            result = parseTrack(parser, track!!, segment)
                        } else if (nameEquals(parser.name, KmlLiterals.LINESTRING_TAG)) {
                            // MyMaps uses LineString elements for its tracks, they contain <coordinates> that do not have a timestamp
                            segment = segment + 1
                            loggerFactory.logger.debug("KML importTrack: MyMaps <coordinates> - no timestamps")
                            result = parseLineString(parser, track!!, segment)
                        } else if (nameEquals(tagName,KmlLiterals.DESCRIPTION_TAG)) {
                            parseMyTracksExtraData(parser, track!!)
                        }
                    }
                }
                eventType = parser.next()
            }
        } catch (e: XmlPullParserException) {
            loggerFactory.logger.error("error reading KML", e)
            track = null
            result = ImportResult.UNKNOWN_FORMAT
        } catch (e: IOException) {
            loggerFactory.logger.error("error reading KML", e)
            track = null
            result = ImportResult.IO_ERROR
        }

        loggerFactory.logger.debug("KML track imported with: ${track?.currentPositionsSnapshot?.count()} positions")
        return ImportAndResult(track, result)
    }

    private fun parseMyTracksExtraData(parser: XmlPullParser, track: Track) {
        var text = parser.nextText()
        val index = text.indexOf(MY_TRACKS_MARKER,0,true)
        // we are looking for the last placemark that has a description with the extra stats that My Maps records
        if (index > -1) {
            track.notes = text.substring(index).replace("<br>", "\n")
        }
    }

    private fun parseMetadata(parser: XmlPullParser, track: Track) {
        var eventType = parser.eventType
        // to leave the loop either we find the end of the document element or the end of the file
        // or we break out because we have fond the track data
        while (
                (eventType != XmlPullParser.END_TAG || !nameEquals(parser.name, KmlLiterals.DOCUMENT_TAG)) &&
                (eventType != XmlPullParser.END_DOCUMENT)
        ) {
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    val tagName = parser.name
                    //loggerFactory.logger.debug("KML parseMetadata: ${tagName}")
                    if (nameEquals(tagName, KmlLiterals.NAME_TAG)) {
                        track.name = parser.nextText()
                    } else if (nameEquals(tagName,KmlLiterals.DESCRIPTION_TAG)) {
                        track.notes = parser.nextText()
                    } else if (nameEquals(tagName,KmlLiterals.SNIPPET_TAG)) {
                        track.notes = parser.nextText()
                    } else if (nameEquals(parser.namespace, tagName, KmlLiterals.ATOM_NAMESPACE_URI, KmlLiterals.NAME_TAG)) {
                        track.notes = parser.nextText()
                    } else if (nameEquals(parser.name, KmlLiterals.LINESTRING_TAG)) {
                        return      // we are at the start of the track segments
                    } else if (nameEquals(parser.namespace, tagName, KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)) {
                        return      // we are at the start of the track segments
                    }
                }
            }
            // jump to next event
            eventType = parser.next()
        }
    }

    // see https://developers.google.com/kml/documentation/kmlreference#linestring
    private fun parseLineString(parser: XmlPullParser, track: Track, segment: Long): ImportResult {
        var eventType = parser.eventType
        // to leave the loop either we find the end of the LineString data element or the end of the file
        while (
            (eventType != XmlPullParser.END_TAG || !nameEquals(parser.name, KmlLiterals.LINESTRING_TAG)) &&
            (eventType != XmlPullParser.END_DOCUMENT)
        ) {
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    val tagName = parser.name
                    if (nameEquals(parser.namespace, tagName, KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.ALTITUDE_OFFSET_TAG)) {
                        // a google extension is to allow for an offset, we dont support it
                        crashReporter.logNonFatalException(NotSupportedException("KmlImport: <gx:altitudeOffset> not supported, will ignore"))
                    } else if (nameEquals(tagName, KmlLiterals.COORDINATE_TAG)) {
                        parseCoordinates(parser.nextText(), track, segment)
                    }
                }
            }
            // jump to next event
            eventType = parser.next()
        }
        return ImportResult.OK
    }

    private fun parseCoordinates(elementText: String?, track: Track, segment: Long) {
        elementText?.let {
            val noEol = it.trim().replace("\n"," ")
            val tuples = noEol.split("\\s+".toRegex())
            loggerFactory.logger.debug("KML parseCoordinates: tuples ${tuples.size}")
            tuples.forEach { oneTuple -> parseCoordinatesTuple(oneTuple, track, segment) }
        }
    }

    private fun parseCoordinatesTuple(tuple: String, track: Track, segment: Long) {
        loggerFactory.logger.debug("KML parseCoordinates: ${tuple}")
        val tokens = tuple.split(',')
        if (tokens.size < 2) {
            return
        }
        val pos = Position(
            tokens[1].toDouble(),
            tokens[0].toDouble(),
            if (tokens.size > 2) tokens[2].toDouble() else null,
            null,
            0F
        )
        pos.segment = segment
        track.addNewPosition(pos)
    }

    // see https://developers.google.com/kml/documentation/kmlreference#gx:track
    private fun parseTrack(parser: XmlPullParser, track: Track, segment: Long): ImportResult {
        val pointTimestamp: MutableList<Calendar> = ArrayList<Calendar>()
        val points: MutableList<Position> = ArrayList<Position>()

        var eventType = parser.eventType
        // to leave the loop either we find the end of the track data element or the end of the file
        while (
                (eventType != XmlPullParser.END_TAG || !nameEquals(parser.namespace, parser.name, KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)) &&
                (eventType != XmlPullParser.END_DOCUMENT)
        ) {
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    val tagName = parser.name
                    if (nameEquals(tagName, KmlLiterals.WHEN_TAG)) {
                        pointTimestamp.add(parseTimestamp(parser.nextText()))
                    } else if (nameEquals(parser.namespace, tagName, KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.COORD_TAG)) {
                        val point = parseCoord(parser.nextText())
                        point?.let { points.add(it) }
                    }
                }
            }
            // jump to next event
            eventType = parser.next()
        }
        loggerFactory.logger.debug("KML parseTrack: points ${points.size}, timestamps ${pointTimestamp.size}")
        if (pointTimestamp.size < 1) {
            crashReporter.logNonFatalException(TrackLoadException("no timestamps", null))
            return ImportResult.NO_TIMESTAMP
        }
        if (pointTimestamp.size != points.size) {
            crashReporter.logNonFatalException(TrackLoadException("mismatched timestamps", null))
            return ImportResult.MISMATCHED_TIMESTAMP
        }
        // right, lets process the track
        for (index in 0..(points.size - 1)) {
            points[index].segment = segment
            points[index].timeRecorded = pointTimestamp[index]
            if (track.isEmpty) {
                // this is the first position - set the start time as well
                track.started = pointTimestamp[index]
            }
            track.addNewPosition(points[index])
        }
        return ImportResult.OK
    }

    private fun parseCoord(text: String?): Position? {
        return text?.let {
            val tokens = it.split(' ')
            if (tokens.size < 2) {
                return null
            }
            return Position(
                    tokens[1].toDouble(),
                    tokens[0].toDouble(),
                    if (tokens.size > 2) tokens[2].toDouble() else null,
                    systemTime.getCurrentTime(),
                    0F
            )
        }
    }

    // see https://developers.google.com/kml/documentation/kmlreference#timestamp
    private fun parseTimestamp(dateStr: String): Calendar {
        // TODO - we need to be able to cope with timestrings in a KML file that are not in UTC
        val datetime = systemTime.getCurrentTime()
        try {
            dateTimeFormatterUtc.parse(dateStr)?.let { datetime.time = it }
        } catch (ex: ParseException) {
            loggerFactory.logger.error("KML parseTimestamp: ${dateStr}", ex)
            // KML files can just use a date as a timestamp - if they do that we will assume it means midnight UTC
            dateTimeFormatterUtc.parse(dateStr + "T00:00:00Z")?.let { datetime.time = it }
        }
        return datetime
    }

    companion object {
        private const val MY_TRACKS_MARKER = "Created by Google My Tracks"
    }
}