/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.statistics.elevation.ElevationHelper
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithm
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithmFactory
import com.andrewandderek.trailblazer.statistics.elevation.IElevationSettings
import java.util.*

class TrackElevationCalculator(
        private val elevationAlgorithmFactory: IElevationAlgorithmFactory,
        private val elevationSettings: IElevationSettings
) : ITrackCalculator {

    private lateinit var elevationAlgorithm: IElevationAlgorithm

    override fun addPosition(trackStartTime: Calendar, positions: List<Position>, index: Int, statistics: TrackStatistics) {
        val isFirstPosition = index == 0

        // We need to do this first as it will calculate the position's processedAltitude which affects everything else
        processPositionAndUpdateGainAndLoss(positions, index, statistics)

        val altitude = ElevationHelper.getAltitude(positions[index], elevationSettings)

        updateMinElevation(altitude, isFirstPosition, statistics)
        updateMaxElevation(altitude, isFirstPosition, statistics)
        updateElevationDifference(statistics)
    }

    private fun updateMinElevation(altitude: Double, isFirstPosition: Boolean, statistics: TrackStatistics) {
        if (isFirstPosition || altitude < statistics.minElevationInMetres)
            statistics.minElevationInMetres = altitude
    }

    private fun updateMaxElevation(altitude: Double, isFirstPosition: Boolean, statistics: TrackStatistics) {
        if (isFirstPosition || altitude > statistics.maxElevationInMetres)
            statistics.maxElevationInMetres = altitude
    }

    private fun updateElevationDifference(statistics: TrackStatistics) {
        statistics.elevationDifferenceInMetres = statistics.maxElevationInMetres - statistics.minElevationInMetres
    }

    private fun processPositionAndUpdateGainAndLoss(positions: List<Position>, index: Int, statistics: TrackStatistics) {
        if (index == 0) {
            // Create the algorithm every time, allowing it to be changed dynamically (eg if we make it a user setting)
            elevationAlgorithm = elevationAlgorithmFactory.getAlgorithm()
            elevationAlgorithm.reset(positions[0])
            return
        }

        if (positions[index].isFromDifferentSegment(positions[index - 1])) {
            elevationAlgorithm.reset(positions[index])
            return
        }

        val gainAndLoss = elevationAlgorithm.process(positions[index])
        statistics.elevationGainInMetres += gainAndLoss.gain
        statistics.elevationLossInMetres += gainAndLoss.loss
    }
}