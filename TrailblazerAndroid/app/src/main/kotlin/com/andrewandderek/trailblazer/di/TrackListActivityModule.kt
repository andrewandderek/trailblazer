/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.di

import com.andrewandderek.trailblazer.ui.tracklist.ITrackListPresenter
import com.andrewandderek.trailblazer.ui.tracklist.TrackListActivity
import com.andrewandderek.trailblazer.ui.tracklist.TrackListPresenter
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [
        ITrackListActivitySubcomponent::class
])
abstract class TrackListActivityModule {
    @Binds
    @IntoMap
    @ClassKey(TrackListActivity::class)
    abstract fun bindActivityInjectorFactory(builder: ITrackListActivitySubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    abstract fun bindPresenter(presenter: TrackListPresenter): ITrackListPresenter
}