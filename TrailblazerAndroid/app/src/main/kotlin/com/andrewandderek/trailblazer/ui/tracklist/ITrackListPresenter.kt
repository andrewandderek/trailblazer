/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.tracklist

import com.andrewandderek.trailblazer.ui.trackcontrol.ITrackControlPresenter

interface ITrackListPresenter : ITrackControlPresenter<ITrackListView> {
    val itemCount: Int
    var inMultiSelectMode: Boolean
    val trackNameSearch: String?
    fun isItemSelected(position: Int): Boolean
    fun onActionItemClicked(itemId: Int): Boolean

    fun getTrackId(position: Int): Long
    fun getTrackLabel(position: Int): String
    fun getTrackSubLabel(position: Int): String
    fun loadTracks()
    fun trackSelected(trackId: Long, position: Int)
    fun trackLongSelected(trackId: Long, position: Int): Boolean

    fun deleteTrack(trackId: Long): Boolean
    fun confirmedDeleteTrack(trackId: String)
    fun editTrackName(trackId: Long): Boolean
    fun confirmedEditName(trackId: String, value: String)
    fun confirmedMultipleDeleteTrack()
    fun exportSelectedTracks()
    fun confirmedMultipleExportTrack(format: String)
    fun confirmedCombineSelectedTracks(name: String)
    fun importTrack()
    fun trackNameSearchTextChange(newText: String?)
    fun trackNameSearchTextClear()
    fun searchActionStart()
    fun searchActionEnd()
}