/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.model

/**
 * the properties of this object are serialised to the track metadata table
 * if you add a property consider adding it to the table and the repository
 */
data class TrackStatistics(
        var distanceInMetres: Double = 0.0,
        var elapsedTimeInMilliseconds: Long = 0,
        var recordedTimeInMilliseconds: Long = 0,
        var movingTimeInMilliseconds: Long = 0,
        var averageSpeedInMetresPerSecond: Double = 0.0,
        var averageMovingSpeedInMetresPerSecond: Double = 0.0,
        var minElevationInMetres: Double = 0.0,
        var maxElevationInMetres: Double = 0.0,
        // Difference between min and max
        var elevationDifferenceInMetres: Double = 0.0,
        // Total amount of "up" and "down" that was travelled
        var elevationGainInMetres: Double = 0.0,
        var elevationLossInMetres: Double = 0.0
)
