/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.model

import com.andrewandderek.trailblazer.utility.CalendarFormatter
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.Calendar

class Track {
    var id: Long = -1L
    var name: String = "no name"
    var notes: String = ""
    var started: Calendar = Calendar.getInstance()
    var metadata: TrackMetadata? = null

    private var trackPositions: ObservableTrackPositions = ObservableTrackPositions()

    fun addNewPosition(newPosition: Position) {
        newPosition.trackId = id
        trackPositions.add(newPosition)
    }

    fun replaceTrack(newPosition: Position?) {
        val newPositions = ArrayList<Position>(1)
        if (newPosition != null) {
            newPositions.add(newPosition)
        }
        replaceTrack(newPositions)
    }

    fun replaceTrack(newPositions: List<Position>) {
        trackPositions.clear(false)
        trackPositions.addAll(newPositions)
        // make sure it belongs to us now
        trackPositions.setAllTrackIds(id)
    }

    fun addTrack(otherTrack: Track) {
        val maxSegment = lastPositionInSnapshotSegment
        var positionsToBeAdded = otherTrack.currentPositionsSnapshot
        if (isEmpty || maxSegment == null) {
            // our track is empty we can just replace us with the new track
            replaceTrack(positionsToBeAdded)
            return
        }
        // segments in tracks start at zero and increment
        // we need to push the segments past the end of the current track
        val segmentOffset = maxSegment + 1
        positionsToBeAdded.forEach { position ->
            run {
                position.segment = position.segment + segmentOffset
                position.trackId = id
            }
        }
        trackPositions.addAll(positionsToBeAdded)
    }

    fun reset() {
        id = -1L
        trackPositions.clear(true)
    }

    fun resetPositions() {
        trackPositions.clear(false)
    }

    // this gives dynamic access to the positions as they are recorded
    val positions: Flowable<List<Position>>
        get() = trackPositions.observable

    // this returns a static readonly list of the current positions
    val currentPositionsSnapshot: List<Position>
        get() = trackPositions.currentSnapshot

    val lastPositionInSnapshot: Position?
        get() {
            return trackPositions.lastPosition
        }

    val lastPositionInSnapshotSegment: Long?
        get() {
            return lastPositionInSnapshot?.segment
        }

    val currentPositionsSnapshotSegments: List<List<Position>>?
        get() {
            var segmentedPositions = ArrayList<List<Position>>()
            val snapshot = currentPositionsSnapshot
            val maxSegment = lastPositionInSnapshotSegment
            if (snapshot.isEmpty() || maxSegment == null) {
                return null
            }
            for (thisSegment in 0L .. maxSegment) {
                segmentedPositions.add(snapshot.filter { position -> position.segment == thisSegment })
            }
            if (segmentedPositions.all { segments -> segments.isEmpty() }) {
                // all the segments are empty
                return null
            }
            return segmentedPositions
        }

    val isMultiDaySegmentedTrack: Boolean
        get() {
            return isSegmentedTrack && isMultiDayTrack
        }

    val isSegmentedTrack: Boolean
        get() {
            lastPositionInSnapshotSegment?.let {
                return it > 0
            }
            return false
        }

    val isMultiDayTrack: Boolean
        get() {
            lastPositionInSnapshot?.timeRecorded?.let {last ->
                val sameDay = started.get(Calendar.DAY_OF_YEAR) == last.get(Calendar.DAY_OF_YEAR) &&
                              started.get(Calendar.YEAR) == last.get(Calendar.YEAR)
                return !sameDay
            }
            return false
        }

    val isNew: Boolean
        get() = id == -1L

    val isFirstPosition: Boolean
        get() = trackPositions.isFirstPosition

    val positionCount: Int
        get() = trackPositions.count

    val isEmpty: Boolean
        get() = trackPositions.isEmpty

    val hasMissingTimestamps: Boolean
        get() = trackPositions.hasMissingTimestamps

    val hasNoAltitudeData: Boolean
        get() = trackPositions.hasNoAltitudeData

    val startDateAndName: String
        get() = "${CalendarFormatter.convertCalendarToSortableDate(started)}-${name}"
}

// We do not directly wrap the list in an observable as we want a bit more control over when to fire the events
class ObservableTrackPositions {
    private var list: MutableList<Position> = ArrayList<Position>()
    private val onChange: PublishSubject<List<Position>> = PublishSubject.create()

    val observable: Flowable<List<Position>>
        get() = onChange.toFlowable(BackpressureStrategy.BUFFER)

    val currentSnapshot: List<Position>
        get() {
            synchronized(list) {
                return ArrayList<Position>(list)
            }
        }

    val lastPosition: Position?
        get() {
            synchronized(list) {
                return list.lastOrNull()
            }
        }

    val isEmpty: Boolean
        get() {
            synchronized(list) {
                return list.isEmpty()
            }
        }

    val hasMissingTimestamps: Boolean
        get() {
            synchronized(list) {
                return list.any {
                    it.timeRecorded == null
                }
            }
        }

    val hasNoAltitudeData: Boolean
        get() {
            synchronized(list) {
                return list.all {
                    // tracks from MyMaps sometimes have elevation data that is all zeros, lets treat that as no data
                    it.altitude == null || it.altitude < 0.001
                }
            }
        }

    private fun triggerSubscribers() {
        if (onChange.hasObservers()) {
            onChange.onNext(list)
        }
    }

    val count: Int
        get() {
            synchronized(list) {
                return list.size
            }
        }

    val isFirstPosition: Boolean
        get() = count == 1

    fun setAllTrackIds(id: Long) {
        synchronized(list) {
            for (pos: Position in list) {
                pos.trackId = id
            }
        }
    }
    fun clear(triggerSubscription: Boolean) {
        synchronized(list) {
            list.clear()
            if (triggerSubscription) {
                triggerSubscribers()
            }
        }
    }

    fun add(value: Position) {
        synchronized(list) {
            list.add(value)
            triggerSubscribers()
        }
    }
    fun addAll(values: List<Position>) {
        synchronized(list) {
            list.addAll(values)
            triggerSubscribers()
        }
    }
}
