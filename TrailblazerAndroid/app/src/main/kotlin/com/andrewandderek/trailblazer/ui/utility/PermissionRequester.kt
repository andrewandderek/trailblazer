/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.utility

import android.app.Activity
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AlertDialog
import com.andrewandderek.trailblazer.R

object PermissionRequester {

    const val MY_PERMISSIONS_REQUEST_LOCATION = 99
    const val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 98
    const val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 97
    const val MY_PERMISSIONS_REQUEST_NOTIFICATION = 96

    fun requestLocationPermission(activity: Activity) {
        requestPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                R.string.location_permission_request_title,
                R.string.location_permission_request_body,
                MY_PERMISSIONS_REQUEST_LOCATION
        )
    }

    fun requestPushNotificationPermission(activity: Activity) {
        requestPermission(
            activity,
            android.Manifest.permission.POST_NOTIFICATIONS,
            R.string.notification_permission_request_title,
            R.string.notification_permission_request_body,
            MY_PERMISSIONS_REQUEST_NOTIFICATION
        )
    }
    fun requestWriteExternalStoragePermission(activity: Activity) {
        requestPermission(
                activity,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                R.string.write_storage_permission_request_title,
                R.string.write_storage_permission_request_body,
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
        )
    }

    fun requestReadExternalStoragePermission(activity: Activity) {
        requestPermission(
                activity,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                R.string.read_storage_permission_request_title,
                R.string.read_storage_permission_request_body,
                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
        )
    }

    fun requestPermission(activity: Activity, permission: String, messageTitle: Int, messageBody: Int, requestCode: Int) {
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            AlertDialog.Builder(activity)
                    .setTitle(messageTitle)
                    .setMessage(messageBody)
                    .setPositiveButton(R.string.ok_button) {
                        _, _ ->
                        run {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(activity, arrayOf<String>(permission), requestCode)
                        }
                    }
                    .create()
                    .show()
        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity, arrayOf<String>(permission), requestCode)
        }
    }

}