/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.tracklist

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.core.view.MenuCompat
import androidx.lifecycle.Lifecycle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.compare.CompareActivity
import com.andrewandderek.trailblazer.ui.settings.SettingsActivity
import com.andrewandderek.trailblazer.ui.trackcontrol.TrackControlActivity
import com.andrewandderek.trailblazer.ui.trackdisplay.TrackDisplayActivity
import com.andrewandderek.trailblazer.ui.utility.PermissionRequester
import com.andrewandderek.trailblazer.ui.utility.ProgressViewHelper
import com.andrewandderek.trailblazer.ui.view.EmptyRecyclerView
import com.andrewandderek.trailblazer.ui.view.FileFormatPromptDialogFragment
import com.andrewandderek.trailblazer.ui.view.OkCancelDialogFragment
import com.andrewandderek.trailblazer.ui.view.ProgressSpinnerView
import com.andrewandderek.trailblazer.ui.view.TrailblazerSearchView
import com.andrewandderek.trailblazer.ui.view.ValuePromptDialogFragment
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import dagger.android.AndroidInjection
import javax.inject.Inject


class TrackListActivity :
        TrackControlActivity(),
        ITrackListView,
        OkCancelDialogFragment.Listener,
        FileFormatPromptDialogFragment.Listener,
        ValuePromptDialogFragment.Listener
{
    companion object {
        private const val TAG_DELETE_PROMPT = "TrackListActivity:delete_track_tag"
        private const val TAG_DELETE_MULTIPLE_PROMPT = "TrackListActivity:multiple_delete_track_tag"
        private const val TAG_EDIT_NAME_PROMPT = "TrackListActivity:edit_name_tag"
        private const val TAG_COMBINE_NAME_PROMPT = "TrackListActivity:combine_name_tag"
        private const val TAG_EXPORT_FORMAT_PROMPT = "TrackListActivity:export_format_tag"
    }

    @Inject lateinit var presenter: ITrackListPresenter
    @Inject lateinit var permissionChecker: IPermissionChecker

    @BindView(R.id.progressBar)
    private lateinit var progressBar: ProgressSpinnerView

    @BindView(R.id.rvTracks)
    private lateinit var tracksView: EmptyRecyclerView

    @BindView(R.id.layNoData)
    private lateinit var noDataView: LinearLayout

    @BindView(R.id.txtNoData)
    private lateinit var emptyListText: TextView

    private lateinit var selectFileLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("TrackListActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracklist)
        CheeseKnife.bind(this)

        requestLocationPermission()
        presenter.bind(this, savedInstanceState)

        tracksView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tracksView.addItemDecoration(androidx.recyclerview.widget.DividerItemDecoration(this, androidx.recyclerview.widget.DividerItemDecoration.VERTICAL))
        adapter = TrackListRecyclerAdapter(this, presenter, loggerFactory)
        tracksView.adapter = adapter

        selectFileLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            handleSelectFileResult(result)
        }

        onBackPressedDispatcher.addCallback(
            this, // LifecycleOwner
            object : OnBackPressedCallback(
                true // default to enabled
            ) {
                override fun handleOnBackPressed() {
                    loggerFactory.logger.debug("TrackListActivity.handleOnBackPressed() back key pressed")
                    currentActionMode?.let {
                        loggerFactory.logger.debug("TrackListActivity.handleOnBackPressed() in action mode, back key suppressed")
                        it.finish()
                        return
                    }
                    loggerFactory.logger.debug("TrackListActivity.handleOnBackPressed() not in action mode")
                    finish()
                }
            }
        )
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("TrackListActivity.onDestroy()")

        super.onDestroy()
        presenter.unbind(this)
    }

    override fun onStart() {
        loggerFactory.logger.debug("TrackListActivity.onStart()")
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        loggerFactory.logger.debug("TrackListActivity.onStop()")
        super.onStop()
        presenter.stop()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        if (presenter.actionSelected(item.itemId)) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.track_list_activity, menu)
        setupSearchView(menu)
        return true
    }

    private fun setupSearchView(menu: Menu?) {
        menu?.findItem(R.id.action_search)?.apply {
            (actionView as TrailblazerSearchView).apply {
                setOnQueryTextListener(object : OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        return true
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        presenter.trackNameSearchTextChange(newText)
                        return true
                    }
                })
                setOnCloseButtonClickListener {
                    setQuery("", false)
                    presenter.trackNameSearchTextClear()
                }
            }
            setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                    setNonSearchActionsVisibility(menu, false)
                    presenter.searchActionStart()
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                    setNonSearchActionsVisibility(menu, true)
                    presenter.searchActionEnd()
                    return true
                }
            })
        }
    }

    private fun setNonSearchActionsVisibility(menu: Menu, visible: Boolean) {
        // We're doing this directly from menu item expand/collapse rather than show/hideNonSearchUi
        // as we don't want to use invalidateOptionsMenu due to UI glitches with the keyboard
        menu.setGroupVisible(R.id.non_search_actions, visible)
    }

    private fun enableMenuItemIfAvailable(menu: Menu, itemId: Int) {
        menu.findItem(itemId)?.isEnabled = presenter.isActionAvailable(itemId)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        enableMenuItemIfAvailable(menu, R.id.action_settings)
        enableMenuItemIfAvailable(menu, R.id.action_import_track)
        return true
    }

    override fun setTitle(title: String) {
        this.title = title
    }

    override fun navigateToCompare(trackIds: List<Long>) {
        val intent = CompareActivity.createIntent(this, trackIds)
        startActivity(intent)
    }

    override fun navigateToTrack(id: Long) {
        val intent = TrackDisplayActivity.createIntent(this, id)
        startActivity(intent)
    }

    override fun navigateToImportTrack(fileUri: String, promptForConfirmation: Boolean) {
        val intent = TrackDisplayActivity.createIntent(this, fileUri, promptForConfirmation)
        startActivity(intent)
    }

    override fun navigateToSettings() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    override fun promptToDelete(prompt: String, trackId: String) {
        val newFragment = OkCancelDialogFragment.newInstance(
                prompt,
                getString(R.string.action_delete),
                getString(R.string.action_cancel),
                trackId
        )
        newFragment.show(supportFragmentManager, TAG_DELETE_PROMPT)
    }

    override fun promptToDeleteMultiple(prompt: String) {
        val newFragment = OkCancelDialogFragment.newInstance(
                prompt,
                getString(R.string.action_delete),
                getString(R.string.action_cancel),
                ""
        )
        newFragment.show(supportFragmentManager, TAG_DELETE_MULTIPLE_PROMPT)
    }

    override fun promptForFileFormat(prompt: String, fileFormat: String) {
        val newFragment = FileFormatPromptDialogFragment.newInstance(
                getString(R.string.action_export),
                getString(R.string.action_cancel),
                prompt,
                fileFormat
        )
        newFragment.show(supportFragmentManager, TAG_EXPORT_FORMAT_PROMPT)
    }

    override fun ok(tag: String, userData: String) {
        loggerFactory.logger.debug("TrackListActivity.OK ${tag}, ${userData}")
        when (tag) {
            TAG_DELETE_PROMPT -> {
                presenter.confirmedDeleteTrack(userData)
            }
            TAG_DELETE_MULTIPLE_PROMPT -> {
                presenter.confirmedMultipleDeleteTrack()
            }
            TAG_EXPORT_FORMAT_PROMPT -> {
                presenter.confirmedMultipleExportTrack(userData)
            }
        }
    }

    override fun cancel(tag: String, userData: String) {
        loggerFactory.logger.debug("TrackListActivity.Cancel ${tag}, ${userData}")
    }

    override fun promptForTrackName(trackId: String, name: String) {
        val newFragment = ValuePromptDialogFragment.newInstance(
                R.layout.dialog_fragment_track_name,
                getString(R.string.action_save),
                getString(R.string.action_cancel),
                trackId,
                getString(R.string.edit_track_name_prompt),
                name
        )
        newFragment.show(supportFragmentManager, TAG_EDIT_NAME_PROMPT)
    }

    override fun promptForCombinedTrackName(name: String) {
        val newFragment = ValuePromptDialogFragment.newInstance(
            R.layout.dialog_fragment_track_name,
            getString(R.string.action_combine),
            getString(R.string.action_cancel),
            "",
            getString(R.string.combined_track_name_prompt),
            name
        )
        newFragment.show(supportFragmentManager, TAG_COMBINE_NAME_PROMPT)
    }

    override fun promptForTrackNameAfterStopping(trackId: String, name: String) {
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            loggerFactory.logger.debug("TrackListActivity.promptForTrackNameAfterStopping() - ignored as we are not on top")
            return
        }
        val newFragment = ValuePromptDialogFragment.newInstance(
            R.layout.dialog_fragment_track_name,
            getString(R.string.action_set_name),
            null,
            trackId,
            getString(R.string.edit_track_name_prompt),
            name
        )
        newFragment.show(supportFragmentManager, TAG_EDIT_NAME_PROMPT)
    }

    override fun ok(tag: String, userData: String, value: String) {
        loggerFactory.logger.debug("TrackListActivity.ok value ${tag}, ${userData}")
        when (tag) {
            TAG_EDIT_NAME_PROMPT -> {
                presenter.confirmedEditName(userData, value)
            }
            TAG_COMBINE_NAME_PROMPT -> {
                presenter.confirmedCombineSelectedTracks(value)
            }
        }
    }

    override fun cancel(tag: String, userData: String, value: String) {
        loggerFactory.logger.debug("TrackListActivity.Cancel value ${tag}, ${userData}")
    }

    protected override fun isActionAvailable(actionId: Int): Boolean {
        return presenter.isActionAvailable(actionId)
    }

    protected override fun actionSelected(itemId: Int): Boolean {
        return presenter.actionSelected(itemId)
    }

    override fun tracksLoaded() {
        // we only want the empty message to be displayed after the tracks have been loaded - not during the load
        tracksView.setEmptyView(noDataView)
        tracksView.adapter?.notifyDataSetChanged()
    }

    override fun startProgress() {
        ProgressViewHelper.startProgress(progressBar, window)
    }

    override fun startProgress(messageId: Int) {
        ProgressViewHelper.startProgress(progressBar, window, messageId, this)
    }

    override fun startProgress(messageId: Int, max: Int) {
        ProgressViewHelper.startProgress(progressBar, window, messageId, this, max)
    }

    override fun updateProgress(progress: Int) {
        progressBar.progress = progress
    }

    override fun completeProgress() {
        ProgressViewHelper.completeProgress(progressBar, window)
        currentActionMode?.finish()
    }

    override fun requestWriteExternalStoragePermission() {
        if (!permissionChecker.hasWriteStoragePermission()) {
            PermissionRequester.requestWriteExternalStoragePermission(this)
        }
    }

    override fun requestReadExternalStoragePermission() {
        if (!permissionChecker.hasReadStoragePermission()) {
            PermissionRequester.requestReadExternalStoragePermission(this)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PermissionRequester.MY_PERMISSIONS_REQUEST_NOTIFICATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    if (permissionChecker.hasPushNotificationPermission()) {
                        loggerFactory.logger.debug("TrackListActivity notification permission granted")
                    }
                } else {
                    // permission denied
                    showMessage(R.string.notification_permission_denied)
                }
                return
            }
            PermissionRequester.MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    if (permissionChecker.hasLocationPermission()) {
                        // make sure the FABs are being displayed
                        updateAvailableActions()
                        // ask for notification permission
                        requestNotificationPermission()
                    }
                } else {
                    // permission denied
                    showMessage(R.string.location_permission_denied)
                }
                return
            }
            PermissionRequester.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    // if we support export from an individual track we will need some extra logic here
                    presenter.exportSelectedTracks()
                } else {
                    // permission denied
                    showMessage(R.string.write_storage_permission_denied)
                }
                return
            }
            PermissionRequester.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    // the way the code works we just enable and disable the menu item if the permission is not available so there is nothing to do
                    presenter.importTrack()
                } else {
                    // permission denied
                    showMessage(R.string.read_storage_permission_denied)
                }
                return
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun promptForFile() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        // Filter using the MIME type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers, it would be "*/*".
        // as we know that other apps do not always report GPX MIME type correctly lets try for everything
        intent.type = "*/*"

        selectFileLauncher.launch(intent)
    }

    override fun setEmptyListText(id: Int) {
        emptyListText.setText(id)
    }

    override fun showNonSearchUi() {
        // This is to avoid the annoying flash of the FABs briefly showing in the wrong place before
        // the keyboard pops down (we don't need it when hiding the FABs before the animation)
        Handler(mainLooper).postDelayed({
            showFabsIfAvailable()
        }, 300)
    }

    override fun hideNonSearchUi() {
        // We can't just do super.updateAvailableActions as that calls invalidateOptionsMenu which
        // causes an annoying extra flash of the keyboard (it quickly pops up, then down, then up again)
        showFabsIfAvailable()
    }

    private fun handleSelectFileResult(result: ActivityResult) {
        if (result.resultCode == Activity.RESULT_OK) {
            result.data?.data?.let {
                val fileUri = it.toString()
                loggerFactory.logger.debug("File selected for import: {}", fileUri)
                navigateToImportTrack(fileUri, false)
            }
        }
    }

    override fun onSupportActionModeStarted(mode: ActionMode) {
        loggerFactory.logger.debug("TrackListActivity: action mode started")
        this.currentActionMode = mode
        super.onSupportActionModeStarted(mode)
    }

    override fun onSupportActionModeFinished(mode: ActionMode) {
        loggerFactory.logger.debug("TrackListActivity: onSupportActionModeFinished")
        this.currentActionMode = null
        super.onSupportActionModeFinished(mode)
    }

    override fun updateList(title: String) {
        adapter?.notifyDataSetChanged()
        currentActionMode?.title = title
    }

    override fun startMultiSelectMode() {
        startSupportActionMode(getMultiSelectActionMode())
    }

    private var currentActionMode: ActionMode? = null
    private var adapter: TrackListRecyclerAdapter? = null

    private fun setActionMode(actionMode: ActionMode?) {
        currentActionMode = actionMode
    }

    private fun getMultiSelectActionMode(): ActionMode.Callback {
        return object : ActionMode.Callback {
            override fun onPrepareActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                setActionMode(actionMode)
                presenter.inMultiSelectMode = true
                return false
            }

            override fun onDestroyActionMode(actionMode: ActionMode?) {
                setActionMode(null)
                loggerFactory.logger.debug("TrackListActivity: action mode ended")
                presenter.inMultiSelectMode = false
            }

            override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                actionMode.setTitle(R.string.track_list_multi_select_action_mode_title)
                actionMode.menuInflater.inflate(R.menu.track_list_activity_multi_select, menu)
                // we want a separator on the menu
                MenuCompat.setGroupDividerEnabled(menu,true);
                return true
            }

            override fun onActionItemClicked(actionMode: ActionMode, menuItem: MenuItem): Boolean {
                return presenter.onActionItemClicked(menuItem.itemId)
            }
        }
    }

    private fun requestLocationPermission() {
        if (!permissionChecker.hasLocationPermission()) {
            PermissionRequester.requestLocationPermission(this)
        }
    }

    private fun requestNotificationPermission() {
        if (!permissionChecker.hasPushNotificationPermission()) {
            PermissionRequester.requestPushNotificationPermission(this)
        }
    }

}