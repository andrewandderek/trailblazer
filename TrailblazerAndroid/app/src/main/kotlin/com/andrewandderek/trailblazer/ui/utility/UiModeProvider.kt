/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.utility

import android.content.Context
import com.andrewandderek.trailblazer.R
import javax.inject.Inject

interface IUiModeProvider {
    fun isInDarkMode(context: Context): Boolean
}

class UiModeProvider
@Inject constructor()
    : IUiModeProvider
{
    override fun isInDarkMode(context: Context): Boolean {
        // this works because we have two versions of the string "uimode" one in values and one in night
        return DARK_MODE_STRING_VALUE == context.getString(R.string.uimode)
    }

    companion object {
        // this needs to match the value in values/night/strings.xml
        const val DARK_MODE_STRING_VALUE = "DARK"
    }
}