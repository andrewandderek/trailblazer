/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.di

import android.app.Application
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.work.WorkManager
import com.andrewandderek.trailblazer.AndroidApplication
import com.andrewandderek.trailblazer.data.DATABASE_FILE_NAME_ROOT
import com.andrewandderek.trailblazer.data.DatabaseFactory
import com.andrewandderek.trailblazer.data.DatabaseHelper
import com.andrewandderek.trailblazer.data.IDatabaseFactory
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.data.repository.PositionRepository
import com.andrewandderek.trailblazer.data.repository.TrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.TrackRepository
import com.andrewandderek.trailblazer.data.utility.DatabaseConfigProvider
import com.andrewandderek.trailblazer.data.utility.DatabaseCopier
import com.andrewandderek.trailblazer.data.utility.IDatabaseConfigProvider
import com.andrewandderek.trailblazer.data.utility.IDatabaseCopier
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.RxEventBus
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.logging.Slf4jLoggerFactory
import com.andrewandderek.trailblazer.metadata.track.BatchTrackMetadataGenerator
import com.andrewandderek.trailblazer.metadata.track.IBatchTrackMetadataGenerator
import com.andrewandderek.trailblazer.metadata.track.ISingleTrackMetadataGenerator
import com.andrewandderek.trailblazer.metadata.track.SingleTrackMetadataGenerator
import com.andrewandderek.trailblazer.settings.IDiagnosticSettings
import com.andrewandderek.trailblazer.settings.IMapThemeSettings
import com.andrewandderek.trailblazer.settings.IRecordingStateProvider
import com.andrewandderek.trailblazer.settings.IThemeSetter
import com.andrewandderek.trailblazer.settings.IThemeSettings
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.settings.IZoomSettings
import com.andrewandderek.trailblazer.settings.ThemeSetter
import com.andrewandderek.trailblazer.settings.UserSettings
import com.andrewandderek.trailblazer.sharing.importexport.ExporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.IBitmapExporter
import com.andrewandderek.trailblazer.sharing.importexport.IExporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.IImporter
import com.andrewandderek.trailblazer.sharing.importexport.IImporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.ITrackExporter
import com.andrewandderek.trailblazer.sharing.importexport.ImporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.MultiformatImporter
import com.andrewandderek.trailblazer.sharing.importexport.bitmap.PngExporter
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxExporter
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxImporter
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmlExporter
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmlImporter
import com.andrewandderek.trailblazer.sharing.importexport.kml.KmzImporter
import com.andrewandderek.trailblazer.sharing.intent.DatabaseSharer
import com.andrewandderek.trailblazer.sharing.intent.EmailTrackFileSharer
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.sharing.intent.ImageSharer
import com.andrewandderek.trailblazer.statistics.AndroidLocationDistanceProvider
import com.andrewandderek.trailblazer.statistics.IDistanceProvider
import com.andrewandderek.trailblazer.statistics.ITrackGroupSorter
import com.andrewandderek.trailblazer.statistics.ITrackGroupSummaryStatisticsProvider
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.statistics.TrackGroupSorter
import com.andrewandderek.trailblazer.statistics.TrackGroupSummaryStatisticsProvider
import com.andrewandderek.trailblazer.statistics.TrackStatisticsProvider
import com.andrewandderek.trailblazer.statistics.TrackSummaryStatisticsProvider
import com.andrewandderek.trailblazer.statistics.elevation.DefaultElevationSettings
import com.andrewandderek.trailblazer.statistics.elevation.ElevationAlgorithmFactory
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithmFactory
import com.andrewandderek.trailblazer.statistics.elevation.IElevationSettings
import com.andrewandderek.trailblazer.ui.chart.ElevationByDistanceEntryMapper
import com.andrewandderek.trailblazer.ui.chart.ElevationByTimeEntryMapper
import com.andrewandderek.trailblazer.ui.chart.ElevationChartDataMapper
import com.andrewandderek.trailblazer.ui.chart.IChartDataMapper
import com.andrewandderek.trailblazer.ui.chart.ILineDataSetFactory
import com.andrewandderek.trailblazer.ui.chart.LineDataSetFactory
import com.andrewandderek.trailblazer.ui.utility.IMapColourSchemeProvider
import com.andrewandderek.trailblazer.ui.utility.IUiModeProvider
import com.andrewandderek.trailblazer.ui.utility.MapColourSchemeProvider
import com.andrewandderek.trailblazer.ui.utility.UiModeProvider
import com.andrewandderek.trailblazer.units.IDistanceUnitProvider
import com.andrewandderek.trailblazer.units.conversion.AltitudeConverter
import com.andrewandderek.trailblazer.units.conversion.DistanceConverter
import com.andrewandderek.trailblazer.units.conversion.IAltitudeConverter
import com.andrewandderek.trailblazer.units.conversion.IDistanceConverter
import com.andrewandderek.trailblazer.units.conversion.IPaceConverter
import com.andrewandderek.trailblazer.units.conversion.ISpeedConverter
import com.andrewandderek.trailblazer.units.conversion.PaceConverter
import com.andrewandderek.trailblazer.units.conversion.SpeedConverter
import com.andrewandderek.trailblazer.units.formatting.AltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.DistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.units.formatting.PaceFormatter
import com.andrewandderek.trailblazer.units.formatting.SpeedFormatter
import com.andrewandderek.trailblazer.utility.AndroidDefaultSharedPreferencesProvider
import com.andrewandderek.trailblazer.utility.AndroidResourceProvider
import com.andrewandderek.trailblazer.utility.CrashlyticsReporter
import com.andrewandderek.trailblazer.utility.EnvironmentInformationProvider
import com.andrewandderek.trailblazer.utility.FileSystemHelper
import com.andrewandderek.trailblazer.utility.FirebaseAnalyticsEngine
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IEnvironmentInformationProvider
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import com.andrewandderek.trailblazer.utility.IPreferencesProvider
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.utility.ISystemTime
import com.andrewandderek.trailblazer.utility.ITextMatcher
import com.andrewandderek.trailblazer.utility.PermissionChecker
import com.andrewandderek.trailblazer.utility.SchedulerProvider
import com.andrewandderek.trailblazer.utility.SystemTime
import com.andrewandderek.trailblazer.utility.TextMatcher
import com.andrewandderek.trailblazer.worker.trackmetadata.ITrackMetadataScheduler
import com.andrewandderek.trailblazer.worker.trackmetadata.TrackMetadataScheduler
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Provider
import javax.inject.Singleton


@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application.applicationContext

    /* Singleton factory that searches generated map for specific provider and uses it to get a ViewModel instance */
    @Provides
    @Singleton
    fun provideViewModelFactory(providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return requireNotNull(providers[modelClass]).get() as T
            }
        }

    @Provides
    @Singleton
    fun provideLoggerFactory(loggerFactory: Slf4jLoggerFactory): ILoggerFactory {
        return loggerFactory
    }

    @Provides
    @Singleton
    fun provideCrashReporter(reporter: CrashlyticsReporter): ICrashReporter {
        return reporter
    }

    @Provides
    @Singleton
    fun provideAnalyticsEngine(engine: FirebaseAnalyticsEngine): IAnalyticsEngine {
        return engine
    }

    @Provides
    @Singleton
    fun provideSystemTime(systemTime: SystemTime): ISystemTime {
        return systemTime
    }

    @Provides
    @Singleton
    fun provideFileSystemHelper(helper: FileSystemHelper): IFileSystemHelper {
        return helper
    }

    @Provides
    @Singleton
    fun provideResourceProvider(provider: AndroidResourceProvider): IResourceProvider {
        return provider
    }

    @Provides
    @Singleton
    fun providePreferencesProvider(provider: AndroidDefaultSharedPreferencesProvider): IPreferencesProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideEnvironmentInformationProvider(provider: EnvironmentInformationProvider): IEnvironmentInformationProvider {
        return provider
    }

    @Provides
    @Singleton
    fun providePermissionChecker(checker: PermissionChecker): IPermissionChecker {
        return checker
    }

    @Provides
    @Singleton
    fun provideContentResolver(): ContentResolver {
        return application.applicationContext.contentResolver
    }

    @Provides
    @Singleton
    fun provideNotificationManager(): NotificationManager {
        return application.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    @Provides
    @Singleton
    fun provideWorkManager(): WorkManager {
        return WorkManager.getInstance(provideApplicationContext())
    }

    @Provides
    @Singleton
    fun provideTrackMetadataScheduler(scheduler: TrackMetadataScheduler): ITrackMetadataScheduler {
        return scheduler
    }

    @Provides
    @Singleton
    fun provideBatchTrackMetadataGenerator(generator: BatchTrackMetadataGenerator): IBatchTrackMetadataGenerator {
        return generator
    }

    @Provides
    @Singleton
    fun provideSingleTrackMetadataGenerator(generator: SingleTrackMetadataGenerator): ISingleTrackMetadataGenerator {
        return generator
    }

    @Provides
    @Singleton
    fun provideEventBus(eventBus: RxEventBus): IEventBus {
        return eventBus
    }


    @Provides
    @Singleton
    fun provideDatabaseFactory(factory: DatabaseFactory): IDatabaseFactory {
        return factory
    }

    @Provides
    @Singleton
    fun provideDefaultDatabase(): DatabaseHelper {
        val factory = DatabaseFactory(Slf4jLoggerFactory(), provideApplicationContext())
        return factory.getDatabaseHelper("${DATABASE_FILE_NAME_ROOT}.db")
    }

    @Provides
    @Singleton
    fun provideDatabaseConfigProvider(provider: DatabaseConfigProvider): IDatabaseConfigProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideDatabaseCopier(copier: DatabaseCopier): IDatabaseCopier {
        return copier
    }

    @Provides
    @Singleton
    fun provideTrackRepository(repository: TrackRepository): ITrackRepository {
        // if you want a repo that talks to a different db then you will need to create a named provider
        return repository
    }

    @Provides
    @Singleton
    fun provideTrackMetadataRepository(repository: TrackMetadataRepository): ITrackMetadataRepository {
        // if you want a repo that talks to a different db then you will need to create a named provider
        return repository
    }

    @Provides
    @Singleton
    fun providePositionRepository(repository: PositionRepository): IPositionRepository {
        // if you want a repo that talks to a different db then you will need to create a named provider
        return repository
    }

    @Provides
    @Singleton
    fun provideSchedulerProvider(provider: SchedulerProvider): ISchedulerProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideDistanceProvider(provider: AndroidLocationDistanceProvider): IDistanceProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideElevationSettings(elevationSettings: DefaultElevationSettings): IElevationSettings {
        return elevationSettings
    }

    @Provides
    @Singleton
    fun provideElevationAlgorithmFactory(factory: ElevationAlgorithmFactory): IElevationAlgorithmFactory {
        return factory
    }

    @Provides
    @Singleton
    @Named(NameLiterals.STATISTICS_FULL)
    fun provideTrackStatisticsProvider(provider: TrackStatisticsProvider): ITrackStatisticsProvider {
        return provider
    }

    @Provides
    @Singleton
    @Named(NameLiterals.STATISTICS_SUMMARY)
    fun provideTrackSummaryStatisticsProvider(provider: TrackSummaryStatisticsProvider): ITrackStatisticsProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideTrackGroupSummaryStatisticsProvider(provider: TrackGroupSummaryStatisticsProvider): ITrackGroupSummaryStatisticsProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideTrackGroupSorter(sorter: TrackGroupSorter): ITrackGroupSorter {
        return sorter
    }

    @Provides
    @Singleton
    fun provideExporterFactory(factory: ExporterFactory): IExporterFactory {
        return factory
    }

    @Provides
    @Singleton
    @Named(NameLiterals.FILE_FORMAT_GPX)
    fun provideGpxExporter(exporter: GpxExporter): ITrackExporter {
        return exporter
    }

    @Provides
    @Singleton
    @Named(NameLiterals.FILE_FORMAT_KML)
    fun provideKmlExporter(exporter: KmlExporter): ITrackExporter {
        return exporter
    }

    @Provides
    @Singleton
    fun providePngExporter(exporter: PngExporter): IBitmapExporter {
        return exporter
    }

    @Provides
    @Singleton
    fun provideImporterFactory(factory: ImporterFactory): IImporterFactory {
        return factory
    }

    @Provides
    @Singleton
    fun provideMultiformatImporter(importer: MultiformatImporter): IImporter {
        return importer
    }

    @Provides
    @Singleton
    @Named(NameLiterals.FILE_FORMAT_GPX)
    fun provideGpxImporter(importer: GpxImporter): IImporter {
        return importer
    }

    @Provides
    @Singleton
    @Named(NameLiterals.FILE_FORMAT_KML)
    fun provideKmlImporter(importer: KmlImporter): IImporter {
        return importer
    }

    @Provides
    @Singleton
    @Named(NameLiterals.FILE_FORMAT_KMZ)
    fun provideKmzImporter(importer: KmzImporter): IImporter {
        return importer
    }

    @Provides
    @Singleton
    @Named(NameLiterals.SHARE_TRACK)
    fun provideEmailSharer(sharer: EmailTrackFileSharer): IIntentSharer {
        return sharer
    }

    @Provides
    @Singleton
    @Named(NameLiterals.SHARE_IMAGE)
    fun provideImageSharer(sharer: ImageSharer): IIntentSharer {
        return sharer
    }

    @Provides
    @Singleton
    @Named(NameLiterals.SHARE_DATABASE)
    fun provideDatabaseSharer(sharer: DatabaseSharer): IIntentSharer {
        return sharer
    }

    @Provides
    @Singleton
    fun provideUserSettings(userSettings: UserSettings): IUserSettings {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideDistanceUnitProvider(userSettings: UserSettings): IDistanceUnitProvider {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideZoomSettings(userSettings: UserSettings): IZoomSettings {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideRecordingState(userSettings: UserSettings): IRecordingStateProvider {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideThemeSettings(userSettings: UserSettings): IThemeSettings {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideMapThemeSettings(userSettings: UserSettings): IMapThemeSettings {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideDiagnosticSettings(userSettings: UserSettings): IDiagnosticSettings {
        return userSettings
    }

    @Provides
    @Singleton
    fun provideThemeSetter(): IThemeSetter {
        return ThemeSetter()
    }

    @Provides
    @Singleton
    fun provideMapColourSchemeProvider(mapColourSchemeProvider: MapColourSchemeProvider): IMapColourSchemeProvider {
        return mapColourSchemeProvider
    }

    @Provides
    @Singleton
    fun provideUiModeProvider(uiModeProvider: UiModeProvider): IUiModeProvider {
        return uiModeProvider
    }

    @Provides
    @Singleton
    fun provideLineDataSetFactory(factory: LineDataSetFactory): ILineDataSetFactory {
        return factory
    }

    @Provides
    @Named(NameLiterals.CHART_DATA_MAPPER_ELEVATION_BY_TIME)
    fun provideElevationByTimeMapper(lineDataSetFactory: ILineDataSetFactory, positionToEntryMapper: ElevationByTimeEntryMapper): IChartDataMapper {
        return ElevationChartDataMapper(lineDataSetFactory, positionToEntryMapper)
    }

    @Provides
    @Named(NameLiterals.CHART_DATA_MAPPER_ELEVATION_BY_DISTANCE)
    fun provideElevationByDistanceMapper(lineDataSetFactory: ILineDataSetFactory, positionToEntryMapper: ElevationByDistanceEntryMapper): IChartDataMapper {
        return ElevationChartDataMapper(lineDataSetFactory, positionToEntryMapper)
    }

    @Provides
    @Singleton
    fun provideAltitudeConverter(converter: AltitudeConverter): IAltitudeConverter {
        return converter
    }

    @Provides
    @Singleton
    fun provideAltitudeFormatter(formatter: AltitudeFormatter): IAltitudeFormatter {
        return formatter
    }

    @Provides
    @Singleton
    fun provideDistanceConverter(converter: DistanceConverter): IDistanceConverter {
        return converter
    }

    @Provides
    @Singleton
    fun provideDistanceFormatter(formatter: DistanceFormatter): IDistanceFormatter {
        return formatter
    }

    @Provides
    @Singleton
    fun providePaceConverter(converter: PaceConverter): IPaceConverter {
        return converter
    }

    @Provides
    @Singleton
    fun providePaceFormatter(formatter: PaceFormatter): IPaceFormatter {
        return formatter
    }

    @Provides
    @Singleton
    fun provideSpeedConverter(converter: SpeedConverter): ISpeedConverter {
        return converter
    }

    @Provides
    @Singleton
    fun provideSpeedFormatter(formatter: SpeedFormatter): ISpeedFormatter {
        return formatter
    }

    @Provides
    @Singleton
    fun provideTextMatcher(textMatcher: TextMatcher): ITextMatcher {
        return textMatcher
    }

}