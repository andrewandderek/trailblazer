/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data

import android.content.Context
import android.database.DatabaseErrorHandler
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.andrewandderek.trailblazer.BuildConfig
import com.andrewandderek.trailblazer.data.schema.PositionTable
import com.andrewandderek.trailblazer.data.schema.TrackMetadataTable
import com.andrewandderek.trailblazer.data.schema.TrackTable
import com.andrewandderek.trailblazer.logging.ILoggerFactory

import javax.inject.Inject

// Schema changes
//
//  Schema Version      Release Notes
//
//  1                   n/a	    Initial dev version
//  2                   n/a     Added to Position table - accuracy and flags
//  3                   v1.0.0  Added to Position table - segment (to support pausing)
//  4                   v1.0.3  Made Position's altitude field nullable (not all positions have an altitude)
//  5                   v1.0.5  Added index on track_id to Position table
//  6                   v1.9.0  Made Position's timestamp nullable
//  7                   v1.11.0 Added track metadata
//  8                   v1.12.0 Added statistics to track metadata
//

private val DATABASE_VERSION: Int = 8

public val DATABASE_FILE_NAME_ROOT = "trailblazer"

class DatabaseHelper
    @Inject constructor(
            context: Context?,
            name: String?,
            errorHandler: DatabaseErrorHandler?,
            private val _logger: ILoggerFactory
    )
    : SQLiteOpenHelper(context, name, null, DATABASE_VERSION, errorHandler) {

    private fun initialiseData(database: SQLiteDatabase) {
        if (!BuildConfig.DEBUG) {
            return  // we do not plant example data in the release build
        }
        TrackTable.onInitialiseData(database)
        PositionTable.onInitialiseData(database)
        TrackMetadataTable.onInitialiseData(database)
    }

    override fun onCreate(database: SQLiteDatabase) {
        _logger.logger.debug("Creating database");
        TrackTable.onCreate(database)
        PositionTable.onCreate(database)
        TrackMetadataTable.onCreate(database)

        initialiseData(database)
    }

    override fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        _logger.logger.debug("Upgrading database from version $oldVersion to $newVersion");
        TrackTable.onUpgrade(database, oldVersion, newVersion, _logger);
        PositionTable.onUpgrade(database, oldVersion, newVersion, _logger);
        TrackMetadataTable.onUpgrade(database, oldVersion, newVersion, _logger);

        initialiseData(database);
    }

    fun getDatabase(): SQLiteDatabase {
        return writableDatabase
    }

    @Throws(SQLException::class)
    fun open(): SQLiteDatabase? {
        var readOnly: Boolean = getDatabase().isReadOnly
        if (!readOnly) {
            // Enable foreign key constraints and also the cascade delete
            getDatabase().execSQL("PRAGMA foreign_keys=ON;")
            // see http://sqlite.org/pragma.html#pragma_synchronous
            // we might want to consider synchronous=OFF
            getDatabase().execSQL("PRAGMA synchronous=NORMAL;")
        }
        return getDatabase()
    }

    public fun upgradeToLatestDatabaseSchema() {
        synchronized(this) {
            try {
                _logger.logger.debug("upgradeToLatestDatabaseSchema DB $databaseName: version ${getDatabase().version}, pagesize ${getDatabase().pageSize}, maxsize ${getDatabase().maximumSize}")
                _logger.logger.debug("upgradeToLatestDatabaseSchema DB $databaseName: writeAheadLogging ${getDatabase().isWriteAheadLoggingEnabled}, path ${getDatabase().path}")
                _logger.logger.debug("upgradeToLatestDatabaseSchema DB $databaseName: opening")
                open()
            } catch (e: Exception) {
                _logger.logger.error("upgradeToLatestDatabaseSchema DB $databaseName: Error ", e)
            } finally {
                getDatabase().close()
                _logger.logger.debug("upgradeToLatestDatabaseSchema DB $databaseName: closed")
            }
        }
    }
}

