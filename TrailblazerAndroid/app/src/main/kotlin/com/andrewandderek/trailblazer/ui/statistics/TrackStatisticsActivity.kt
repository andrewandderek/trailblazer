/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.statistics

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.chart.IChartDataMapper
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.utility.ProgressViewHelper
import com.andrewandderek.trailblazer.ui.utility.ViewBitmapHelper
import com.andrewandderek.trailblazer.ui.view.ProgressSpinnerView
import com.andrewandderek.trailblazer.ui.view.TrackDataItemView
import com.andrewandderek.trailblazer.ui.view.TrackElevationChart
import dagger.android.AndroidInjection
import javax.inject.Inject
import javax.inject.Named


class TrackStatisticsActivity : AppCompatActivity(),  ITrackStatisticsView {
    companion object {
        const val PARAM_TRACK_ID = "trackID"

        fun createIntent(context: Context, trackId: Long): Intent {
            val intent = Intent(context,  TrackStatisticsActivity::class.java)
            intent.putExtra(PARAM_TRACK_ID, trackId)
            return intent
        }
    }

    @BindView(R.id.progressBar)
    protected lateinit var progressBar: ProgressSpinnerView

    @BindView(R.id.container)
    lateinit var scrollContainer: ScrollView

    @BindView(R.id.distance)
    lateinit var distanceView: TrackDataItemView

    @BindView(R.id.stats_row_time)
    lateinit var timeRow: LinearLayout

    @BindView(R.id.recorded_time)
    lateinit var recordedTimeView: TrackDataItemView

    @BindView(R.id.moving_time)
    lateinit var movingTimeView: TrackDataItemView

    @BindView(R.id.stats_row_speed)
    lateinit var speedRow: LinearLayout

    @BindView(R.id.average_speed)
    lateinit var averageSpeedView: TrackDataItemView

    @BindView(R.id.average_moving_speed)
    lateinit var averageMovingSpeedView: TrackDataItemView

    @BindView(R.id.stats_row_pace)
    lateinit var paceRow: LinearLayout

    @BindView(R.id.average_pace)
    lateinit var averagePaceView: TrackDataItemView

    @BindView(R.id.average_moving_pace)
    lateinit var averageMovingPaceView: TrackDataItemView

    @BindView(R.id.stats_row_elevation_gain)
    lateinit var elevationGainRow: LinearLayout

    @BindView(R.id.elevation_gain)
    lateinit var elevationGainView: TrackDataItemView

    @BindView(R.id.elevation_loss)
    lateinit var elevationLossView: TrackDataItemView

    @BindView(R.id.stats_row_elevation_difference)
    lateinit var elevationDifferenceRow: LinearLayout

    @BindView(R.id.elevation_difference)
    lateinit var elevationDifferenceView: TrackDataItemView

    @BindView(R.id.elevation_min_max)
    lateinit var elevationMinMaxView: TextView

    @BindView(R.id.stats_row_elevation_by_distance_chart)
    lateinit var elevationByDistanceChartRow: LinearLayout

    @BindView(R.id.elevation_by_distance_chart)
    lateinit var elevationByDistanceChart: TrackElevationChart

    @BindView(R.id.stats_row_elevation_by_time_chart)
    lateinit var elevationByTimeChartRow: LinearLayout

    @BindView(R.id.elevation_by_time_chart)
    lateinit var elevationByTimeChart: TrackElevationChart

    @BindView(R.id.stats_row_notes)
    lateinit var notesRow: LinearLayout

    @BindView(R.id.txtStatsNotes)
    lateinit var notesText: TextView

    @BindView(R.id.stats_row_segments)
    lateinit var segmentsRow: LinearLayout

    @BindView(R.id.txtStatsSegments)
    lateinit var segmentsText: TextView

    @Inject lateinit var loggerFactory: ILoggerFactory
    @Inject @Named(NameLiterals.CHART_DATA_MAPPER_ELEVATION_BY_DISTANCE) lateinit var elevationByDistanceChartMapper: IChartDataMapper
    @Inject @Named(NameLiterals.CHART_DATA_MAPPER_ELEVATION_BY_TIME) lateinit var elevationByTimeChartMapper: IChartDataMapper
    @Inject lateinit var presenter: ITrackStatisticsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("TrackStatisticsActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)
        CheeseKnife.bind(this)
        setupCharts()

        presenter.bind(this)
        presenter.loadTrack(getSuppliedTrackId())
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("TrackListActivity.onDestroy()")

        super.onDestroy()
        presenter.unbind(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.statistics_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (presenter.actionSelected(item.itemId)) {
            return true
        }
        if (item.itemId == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(messageId: Int) {
        Toast.makeText(this, messageId, Toast.LENGTH_LONG).show()
    }

    override fun startProgress(messageId: Int) {
        ProgressViewHelper.startProgress(progressBar, window, messageId, this)
    }

    override fun completeProgress() {
        ProgressViewHelper.completeProgress(progressBar, window)
    }

    override fun startIntent(intent: Intent, title: String) {
        startActivity(Intent.createChooser(intent, title));
    }

    override fun hideAltitudeBasedRows() {
        this.elevationGainRow.visibility = View.GONE
        this.elevationDifferenceRow.visibility = View.GONE
        this.elevationByDistanceChartRow.visibility = View.GONE
        this.elevationByTimeChartRow.visibility = View.GONE
    }

    override fun hideTimeBasedRows() {
        this.timeRow.visibility = View.GONE
        this.speedRow.visibility = View.GONE
        this.paceRow.visibility = View.GONE
        hideTimeBasedGraph()
    }

    override fun hideTimeBasedGraph() {
        this.elevationByTimeChartRow.visibility = View.GONE
    }

    override fun hideNotes() {
        this.notesRow.visibility = View.GONE
    }

    override fun setNotes(text: String) {
        this.notesText.text = text
    }

    override fun hideSegmentsRow() {
        this.segmentsRow.visibility = View.GONE
    }

    override fun setSegmentsText(text: String) {
        this.segmentsText.text = text
    }

    override fun getScreenshotBitmap(): Bitmap {
        return ViewBitmapHelper.createViewBitmap(scrollContainer, ContextCompat.getColor(this, R.color.bitmap_share_background))
    }

    override fun setTrackTitle(title: String) {
        this.title = title
    }

    override fun setDistance(distance: String, unit: String) {
        distanceView.setValue(distance, unit)
    }

    override fun setRecordedTime(recordedTime: String) {
        recordedTimeView.setValue(recordedTime, "")
    }

    override fun setMovingTime(movingTime: String) {
        movingTimeView.setValue(movingTime, "")
    }

    override fun setElevationValues(elevationGain: String, elevationLoss: String, minElevation: String, maxElevation: String, elevationDifference: String, unit: String) {
        elevationGainView.setValue(elevationGain, unit)
        elevationLossView.setValue(elevationLoss, unit)
        elevationDifferenceView.setValue(elevationDifference, unit)
        elevationMinMaxView.text = getString(R.string.statistics_min_max_elevation, minElevation, maxElevation, unit)
    }

    override fun setSpeedValues(averageSpeed: String, averageMovingSpeed: String, unit: String) {
        averageSpeedView.setValue(averageSpeed, unit)
        averageMovingSpeedView.setValue(averageMovingSpeed, unit)
    }

    override fun setPaceValues(averagePace: String, averageMovingPace: String, paceUnitLabel: String) {
        averagePaceView.setValue(averagePace, paceUnitLabel)
        averageMovingPaceView.setValue(averageMovingPace, paceUnitLabel)
    }

    override fun setElevationByDistanceChartPositions(positions: List<Position>) {
        elevationByDistanceChart.setPositions(positions)
    }

    override fun setElevationByTimeChartPositions(positions: List<Position>) {
        elevationByTimeChart.setPositions(positions)
    }

    private fun setupCharts() {
        elevationByDistanceChart.mapper = elevationByDistanceChartMapper
        elevationByTimeChart.mapper = elevationByTimeChartMapper
        // the chart has time as its X axis we add this line to attach a formatter for it
        elevationByTimeChart.setTimeBasedXAxisFormatter()
    }

    private fun getSuppliedTrackId(): Long {
        return intent.getLongExtra(PARAM_TRACK_ID, 0)
    }

}
