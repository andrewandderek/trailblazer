/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.repository

import android.content.ContentValues
import android.database.Cursor
import com.andrewandderek.trailblazer.data.DatabaseHelper
import com.andrewandderek.trailblazer.data.schema.TrackMetadataTable
import com.andrewandderek.trailblazer.data.utility.DatabaseFieldConverter
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.TrackMetadata
import javax.inject.Inject

interface ITrackMetadataRepository : IRepository<TrackMetadata, Long> {
    fun getWhereVersionCodeBefore(versionCode: Int): List<TrackMetadata>
}

class TrackMetadataRepository
@Inject constructor(
    internal var loggerFactory: ILoggerFactory,
    private var databaseHelper: DatabaseHelper
) : BaseRepository<TrackMetadata>(), ITrackMetadataRepository {

    // use this with the simple SQLiteDatabase.query()
    // the track repository also has the column names in QUERY_TRACK_AND_METADATA_SQL as it needs to load the metadata
    private val allColumns = arrayOf(
        TrackMetadataTable.COLUMN_TRACK_ID,
        TrackMetadataTable.COLUMN_VERSION_NAME,
        TrackMetadataTable.COLUMN_VERSION_CODE,
        DatabaseFieldConverter.getCalendarSelect(TrackMetadataTable.COLUMN_TIME_ENDED),
        TrackMetadataTable.COLUMN_STATISTICS_DISTANCE_METERS,
        TrackMetadataTable.COLUMN_STATISTICS_ELAPSED_MILLISECONDS,
        TrackMetadataTable.COLUMN_STATISTICS_RECORDED_MILLISECONDS,
        TrackMetadataTable.COLUMN_STATISTICS_MOVING_MILLISECONDS,
        TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_SPEED_METERS_PER_SECOND,
        TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_MOVING_SPEED_METERS_PER_SECOND,
        TrackMetadataTable.COLUMN_STATISTICS_MINIMUM_ELEVATION_METERS,
        TrackMetadataTable.COLUMN_STATISTICS_MAXIMUM_ELEVATION_METERS,
        TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_DIFFERENCE_METERS,
        TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_GAIN_METERS,
        TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_LOSS_METERS,
    )

    // the table track_metadata has a 1:1 relationship with the track table
    // that is there is a maximum of one row on the track_metadata table for every track (and there may be zero rows)
    // this is reflected in the model data in that a track has a nullable property of one metadata object
    // this is enforced by the UNIQUE constraint on the track_metadata.track_id column
    // thus we need to use the replace command to either insert or update
    private fun replace(item: TrackMetadata): TrackMetadata {
        val values: ContentValues = trackMetadataToContentValues(item)
        loggerFactory.logger.debug("replacing track metadata with id ${item.trackId}")
        databaseHelper.getDatabase().replace(TrackMetadataTable.TABLE_TRACK_METADATA, null, values)
        return(getById(item.trackId)!!)
    }

    override fun create(newItem: TrackMetadata): TrackMetadata {
        return replace(newItem)
    }

    override fun update(item: TrackMetadata): TrackMetadata {
        return replace(item)
    }

    override fun delete(item: TrackMetadata) {
        deleteById(item.trackId)
    }

    // usually we dont need to call this as deletes are cascaded when the track is deleted
    override fun deleteById(id: Long) {
        loggerFactory.logger.debug("deleting track metadata with id $id")
        databaseHelper.getDatabase().delete(TrackMetadataTable.TABLE_TRACK_METADATA, "${TrackMetadataTable.COLUMN_TRACK_ID} = ${id}", null)
    }

    override fun deleteAll() {
        loggerFactory.logger.debug("deleted ALL track metadata")
        databaseHelper.getDatabase().delete(TrackMetadataTable.TABLE_TRACK_METADATA, null, null)
    }

    override fun getById(id: Long): TrackMetadata? {
        loggerFactory.logger.debug("getting track metadata with id $id")
        val cursor = databaseHelper.getDatabase().query(TrackMetadataTable.TABLE_TRACK_METADATA, allColumns, "${TrackMetadataTable.COLUMN_TRACK_ID} = ${id}", null, null, null, null)
        return getSingle(cursor, TrackMetadataRepository::cursorToTrackMetadata)
    }

    override fun getAll(): List<TrackMetadata> {
        loggerFactory.logger.debug("getting all track metadata")
        val cursor = databaseHelper.getDatabase().query(TrackMetadataTable.TABLE_TRACK_METADATA, allColumns, null, null, null, null, null)
        return getMultiple(cursor, TrackMetadataRepository::cursorToTrackMetadata)
    }

    override fun getWhereVersionCodeBefore(versionCode: Int): List<TrackMetadata> {
        loggerFactory.logger.debug("getting track metadata before version code $versionCode")
        val cursor = databaseHelper.getDatabase().query(TrackMetadataTable.TABLE_TRACK_METADATA, allColumns, "${TrackMetadataTable.COLUMN_VERSION_CODE} < ${versionCode}", null, null, null, null)
        return getMultiple(cursor, TrackMetadataRepository::cursorToTrackMetadata)
    }

    private fun trackMetadataToContentValues(item: TrackMetadata): ContentValues {
        val values = ContentValues()
        // the ID is allocated by the DB - do not put it in here
        values.put(TrackMetadataTable.COLUMN_TRACK_ID, item.trackId)
        values.put(TrackMetadataTable.COLUMN_VERSION_NAME, item.versionName)
        values.put(TrackMetadataTable.COLUMN_VERSION_CODE, item.versionCode)
        values.put(TrackMetadataTable.COLUMN_TIME_ENDED, DatabaseFieldConverter.convertCalendarTimeToDbString(item.ended))
        values.put(TrackMetadataTable.COLUMN_STATISTICS_DISTANCE_METERS, item.statistics.distanceInMetres)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_ELAPSED_MILLISECONDS, item.statistics.elapsedTimeInMilliseconds)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_RECORDED_MILLISECONDS, item.statistics.recordedTimeInMilliseconds)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_MOVING_MILLISECONDS, item.statistics.movingTimeInMilliseconds)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_SPEED_METERS_PER_SECOND, item.statistics.averageSpeedInMetresPerSecond)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_MOVING_SPEED_METERS_PER_SECOND, item.statistics.averageMovingSpeedInMetresPerSecond)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_MINIMUM_ELEVATION_METERS, item.statistics.minElevationInMetres)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_MAXIMUM_ELEVATION_METERS, item.statistics.maxElevationInMetres)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_DIFFERENCE_METERS, item.statistics.elevationDifferenceInMetres)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_GAIN_METERS, item.statistics.elevationGainInMetres)
        values.put(TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_LOSS_METERS, item.statistics.elevationLossInMetres)
        return values
    }

    companion object {

        // this is public and static as it does not need any instance variables and we need the logic in the TrackRepository
        public fun cursorToTrackMetadata(cursor: Cursor): TrackMetadata {
            val trackMetadata = TrackMetadata()
            trackMetadata.trackId = DatabaseFieldConverter.getLong(cursor, TrackMetadataTable.COLUMN_TRACK_ID)
            trackMetadata.versionName = DatabaseFieldConverter.getString(cursor, TrackMetadataTable.COLUMN_VERSION_NAME)
            trackMetadata.versionCode = DatabaseFieldConverter.getInt(cursor, TrackMetadataTable.COLUMN_VERSION_CODE)
            trackMetadata.ended = DatabaseFieldConverter.getNullable(cursor, TrackMetadataTable.COLUMN_TIME_ENDED, DatabaseFieldConverter::getCalendar)
            trackMetadata.statistics.distanceInMetres = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_DISTANCE_METERS)
            trackMetadata.statistics.elapsedTimeInMilliseconds = DatabaseFieldConverter.getLong(cursor, TrackMetadataTable.COLUMN_STATISTICS_ELAPSED_MILLISECONDS)
            trackMetadata.statistics.recordedTimeInMilliseconds = DatabaseFieldConverter.getLong(cursor, TrackMetadataTable.COLUMN_STATISTICS_RECORDED_MILLISECONDS)
            trackMetadata.statistics.movingTimeInMilliseconds = DatabaseFieldConverter.getLong(cursor, TrackMetadataTable.COLUMN_STATISTICS_MOVING_MILLISECONDS)
            trackMetadata.statistics.averageSpeedInMetresPerSecond = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_SPEED_METERS_PER_SECOND)
            trackMetadata.statistics.averageMovingSpeedInMetresPerSecond = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_AVERAGE_MOVING_SPEED_METERS_PER_SECOND)
            trackMetadata.statistics.minElevationInMetres = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_MINIMUM_ELEVATION_METERS)
            trackMetadata.statistics.maxElevationInMetres = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_MAXIMUM_ELEVATION_METERS)
            trackMetadata.statistics.elevationDifferenceInMetres = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_DIFFERENCE_METERS)
            trackMetadata.statistics.elevationGainInMetres = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_GAIN_METERS)
            trackMetadata.statistics.elevationLossInMetres = DatabaseFieldConverter.getDouble(cursor, TrackMetadataTable.COLUMN_STATISTICS_ELEVATION_LOSS_METERS)
            return trackMetadata
        }
    }
}
