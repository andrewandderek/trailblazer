/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.service.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class TrackRecordingControlsReceiver : BroadcastReceiver() {
    @Inject lateinit var trackRecordingControls: ITrackRecordingControls
    @Inject lateinit var loggerFactory: ILoggerFactory

    override fun onReceive(context: Context?, intent: Intent?) {
        AndroidInjection.inject(this, context)
        loggerFactory.logger.debug("TrackRecordingControlsReceiver: onReceive ${intent?.action}")
        intent?.let { trackRecordingControls.performAction(it.action ?: "") }
    }
}