/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.settings

import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.logging.LoggingLevel
import com.andrewandderek.trailblazer.units.PreferredUnits.*
import com.andrewandderek.trailblazer.units.*
import com.andrewandderek.trailblazer.utility.IPreferencesProvider
import com.andrewandderek.trailblazer.utility.IResourceProvider
import javax.inject.Inject

interface IUserSettings:
    IDistanceUnitProvider,
    ISpeedUnitProvider,
    IAltitudeUnitProvider,
    IPaceUnitProvider,
    IZoomSettings,
    IThemeSettings,
    IMapThemeSettings,
    IRecordingStateProvider,
    IDiagnosticSettings
{
    fun resetCache()
}

class UserSettings
    @Inject constructor(
            private val loggerFactory: ILoggerFactory,
            private val resourceProvider: IResourceProvider,
            private val preferencesProvider: IPreferencesProvider
    )
    : IUserSettings {

    private var cachedUnitPreferences: UnitPreferences? = null
    private val preferredUnits: UnitPreferences
        get() {
            // we need to cache as its very expensive to read from shared preferences, but we need to reset the cache when the value has changed
            cachedUnitPreferences?.let { return it }

            val preferredUnitsFromPreferences: PreferredUnits = getEnum(getKey(R.string.settings_preferred_units_key), PreferredUnits::class.java, METRIC)
            cachedUnitPreferences = when (preferredUnitsFromPreferences) {
                METRIC -> UnitPreferences(
                        DistanceUnit.Kilometres,
                        resourceProvider.getString(R.string.distance_label_metric),
                        SpeedUnit.KilometresPerHour,
                        resourceProvider.getString(R.string.speed_label_metric),
                        AltitudeUnit.Metres,
                        resourceProvider.getString(R.string.altitude_label_metric),
                        PaceUnit.MinutesPerKilometre,
                        resourceProvider.getString(R.string.pace_label_metric)
                )
                IMPERIAL -> UnitPreferences(
                        DistanceUnit.Miles,
                        resourceProvider.getString(R.string.distance_label_imperial),
                        SpeedUnit.MilesPerHour,
                        resourceProvider.getString(R.string.speed_label_imperial),
                        AltitudeUnit.Feet,
                        resourceProvider.getString(R.string.altitude_label_imperial),
                        PaceUnit.MinutesPerMile,
                        resourceProvider.getString(R.string.pace_label_imperial)
                )
            }
            return cachedUnitPreferences!!
        }

    override fun resetCache() {
        loggerFactory.logger.debug("UserSettings: cache reset")
        cachedUnitPreferences = null
    }

    private data class UnitPreferences (
            val distanceUnit: DistanceUnit,
            val distanceUnitLabel: String,
            val speedUnit: SpeedUnit,
            val speedUnitLabel: String,
            val altitudeUnit: AltitudeUnit,
            val altitudeUnitLabel: String,
            val PaceUnit: PaceUnit,
            val paceUnitLabel: String
    )

    override val distanceUnit: DistanceUnit
        get() { return preferredUnits.distanceUnit }

    override val distanceUnitLabel: String
        get() { return preferredUnits.distanceUnitLabel }

    override val speedUnit: SpeedUnit
        get() { return preferredUnits.speedUnit }

    override val speedUnitLabel: String
        get() { return preferredUnits.speedUnitLabel }

    override val altitudeUnit: AltitudeUnit
        get() { return preferredUnits.altitudeUnit }

    override val altitudeUnitLabel: String
        get() { return preferredUnits.altitudeUnitLabel }

    override val paceUnit: PaceUnit
        get() { return preferredUnits.PaceUnit }

    override val paceUnitLabel: String
        get() { return preferredUnits.paceUnitLabel }

    override var zoomLevel: Float
        get() { return preferencesProvider.getPreferenceFloat(getKey(R.string.settings_zoom_level_key), DEFAULT_ZOOM_LEVEL)}
        set(value) { preferencesProvider.setPreferenceFloat(getKey(R.string.settings_zoom_level_key), value)}

    override val appTheme: AppTheme
        get() = getEnum(getKey(R.string.settings_theme_key), AppTheme::class.java, getDefaultTheme())

    override val mapTheme: MapTheme
        get() = getEnum(getKey(R.string.settings_map_theme_key), MapTheme::class.java, DEFAULT_MAP_THEME)

    override var recordingState: RecordingState
        get() = getRecordingStateImpl()
        set(value) { setRecordingStateImpl(value) }

    override val loggingLevel: LoggingLevel
        get() = getEnum(getKey(R.string.settings_diag_logging_key), LoggingLevel::class.java, LoggingLevel.PRODUCTION)

    private fun setRecordingStateImpl(value: RecordingState) {
        preferencesProvider.setPreferenceLong(getKey(R.string.setting_recording_state_track_id_key), value.trackId)
        preferencesProvider.setPreferenceLong(getKey(R.string.setting_recording_state_current_segment_key), value.currentSegment)
        preferencesProvider.setPreferenceBoolean(getKey(R.string.setting_recording_state_is_paused_key), value.isPaused)
        preferencesProvider.setPreferenceBoolean(getKey(R.string.setting_recording_state_is_recording_key), value.isRecording)
    }

    private fun getRecordingStateImpl(): RecordingState {
        return RecordingState(
            preferencesProvider.getPreferenceLong(getKey(R.string.setting_recording_state_track_id_key), -1L),
            preferencesProvider.getPreferenceLong(getKey(R.string.setting_recording_state_current_segment_key), -1L),
            preferencesProvider.getPreferenceBoolean(getKey(R.string.setting_recording_state_is_paused_key), false),
            preferencesProvider.getPreferenceBoolean(getKey(R.string.setting_recording_state_is_recording_key), false),
        )
    }

    private fun <T : Enum<T>> getEnum(key: String, enumClass: Class<T>, defaultValue: T): T {
        val stringValue = preferencesProvider.getPreferenceString(key, defaultValue.toString())
        loggerFactory.logger.debug("UserSettings: getting user setting ${key}, ${stringValue}")
        // We'd like to do this, but we can't
        //return T.valueOf<T>(enumClass, stringValue)
        return java.lang.Enum.valueOf(enumClass, stringValue)
    }

    private fun getKey(id: Int): String {
        return resourceProvider.getString(id)
    }

    private fun getDefaultTheme(): AppTheme {
        // The default depends on the OS version so we can't just hardcode it here
        val stringValue = resourceProvider.getString(R.string.settings_default_theme)
        return java.lang.Enum.valueOf(AppTheme::class.java, stringValue)
    }

    companion object {
        private const val DEFAULT_ZOOM_LEVEL = 15.0F
        private val DEFAULT_MAP_THEME = MapTheme.FOLLOW_APP
    }
}
