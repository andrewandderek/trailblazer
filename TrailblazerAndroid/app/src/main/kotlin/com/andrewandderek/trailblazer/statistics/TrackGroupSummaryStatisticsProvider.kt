/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.FormattedTrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.model.ITrackAndStatistics
import com.andrewandderek.trailblazer.model.TrackGroupSummaryStatistics
import com.andrewandderek.trailblazer.settings.IUserSettings
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.utility.DurationFormatter
import javax.inject.Inject

class TrackGroupSummaryStatisticsProvider
    @Inject constructor(
            private val userSettings: IUserSettings,
            private val distanceFormatter: IDistanceFormatter,
            private val speedFormatter: ISpeedFormatter,
            private val paceFormatter: IPaceFormatter
    )
    : ITrackGroupSummaryStatisticsProvider {
    override fun getSummaryStatistics(trackGroup: List<ITrackAndStatistics>): TrackGroupSummaryStatistics {
        var summary = TrackGroupSummaryStatistics()
        summary.numberOfTracksInGroup = trackGroup.size
        for (track in trackGroup) {
            summary.totalDistanceInMetres += track.statistics.distanceInMetres
            summary.totalRecordedTimeInMilliseconds += track.statistics.recordedTimeInMilliseconds
            summary.totalMovingTimeInMilliseconds += track.statistics.movingTimeInMilliseconds
        }
        summary.averageMovingSpeedInMetresPerSecond = if (summary.totalMovingTimeInMilliseconds ==0L)
                0.0
            else
                summary.totalDistanceInMetres * 1000.0 / summary.totalMovingTimeInMilliseconds.toDouble()
        return summary
    }

    override fun getFormattedSummaryStatistics(summary: TrackGroupSummaryStatistics): FormattedTrackGroupSummaryStatistics {
        var formattedSummary = FormattedTrackGroupSummaryStatistics()
        formattedSummary.distanceUnits = userSettings.distanceUnitLabel
        formattedSummary.speedUnits = userSettings.speedUnitLabel
        formattedSummary.paceUnits = userSettings.paceUnitLabel
        formattedSummary.totalDistance = distanceFormatter.formatDistance(summary.totalDistanceInMetres, false)
        formattedSummary.averageDistance = distanceFormatter.formatDistance(summary.totalDistanceInMetres / summary.numberOfTracksInGroup, false)
        formattedSummary.totalRecordedTime = DurationFormatter.formatMilliseconds(summary.totalRecordedTimeInMilliseconds)
        formattedSummary.averageRecordedTime = DurationFormatter.formatMilliseconds(summary.totalRecordedTimeInMilliseconds / summary.numberOfTracksInGroup)
        formattedSummary.totalMovingTime = DurationFormatter.formatMilliseconds(summary.totalMovingTimeInMilliseconds)
        formattedSummary.averageMovingTime = DurationFormatter.formatMilliseconds(summary.totalMovingTimeInMilliseconds / summary.numberOfTracksInGroup)
        formattedSummary.averageMovingSpeed = speedFormatter.formatSpeed(summary.averageMovingSpeedInMetresPerSecond, false)
        formattedSummary.averageMovingPace = paceFormatter.formatPace(summary.averageMovingSpeedInMetresPerSecond, false)
        return formattedSummary
    }
}