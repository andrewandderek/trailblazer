/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.service.gpslogger

import android.annotation.SuppressLint
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.exception.RecordingReloadedAfterForcedExitException
import com.andrewandderek.trailblazer.exception.RecordingReloadedAfterForcedExitFailedException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.metadata.track.ISingleTrackMetadataGenerator
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.settings.IRecordingStateProvider
import com.andrewandderek.trailblazer.settings.RecordingState
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import com.andrewandderek.trailblazer.utility.DurationFormatter
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.utility.ISystemTime
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Named


interface ITrackRecorder {
    fun getNotificationText(): String
    fun getNotificationTitle(): String
    fun getNotificationAccessibilityText(): String
    fun getNotificationWhenTimeInMilliseconds(): Long

    fun createNewTrack()
    fun addNewPosition(service: IGpsLoggerService, newPosition: Position)

    fun resume(fromNotification: Boolean)
    fun stop(fromNotification: Boolean)
    fun pause(fromNotification: Boolean)
    fun reloadState()

    val isTrackRecording: Boolean
    val isTrackRecordingPaused: Boolean
    val currentTrack: Flowable<Track>
    val currentTrackPositions: Flowable<List<Position>>
    val currentTrackObject: Track?
}

class TrackRecorder
@Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val trackRepository: ITrackRepository,
    private val positionRepository: IPositionRepository,
    private val systemTime: ISystemTime,
    private val resourceProvider: IResourceProvider,
    @Named(NameLiterals.STATISTICS_SUMMARY) private val statisticsProvider: ITrackStatisticsProvider,
    private val distanceFormatter: IDistanceFormatter,
    private val paceFormatter: IPaceFormatter,
    private val crashReporter: ICrashReporter,
    private val recordingStateProvider: IRecordingStateProvider,
    private val eventBus: IEventBus,
    private val schedulerProvider: ISchedulerProvider,
    private val singleTrackGenerator: ISingleTrackMetadataGenerator,
)
    : ITrackRecorder
{
    private lateinit var currentRecordingTrack: Track
    private val observableTrack: PublishSubject<Track> = PublishSubject.create()
    private var currentSegment: Long = 0L    // maybe this should be in the Track object ?

    init {
        resetRecordingTrack()
    }

    override fun reloadState() {
        // we need to check if we have any saved state
        loggerFactory.logger.debug("TrackRecorder: reloadState")
        try {
            val state = recordingStateProvider.recordingState
            if (state.trackId == -1L) {
                // the state was empty
                loggerFactory.logger.debug("TrackRecorder: reloadState - empty state")
                return
            }
            if (!state.isRecording && !state.isPaused) {
                // the track was stopped - we can ignore it
                loggerFactory.logger.debug("TrackRecorder: reloadState - track ${state.trackId} was stopped, will ignore")
                return
            }
            loggerFactory.logger.info("TrackRecorder: reloadState - reload track ${state.trackId}")

            // we want to throw if the load fails
            val track = trackRepository.getWithPositionsAndAccuracy(state.trackId, Position.ACCURACY_THRESHOLD, positionRepository)!!
            currentRecordingTrack = track

            // restore the state to the recorder
            currentSegment = state.currentSegment
            isTrackRecordingPaused = state.isPaused
            isTrackRecording = state.isRecording
            loggerFactory.logger.warn("TrackRecorder: reloadState - reloaded track ${currentRecordingTrack.id}, seg: ${currentSegment}, paused: ${isTrackRecordingPaused}, recording: ${isTrackRecording}")
            crashReporter.logNonFatalException(RecordingReloadedAfterForcedExitException("track ${currentRecordingTrack.id}, seg: ${currentSegment}, paused: ${isTrackRecordingPaused}, recording: ${isTrackRecording}"))
            if (isTrackRecording) {
                eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_RESUME, track.id, false))
            }
        }
        catch (error: Throwable) {
            // cannot reload track
            crashReporter.logNonFatalException(RecordingReloadedAfterForcedExitFailedException("Cannot reload track", error))
            return
        }
    }

    private fun setRecordingState() {
        recordingStateProvider.recordingState = RecordingState(
            currentRecordingTrack.id,
            currentSegment,
            isTrackRecordingPaused,
            isTrackRecording
        )
    }

    private fun resetRecordingTrack() {
        currentRecordingTrack = Track()
        currentRecordingTrack.started = systemTime.getCurrentTime()
        currentRecordingTrack.name = CalendarFormatter.convertCalendarToTrackName(currentRecordingTrack.started)
        currentSegment = 0L
    }

    override fun createNewTrack() {
        loggerFactory.logger.debug("TrackRecorder: Create New Track")
        resetRecordingTrack()
        currentRecordingTrack = trackRepository.create(currentRecordingTrack)
        loggerFactory.logger.debug("new track created in the DB ${currentRecordingTrack.id}")
        setRecordingState()
        // bear in mind the subscribers are triggered async
        // if we get a position update before the async happens we might miss a point - but its not likely
        triggerSubscribers()
    }

    override fun pause(fromNotification: Boolean) {
        isTrackRecording = false
        isTrackRecordingPaused = true
        currentSegment += 1L        // any new positions recorded into this track will be into a new segment
        setRecordingState()
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_PAUSE, currentRecordingTrack.id, fromNotification))
    }

    override fun stop(fromNotification: Boolean) {
        isTrackRecording = false
        isTrackRecordingPaused = false
        setRecordingState()
        generateTrackMetadata(currentRecordingTrack)
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_STOP, currentRecordingTrack.id, fromNotification))
    }

    override fun resume(fromNotification: Boolean) {
        isTrackRecording = true
        isTrackRecordingPaused = false
        setRecordingState()
        eventBus.publish(RecordingStatusChangedEvent(TrackRecordingControls.ACTION_RESUME, currentRecordingTrack.id, fromNotification))
    }

    override var isTrackRecording: Boolean = false
        private set

    override var isTrackRecordingPaused: Boolean = false
        private set

    override val currentTrackObject: Track?
        get() = currentRecordingTrack

    // this observer is triggered when we change the Track object reference - not any of its property values
    override val currentTrack: Flowable<Track>
        get() = observableTrack.toFlowable(BackpressureStrategy.BUFFER)

    override val currentTrackPositions: Flowable<List<Position>>
        get() = currentRecordingTrack.positions

    private fun triggerSubscribers() {
        if (observableTrack.hasObservers()) {
            observableTrack.onNext(currentRecordingTrack)
        }
    }

    override fun addNewPosition(service: IGpsLoggerService, newPosition: Position) {
        if (!isTrackRecording)
            return

        // keep the code in this method to a minimum as its what gets executed every new location update
        // ultimately on a long track its this method that will eat the battery power
        newPosition.trackId = currentRecordingTrack.id
        newPosition.segment = currentSegment
        loggerFactory.logger.debug("TrackRecorder New position: Accuracy: ${newPosition.accuracy} Lat: ${newPosition.latitude} Long: ${newPosition.longitude}, Segment ${newPosition.segment}")
        try {
            newPosition.id = positionRepository.createFast(newPosition)
        }
        catch (error: Throwable) {
            loggerFactory.logger.error("TrackRecorder error creating a position", error)
            // the most likely constraint exception is going to be because the track has been deleted causing a foreign key exception
            // but whatever the problem we should stop recording
            service.stopTracking(false)
            // we need to know about it
            crashReporter.logNonFatalException(error)
        }
        if (newPosition.id < 0) {
            // we were unable to create the position in the DB
            service.stopTracking(false)
        }
        currentRecordingTrack.addNewPosition(newPosition)       // this will trigger the subscribers to the track positions
    }

    // We don't need to store the result of the subscribe
    @SuppressLint("CheckResult")
    private fun generateTrackMetadata(track: Track) {
        Flowable.fromCallable { singleTrackGenerator.generateMetadataForTrack(track) }
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe (
                {
                    loggerFactory.logger.debug("generateTrackMetadata - complete: ${track.id}")
                    eventBus.publish(TrackMetadataUpdatedEvent(listOf(track.id)))
                },
                { error: Throwable ->
                    loggerFactory.logger.error("generateTrackMetadata - error", error)
                    crashReporter.logNonFatalException(error)
                }
            )
    }

    override fun getNotificationText(): String {
        val statistics = statisticsProvider.getStatistics(currentRecordingTrack)
        val text = String.format(
                resourceProvider.getString(R.string.service_notification_content_fmt),
                DurationFormatter.formatMilliseconds(statistics.recordedTimeInMilliseconds),
                distanceFormatter.formatDistance(statistics.distanceInMetres, true),
                paceFormatter.formatPace(statistics.averageSpeedInMetresPerSecond, true)
        )
        loggerFactory.logger.debug("getNotificationText: ${text}")
        return text
    }

    override fun getNotificationTitle(): String {
        if (isTrackRecordingPaused) {
            return resourceProvider.getString(R.string.service_notification_title_paused)
        }
        return String.format(
                resourceProvider.getString(R.string.service_notification_title_fmt),
                CalendarFormatter.convertCalendarToShortTimeString(currentRecordingTrack.started)
        )
    }

    override fun getNotificationAccessibilityText(): String {
        return String.format(
                resourceProvider.getString(R.string.service_notification_accessibility_fmt),
                CalendarFormatter.convertCalendarToShortTimeString(currentRecordingTrack.started)
        )
    }

    override fun getNotificationWhenTimeInMilliseconds(): Long {
        return systemTime.getCurrentTime().timeInMillis
    }
}
