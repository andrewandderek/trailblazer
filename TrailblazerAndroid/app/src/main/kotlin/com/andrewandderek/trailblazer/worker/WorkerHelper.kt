/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.worker

import android.os.Build
import androidx.work.ListenableWorker
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.andrewandderek.trailblazer.logging.ILoggerFactory

object WorkerHelper {

    public fun isWorkScheduled(loggerFactory: ILoggerFactory, workManager: WorkManager, name: String): Boolean {
        val statuses = workManager.getWorkInfosForUniqueWork(name)
        try {
            var running = false
            val workInfoList = statuses.get()
            loggerFactory.logger.debug("WorkerHelper: isWorkScheduled ${name}, ${workInfoList.size}")
            for (workInfo in workInfoList) {
                val state = workInfo.state
                running = (state == WorkInfo.State.RUNNING) or (state == WorkInfo.State.ENQUEUED)
            }
            return running
        } catch (e: Throwable) {
            loggerFactory.logger.error("WorkerHelper: isWorkScheduled ${name}", e)
            return false
        }
    }

    public fun getPrintableStopReasonFromListenableWorker(worker: ListenableWorker): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            // stopReason only added in Android 12
            getPrintableStopReason(worker.stopReason)
        } else {
            "NOT AVAILABLE"
        }
    }

    private fun getPrintableStopReason(reason: Int): String {
        return when (reason) {
            WorkInfo.STOP_REASON_NOT_STOPPED -> "STOP_REASON_NOT_STOPPED"
            WorkInfo.STOP_REASON_CANCELLED_BY_APP -> "STOP_REASON_CANCELLED_BY_APP"
            WorkInfo.STOP_REASON_PREEMPT -> "STOP_REASON_PREEMPT"
            WorkInfo.STOP_REASON_TIMEOUT -> "STOP_REASON_TIMEOUT"
            WorkInfo.STOP_REASON_DEVICE_STATE -> "STOP_REASON_DEVICE_STATE"
            WorkInfo.STOP_REASON_CONSTRAINT_BATTERY_NOT_LOW -> "STOP_REASON_CONSTRAINT_BATTERY_NOT_LOW"
            WorkInfo.STOP_REASON_CONSTRAINT_CHARGING -> "STOP_REASON_CONSTRAINT_CHARGING"
            WorkInfo.STOP_REASON_CONSTRAINT_CONNECTIVITY -> "STOP_REASON_CONSTRAINT_CONNECTIVITY"
            WorkInfo.STOP_REASON_CONSTRAINT_DEVICE_IDLE -> "STOP_REASON_CONSTRAINT_DEVICE_IDLE"
            WorkInfo.STOP_REASON_CONSTRAINT_STORAGE_NOT_LOW -> "STOP_REASON_CONSTRAINT_STORAGE_NOT_LOW"
            WorkInfo.STOP_REASON_QUOTA -> "STOP_REASON_QUOTA"
            WorkInfo.STOP_REASON_BACKGROUND_RESTRICTION -> "STOP_REASON_BACKGROUND_RESTRICTION"
            WorkInfo.STOP_REASON_APP_STANDBY -> "STOP_REASON_APP_STANDBY"
            WorkInfo.STOP_REASON_USER -> "STOP_REASON_USER"
            WorkInfo.STOP_REASON_SYSTEM_PROCESSING -> "STOP_REASON_SYSTEM_PROCESSING"
            WorkInfo.STOP_REASON_ESTIMATED_APP_LAUNCH_TIME_CHANGED -> "STOP_REASON_ESTIMATED_APP_LAUNCH_TIME_CHANGED"
            else -> "STOP_REASON_UNKNOWN: ${reason}"
        }
    }
}