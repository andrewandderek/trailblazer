/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.NotSupportedException
import javax.inject.Inject
import javax.inject.Named

interface IExporterFactory {
    fun getExporter(format: String): ITrackExporter
}

class ExporterFactory @Inject constructor(
    @Named(NameLiterals.FILE_FORMAT_GPX) private val gpxExporter: ITrackExporter,
    @Named(NameLiterals.FILE_FORMAT_KML) private val kmlExporter: ITrackExporter
) : IExporterFactory {
    override fun getExporter(format: String): ITrackExporter {
        return when (format) {
            NameLiterals.FILE_FORMAT_GPX -> gpxExporter
            NameLiterals.FILE_FORMAT_KML -> kmlExporter
            else -> throw NotSupportedException("Export format not supported: ${format}")
        }
    }
}