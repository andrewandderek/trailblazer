/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics

object StatisticsHelper {
    fun getStatistics(calculators: List<ITrackCalculator>, track: Track?, accuracyThreshold: Float = Float.MAX_VALUE): TrackStatistics {
        val statistics = TrackStatistics()

        if (track != null) {
            // tracks loaded from the DB could have already filtered out inaccurate positions
            // tracks in memory may still have those positions in the list
            // its OK to use filter because the Kotlin docs say that the iterable order is retailed
            val positions = track.currentPositionsSnapshot.filter { position -> position.accuracy < accuracyThreshold }

            for (index in 0..(positions.size - 1)) {
                addPosition(calculators, track, positions, index, statistics)
            }
        }

        return statistics
    }

    private fun addPosition(calculators: List<ITrackCalculator>, track: Track, positions: List<Position>, index: Int, statistics: TrackStatistics) {
        calculators.forEach { calculator -> calculator.addPosition(track.started, positions, index, statistics) }
    }
}