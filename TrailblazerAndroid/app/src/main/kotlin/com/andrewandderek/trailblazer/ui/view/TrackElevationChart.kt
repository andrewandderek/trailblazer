/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.util.AttributeSet
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.ui.chart.IChartDataMapper
import com.andrewandderek.trailblazer.utility.DurationFormatter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.formatter.ValueFormatter

class TrackElevationChart : LineChart {

    constructor(context: Context) : super(context) {
        setupChart()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupChart()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setupChart()
    }

    lateinit var mapper: IChartDataMapper

    fun setPositions(positions: List<Position>) {
        data = mapper.mapToLineData(positions)
        invalidate()
    }

    fun setTimeBasedXAxisFormatter() {
        xAxis.valueFormatter = TimeFormatter()
    }

    private fun setupChart() {
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        axisLeft.axisMinimum = 0.0F
        axisRight.isEnabled = false
        legend.isEnabled = false
        description.isEnabled = false

        setupAxisColours()
    }

    private fun setupAxisColours() {
        val axisTextColour = resources.getColor(R.color.chart_axis_text, null)
        xAxis.textColor = axisTextColour
        axisLeft.textColor = axisTextColour
    }

    class TimeFormatter : ValueFormatter() {
        override fun getFormattedValue(value: Float): String {
            return DurationFormatter.formatMillisecondsShort(value.toLong())
        }
    }
}

