/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.utility

import android.graphics.Bitmap
import android.graphics.Canvas
import android.widget.ScrollView
import androidx.annotation.ColorInt

object ViewBitmapHelper {
    fun createViewBitmap(scrollView: ScrollView, @ColorInt backgroundColour: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(
                scrollView.getChildAt(0).width,
                scrollView.getChildAt(0).height,
                Bitmap.Config.ARGB_8888)
        val c = Canvas(bitmap)
        c.drawColor(backgroundColour)
        scrollView.getChildAt(0).draw(c)
        return bitmap
    }
}