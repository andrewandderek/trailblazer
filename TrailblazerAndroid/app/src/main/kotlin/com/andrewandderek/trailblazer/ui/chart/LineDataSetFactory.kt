/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.chart

import android.content.Context
import com.andrewandderek.trailblazer.R
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import javax.inject.Inject

class LineDataSetFactory
@Inject constructor(private val context: Context)
    : ILineDataSetFactory {

    override fun createLineDataSet(dataSetIndex: Long, entries: List<Entry>): ILineDataSet {
        val dataSet = LineDataSet(entries, "")
        dataSet.color = getDataSetColour(dataSetIndex)
        dataSet.fillColor = dataSet.color
        dataSet.setDrawValues(false)
        dataSet.setDrawHighlightIndicators(false)
        dataSet.setDrawCircles(false)
        dataSet.setDrawFilled(true)

        return dataSet
    }

    // Deprecated getColor suppressed as the replacement is not available until SDK 23
    @Suppress("DEPRECATION")
    private fun getDataSetColour(dataSetIndex: Long): Int {
        var colourResourceId = if (dataSetIndex % 2 == 0L) { R.color.map_segment_even } else { R.color.map_segment_odd }
        return context.resources.getColor(colourResourceId)
    }
}