/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.kml

import android.util.Xml
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.exception.ExportException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.sharing.importexport.BaseExporter
import com.andrewandderek.trailblazer.sharing.importexport.ITrackExporter
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import com.andrewandderek.trailblazer.units.formatting.IAltitudeFormatter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.utility.*
import org.xmlpull.v1.XmlSerializer
import java.io.Writer
import javax.inject.Inject
import javax.inject.Named

class KmlExporter @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private var fileSystemHelper: IFileSystemHelper,
    private var environmentInformationProvider: IEnvironmentInformationProvider,
    private var systemTime: ISystemTime,
    @Named(NameLiterals.STATISTICS_FULL) private val statisticsProvider: ITrackStatisticsProvider,
    private val altitudeFormatter: IAltitudeFormatter,
    private val distanceFormatter: IDistanceFormatter,
    private val speedFormatter: ISpeedFormatter,
    private val paceFormatter: IPaceFormatter,
    private val resourceProvider: IResourceProvider
    ) : BaseExporter(fileSystemHelper), ITrackExporter
{
    override val fileExtension: String
        get() = KmlLiterals.KML_EXTENSION

    override fun exportTrack(track: Track, filename: String): Boolean {
        loggerFactory.logger.debug("Export track to: ${filename}")
        val writer = fileSystemHelper.createWriter(filename)
        val retval =  exportTrack(track, writer)
        writer.flush()
        writer.close()
        return retval
    }

    private fun exportTrack(track: Track, writer: Writer): Boolean {
        val xmlSerializer = Xml.newSerializer()

        xmlSerializer.setOutput(writer)
        // start DOCUMENT
        xmlSerializer.startDocument("UTF-8", false)

        xmlSerializer.startTag("", KmlLiterals.KML_TAG)
        xmlSerializer.attribute("", "xmlns", KmlLiterals.KML_DEFAULT_NAMESPACE)
        xmlSerializer.attribute("", "xmlns:${KmlLiterals.GX_NAMESPACE}", KmlLiterals.GX_NAMESPACE_URI)
        xmlSerializer.attribute("", "xmlns:${KmlLiterals.ATOM_NAMESPACE}", KmlLiterals.ATOM_NAMESPACE_URI)

        xmlSerializer.setPrefix(KmlLiterals.ATOM_NAMESPACE, KmlLiterals.ATOM_NAMESPACE_URI);
        xmlSerializer.setPrefix(KmlLiterals.GX_NAMESPACE, KmlLiterals.GX_NAMESPACE_URI);

        writeTrack(xmlSerializer, track)

        xmlSerializer.endTag("", KmlLiterals.KML_TAG)

        // end DOCUMENT
        xmlSerializer.endDocument()

        loggerFactory.logger.debug("Track exported")
        return true
    }

    private fun writeTrack(xmlSerializer: XmlSerializer, track: Track) {
        xmlSerializer.startTag("", KmlLiterals.DOCUMENT_TAG)

        writeMetaData(xmlSerializer, track)
        writeStyles(xmlSerializer)
        writeStyleMap(xmlSerializer, KmlLiterals.TRACK_STYLE)

        if (!track.isEmpty) {
            writePlacemark(xmlSerializer,
                    KmlLiterals.START_STYLE,
                    "${track.name} (start)",
                    track.notes,
                    track.currentPositionsSnapshot[0]
            )
            if (track.hasMissingTimestamps) {
                // we need to use LineString as we do not have timestamp data
                writeAllPointsAsLineString(xmlSerializer, track)
            } else {
                // we can use GX:Track as we have timestamp info
                writeAllPointsAsGxTrack(xmlSerializer, track)
            }
            if (track.currentPositionsSnapshot.size > 1) {
                writePlacemark(xmlSerializer,
                        KmlLiterals.END_STYLE,
                        "${track.name} (end)",
                        getTrackDescription(track),
                        track.currentPositionsSnapshot[track.currentPositionsSnapshot.size - 1]
                )
            }
        }

        xmlSerializer.endTag("", KmlLiterals.DOCUMENT_TAG)
    }

    private fun getTrackDescription(track: Track): String {
        val statistics = statisticsProvider.getStatistics(track)
        val format = resourceProvider.getString(R.string.share_track_information).trimIndent()
        return String.format(
                format,
                environmentInformationProvider.getApplicationVersion(),
                track.name,
                distanceFormatter.formatDistance(statistics.distanceInMetres, true),
                DurationFormatter.formatMilliseconds(statistics.elapsedTimeInMilliseconds),
                DurationFormatter.formatMilliseconds(statistics.movingTimeInMilliseconds),
                DurationFormatter.formatMilliseconds(statistics.recordedTimeInMilliseconds),
                speedFormatter.formatSpeed(statistics.averageSpeedInMetresPerSecond, true),
                speedFormatter.formatSpeed(statistics.averageMovingSpeedInMetresPerSecond, true),
                paceFormatter.formatPace(statistics.averageSpeedInMetresPerSecond, true),
                paceFormatter.formatPace(statistics.averageMovingSpeedInMetresPerSecond, true),
                altitudeFormatter.formatAltitude(statistics.minElevationInMetres, true),
                altitudeFormatter.formatAltitude(statistics.maxElevationInMetres, true),
                altitudeFormatter.formatAltitude(statistics.elevationGainInMetres, true),
                altitudeFormatter.formatAltitude(statistics.elevationLossInMetres, true),
                altitudeFormatter.formatAltitude(statistics.elevationDifferenceInMetres, true),
                CalendarFormatter.convertCalendarToDateTimeString(track.started)
        )
    }

    private fun writeMetaData(xmlSerializer: XmlSerializer, track: Track) {
        xmlSerializer.startTag("", KmlLiterals.OPEN_TAG)
        xmlSerializer.text("1")
        xmlSerializer.endTag("", KmlLiterals.OPEN_TAG)
        xmlSerializer.startTag("", KmlLiterals.VISIBILITY_TAG)
        xmlSerializer.text("1")
        xmlSerializer.endTag("", KmlLiterals.VISIBILITY_TAG)
        xmlSerializer.startTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.cdsect(track.name)
        xmlSerializer.endTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.startTag("", KmlLiterals.SNIPPET_TAG)
        xmlSerializer.text("Created ${CalendarFormatter.convertCalendarToDateTimeString(systemTime.getCurrentTime())}")
        xmlSerializer.endTag("", KmlLiterals.SNIPPET_TAG)
        xmlSerializer.startTag(KmlLiterals.ATOM_NAMESPACE_URI, KmlLiterals.AUTHOR_TAG)
        xmlSerializer.cdsect("Trailblazer v${environmentInformationProvider.getApplicationVersion()}")
        xmlSerializer.endTag(KmlLiterals.ATOM_NAMESPACE_URI, KmlLiterals.AUTHOR_TAG)
    }

    private fun writeStyles(xmlSerializer: XmlSerializer) {
        writeStyle(xmlSerializer,
                "${KmlLiterals.TRACK_STYLE}_${KmlLiterals.NORMAL_TRACK_STYLE_SUFFIX}",
                "${KmlLiterals.NORMAL_TRACK_STYLE_WIDTH_VALUE}",
                "${KmlLiterals.NORNAL_TRACK_STYLE_COLOUR_VALUE}",
                "${KmlLiterals.NORMAL_TRACK_STYLE_ICONREF_VALUE}",
                "${KmlLiterals.NORMAL_TRACK_STYLE_SCALE_VALUE}",
                null,
                null)
        writeStyle(xmlSerializer,
                "${KmlLiterals.TRACK_STYLE}_${KmlLiterals.HIGHLIGHT_TRACK_STYLE_SUFFIX}",
                "${KmlLiterals.HIGHLIGHT_TRACK_STYLE_WIDTH_VALUE}",
                "${KmlLiterals.HIGHLIGHT_TRACK_STYLE_COLOUR_VALUE}",
                "${KmlLiterals.HIGHLIGHT_TRACK_STYLE_ICONREF_VALUE}",
                "${KmlLiterals.HIGHLIGHT_TRACK_STYLE_SCALE_VALUE}",
                null,
                null)
        writeStyle(xmlSerializer,
                KmlLiterals.START_STYLE,
                null,
                null,
                "${KmlLiterals.START_TRACK_STYLE_ICONREF_VALUE}",
                "${KmlLiterals.START_TRACK_STYLE_SCALE_VALUE}",
                "${KmlLiterals.START_TRACK_STYLE_HOTSPOT_X_VALUE}",
                "${KmlLiterals.START_TRACK_STYLE_HOTSPOT_Y_VALUE}")
        writeStyle(xmlSerializer,
                KmlLiterals.END_STYLE,
                null,
                null,
                "${KmlLiterals.END_TRACK_STYLE_ICONREF_VALUE}",
                "${KmlLiterals.END_TRACK_STYLE_SCALE_VALUE}",
                "${KmlLiterals.END_TRACK_STYLE_HOTSPOT_X_VALUE}",
                "${KmlLiterals.END_TRACK_STYLE_HOTSPOT_Y_VALUE}")
    }

    private fun writeStyleMap(xmlSerializer: XmlSerializer, id: String) {
        xmlSerializer.startTag("", KmlLiterals.STYLEMAP_TAG)
        xmlSerializer.attribute("", KmlLiterals.ID_TAG, id)

        xmlSerializer.startTag("", KmlLiterals.PAIR_TAG)
        xmlSerializer.startTag("", KmlLiterals.KEY_TAG)
        xmlSerializer.text("normal")
        xmlSerializer.endTag("", KmlLiterals.KEY_TAG)
        xmlSerializer.startTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.text("#${id}_${KmlLiterals.NORMAL_TRACK_STYLE_SUFFIX}")
        xmlSerializer.endTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.endTag("", KmlLiterals.PAIR_TAG)

        xmlSerializer.startTag("", KmlLiterals.PAIR_TAG)
        xmlSerializer.startTag("", KmlLiterals.KEY_TAG)
        xmlSerializer.text("highlight")
        xmlSerializer.endTag("", KmlLiterals.KEY_TAG)
        xmlSerializer.startTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.text("#${id}_${KmlLiterals.HIGHLIGHT_TRACK_STYLE_SUFFIX}")
        xmlSerializer.endTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.endTag("", KmlLiterals.PAIR_TAG)

        xmlSerializer.endTag("", KmlLiterals.STYLEMAP_TAG)
    }

    private fun writeStyle(xmlSerializer: XmlSerializer,
                           id: String,
                           width: String?,
                           color: String?,
                           iconHref: String?,
                           scale: String?,
                           hotspotx: String?,
                           hotspoty: String?
    ) {
        xmlSerializer.startTag("", KmlLiterals.STYLE_TAG)
        xmlSerializer.attribute("", KmlLiterals.ID_TAG, id)
        if (color != null && width != null) {
            xmlSerializer.startTag("", KmlLiterals.LINESTYLE_TAG)
            xmlSerializer.startTag("", KmlLiterals.COLOR_TAG)
            xmlSerializer.text(color)
            xmlSerializer.endTag("", KmlLiterals.COLOR_TAG)
            xmlSerializer.startTag("", KmlLiterals.WIDTH_TAG)
            xmlSerializer.text(width)
            xmlSerializer.endTag("", KmlLiterals.WIDTH_TAG)
            xmlSerializer.endTag("", KmlLiterals.LINESTYLE_TAG)
        }
        if (scale != null && iconHref != null) {
            xmlSerializer.startTag("", KmlLiterals.ICONSTYLE_TAG)
            xmlSerializer.startTag("", KmlLiterals.SCALE_TAG)
            xmlSerializer.text(scale)
            xmlSerializer.endTag("", KmlLiterals.SCALE_TAG)
            xmlSerializer.startTag("", KmlLiterals.ICON_TAG)
            xmlSerializer.startTag("", KmlLiterals.HREF_TAG)
            xmlSerializer.text(iconHref)
            xmlSerializer.endTag("", KmlLiterals.HREF_TAG)
            xmlSerializer.endTag("", KmlLiterals.ICON_TAG)
            xmlSerializer.endTag("", KmlLiterals.ICONSTYLE_TAG)

            if (hotspotx != null && hotspoty != null) {
                xmlSerializer.startTag("", KmlLiterals.HOTSPOT_TAG)
                xmlSerializer.attribute("", KmlLiterals.X_TAG, hotspotx)
                xmlSerializer.attribute("", KmlLiterals.Y_TAG, hotspoty)
                xmlSerializer.attribute("", KmlLiterals.XUNITS_TAG, "pixels")
                xmlSerializer.attribute("", KmlLiterals.YUNITS_TAG, "pixels")
                xmlSerializer.endTag("", KmlLiterals.HOTSPOT_TAG)
            }
        }
        xmlSerializer.endTag("", KmlLiterals.STYLE_TAG)
    }

    private fun writeAllPointsAsLineString(xmlSerializer: XmlSerializer, track: Track) {
        var segment:Int = 1

        writeLineStringSegmentStart(xmlSerializer, track, segment)

        var lastPoint: Position? = null
        var coordinates: StringBuilder = StringBuilder(1000)
        for (point in track.currentPositionsSnapshot) {
            if (lastPoint != null && lastPoint.isFromDifferentSegment(point)) {
                xmlSerializer.text(coordinates.toString())
                writeLineStringSegmentEnd(xmlSerializer)
                segment = segment + 1
                writeLineStringSegmentStart(xmlSerializer, track, segment)
            }
            coordinates.append(getCoordinatesPoint(point))
            lastPoint = point
        }

        xmlSerializer.text(coordinates.toString())
        writeLineStringSegmentEnd(xmlSerializer)
    }

    private fun getCoordinatesPoint(point: Position): String {
        val altitude = if (point.altitude != null) ",${point.altitude}" else ""
        // the trailing space is important for parsing - dont delete it
        return "${point.longitude},${point.latitude}${altitude} "
    }

    private fun writeLineStringSegmentStart(xmlSerializer: XmlSerializer, track: Track, segment: Int) {
        writeTrackSegmentPlacemark(xmlSerializer, track, segment)
        xmlSerializer.startTag("", KmlLiterals.LINESTRING_TAG)
        writeTessellateMode(xmlSerializer, "1")
        xmlSerializer.startTag("", KmlLiterals.COORDINATE_TAG)
    }

    private fun writeLineStringSegmentEnd(xmlSerializer: XmlSerializer) {
        xmlSerializer.endTag("", KmlLiterals.COORDINATE_TAG)
        xmlSerializer.endTag("", KmlLiterals.LINESTRING_TAG)
        xmlSerializer.endTag("", KmlLiterals.PLACEMARK_TAG)
    }

    private fun writeTrackSegmentPlacemark(xmlSerializer: XmlSerializer, track: Track, segment: Int) {
        xmlSerializer.startTag("", KmlLiterals.PLACEMARK_TAG)
        xmlSerializer.startTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.cdsect("${track.name} (Part ${segment})")
        xmlSerializer.endTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.startTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.text("#${KmlLiterals.TRACK_STYLE}")
        xmlSerializer.endTag("", KmlLiterals.STYLEURL_TAG)
    }

    private fun writeTessellateMode(xmlSerializer: XmlSerializer, mode: String) {
        xmlSerializer.startTag("", KmlLiterals.TESSELLATE_TAG)
        xmlSerializer.text(mode)
        xmlSerializer.endTag("", KmlLiterals.TESSELLATE_TAG)
    }

    private fun writeAllPointsAsGxTrack(xmlSerializer: XmlSerializer, track: Track) {
        xmlSerializer.startTag("", KmlLiterals.PLACEMARK_TAG)
        xmlSerializer.attribute("", KmlLiterals.ID_TAG, "tour")
        xmlSerializer.startTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.cdsect(track.name)
        xmlSerializer.endTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.startTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.text("#${KmlLiterals.TRACK_STYLE}")
        xmlSerializer.endTag("", KmlLiterals.STYLEURL_TAG)

        xmlSerializer.startTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.MULTITRACK_TAG)

        writeAltitudeMode(xmlSerializer, KmlLiterals.ABSOLUTE_ALTITUDE_MODE)
        writeInterpolateMode(xmlSerializer, "1")

        xmlSerializer.startTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)
        writeAltitudeMode(xmlSerializer, KmlLiterals.ABSOLUTE_ALTITUDE_MODE)
        var lastPoint: Position? = null
        for (point in track.currentPositionsSnapshot) {
            if (lastPoint != null && lastPoint.isFromDifferentSegment(point)) {
                xmlSerializer.endTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)
                xmlSerializer.startTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)
                writeAltitudeMode(xmlSerializer, KmlLiterals.ABSOLUTE_ALTITUDE_MODE)
            }
            writeCoordPoint(xmlSerializer, point)
            lastPoint = point
        }
        xmlSerializer.endTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.TRACK_TAG)

        xmlSerializer.endTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.MULTITRACK_TAG)

        xmlSerializer.endTag("", KmlLiterals.PLACEMARK_TAG)
    }

    private fun writeAltitudeMode(xmlSerializer: XmlSerializer, mode: String) {
        xmlSerializer.startTag("", KmlLiterals.ALTITUDEMODE_TAG)
        xmlSerializer.text(mode)
        xmlSerializer.endTag("", KmlLiterals.ALTITUDEMODE_TAG)
    }

    private fun writeInterpolateMode(xmlSerializer: XmlSerializer, mode: String) {
        xmlSerializer.startTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.INTERPOLATE_TAG)
        xmlSerializer.text(mode)
        xmlSerializer.endTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.INTERPOLATE_TAG)
    }

    private fun writeCoordPoint(xmlSerializer: XmlSerializer, point: Position) {
        // the number of when and coord elements must be equal
        xmlSerializer.startTag("", KmlLiterals.WHEN_TAG)
        point.timeRecorded?.let {
            xmlSerializer.text(CalendarFormatter.convertCalendarToXmlSchemaDateTimeString(it))
        } ?: run {
            // its illegal to have an empty WHEN element in a GX:TRACK
            // see https://developers.google.com/kml/documentation/kmlreference#gxtrack
            throw ExportException("Cannot export track to KML GX:TRACK if there are no timestamps", null)
        }
        xmlSerializer.endTag("", KmlLiterals.WHEN_TAG)

        xmlSerializer.startTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.COORD_TAG)
        val altitude = if (point.altitude != null) " ${point.altitude}" else ""
        xmlSerializer.text("${point.longitude} ${point.latitude}${altitude}")
        xmlSerializer.endTag(KmlLiterals.GX_NAMESPACE_URI, KmlLiterals.COORD_TAG)
    }

    private fun writePlacemark(xmlSerializer: XmlSerializer, style: String, name: String, description: String, point: Position) {
        xmlSerializer.startTag("", KmlLiterals.PLACEMARK_TAG)

        xmlSerializer.startTag("", KmlLiterals.NAME_TAG)
        xmlSerializer.cdsect(name)
        xmlSerializer.endTag("", KmlLiterals.NAME_TAG)

        xmlSerializer.startTag("", KmlLiterals.DESCRIPTION_TAG)
        xmlSerializer.cdsect(description)
        xmlSerializer.endTag("", KmlLiterals.DESCRIPTION_TAG)

        // the timestamp is optional
        // see https://schemas.opengis.net/kml/2.2.0/ogckml22.xsd
        point.timeRecorded?.let {
            xmlSerializer.startTag("", KmlLiterals.TIMESTAMP_TAG)
            xmlSerializer.startTag("", KmlLiterals.WHEN_TAG)
            xmlSerializer.text(CalendarFormatter.convertCalendarToXmlSchemaDateTimeString(it))
            xmlSerializer.endTag("", KmlLiterals.WHEN_TAG)
            xmlSerializer.endTag("", KmlLiterals.TIMESTAMP_TAG)
        }

        xmlSerializer.startTag("", KmlLiterals.STYLEURL_TAG)
        xmlSerializer.text("#${style}")
        xmlSerializer.endTag("", KmlLiterals.STYLEURL_TAG)

        xmlSerializer.startTag("", KmlLiterals.POINT_TAG)
        xmlSerializer.startTag("", KmlLiterals.COORDINATE_TAG)
        val altitude = if (point.altitude != null) ",${point.altitude}" else ""
        xmlSerializer.text("${point.longitude},${point.latitude}${altitude}")
        xmlSerializer.endTag("", KmlLiterals.COORDINATE_TAG)
        xmlSerializer.endTag("", KmlLiterals.POINT_TAG)

        xmlSerializer.endTag("", KmlLiterals.PLACEMARK_TAG)
    }
}