/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.worker

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.andrewandderek.trailblazer.di.ChildWorkerFactory
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import javax.inject.Inject
import javax.inject.Provider

class DaggerWorkerFactory @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val workers: Map<Class<out Worker>, @JvmSuppressWildcards Provider<ChildWorkerFactory>>
) : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        try {
            // this mechanism is pretty much the same as the ViewModel factory in the ApplicationModule
            loggerFactory.logger.debug("DaggerWorkerFactory: createWorker - ${workerClassName}")
            val workerClass = Class.forName(workerClassName).asSubclass(Worker::class.java)
            var provider = workers[workerClass]
            if (provider == null) {
                // have you added the worker *factory* by @Binds @IntoMap in the WorkerModule?
                loggerFactory.logger.error("DaggerWorkerFactory: Missing binding for ${workerClassName}")
                throw IllegalArgumentException("DaggerWorkerFactory: Missing binding for $workerClassName")
            }
            return provider.get().create(appContext, workerParameters)
        } catch (e: Exception) {
            loggerFactory.logger.error("DaggerWorkerFactory", e)
            throw RuntimeException(e)
        }
    }
}
