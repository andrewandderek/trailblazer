/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.compare

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SelectableStringListBottomSheetFragment : BottomSheetDialogFragment() {

    companion object {
        private const val TITLE = "title_key"
        private const val OPTIONS = "options_key"

        fun newInstance(title: String, options: List<SelectableString>): SelectableStringListBottomSheetFragment {
            val fragment = SelectableStringListBottomSheetFragment()
            val args = Bundle()
            args.putString(TITLE, title)
            addOptionsToArgs(args, options)
            fragment.arguments = args
            return fragment
        }

        private fun addOptionsToArgs(args: Bundle, options: List<SelectableString>) {
            args.putParcelableArrayList(OPTIONS, ArrayList(options))
        }

        private fun getOptionsFromArgs(args: Bundle) : List<SelectableString> {
            // We need to set the class loader:
            // https://developer.android.com/reference/android/os/Bundle#getParcelableArrayList(java.lang.String,%20java.lang.Class%3C?%20extends%20T%3E)
            val originalClassLoader = args.classLoader
            args.classLoader = SelectableString::class.java.classLoader

            var options: List<SelectableString>?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                options = args.getParcelableArrayList<SelectableString>(OPTIONS, SelectableString::class.java)
            } else {
                // excellent work google - way to break an API - there really should be a compat version - but there isnt
                @Suppress("DEPRECATION")
                options = args.getParcelableArrayList<SelectableString>(OPTIONS)
            }
            args.classLoader = originalClassLoader
            return options!!
        }
    }

    interface Listener {
        fun itemSelected(tag: String, position: Int)
    }

    @BindView(R.id.bottomsheet_slectable_string_list_title)
    protected lateinit var titleView: TextView

    @BindView(R.id.bottomsheet_slectable_string_list)
    protected lateinit var listView: ListView

    private lateinit var options: List<SelectableString>
    private lateinit var listener: Listener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_bottomsheet_selectable_string_list, container, false)
        CheeseKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            listener = activity as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement Listener")
        }

        val args = requireArguments()

        titleView.text = args.getString(TITLE)
        options = getOptionsFromArgs(args)

        val adapter = SelectableStringArrayAdapter(requireContext(), options)
        listView.adapter = adapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            listener.itemSelected(tag!!, position)
            dismiss()
        }

    }

    override fun onStart() {
        super.onStart()
        //this forces the sheet to appear at max height even on landscape
        val behavior = BottomSheetBehavior.from(requireView().parent as View)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }
}