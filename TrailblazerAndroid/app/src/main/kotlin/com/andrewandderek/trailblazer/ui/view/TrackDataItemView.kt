/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.content.Context
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView

class TrackDataItemView : LinearLayout {

    companion object {
        val UNIT_SIZE_RATIO = 0.5
    }

    @BindView(R.id.data_item_label)
    lateinit var labelView: TextView

    @BindView(R.id.data_item_value)
    lateinit var valueView: TextView

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    var label: String? = null
        set(value) {
            field = value
            labelView.text = value
        }

    fun setValue(value: String, unit: String) {
        // I think this is still OK in RTL languages, ie. <value><unit>
        val valueLength = value.length
        val unitLength = unit.length
        val totalLength = valueLength + unitLength

        val valueSpan = SpannableString(value + unit)
        valueSpan.setSpan(RelativeSizeSpan(UNIT_SIZE_RATIO.toFloat()), valueLength, totalLength, 0)

        valueView.text = valueSpan
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        val view = inflateView(context)
        CheeseKnife.bind(this, view)

        loadAttributes(attrs, defStyle)
    }

    private fun loadAttributes(attrs: AttributeSet?, defStyle: Int) {
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.TrackDataItemView, defStyle, 0)

        label = a.getString(
                R.styleable.TrackDataItemView_label)

        a.recycle()
    }

    private fun inflateView(context: Context): View {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.track_data_item_view, this, true)
    }
}

