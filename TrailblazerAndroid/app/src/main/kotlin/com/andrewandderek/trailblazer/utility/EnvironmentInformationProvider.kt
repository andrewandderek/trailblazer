/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.content.pm.PackageManager
import android.content.Context
import android.os.Build
import javax.inject.Inject

interface IEnvironmentInformationProvider {
    fun getApplicationVersion(): String
}

class EnvironmentInformationProvider @Inject constructor(
        private var applicationContext: Context
) : IEnvironmentInformationProvider
{
    override fun getApplicationVersion(): String {
        applicationContext.packageManager?.let {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    val packageInfo = it.getPackageInfo(applicationContext.packageName, PackageManager.PackageInfoFlags.of(0))
                    return packageInfo.versionName
                } else {
                    // excellent work google - way to break an API - there really should be a compat version - but there isnt
                    @Suppress("DEPRECATION")
                    val packageInfo = it.getPackageInfo(applicationContext.packageName, 0)
                    return packageInfo.versionName
                }
            } catch (e: PackageManager.NameNotFoundException) {
                return "UNKNOWN"
            }
        }
        return "UNKNOWN"
    }
}