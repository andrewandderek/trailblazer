/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.service.gpslogger

import android.annotation.SuppressLint
import android.app.*
import android.content.Intent
import android.content.pm.ServiceInfo
import android.location.Location
import android.os.*
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.ServiceCompat
import androidx.core.content.ContextCompat
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.exception.RecordingFailedException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControls
import com.andrewandderek.trailblazer.service.receiver.TrackRecordingControlsReceiver
import com.andrewandderek.trailblazer.ui.trackdisplay.TrackDisplayActivity
import com.andrewandderek.trailblazer.utility.ISystemTime
import com.google.android.gms.location.*
import dagger.android.AndroidInjection
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject


interface IGpsLoggerService {
    val isTracking: Boolean
    val isPaused: Boolean
    val currentTrack: Flowable<Track>
    val currentTrackPositions: Flowable<List<Position>>
    val currentPosition: Flowable<Position>
    val currentTrackObject: Track?
    fun startTracking()
    fun stopTracking(fromNotification: Boolean)
    fun pauseTracking(fromNotification: Boolean)
    fun resumeTracking(fromNotification: Boolean)
    fun restartCurrentPositionUpdates()
}

class GpsLoggerService :
        Service(),
        IGpsLoggerService
{
    @Inject lateinit var trackRecorder: ITrackRecorder
    @Inject lateinit var loggerFactory: ILoggerFactory
    @Inject lateinit var systemTime: ISystemTime
    @Inject lateinit var notificationManager: NotificationManager
    @Inject lateinit var trackingServiceController: IGpsLoggerServiceController

    //runs without a timer by reposting this handler at the end of the runnable
    private val timerHandler: Handler = Handler(Looper.myLooper()!!)
    private var timerRunnable: Runnable? = null

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var looper: Looper

    private val binder = LocalBinder()
    private val observableCurrentPosition: PublishSubject<Position> = PublishSubject.create()

    inner class LocalBinder : Binder() {
        val service: IGpsLoggerService
            get() = this@GpsLoggerService
    }

    override fun onCreate() {
        // we end up here when the first UI element binds to the service
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("GpsLoggerService: onCreate")
        initLocationClient()
        // we dont need to wait for the client to attach
        startLocationUpdates(false)

        if (isPaused || isTracking) {
            // we already are recording so there is no need to reload state
            loggerFactory.logger.debug("GpsLoggerService: onCreate - already recording")
            return
        }

        // reload any saved state
        trackRecorder.reloadState()

        // lets check if there was any reloaded state
        // we need to restart the notification message if there is
        when {
            isTracking -> {
                loggerFactory.logger.debug("GpsLoggerService: onCreate - restart tracking")
                // this will start the service if its not already started - and makes sure it does not go away
                trackingServiceController.startService()
                startLocationUpdates(false)
                startForeground()
                updateNotification()
                startTimedNotificationUpdates()
            }
            isPaused -> {
                loggerFactory.logger.debug("GpsLoggerService: onCreate - paused tracking")
                // this will start the service if its not already started - and makes sure it does not go away
                trackingServiceController.startService()
                startForeground()
                stopLocationUpdates()
            }
        }
    }

    private fun initLocationClient() {
        loggerFactory.logger.debug("GpsLoggerService: init the fused location client")
        looper = Looper.myLooper()!!
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                locationResult.lastLocation?.let {
                    onLocationChanged(it)
                }
            }
        }
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("GpsLoggerService: onDestroy")

        stopTimedNotificationUpdates()
    }

    override fun onBind(intent: Intent): IBinder {
        loggerFactory.logger.debug("GpsLoggerService: onBind")
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        loggerFactory.logger.debug("GpsLoggerService: onStartCommand")
        return START_STICKY
    }

    override val isTracking: Boolean
        get() = trackRecorder.isTrackRecording

    override val isPaused: Boolean
        get() = trackRecorder.isTrackRecordingPaused

    override val currentTrackObject: Track?
        get() = trackRecorder.currentTrackObject

    override val currentTrack: Flowable<Track>
        get() = trackRecorder.currentTrack

    override val currentTrackPositions: Flowable<List<Position>>
        get() = trackRecorder.currentTrackPositions

    override val currentPosition: Flowable<Position>
        get() = observableCurrentPosition.toFlowable(BackpressureStrategy.LATEST)

    override fun startTracking() {
        loggerFactory.logger.debug("GpsLoggerService: startTracking")
        trackRecorder.createNewTrack()
        // tracks cannot be started from the notification
        resumeTracking(false)
    }

    override fun resumeTracking(fromNotification: Boolean) {
        loggerFactory.logger.debug("GpsLoggerService: resumeTracking fromNotification=${fromNotification}")
        startLocationUpdates(true)
        startForeground()
        trackRecorder.resume(fromNotification)
        updateNotification()
        startTimedNotificationUpdates()
    }

    override fun stopTracking(fromNotification: Boolean) {
        loggerFactory.logger.debug("GpsLoggerService: stopTracking fromNotification=${fromNotification}")
        trackRecorder.stop(fromNotification)
        stopLocationUpdates()
        stopTimedNotificationUpdates()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(STOP_FOREGROUND_REMOVE)
        } else {
            @Suppress("DEPRECATION")
            stopForeground(true)
        }
    }

    override fun pauseTracking(fromNotification: Boolean) {
        loggerFactory.logger.debug("GpsLoggerService: pauseTracking fromNotification=${fromNotification}")
        trackRecorder.pause(fromNotification)
        stopLocationUpdates()
        stopTimedNotificationUpdates()
        updateNotification()
    }

    override fun restartCurrentPositionUpdates() {
        startLocationUpdates(true)
    }

    private fun updateNotification() {
        loggerFactory.logger.debug("GpsLoggerService: updateNotification - start")
        notificationManager.notify(FOREGROUND_NOTIFICATION_ID, buildForegroundNotification())
        loggerFactory.logger.debug("GpsLoggerService: updateNotification - end")
    }

    private fun startTimedNotificationUpdates() {
        stopTimedNotificationUpdates()
        timerRunnable = Runnable {
            updateNotification()
            timerRunnable?.let { timerHandler.postDelayed(it, NOTIFICATION_UPDATE_INTERVAL_MILLISECONDS) }
        }
        timerRunnable?.let { timerHandler.postDelayed(it, NOTIFICATION_UPDATE_INTERVAL_MILLISECONDS) }
    }

    private fun stopTimedNotificationUpdates() {
        timerRunnable?.let { timerHandler.removeCallbacks(it) }
    }

    fun onLocationChanged(location: Location) {
        val position = Position(location, systemTime.getCurrentTime())

        if (observableCurrentPosition.hasObservers())
            observableCurrentPosition.onNext(position)

        trackRecorder.addNewPosition(this, position)
    }

    private fun startLocationUpdates(throwIfNoPermission: Boolean) {
        locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, LOCATION_UPDATE_INTERVAL.toLong()).apply {
            setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
        }.build()
        try {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, looper)
        } catch (exception: SecurityException) {
            loggerFactory.logger.error("Security exception requesting location updates - probably lack of permissions")
            if (throwIfNoPermission)
                throw RecordingFailedException("Cannot start recording track", exception)
        }
    }

    private fun stopLocationUpdates() {
        // the docs say its good practice to call this when you no longer need updates
        // https://developers.google.com/android/reference/com/google/android/gms/location/FusedLocationProviderClient#requestLocationUpdates(com.google.android.gms.location.LocationRequest,%20com.google.android.gms.location.LocationCallback,%20android.os.Looper)
        // so lets be good citizens
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    // ServiceCompat.startForeground handles the requisite version checks so we don't have to
    @SuppressLint("InlinedApi")
    private fun startForeground() {
        loggerFactory.logger.debug("GpsLoggerService: startForeground - start")
        ServiceCompat.startForeground(this, FOREGROUND_NOTIFICATION_ID, buildForegroundNotification(), ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION)
        loggerFactory.logger.debug("GpsLoggerService: startForeground - end")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "Trailblazer Notifications", NotificationManager.IMPORTANCE_LOW)

            // Configure the notification channel.
            notificationChannel.description = "Track recording updates"
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun buildForegroundNotification(): Notification {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        builder.setOngoing(true)
                .setContentTitle(trackRecorder.getNotificationTitle())
                .setContentText(trackRecorder.getNotificationText())
                .setTicker(trackRecorder.getNotificationAccessibilityText())
                .setWhen(trackRecorder.getNotificationWhenTimeInMilliseconds())
                .setShowWhen(true)
                .setSmallIcon(android.R.drawable.ic_menu_compass)
                .setColor(ContextCompat.getColor(this, R.color.indigo))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)        // so it will show on the lock screen
                .setContentIntent(getNotificationIntent())

        builder.addAction(
                NotificationCompat.Action(R.drawable.ic_stop,
                        getString(R.string.action_stop),
                        getButtonIntent(TrackRecordingControls.ACTION_STOP)
                )
        )
        if (trackRecorder.isTrackRecordingPaused) {
            builder.addAction(
                    NotificationCompat.Action(R.drawable.ic_add,
                            getString(R.string.action_resume),
                            getButtonIntent(TrackRecordingControls.ACTION_RESUME)
                    )
            )
        } else {
            builder.addAction(
                    NotificationCompat.Action(R.drawable.ic_pause,
                            getString(R.string.action_pause),
                            getButtonIntent(TrackRecordingControls.ACTION_PAUSE)
                    )
            )
        }

        return builder.build()
    }

    private fun getButtonIntent(action: String): PendingIntent {
        val intent = Intent(this, TrackRecordingControlsReceiver::class.java)
        intent.action = action
        return PendingIntent.getBroadcast(
            this,
            FOREGROUND_NOTIFICATION_ID,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE)
    }

    private fun getNotificationIntent(): PendingIntent {
        val intent = Intent(this, TrackDisplayActivity::class.java)
                .setAction(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_LAUNCHER)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
    }

    companion object {
        // we cannot modify a channel once its created - so we need to increment this ID or reinstall
        private const val NOTIFICATION_CHANNEL_ID = "trailblazer_channel_id_03"
        private const val FOREGROUND_NOTIFICATION_ID = 111
        // TODO - get from settings
        // TODO - suggested value is 5000 see - https://developer.android.com/training/location/change-location-settings
        private const val LOCATION_UPDATE_INTERVAL = 1000
        private const val NOTIFICATION_UPDATE_INTERVAL_MILLISECONDS = 3 * 60 * 1000L
    }
}
