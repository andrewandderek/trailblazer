/*
 *  Copyright 2017-2023 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.settings

import androidx.appcompat.app.AppCompatDelegate

class ThemeSetter : IThemeSetter {
    override fun setTheme(theme: AppTheme) {
        when(theme) {
            AppTheme.LIGHT -> setThemeMode(AppCompatDelegate.MODE_NIGHT_NO)
            AppTheme.DARK -> setThemeMode(AppCompatDelegate.MODE_NIGHT_YES)
            AppTheme.SYSTEM -> setThemeMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
    }

    private fun setThemeMode(themeMode: Int) {
        AppCompatDelegate.setDefaultNightMode(themeMode)
    }
}