/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.worker.trackmetadata

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.andrewandderek.trailblazer.di.ChildWorkerFactory
import com.andrewandderek.trailblazer.exception.WorkerException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.metadata.track.IBatchTrackMetadataGenerator
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.ISystemTime
import com.andrewandderek.trailblazer.worker.WorkerHelper
import javax.inject.Inject
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class TrackMetadataWorker @Inject constructor(
    appContext: Context,
    params: WorkerParameters,
    private val loggerFactory: ILoggerFactory,
    private val crashReporter: ICrashReporter,
    private val systemTime: ISystemTime,
    private val generator: IBatchTrackMetadataGenerator
) : Worker(appContext, params) {

    companion object {
        private const val BATCH_SIZE = 25
        private const val MAX_DURATION_MINUTES = 5
    }

    private var timer: Long = 0
    private var doWorkDuration: Duration = Duration.INFINITE        // the duration of the completed call to doWork, if its in progress this will not be set
    private var totalNumberOfItemsProcessed: Int = 0

    private fun getElapsedTime(): Duration {
        val durationNanoSeconds = systemTime.getNanoTime() - timer
        return durationNanoSeconds.toDuration(DurationUnit.NANOSECONDS)
    }

    private fun okToContinue(processedItems: Int): Boolean {
        if (processedItems < BATCH_SIZE) {
            // there were fewer items to process than the batch size - we are done
            return false
        }
        if (isStopped) {
            // we were told to stop
            return false
        }
        if (getElapsedTime().inWholeMinutes >= MAX_DURATION_MINUTES) {
            // we have taken too much time
            return false
        }
        return true
    }

     /**
     * Override this method to do your actual background processing.  This method is called on a
     * background thread - you are required to **synchronously** do your work and return the
     * [androidx.work.ListenableWorker.Result] from this method.  Once you return from this
     * method, the Worker is considered to have finished what its doing and will be destroyed.  If
     * you need to do your work asynchronously on a thread of your own choice, see
     * [ListenableWorker].
     *
     *
     * A Worker is given a maximum of ten minutes to finish its execution and return a
     * [androidx.work.ListenableWorker.Result].  After this time has expired, the Worker will
     * be signalled to stop.
     *
     * @return The [androidx.work.ListenableWorker.Result] of the computation; note that
     * dependent work will not execute if you use
     * [androidx.work.ListenableWorker.Result.failure] or
     * [androidx.work.ListenableWorker.Result.failure]
     */
    override fun doWork(): Result {
        timer = systemTime.getNanoTime()
        totalNumberOfItemsProcessed = 0
        loggerFactory.logger.debug("TrackMetadataWorker: doWork - start -  batch = $BATCH_SIZE, max mins = $MAX_DURATION_MINUTES")

        // try and keep all the actual work in the ITrackMetadataGenerator
        // as then we can just call it from a background thread without all the WorkManager gubbins
        try {
            do {
                // we only check if we should stop after each batch, so dont make the batch size too large
                val processedItems = generator.generateMetadata(BATCH_SIZE)
                totalNumberOfItemsProcessed = totalNumberOfItemsProcessed + processedItems
            } while (okToContinue(processedItems))
        } catch (ex: Throwable) {
            loggerFactory.logger.error("TrackMetadataWorker: doWork - $BATCH_SIZE, ${getElapsedTime()}", ex)
            crashReporter.logNonFatalException(ex)
        }

        loggerFactory.logger.debug("TrackMetadataWorker: doWork - end - batch = $BATCH_SIZE, processed = ${totalNumberOfItemsProcessed}, elapsed = ${getElapsedTime()}, isStopped = ${isStopped}")
        doWorkDuration = getElapsedTime()
        return Result.success()
    }

    override fun onStopped() {
        val reason = WorkerHelper.getPrintableStopReasonFromListenableWorker(this)
        loggerFactory.logger.debug("TrackMetadataWorker: onStopped - ${getElapsedTime()}, ${doWorkDuration}, ${reason}")
        crashReporter.logNonFatalException(WorkerException("TrackMetadataWorker: onStopped - reason = ${reason}, batch = $BATCH_SIZE, processed = ${totalNumberOfItemsProcessed}, workDuration = ${doWorkDuration}, elapsed = ${getElapsedTime()}", null))
        super.onStopped()
    }

    class Factory @Inject constructor(
        private val loggerFactory: ILoggerFactory,
        private val crashReporter: ICrashReporter,
        private val systemTime: ISystemTime,
        private val generator: IBatchTrackMetadataGenerator
    ): ChildWorkerFactory {

        override fun create(appContext: Context, params: WorkerParameters): Worker {
            return TrackMetadataWorker(appContext, params, loggerFactory, crashReporter, systemTime, generator)
        }
    }
}