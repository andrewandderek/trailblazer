/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.trackdisplay

import android.content.Intent
import android.os.Bundle
import com.andrewandderek.trailblazer.ui.trackcontrol.ITrackControlPresenter

interface ITrackDisplayPresenter : ITrackControlPresenter<ITrackDisplayView> {
    fun displayTrack(trackId: Long)
    fun exportTrack()
    fun exportTrack(format: String)
    fun importTrack(uriString: String)
    fun unpackIntent(intent: Intent)
    fun refreshTitle()
    fun saveState(outState: Bundle?)
    fun locationPermissionGranted()
    fun mapZoomLevelChanged(newZoomLevel: Float)
    fun confirmedRename(trackId: String, value: String)
}
