/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.gpx

import android.content.ContentResolver
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.sharing.importexport.BaseImporter
import com.andrewandderek.trailblazer.sharing.importexport.ImportAndResult
import com.andrewandderek.trailblazer.sharing.importexport.ImportResult
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.ISystemTime
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.IOException
import java.io.InputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class GpxImporter @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    fileSystemHelper: IFileSystemHelper,
    private val systemTime: ISystemTime,
    contentResolver: ContentResolver,
    crashReporter: ICrashReporter,
) : BaseImporter(loggerFactory, fileSystemHelper, contentResolver, crashReporter) {

    override fun importTrack(stream: InputStream): ImportAndResult {
        var result: ImportResult = ImportResult.OK
        var track: Track? = null
        var segment: Long = 0

        try {
            val parser = XmlPullParserFactory.newInstance().newPullParser()
            parser.setInput(stream, null)
            var eventType = parser.eventType
            var rootElementFound = false
            while (eventType != XmlPullParser.END_DOCUMENT) {
                when (eventType) {
                    XmlPullParser.START_DOCUMENT -> {
                        track = Track()
                    }

                    XmlPullParser.START_TAG -> {
                        val tagName = parser.name
                        if (!rootElementFound) {
                            if (nameEquals(tagName, GpxLiterals.GPX_TAG)) {
                                rootElementFound = true
                            } else {
                                loggerFactory.logger.warn("Unexpected root node ${tagName}")
                                return ImportAndResult(null, ImportResult.UNKNOWN_FORMAT)
                            }
                        }
                        if (nameEquals(tagName,GpxLiterals.TRACK_TAG)) {
                            parseTrack(parser, track!!)
                        } else if (nameEquals(tagName, GpxLiterals.TRACK_SEGEMENT_TAG)) {
                            segment = segment + 1
                            loggerFactory.logger.debug("GPX new segment: ${segment}")
                        } else if (nameEquals(tagName,GpxLiterals.TRACK_POINT_TAG)) {
                            parsePoint(parser, track!!, segment)
                        }
                    }
                }
                eventType = parser.next()
            }
        } catch (e: XmlPullParserException) {
            loggerFactory.logger.error("error reading GPX ", e)
            track = null
            result = ImportResult.UNKNOWN_FORMAT
        } catch (e: IOException) {
            loggerFactory.logger.error("error reading GPX ", e)
            track = null
            result = ImportResult.IO_ERROR
        }

        loggerFactory.logger.debug("GPX track imported with: ${track?.currentPositionsSnapshot?.count()} positions")
        return ImportAndResult(track, result)
    }

    private fun parsePoint(parser: XmlPullParser, track: Track, segment: Long) {
        var latitude = 0.0
        var longitude = 0.0
        var altitude: Double? = null
        var datetime = systemTime.getCurrentTime()

        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_TAG) {
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    val tagName = parser.name
                    if (nameEquals(tagName,GpxLiterals.TRACK_POINT_TAG)) {
                        latitude = parseAttributeToDouble(parser, GpxLiterals.LATITUDE_ATTR)
                        longitude = parseAttributeToDouble(parser, GpxLiterals.LONGITUDE_ATTR)
                    } else if (nameEquals(tagName,GpxLiterals.ELEVATION_TAG)) {
                        val eleStr = parser.nextText()
                        try {
                            altitude = eleStr.toDouble()
                        } catch (e: NumberFormatException) {
                            loggerFactory.logger.error("GpxImporter Elevation Exception (${eleStr}) :",e)
                        }
                    } else if (nameEquals(tagName,GpxLiterals.TIME_TAG)) {
                        try {
                            val dateStr = parser.nextText()
                            dateTimeFormatterUtc.parse(dateStr)?.let { datetime.time = it }
                        } catch (e: ParseException) {
                            loggerFactory.logger.error("DateTime Exception :",e)
                        }
                    }
                }
            }
            // jump to next event
            eventType = parser.next()
        }
        // TODO - is there any way we can transmit the accuracy information into the GPX format
        var position = Position(latitude, longitude, altitude, datetime, 0F)
        position.segment = segment
        if (track.isEmpty) {
            // this is the first position - set the start time as well
            track.started = datetime
        }
        track.addNewPosition(position)
    }

    private fun parseTrack(parser: XmlPullParser, track: Track) {
        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_TAG) {
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    val tagName = parser.name
                    if (nameEquals(tagName, GpxLiterals.TRACK_NAME_TAG)) {
                        track.name = parser.nextText()
                    } else if (nameEquals(tagName,GpxLiterals.TRACK_DESC_TAG)) {
                        track.notes = parser.nextText()
                    } else if (nameEquals(tagName,GpxLiterals.TRACK_SEGEMENT_TAG)) {
                        return      // we are at the start of the track segments
                    }
                }
            }
            // jump to next event
            eventType = parser.next()
        }
    }

    private fun parseAttributeToDouble(parser: XmlPullParser, attributeName: String):Double {
        var value = 0.0
        try {
            val latStr = parser.getAttributeValue(null, attributeName)
            if (!latStr.isEmpty()) {
                value = latStr.toDouble()
            }
        } catch (e: Exception) {
            loggerFactory.logger.error("GpxImporter Attr ${attributeName} Exception) :",e)
        }
        return value
    }
}