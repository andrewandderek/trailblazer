/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.schema

import android.database.sqlite.SQLiteDatabase
import com.andrewandderek.trailblazer.logging.ILoggerFactory

// Database table
object TrackTable {
    const val TABLE_TRACK = "track"
    const val TABLE_TRACK_BACKUP = "track_backup"

    const val COLUMN_ID = "_id"
    const val COLUMN_NAME = "name"
    const val COLUMN_NOTES = "notes"
    const val COLUMN_TIME_STARTED = "time_started_utc"        // UTC milliseconds

    // Database creation SQL statement
    private const val DATABASE_CREATE = """
    create table $TABLE_TRACK(
     $COLUMN_ID integer primary key autoincrement,
     $COLUMN_NAME varchar(255) not null default '',
     $COLUMN_NOTES varchar(255) not null default '',
     $COLUMN_TIME_STARTED DATETIME DEFAULT CURRENT_TIMESTAMP
    );"""

    fun onCreate(database: SQLiteDatabase) {
        database.execSQL(DATABASE_CREATE)
    }

    @Suppress("UNUSED_PARAMETER")
    fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int, loggerFactory: ILoggerFactory) {
        // make sure that any modification statements match the create statement above

        // if we end up having to drop and recreate the track table then we may want to consider dropping the metadata like this
        // DatabaseUtilities.dropTable(database, TrackMetadataTable.TABLE_TRACK_METADATA)
    }

    fun onInitialiseData(@Suppress("UNUSED_PARAMETER") database: SQLiteDatabase) {
    }
}