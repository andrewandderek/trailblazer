/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.tracklist

import com.andrewandderek.trailblazer.ui.trackcontrol.ITrackControlView

interface ITrackListView : ITrackControlView{
    fun navigateToTrack(id: Long)

    fun promptToDelete(prompt: String, trackId: String)
    fun promptForTrackName(trackId: String, name: String)
    fun promptForCombinedTrackName(name: String)
    fun promptForTrackNameAfterStopping(trackId: String, name: String)
    fun promptToDeleteMultiple(prompt: String)

    fun tracksLoaded()
    fun startProgress()
    fun startProgress(messageId: Int)
    fun startProgress(messageId: Int, max: Int)
    fun updateProgress(progress: Int)
    fun completeProgress()
    fun startMultiSelectMode()
    fun setTitle(title: String)
    fun updateList(title: String)
    fun requestWriteExternalStoragePermission()
    fun requestReadExternalStoragePermission()
    fun promptForFileFormat(prompt: String, fileFormat: String)
    fun promptForFile()
    fun navigateToCompare(trackIds: List<Long>)
    fun navigateToSettings()
    fun navigateToImportTrack(fileUri: String, promptForConfirmation: Boolean)
    fun setEmptyListText(id: Int)
    fun showNonSearchUi()
    fun hideNonSearchUi()
}