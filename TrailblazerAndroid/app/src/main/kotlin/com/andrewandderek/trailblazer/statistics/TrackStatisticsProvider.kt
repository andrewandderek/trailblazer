/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackStatistics
import com.andrewandderek.trailblazer.statistics.elevation.IElevationAlgorithmFactory
import com.andrewandderek.trailblazer.statistics.elevation.IElevationSettings
import javax.inject.Inject
import javax.inject.Named

// full statistics provider used in the statistics UI
class TrackStatisticsProvider
@Inject constructor(
        private val distanceProvider: IDistanceProvider,
        private val elevationAlgorithmFactory: IElevationAlgorithmFactory,
        private val elevationSettings: IElevationSettings
) : ITrackStatisticsProvider
{
    private val calculators = ArrayList<ITrackCalculator>()

    init {
        // Note that the order here is important - dependent calculators should be added after those on
        // which they depend (eg. speed should come after distance and time)
        calculators.add(TrackDistanceCalculator(distanceProvider))
        calculators.add(TrackTimeCalculator())
        calculators.add(TrackSpeedCalculator())
        calculators.add(TrackElevationCalculator(elevationAlgorithmFactory, elevationSettings))
    }

    override fun getStatistics(track: Track?): TrackStatistics {
        // the statistics can be run on the track in memory which can have points that would be excluded by
        // loading the track from the repo so lets provide the threshold so we can eliminate them here as well
        return StatisticsHelper.getStatistics(calculators, track, Position.ACCURACY_THRESHOLD)
    }
}