/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.metadata.track

import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.utility.ICrashReporter
import javax.inject.Inject

interface IBatchTrackMetadataGenerator {
    fun generateMetadata(numberToProcess: Int): Int
}

/***
 * this generator is usually called from a worker task - it does long running tasks
 * if you want to call it directly then the caller will need to work out how to get the generator off the UI thread using Rx or coroutines etc.
 * do not put threading code in here as the worker needs to do its work synchronously
 */
class BatchTrackMetadataGenerator @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val trackRepository: ITrackRepository,
    private val trackMetadataRepository: ITrackMetadataRepository,
    private val positionRepository: IPositionRepository,
    private val eventBus: IEventBus,
    private val crashReporter: ICrashReporter,
    private val singleTrackGenerator: ISingleTrackMetadataGenerator,
) : IBatchTrackMetadataGenerator
{
    companion object {
        // metadata generated before this version code will be regenerated
        // version code == 25 == stats added to metadata
        public const val LAST_VERSION_CODE_WITH_TRUSTED_METADATA = 25
    }

    override fun generateMetadata(numberToProcess: Int): Int {
        var processedItemIds = ArrayList<Long>()
        val tracksWithoutMetadata = getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
        loggerFactory.logger.debug("TrackMetadataGenerator: generateMetadata - null metadata size = ${tracksWithoutMetadata.size}")

        for (thisTrack in tracksWithoutMetadata.take(numberToProcess)) {
            generateMetadataForTrack(thisTrack.id)
            processedItemIds.add(thisTrack.id)
        }

        if (tracksWithoutMetadata.size >= numberToProcess) {
            // the tracks without metadata list has provided enough tracks for the number we have been asked to process
            // we can stop
            notifyTrackMetadataUpdated(processedItemIds)
            return processedItemIds.size
        }
        val oldDataToProcess = numberToProcess - tracksWithoutMetadata.size
        val oldMetadata = getOldTrackMetadata()
        loggerFactory.logger.debug("TrackMetadataGenerator: generateMetadata - old metadata size = ${oldMetadata.size}")
        for (thisTrackMetadata in oldMetadata.take(oldDataToProcess)) {
            generateMetadataForTrack(thisTrackMetadata.trackId)
            processedItemIds.add(thisTrackMetadata.trackId)
        }

        if (processedItemIds.size > 0) {
            notifyTrackMetadataUpdated(processedItemIds)
        }
        return processedItemIds.size
    }

    private  fun getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated(): List<Track> {
        // we also consider a track being recorded as if it didnt have metadata as any metadata it has is wrong
        return trackRepository.getTracksWithoutAnyMetadataOrWithPositionsRecordedAfterMetadataWasGenerated()
    }

    private fun getOldTrackMetadata(): List<TrackMetadata> {
        return trackMetadataRepository.getWhereVersionCodeBefore(LAST_VERSION_CODE_WITH_TRUSTED_METADATA)
    }

    private fun notifyTrackMetadataUpdated(processedItemIds: ArrayList<Long>) {
        eventBus.publish(TrackMetadataUpdatedEvent(processedItemIds))
    }

    private fun generateMetadataForTrack(id: Long) {
        // we need to load the complete track to be able to generate the metadata
        val track = trackRepository.getWithPositionsAndAccuracy(id, Position.ACCURACY_THRESHOLD, positionRepository)
        track?.let {
            singleTrackGenerator.generateMetadataForTrack(it)
        } ?: run {
            loggerFactory.logger.warn("TrackMetadataGenerator: generateMetadataForTrack cannot load track - ${id}")
            crashReporter.logNonFatalException(TrackLoadException("cannot load track - ${id}", null))
        }
    }
}