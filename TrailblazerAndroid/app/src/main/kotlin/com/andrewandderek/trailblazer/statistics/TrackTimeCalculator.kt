/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.TrackStatistics
import java.util.Calendar

/**
 * Calculate track times.
 * All times are in milliseconds.
 */
class TrackTimeCalculator : ITrackCalculator {
    override fun addPosition(trackStartTime: Calendar, positions: List<Position>, index: Int, statistics: TrackStatistics) {
        statistics.elapsedTimeInMilliseconds = getElapsedTime(trackStartTime, positions[index])
        updateRecordedTime(positions, index, statistics)
        updateMovingTime(positions, index, statistics)
    }

    private fun updateRecordedTime(positions: List<Position>, index: Int, statistics: TrackStatistics) {
        if (index == 0) {
            statistics.recordedTimeInMilliseconds = statistics.elapsedTimeInMilliseconds
        }
        else {
            if (!positions[index].isFromDifferentSegment(positions[index - 1]))
                statistics.recordedTimeInMilliseconds += getElapsedTime(positions[index - 1], positions[index])
        }
    }

    private fun updateMovingTime(positions: List<Position>, index: Int, statistics: TrackStatistics) {
        if (index == 0) {
            statistics.movingTimeInMilliseconds = statistics.elapsedTimeInMilliseconds
        }
        else {
            if (!positions[index].isFromDifferentSegment(positions[index - 1]) && !equalPositions(positions[index - 1], positions[index])) {
                statistics.movingTimeInMilliseconds += getElapsedTime(positions[index - 1], positions[index])
            }
        }
    }

    private fun equalPositions(position1: Position, position2: Position): Boolean {
        return (position1.latitude == position2.latitude && position1.longitude == position2.longitude)
    }

    private fun getElapsedTime(trackStartTime: Calendar, lastPosition: Position): Long {
        lastPosition.timeRecorded?.let {
            return it.timeInMillis - trackStartTime.timeInMillis
        }
        return 0L
    }

    private fun getElapsedTime(position1: Position, position2: Position): Long {
        position1.timeRecorded?.let { pos1 ->
            position2.timeRecorded?.let { pos2 ->
                return pos2.timeInMillis - pos1.timeInMillis
            }
        }
        return 0L
    }
}