/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.view

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class OkCancelDialogFragment : DialogFragment() {
    companion object {
        const val MESSAGE = "message_key"
        const val OK = "ok_key"
        const val CANCEL = "cancel_key"
        const val CUSTOM = "custom_key"

        fun newInstance(message: String, ok: String, cancel: String, data: String): OkCancelDialogFragment {
            val fragment = OkCancelDialogFragment()
            val args = Bundle()
            args.putString(MESSAGE, message)
            args.putString(OK, ok)
            args.putString(CANCEL, cancel)
            args.putString(CUSTOM, data)        // consider setting a serializable object
            fragment.arguments = args
            return fragment
        }
    }

    interface Listener {
        fun ok(tag: String, userData: String)
        fun cancel(tag: String, userData: String)
    }

    private lateinit var listener: Listener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        try {
            listener = activity as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement Listener")
        }

        val args = requireArguments()
        val tag = tag!!
        val message = args.getString(MESSAGE)
        val ok = args.getString(OK)
        val cancel = args.getString(CANCEL)
        val customData = args.getString(CUSTOM)
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
        builder
                .setPositiveButton(ok) { _, _ -> listener.ok(tag, customData ?: "") }
                .setNegativeButton(cancel) { _, _ -> listener.cancel(tag, customData ?: "") }
        return builder.create()
    }
}