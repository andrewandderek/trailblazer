/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.intent

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.sharing.BaseSharer
import com.andrewandderek.trailblazer.utility.IResourceProvider
import javax.inject.Inject

class DatabaseSharer
@Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val context: Context,
    private val resourceProvider: IResourceProvider
)
    : BaseSharer(loggerFactory, context, resourceProvider), IIntentSharer
{
    override fun getShareIntentWithMultipleFiles(trackName: String, fileNames: List<String>): Intent {
        loggerFactory.logger.debug("Share database")
        val attachmentUris = ArrayList<Uri>()
        for (filename in fileNames) {
            attachmentUris.add(getAttachmentUri(filename))
        }
        return getDatabaseSharingIntent(getDatabaseShareText(), attachmentUris)
    }

    private fun getDatabaseShareText(): String {
        val format = resourceProvider.getString(R.string.share_database_email_body_text_fmt)
        return String.format(format,
            getAdvertisingUrl(),
        )
    }

    private fun getDatabaseSharingIntent(shareText: String, attachmentUris: ArrayList<Uri>): Intent {
        val sharingIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
        sharingIntent.type = "*/*"
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareText)
        sharingIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachmentUris)
        return sharingIntent
    }
}
