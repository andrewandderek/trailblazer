/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport.kml

import android.content.ContentResolver
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.sharing.importexport.BaseImporter
import com.andrewandderek.trailblazer.sharing.importexport.IImporter
import com.andrewandderek.trailblazer.sharing.importexport.ImportAndResult
import com.andrewandderek.trailblazer.sharing.importexport.ImportResult
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.ISystemTime
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipException
import java.util.zip.ZipInputStream
import javax.inject.Inject
import javax.inject.Named


class KmzImporter @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    fileSystemHelper: IFileSystemHelper,
    private val systemTime: ISystemTime,
    contentResolver: ContentResolver,
    @Named(NameLiterals.FILE_FORMAT_KML) private val kmlImporter: IImporter,
    crashReporter: ICrashReporter,
) : BaseImporter(loggerFactory, fileSystemHelper, contentResolver, crashReporter) {

    override fun importTrack(stream: InputStream): ImportAndResult {
        loggerFactory.logger.debug("KMZ importTrack")

        var result: ImportResult = ImportResult.UNKNOWN_FORMAT
        try {
            val zipStream = ZipInputStream(BufferedInputStream(stream))
            var entry: ZipEntry? = zipStream.nextEntry ?: return ImportAndResult(null, ImportResult.UNKNOWN_FORMAT)
            while (entry != null) {
                loggerFactory.logger.debug("KMZ importTrack = file ${entry.name}")
                // according to google we should only consider the first KML file
                // https://developers.google.com/kml/documentation/kmzarchives
                if (entry.name.endsWith(".${KmlLiterals.KML_EXTENSION}", true)) {
                    val buffer = ByteArray(1024 * 16)
                    val kmlStream = ByteArrayOutputStream()
                    var amountRead: Int = zipStream.read(buffer)
                    while (amountRead > 0) {
                        kmlStream.write(buffer, 0, amountRead)
                        amountRead = zipStream.read(buffer)
                    }
                    // a KMZ is just a zipped KMZ - hand off to the KML importer
                    return kmlImporter.importTrack(ByteArrayInputStream(kmlStream.toByteArray()))
                }
                entry = zipStream.nextEntry
            }
        } catch (e: IOException) {
            loggerFactory.logger.error("error reading KMZ", e)
            result = ImportResult.IO_ERROR
        } catch (e: ZipException) {
            loggerFactory.logger.error("error reading KMZ", e)
            result = ImportResult.IO_ERROR
        }

        return ImportAndResult(null, result)
    }
}


