/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.worker.trackmetadata

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.worker.WorkerHelper
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface ITrackMetadataScheduler {
    fun scheduleWorker()
    fun isWorkScheduled() : Boolean
}

class TrackMetadataScheduler @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val workManager: WorkManager,
) : ITrackMetadataScheduler
{
    override fun scheduleWorker() {
        loggerFactory.logger.debug("TrackMetadataScheduler: scheduleWorker")

        workManager.enqueueUniquePeriodicWork(WORK_NAME, ExistingPeriodicWorkPolicy.CANCEL_AND_REENQUEUE, createWorkRequest())

        loggerFactory.logger.debug("TrackMetadataScheduler: scheduleWorker work scheduled = ${isWorkScheduled()}")
    }

    private fun createWorkRequest(): PeriodicWorkRequest {
        return PeriodicWorkRequest.Builder(TrackMetadataWorker::class.java, 60, TimeUnit.MINUTES)
            .build()
    }

    override fun isWorkScheduled(): Boolean {
        return WorkerHelper.isWorkScheduled(loggerFactory, workManager, WORK_NAME)
    }

    companion object {
        private const val WORK_NAME = "com.andrewandderek.trailblazer.worker.trackmetadata"
    }
}