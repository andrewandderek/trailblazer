/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.trackdisplay

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.event.TrackImportedEvent
import com.andrewandderek.trailblazer.event.TrackRenamedEvent
import com.andrewandderek.trailblazer.exception.ExportException
import com.andrewandderek.trailblazer.exception.ShareException
import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.exception.TrackRenderException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.settings.IZoomSettings
import com.andrewandderek.trailblazer.sharing.importexport.IExporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.IImporter
import com.andrewandderek.trailblazer.sharing.importexport.ImportAndResult
import com.andrewandderek.trailblazer.sharing.importexport.ImportResult
import com.andrewandderek.trailblazer.sharing.intent.IIntentSharer
import com.andrewandderek.trailblazer.ui.trackcontrol.TrackControlPresenter
import com.andrewandderek.trailblazer.utility.*
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject
import javax.inject.Named

class TrackDisplayPresenter
    @Inject constructor(
            private val loggerFactory: ILoggerFactory,
            private val schedulerProvider: ISchedulerProvider,
            trackingServiceController: IGpsLoggerServiceController,
            private val permissionChecker: IPermissionChecker,
            private val trackRepository: ITrackRepository,
            private val positionRepository: IPositionRepository,
            private val exporterFactory: IExporterFactory,
            private val importer: IImporter,
            @Named(NameLiterals.SHARE_TRACK) private val sharer: IIntentSharer,
            private val resourceProvider: IResourceProvider,
            private val analyticsEngine: IAnalyticsEngine,
            private val crashReporter: ICrashReporter,
            eventBus: IEventBus,
            private val zoomSettings: IZoomSettings
)
    : TrackControlPresenter<ITrackDisplayView>(loggerFactory, schedulerProvider, trackingServiceController, permissionChecker, analyticsEngine, crashReporter, eventBus),
    ITrackDisplayPresenter {

    companion object {
        const val PARAM_TRACK_ID = "trackID"
        const val PARAM_PROMPT_FOR_IMPORT_CONFIRMATION = "promptForConformation"
    }

    override fun bind(theView: ITrackDisplayView, savedInstanceState: Bundle?) {
        loggerFactory.logger.debug("TrackDisplayPresenter.bind")
        super.bind(theView, savedInstanceState)

        trackRenamedEventSubscriber = eventBus.subscribe(TrackRenamedEvent::class.java)
        {
            onTrackRenamed(it.trackId, it.oldName, it.newName)
        }

        savedInstanceState?.let {
            val trackId = it.getLong(STATE_KEY_DISPLAY_TRACK_ID, -1L)
            loggerFactory.logger.debug("TrackDisplayPresenter: bind track ID ${trackId}")
            if (trackId != -1L) {
                displayTrack(trackId)
            }
        }
    }

    protected override fun onRecordingStatusChanged(event: RecordingStatusChangedEvent) {
        loggerFactory.logger.debug("TrackDisplayPresenter: onRecordingStatusChanged: recording status changed ${event.statusChangedTo}, ${event.trackId}, ${event.fromNotification}")
        super.onRecordingStatusChanged(event)
        refreshTitle()

        if (event.shouldPromptForName) {
            loggerFactory.logger.debug("TrackDisplayPresenter: onRecordingStatusChanged: ${event.trackId} - stopped, current = ${currentTrackId}")
            if (event.trackId < 0) {
                return
            }
            if (currentTrackId < 0) {
                return
            }
            if (currentTrackId != event.trackId) {
                // this could happen if we are recording a track but its not the current displayed track
                loggerFactory.logger.debug("TrackDisplayPresenter: onRecordingStatusChanged: ignored as ${event.trackId} is not current current = ${currentTrackId}")
                return
            }

            // remember events fire async - we may not be on top and then we will not be able to prompt here
            view?.promptForTrackNameAfterStopping(currentTrackId.toString(), currentTrackName)
        }
    }

    private fun onTrackRenamed(trackId: Long, oldName: String, newName: String) {
        loggerFactory.logger.debug("TrackDisplayPresenter: onTrackRenamed ${trackId}, ${oldName} -> ${newName}")
        refreshTitle()
    }

    override fun unbind(theView: ITrackDisplayView) {
        loggerFactory.logger.debug("TrackDisplayPresenter.unbind")
        super.unbind(theView)

        trackLoaderSubscriber?.dispose()
        trackLoaderSubscriber = null
        trackExportSubscriber?.dispose()
        trackExportSubscriber = null
        trackImportSubscriber?.dispose()
        trackImportSubscriber = null

        trackRenamedEventSubscriber?.unsubscribe()
        trackRenamedEventSubscriber = null
    }

    override fun start() {
        loggerFactory.logger.debug("TrackDisplayPresenter.start")
        super.start()
        wholeTrackShouldBeDrawn = true      // we may have missed some points while we were not drawing
    }

    override fun onLocationTrackingServiceConnected() {
        super.onLocationTrackingServiceConnected()
        subscribeToTrackPositions()
        subscribeToCurrentPosition()
    }

    override fun saveState(outState: Bundle?) {
        loggerFactory.logger.debug("TrackDisplayPresenter: saveState")
        // yes I know this is android specific code in the presenter - but it can be tested using mocks without using a wrapper
        trackToDisplay?.let {
            loggerFactory.logger.debug("TrackDisplayPresenter: saveState ID ${it.id}")
            outState?.putLong(STATE_KEY_DISPLAY_TRACK_ID, it.id)
        }
    }

    override fun locationPermissionGranted() {
        if (permissionChecker.hasLocationPermission())
            locationTrackingService?.restartCurrentPositionUpdates()
    }

    override fun mapZoomLevelChanged(newZoomLevel: Float) {
        zoomSettings.zoomLevel = newZoomLevel
    }

    override fun isActionAvailable(actionId: Int): Boolean {
        if (isTrackControlAction(actionId)) {
            return super.isActionAvailable(actionId)
        }
        when (actionId) {
            R.id.action_rename_track,
            R.id.action_share_track,
            R.id.action_export_track,
            R.id.action_statistics -> {
                return isRecordingOrViewingTrack
            }
        }
        return false
    }

    override fun actionSelected(itemId: Int): Boolean {
        loggerFactory.logger.debug("TrackDisplayPresenter: actionSelected ${itemId}")
        if (super.actionSelected(itemId)) {
            return true
        }
        when (itemId) {
            R.id.action_rename_track -> {
                renameTrack()
                return true
            }
            R.id.action_share_track -> {
                shareTrack()
                return true
            }
            R.id.action_export_track -> {
                if (!permissionChecker.hasWriteStoragePermission()) {
                    view?.requestWriteExternalStoragePermission()
                    return true
                }
                exportTrack()
                return true
            }
            R.id.action_statistics -> {
                navigateToStatistics()
                return true
            }
        }
        return false
    }

    protected override fun onTrackStarted(track: Track?) {
        super.onTrackStarted(track)
        displayTrack(-1)
        refreshTitle()
    }

    protected override fun onTrackResumed() {
        super.onTrackResumed()
        refreshTitle()
    }

    protected override fun onTrackPaused() {
        super.onTrackPaused()
        refreshTitle()
    }

    protected override fun onTrackStopped() {
        super.onTrackStopped()
        // we want to treat the current recording track as the track being displayed
        // so lets effectively do what would happen if the user did that in the UI
        // this will work OK as long as we don't clear the trackBeingRecorded just by stopping
        trackBeingRecorded?.let {
            displayTrack(it.id)
        } ?: refreshTitle()
    }

    override fun unpackIntent(intent: Intent) {
        loggerFactory.logger.debug("TrackDisplayPresenter: unpackIntent ${intent.action}, ${intent.data}")
        val suppliedTrackId = getSuppliedTrackId(intent)
        if (suppliedTrackId > 0) {
            displayTrack(suppliedTrackId)
            return
        }
        val intentUri = getSuppliedImportUri(intent)
        if (intentUri != null) {
            if (trackBeingImported) {
                // we are already doing an import - lets wait
                view?.showMessage(R.string.track_being_imported)
                return
            }
            analyticsEngine.recordClick(AnalyticsConstants.ACTION_IMPORT_SHARED_ID, AnalyticsConstants.ACTION_IMPORT_SHARED_NAME, AnalyticsConstants.ACTION_TYPE)
            if (isPromptForImportConfirmationRequired(intent)) {
                promptToImportTrack(intentUri.toString())
            }
            else {
                importTrack(intentUri.toString())
            }
        }
    }

    override fun importTrack(uriString: String) {
        loggerFactory.logger.debug("TrackDisplayPresenter.importTrack ${uriString}")
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_IMPORT_ID, AnalyticsConstants.ACTION_IMPORT_NAME, AnalyticsConstants.ACTION_TYPE)
        view?.startProgress(R.string.track_loading_progress)
        trackImportSubscriber?.dispose()
        trackBeingImported = true
        trackImportSubscriber = Flowable.fromCallable {importAndStoreTrack(uriString) }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        { importedTrack: ImportAndResult ->
                            view?.completeProgress()
                            when (importedTrack.result) {
                                ImportResult.OK -> {
                                    view?.showMessage(R.string.track_imported)
                                    renderTrack(importedTrack.track, true)
                                }
                                ImportResult.NO_TIMESTAMP,
                                ImportResult.MISMATCHED_TIMESTAMP -> {
                                    view?.showMessage(R.string.no_timestamps_in_track)
                                    // there is no point in displaying an empty mapview
                                    view?.exit()
                                }
                                else -> {
                                    view?.showMessage(R.string.error_loading_track)
                                    // there is no point in displaying an empty mapview
                                    view?.exit()
                                }
                            }
                            trackBeingImported = false
                        },
                        { error: Throwable ->
                            trackToDisplay = null
                            loggerFactory.logger.error("importTrack", error)
                            crashReporter.logNonFatalException(TrackLoadException("uri $uriString", error))
                            view?.completeProgress()
                            view?.showMessage(R.string.error_loading_track)
                            trackBeingImported = false
                            view?.exit()
                        }
                )
    }

    private fun importAndStoreTrack(uriString: String): ImportAndResult {
        val importedTrack = importer.importTrack(uriString)
        if (importedTrack.result == ImportResult.OK && importedTrack.track != null) {
            val savedTrack = trackRepository.create(importedTrack.track)
            // we dont know the track ID until its created in the DB - we call replaceTrack to ensure the points all have that new ID
            savedTrack.replaceTrack(importedTrack.track.currentPositionsSnapshot)
            // only save the points after the track IDs have been set
            positionRepository.createTrack(savedTrack.currentPositionsSnapshot)
            eventBus.publish(TrackImportedEvent(savedTrack.id))
            return ImportAndResult(savedTrack, ImportResult.OK)
        }
        return importedTrack
    }

    private fun renameTrack() {
        loggerFactory.logger.debug("TrackDisplayPresenter.renameTrack ${currentTrackId}")
        if (currentTrackId < 0) {
            return
        }
        view?.promptForTrackName(currentTrackId.toString(), currentTrackName)
        return
    }

    override fun confirmedRename(trackId: String, value: String) {
        loggerFactory.logger.debug("TrackDisplayPresenter.confirmedRename ${trackId}, ${value}")
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_RENAME_ID, AnalyticsConstants.ACTION_RENAME_NAME, AnalyticsConstants.ACTION_TYPE)

        val id: Long = trackId.toLong()
        var track: Track? = null
        if (isDisplayingPreviouslyRecordedTrack) {
            track = trackToDisplay!!
        }
        else if (trackBeingRecorded != null) {
            track = trackBeingRecorded!!
        }
        track?.let {
            if (it.id != id) {
                // the ID is incorrect, we need to ignore it
                loggerFactory.logger.debug("TrackDisplayPresenter.confirmedRename id mismatch UI ${id}, logic ${it.id}")
                return
            }
            val event = TrackRenamedEvent(it.id, it.name, value)
            it.name = value
            trackRepository.update(it)
            // all we do is update the data, the UI update is done by firing the event
            eventBus.publish(event)
        }
    }

    private fun shareTrack() {
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_SHARE_ID, AnalyticsConstants.ACTION_SHARE_NAME, AnalyticsConstants.ACTION_TYPE)
        exportTrack(true, NameLiterals.FILE_FORMAT_GPX)
    }

    override fun exportTrack() {
        view?.promptForFileFormat(resourceProvider.getString(R.string.file_format_export_prompt), NameLiterals.FILE_FORMAT_GPX)
    }

    override fun exportTrack(format: String) {
        when (format) {
            NameLiterals.FILE_FORMAT_GPX -> {
                analyticsEngine.recordClick(AnalyticsConstants.ACTION_EXPORT_ID, AnalyticsConstants.ACTION_EXPORT_NAME, AnalyticsConstants.ACTION_TYPE)
            }
            NameLiterals.FILE_FORMAT_KML -> {
                analyticsEngine.recordClick(AnalyticsConstants.ACTION_EXPORT_KML_ID, AnalyticsConstants.ACTION_EXPORT_KML_NAME, AnalyticsConstants.ACTION_TYPE)
            }
        }
        exportTrack(false, format)
    }

    private fun exportTrack(share: Boolean, format: String) {
        val track: Track
        if (isDisplayingPreviouslyRecordedTrack) {
            track = trackToDisplay!!
        }
        else if (trackBeingRecorded != null && trackBeingRecorded?.id != -1L) {
            track = trackBeingRecorded!!
        }
        else {
            view?.showMessage(R.string.track_not_exported)
            return
        }
        if (track.currentPositionsSnapshot.isEmpty()) {
            view?.showMessage(R.string.track_not_exported)
            return
        }
        // when we share we share all formats as we don't know which one the recipient will want
        val formats :List<String> = if (share) listOf(NameLiterals.FILE_FORMAT_GPX, NameLiterals.FILE_FORMAT_KML) else listOf(format)
        view?.startProgress(R.string.track_exporting_progress)
        trackExportSubscriber?.dispose()
        trackExportSubscriber = Flowable.fromCallable { writeTracksInFormats(share, track, formats) }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        { filenames: List<String> ->
                            view?.completeProgress()
                            if (filenames.isNotEmpty()) {
                                if (share) {
                                    val intent = sharer.getShareIntentWithMultipleFiles(track.name, filenames)
                                    view?.startIntent(intent, resourceProvider.getString(R.string.email_share_title))
                                } else {
                                    view?.showMessage(String.format(resourceProvider.getString(R.string.track_exported_fmt), filenames[0]))
                                }
                            } else {
                                view?.showMessage(R.string.track_export_error)
                            }
                        },
                        { error: Throwable ->
                            view?.completeProgress()
                            view?.showMessage(R.string.track_export_error)
                            loggerFactory.logger.error("exportTrack", error)
                            crashReporter.logNonFatalException(ShareException("ID ${track.id} share=${share} format=${formats}", error))
                        }
                )
    }

    private fun writeTracksInFormats(share: Boolean, track: Track, formats: List<String>): List<String> {
        val filenames = ArrayList<String>()
        for (format in formats) {
            try {
                val exporter = exporterFactory.getExporter(format)
                val filename = if (share) exporter.getShareFilename(track.name) else exporter.getExportFilename(track.startDateAndName, true)
                exporter.exportTrack(track, filename)
                filenames.add(filename)
            } catch (error: Throwable) {
                if (formats.size > 1) {
                    // if we are writing multiple formats then we can ignore the error as long as one of them succeeds
                    crashReporter.logNonFatalException(ShareException("ID ${track.id} share=${share} format=${formats}", error))
                } else {
                    // there are not multiple formats
                    throw error
                }
            }
        }
        if (filenames.size < 1) {
            // all of the exporters failed
            throw ExportException("Export has failed for all formats", null)
        }
        return filenames
    }

    public val isDisplayingPreviouslyRecordedTrack: Boolean
        get() = trackToDisplay != null

    override fun displayTrack(trackId: Long) {
        loggerFactory.logger.debug("TrackDisplayPresenter: display track ${trackId}")
        displayTrack(trackId, true)
    }

    private fun displayTrack(trackId: Long, centreAndZoomToFit: Boolean) {
        loggerFactory.logger.debug("TrackDisplayPresenter: display track $trackId, centre and zoom $centreAndZoomToFit")
        if (trackId == -1L) {
            trackToDisplay = null
            clearTrack()
            refreshTitle()
            return
        }
        if (isThisTrackCurrentlyBeingRecorded(trackId)) {
            // we want to display the current recording track
            // just kill the existing track display and the current track will be display when we get an update
            if (isDisplayingPreviouslyRecordedTrack) {
                // we were displaying a track then redraw the whole thing
                wholeTrackShouldBeDrawn = true
                zoomToFitOnRedraw = true
                trackToDisplay = null
            }
            // otherwise just leave it doing what it was before
            return
        }
        readAndRenderTrack(trackId, centreAndZoomToFit)
    }

    private fun readAndRenderTrack(trackId: Long, centreAndZoomToFit: Boolean) {
        view?.startProgress(R.string.track_loading_progress)
        trackLoaderSubscriber?.dispose()
        trackLoaderSubscriber = Flowable.fromCallable { trackRepository.getWithPositionsAndAccuracy(trackId, Position.ACCURACY_THRESHOLD, positionRepository)!! }
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe(
                    { track: Track? ->
                        renderTrack(track, centreAndZoomToFit)
                        view?.completeProgress()
                    },
                    { error: Throwable ->
                        trackToDisplay = null
                        refreshTitle()
                        view?.showMessage(R.string.error_loading_track)
                        view?.completeProgress()
                        loggerFactory.logger.error("displayTrack", error)
                        crashReporter.logNonFatalException(TrackLoadException("ID ${trackId}", error))
                    }
            )
    }

    private fun renderTrack(track: Track?, centreAndZoomToFit: Boolean) {
        trackToDisplay = track
        if (trackToDisplay == null) {
            view?.showMessage(R.string.track_not_found)
            refreshTitle()
            return
        }
        else if (trackToDisplay!!.isEmpty) {
            trackToDisplay = null
            view?.showMessage(R.string.track_empty)
            refreshTitle()
            return
        }
        loggerFactory.logger.debug("TrackDisplayPresenter: renderTrack points = ${track!!.positionCount}")
        refreshTitle()
        wholeTrackShouldBeDrawn = true

        val trackPositions = track.currentPositionsSnapshot
        if (trackPositions.isNotEmpty()) {
            if (centreAndZoomToFit)
                centreMapAndZoomToFit(trackPositions)
            else
                moveMapToPosition(trackPositions[0])
        }
        view?.drawAllPositions(trackPositions)
        view?.updateAvailableActions()
    }

    private fun clearTrack() {
        wholeTrackShouldBeDrawn = true
        view?.clearAllPositions()
    }

    protected override fun resetSubscriptions() {
        loggerFactory.logger.debug("TrackDisplayPresenter: resetSubscriptions")
        super.resetSubscriptions()
        trackPositionsSubscriber?.dispose()
        trackPositionsSubscriber = null
        currentPositionSubscriber?.dispose()
        currentPositionSubscriber = null
    }

    private fun subscribeToCurrentPosition() {
        loggerFactory.logger.debug("TrackDisplayPresenter: subscribeToTrack")

        currentPositionSubscriber?.dispose()

        currentPositionSubscriber = locationTrackingService!!
                .currentPosition.subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        { position ->
                            loggerFactory.logger.debug("TrackDisplayPresenter: new current position (${position.latitude}, ${position.longitude})")
                            if (!isRecordingOrViewingTrack)
                                moveMapToPosition(position)
                            currentPositionSubscriber?.dispose()
                            currentPositionSubscriber = null
                        },
                        { error: Throwable ->
                            loggerFactory.logger.error("TrackDisplayPresenter: current position error", error)
                            crashReporter.logNonFatalException(TrackRenderException("location update", error))
                        }
                )
    }

    protected override fun onTrackSubscribed(track: Track) {
        super.onTrackSubscribed(track)
        subscribeToTrackPositions()
    }

    private fun subscribeToTrackPositions() {
        loggerFactory.logger.debug("TrackDisplayPresenter: subscribeToTrackPositions")

        trackPositionsSubscriber?.dispose()     // discard any existing subscription

        val observable = locationTrackingService!!.currentTrackPositions
        trackPositionsSubscriber = observable
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe (
                    { points ->
                        updateTrackPositions(points)
                    },
                    { error: Throwable ->
                        view?.showMessage(R.string.error_updating_position)
                        loggerFactory.logger.error("TrackDisplayPresenter: subscribeToTrackPositions", error)
                        crashReporter.logNonFatalException(TrackRenderException("position update", error))
                    }
                )
    }

    private fun discardPointsAboveAccuracyThreshold(points: List<Position>): List<Position> {
        return points.filter { point -> point.accuracy < Position.ACCURACY_THRESHOLD }
    }

    private fun updateTrackPositions(points: List<Position>) {
        loggerFactory.logger.debug("TrackDisplayPresenter: updateTrackPositions ***number of points ${points.count()}")
        if (isDisplayingPreviouslyRecordedTrack) {
            // we are not displaying the current recording track - we are displaying a previously recorded track (or the current recording track paused)
            if (points.isNotEmpty() && points[points.count()-1].trackId == trackToDisplay?.id) {
                // this is an recording update to the track we are currently displaying - its been paused and we have just got an update
                // we need to stop treating this track as if it was a previously recorded track and start taking updates
                loggerFactory.logger.debug("TrackDisplayPresenter: got an update to the track currently being displayed")
                wholeTrackShouldBeDrawn = true
                trackToDisplay = null
            } else {
                // we want to ignore these updates and carry on displaying the previously recorded track
                loggerFactory.logger.debug("TrackDisplayPresenter: ignoring the track update as we are displaying a track")
                return
            }
        }

        if (points.isEmpty()) {
            return
        }
        if (wholeTrackShouldBeDrawn) {
            loggerFactory.logger.debug("TrackDisplayPresenter: drawing the complete track")
            val filteredPoints = discardPointsAboveAccuracyThreshold(points)
            if (filteredPoints.isEmpty()) {
                return
            }
            refreshTitle()
            if (zoomToFitOnRedraw)
                centreMapAndZoomToFit(filteredPoints)
            else
                moveMapToPosition(filteredPoints[filteredPoints.count() - 1])
            view?.drawAllPositions(filteredPoints)
            wholeTrackShouldBeDrawn = false
            zoomToFitOnRedraw = false
        }
        else {
            loggerFactory.logger.debug("TrackDisplayPresenter: drawing the last position")
            val lastPosition  = points[points.count()-1]
            if (lastPosition.accuracy < Position.ACCURACY_THRESHOLD) {
                // just add a single point to the track being drawn
                view?.addPosition(lastPosition)
            }
        }
    }

    private fun centreMapAndZoomToFit(points: List<Position>) {
        view?.centreMapAndZoomToFit(points)
    }

    private fun moveMapToPosition(position: Position) {
        view?.centreMapAndZoom(position, zoomSettings.zoomLevel)
    }

    override fun refreshTitle() {
        var track: Track? = null
        if (isDisplayingPreviouslyRecordedTrack) {
            track = trackToDisplay!!
        }
        else if (trackBeingRecorded != null) {
            track = trackBeingRecorded!!
        }
        setTitle(track)
    }

    private fun setTitle(track: Track?) {
        if (track == null || track.id == -1L || track.name.isEmpty()) {
            view?.setTitle(resourceProvider.getString(R.string.app_name))
            return
        }
        if (isThisTrackCurrentlyBeingRecorded(track.id)) {
            view?.setTitle(String.format(resourceProvider.getString(R.string.main_title_recording_fmt), track.name))
            return
        } else if (isThisTrackCurrentlyPaused(track.id)) {
            view?.setTitle(String.format(resourceProvider.getString(R.string.main_title_paused_fmt), track.name))
            return
        }
        view?.setTitle(track.name)
    }

    private val currentTrackId: Long
        get() {
            if (trackToDisplay != null) {
                return trackToDisplay!!.id
            }
            if (trackBeingRecorded != null) {
                return trackBeingRecorded!!.id
            }
            return -1
        }

    private val currentTrackName: String
        get() {
            if (trackToDisplay != null) {
                return trackToDisplay!!.name
            }
            if (trackBeingRecorded != null) {
                return trackBeingRecorded!!.name
            }
            return "<UNKNOWN>"
        }

    private fun navigateToStatistics() {
        if (!isRecordingOrViewingTrack) {
            return
        }
        view?.navigateToStatistics(currentTrackId)
    }

    private val isRecordingOrViewingTrack
        get() = currentTrackId != -1L || locationTrackingService?.isTracking == true


    private fun getSuppliedTrackId(intent: Intent): Long {
        return intent.getLongExtra(PARAM_TRACK_ID, -1)
    }

    private fun getSuppliedImportUri(intent: Intent): Uri? {
        return intent.data
    }

    private fun isPromptForImportConfirmationRequired(intent: Intent): Boolean {
        return intent.getBooleanExtra(PARAM_PROMPT_FOR_IMPORT_CONFIRMATION, true)
    }

    private fun promptToImportTrack(importSource: String) {
        val prompt = String.format(resourceProvider.getString(R.string.import_track_prompt_fmt), importSource)
        view?.promptToImport(prompt, importSource)
    }

    // state
    private val STATE_KEY_DISPLAY_TRACK_ID = "TrackDisplayPresenter-display-track-id"
    private var trackToDisplay: Track? = null

    // Rx subscribers
    private var trackLoaderSubscriber: Disposable? = null
    private var trackExportSubscriber: Disposable? = null
    private var trackImportSubscriber: Disposable? = null
    private var trackBeingImported: Boolean = false

    // EventBus subscribers
    private var trackRenamedEventSubscriber: IEventBusSubscription? = null

    private var wholeTrackShouldBeDrawn: Boolean = false
    private var zoomToFitOnRedraw: Boolean = false

    private var trackPositionsSubscriber: Disposable? = null
    private var currentPositionSubscriber: Disposable? = null

}