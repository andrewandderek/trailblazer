/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.metadata.track

import com.andrewandderek.trailblazer.BuildConfig
import com.andrewandderek.trailblazer.data.repository.ITrackMetadataRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.model.TrackMetadata
import com.andrewandderek.trailblazer.statistics.ITrackStatisticsProvider
import javax.inject.Inject
import javax.inject.Named

interface ISingleTrackMetadataGenerator {
    fun generateMetadataForTrack(track: Track)
}

class SingleTrackMetadataGenerator @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val trackMetadataRepository: ITrackMetadataRepository,
    @Named(NameLiterals.STATISTICS_FULL) private val statisticsProvider: ITrackStatisticsProvider,
) : ISingleTrackMetadataGenerator
{
    /**
     * generates and stores the metadata fpr the track
     * the track must have been complete loaded including positions
     */
    override fun generateMetadataForTrack(track: Track) {
        loggerFactory.logger.debug("SingleTrackMetadataGenerator: generateMetadataForTrack - ${track.id}")

        // we are just going to overwrite whatever metadata there was
        val newMetadata = TrackMetadata()
        newMetadata.trackId = track.id
        newMetadata.versionCode = BuildConfig.VERSION_CODE      // value supplied from gradle file
        newMetadata.versionName = BuildConfig.VERSION_NAME      // value supplied from gradle file
        newMetadata.ended = track.lastPositionInSnapshot?.timeRecorded
        newMetadata.statistics = statisticsProvider.getStatistics(track)

        trackMetadataRepository.update(newMetadata)
    }
}