/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.units.conversion

import com.andrewandderek.trailblazer.units.DistanceUnit
import javax.inject.Inject

interface IDistanceConverter {
    fun convertMetres(metres: Double, units: DistanceUnit): Double
}

class DistanceConverter @Inject constructor()
    : IDistanceConverter
{
    override fun convertMetres(metres: Double, units: DistanceUnit): Double {
        return when (units) {
            DistanceUnit.Kilometres -> convertMetresToKilometres(metres)
            DistanceUnit.Miles -> convertMetresToMiles(metres)
        }
    }

    private fun convertMetresToKilometres(metres: Double): Double {
        return metres / Constants.METRES_PER_KILOMETRE.toDouble()
    }

    private fun convertMetresToMiles(metres: Double): Double {
        return metres / Constants.METRES_PER_MILE
    }
}