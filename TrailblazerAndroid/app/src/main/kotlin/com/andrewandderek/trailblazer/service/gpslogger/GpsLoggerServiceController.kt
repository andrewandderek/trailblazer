/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.service.gpslogger

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import javax.inject.Inject
import com.andrewandderek.trailblazer.logging.ILoggerFactory

interface IGpsLoggerServiceConnectionListener {
    fun connectService(serviceBinder: GpsLoggerService.LocalBinder)
    fun disconnectService()
}

/*
for all your service start/stop bind/unbind connect/disconnect needs
 */
interface IGpsLoggerServiceController {
    fun startService()
    fun stopService()
    fun bindToService(listener: IGpsLoggerServiceConnectionListener)
    fun unbindFromService()
}

class GpsLoggerServiceController
@Inject constructor(
        private val loggerFactory: ILoggerFactory,
        private val applicationContext: Context
)
    : IGpsLoggerServiceController, ServiceConnection
{
    private var connectionListener: IGpsLoggerServiceConnectionListener? = null

    override fun startService() {
        applicationContext.startService(serviceIntent)
    }

    override fun stopService() {
        applicationContext.stopService(serviceIntent)
    }

    private val serviceIntent: Intent
        get() = Intent(applicationContext, GpsLoggerService::class.java)

    override fun bindToService(listener: IGpsLoggerServiceConnectionListener) {
        loggerFactory.logger.debug("GpsLoggerServiceController: bindToService")
        connectionListener = listener
        applicationContext.bindService(serviceIntent, this, Context.BIND_AUTO_CREATE)
    }

    override fun unbindFromService() {
        loggerFactory.logger.debug("GpsLoggerServiceController: unbindFromService")
        applicationContext.unbindService(this)
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        loggerFactory.logger.debug("GpsLoggerServiceController: onServiceConnected ${name}")
        val binder = service as GpsLoggerService.LocalBinder
        connectionListener?.connectService(binder)
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        loggerFactory.logger.debug("GpsLoggerServiceController: onServiceDisconnected ${name}")
        connectionListener?.disconnectService()
        connectionListener = null
    }
}
