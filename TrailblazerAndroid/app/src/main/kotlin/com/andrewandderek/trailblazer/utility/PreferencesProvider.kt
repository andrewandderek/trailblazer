/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import javax.inject.Inject

interface IPreferencesProvider {
    fun setDefaultValues(settingsId: Int)
    fun getPreferenceString(key: String, defaultValue: String): String
    fun setPreferenceString(key: String, value: String)
    fun getPreferenceFloat(key: String, defaultValue: Float): Float
    fun setPreferenceFloat(key: String, value: Float)
    fun getPreferenceLong(key: String, defaultValue: Long): Long
    fun setPreferenceLong(key: String, value: Long)
    fun getPreferenceBoolean(key: String, defaultValue: Boolean): Boolean
    fun setPreferenceBoolean(key: String, value: Boolean)
}

class AndroidDefaultSharedPreferencesProvider
@Inject constructor(
        internal var context: Context
) : IPreferencesProvider {
    private fun getPreferences(): SharedPreferences {
        // by default the SettingsFragment uses the default shared preferences so we need to use them here as well
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    override fun setDefaultValues(settingsId: Int) {
        // by default the SettingsFragment uses the default shared preferences so we need to use them here as well
        PreferenceManager.setDefaultValues(context, settingsId, false)
    }

    override fun getPreferenceString(key: String, defaultValue: String): String {
        return getPreferences().getString(key, defaultValue) ?: defaultValue
    }

    override fun setPreferenceString(key: String, value: String) {
        getPreferences().edit()
                .putString(key, value)
                .apply()
    }

    override fun getPreferenceFloat(key: String, defaultValue: Float): Float {
        return getPreferences().getFloat(key, defaultValue)
    }

    override fun setPreferenceFloat(key: String, value: Float) {
        getPreferences().edit()
                .putFloat(key, value)
                .apply()
    }

    override fun getPreferenceLong(key: String, defaultValue: Long): Long {
        return getPreferences().getLong(key, defaultValue)
    }

    override fun setPreferenceLong(key: String, value: Long) {
        getPreferences().edit()
            .putLong(key, value)
            .apply()
    }

    override fun getPreferenceBoolean(key: String, defaultValue: Boolean): Boolean {
        return getPreferences().getBoolean(key, defaultValue)
    }

    override fun setPreferenceBoolean(key: String, value: Boolean) {
        getPreferences().edit()
            .putBoolean(key, value)
            .apply()
    }
}