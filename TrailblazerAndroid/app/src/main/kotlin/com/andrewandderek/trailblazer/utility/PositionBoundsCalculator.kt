/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.PositionBounds

class PositionBoundsCalculator {
    fun calculateBounds(positions: List<Position>): PositionBounds {
        if (positions.isEmpty())
            throw IllegalArgumentException("At least 1 position required to calculate bounds")

        var minLatitude = positions[0].latitude
        var minLongitude = positions[0].longitude
        var maxLatitude = minLatitude
        var maxLongitude = minLongitude

        positions.forEach {
            if (it.latitude < minLatitude)
                minLatitude = it.latitude
            if (it.longitude < minLongitude)
                minLongitude = it.longitude
            if (it.latitude > maxLatitude)
                maxLatitude = it.latitude
            if (it.longitude > maxLongitude)
                maxLongitude = it.longitude
        }

        return PositionBounds(
                createLatLongPosition(minLatitude, minLongitude),
                createLatLongPosition(maxLatitude, maxLongitude))
    }

    private fun createLatLongPosition(latitude: Double, longitude: Double): Position {
        return Position(latitude, longitude, null, 0)
    }
}