/*
 *  Copyright 2017-2020 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.compare

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView
import com.andrewandderek.trailblazer.ui.utility.ProgressViewHelper
import com.andrewandderek.trailblazer.ui.view.ProgressSpinnerView
import com.andrewandderek.trailblazer.ui.view.TextAndButtonView
import com.andrewandderek.trailblazer.ui.view.TrackDataItemView
import dagger.android.AndroidInjection
import javax.inject.Inject

class CompareActivity :
        AppCompatActivity(),
        SelectableStringListBottomSheetFragment.Listener
{
    companion object {
        const val TAG_SORT_BY_BOTTOMSHEET = "CompareActivity:SortByBottomSheet"
        const val TAG_SORT_DIRECTION_BOTTOMSHEET = "CompareActivity:SortDirectionBottomSheet"
        const val PARAM_TRACK_IDS = "trackIDs"

        fun createIntent(context: Context, trackIds: List<Long>): Intent {
            val intent = Intent(context,  CompareActivity::class.java)
            intent.putExtra(PARAM_TRACK_IDS, trackIds.toLongArray())
            return intent
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    @BindView(R.id.progressBar)
    protected lateinit var progressBar: ProgressSpinnerView

    @BindView(R.id.total_distance)
    lateinit var totalDistanceItem: TrackDataItemView

    @BindView(R.id.average_distance)
    lateinit var averageDistanceItem: TrackDataItemView

    @BindView(R.id.total_recorded_time)
    lateinit var totalRecodedTimeItem: TrackDataItemView

    @BindView(R.id.average_recorded_time)
    lateinit var averageRecodedTimeItem: TrackDataItemView

    @BindView(R.id.total_moving_time)
    lateinit var totalMovingTimeItem: TrackDataItemView

    @BindView(R.id.average_moving_time)
    lateinit var averageMovingTimeItem: TrackDataItemView

    @BindView(R.id.average_moving_speed)
    lateinit var averageMovingSpeedItem: TrackDataItemView

    @BindView(R.id.average_moving_pace)
    lateinit var averageMovingPaceItem: TrackDataItemView

    @BindView(R.id.compare_track_list_sort_direction_button)
    lateinit var trackListSortDirectionButton: TextAndButtonView

    @BindView(R.id.compare_track_list_sort_button)
    lateinit var trackListSortButton: TextAndButtonView

    @BindView(R.id.compare_track_list)
    protected lateinit var trackList: RecyclerView

    private lateinit var viewModel: CompareViewModel
    private lateinit var trackListAdapter: CompareTrackListRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("CompareActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compare)
        CheeseKnife.bind(this)

        trackList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        trackList.addItemDecoration(androidx.recyclerview.widget.DividerItemDecoration(this, androidx.recyclerview.widget.DividerItemDecoration.VERTICAL))
        trackListAdapter = CompareTrackListRecyclerAdapter(this)
        trackList.adapter = trackListAdapter

        viewModel = ViewModelProvider(this, viewModelFactory).get(CompareViewModel::class.java)
        lifecycle.addObserver(viewModel)

        val ids = intent.getLongArrayExtra(PARAM_TRACK_IDS)
        viewModel.init(ids!!)

        viewModel.observables.title.observe(this) {
            title = it
        }
        viewModel.observables.progressStart.observe(this) {
            ProgressViewHelper.startProgress(progressBar, window, it)
        }
        viewModel.observables.progressPosition.observe(this) {
            progressBar.progress = it
        }
        viewModel.observables.progressEnd.observe(this) {
            ProgressViewHelper.completeProgress(progressBar, window)
        }
        viewModel.observables.message.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
        viewModel.observables.summaryStatistics.observe(this) {
            totalDistanceItem.setValue(it.totalDistance, it.distanceUnits)
            averageDistanceItem.setValue(it.averageDistance, it.distanceUnits)
            totalMovingTimeItem.setValue(it.totalMovingTime, "")
            averageMovingTimeItem.setValue(it.averageMovingTime, "")
            totalRecodedTimeItem.setValue(it.totalRecordedTime, "")
            averageRecodedTimeItem.setValue(it.averageRecordedTime, "")
            averageMovingSpeedItem.setValue(it.averageMovingSpeed, it.speedUnits)
            averageMovingPaceItem.setValue(it.averageMovingPace, it.paceUnits)
        }
        viewModel.observables.sortedTrackList.observe(this) {
            trackListSortDirectionButton.text = it.listOrderLabel
            trackListSortButton.text = it.listSortLabel
            trackListAdapter.setTracks(it.tracksAndStatistics)
        }

        trackListSortButton.setOnClickListener {
            val sheet = SelectableStringListBottomSheetFragment.newInstance(
                    getString(R.string.compare_track_bottomsheet_sortby_title),
                    viewModel.getSortByOptions()
            )
            sheet.apply {
                show(supportFragmentManager, TAG_SORT_BY_BOTTOMSHEET)
            }
        }

        trackListSortDirectionButton.setOnClickListener {
            val sheet = SelectableStringListBottomSheetFragment.newInstance(
                    getString(R.string.compare_track_bottomsheet_sort_direction_title),
                    viewModel.getSortDirectionOptions()
            )
            sheet.apply {
                show(supportFragmentManager, TAG_SORT_DIRECTION_BOTTOMSHEET)
            }
        }
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("CompareActivity.onDestroy()")
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }

    override fun itemSelected(tag: String, position: Int) {
        when (tag) {
            TAG_SORT_BY_BOTTOMSHEET -> {
                viewModel.setSortBy(position)
            }
            TAG_SORT_DIRECTION_BOTTOMSHEET -> {
                viewModel.setSortDirection(position)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}