/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics

import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.TrackStatistics
import java.util.*
import javax.inject.Inject

/**
 * Calculate position/track distances.
 * All distances are in metres.
 */
class TrackDistanceCalculator
@Inject constructor(
        private val distanceProvider: IDistanceProvider
) : ITrackCalculator {

    override fun addPosition(trackStartTime: Calendar, positions: List<Position>, index: Int, statistics: TrackStatistics) {
        if (index == 0) {
            positions[index].distanceFromStart = 0.0
            return
        }

        if (!positions[index].isFromDifferentSegment(positions[index - 1]))
            statistics.distanceInMetres += getDistance(positions[index - 1], positions[index])

        positions[index].distanceFromStart = statistics.distanceInMetres
    }

    private fun getDistance(position1: Position, position2: Position): Double {
        return getDistance(position1.latitude, position1.longitude, position2.latitude, position2.longitude)
    }

    private fun getDistance(latitude1: Double, longitude1: Double, latitude2: Double, longitude2: Double): Double {
        return distanceProvider.getDistanceInMetres(latitude1, longitude1, latitude2, longitude2)
    }
}