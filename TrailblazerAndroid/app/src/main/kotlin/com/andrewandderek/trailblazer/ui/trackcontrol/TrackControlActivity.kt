/*
 *  Copyright 2017-2022 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.trackcontrol

import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.OnClick
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

abstract class TrackControlActivity :
    AppCompatActivity(),
    ITrackControlView
{
    @Inject
    lateinit var loggerFactory: ILoggerFactory

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(messageId: Int) {
        Toast.makeText(this, messageId, Toast.LENGTH_LONG).show()
    }

    override fun updateAvailableActions() {
        // Items on the action bar may need updating
        invalidateOptionsMenu()
        showFabsIfAvailable()
    }

    protected fun showFabsIfAvailable() {
        showFabIfAvailable(R.id.fab_start)
        showFabIfAvailable(R.id.fab_stop)
        showFabIfAvailable(R.id.fab_pause)
        showFabIfAvailable(R.id.fab_resume)
    }

    private fun showFabIfAvailable(itemId: Int) {
        val available = isActionAvailable(itemId)
        loggerFactory.logger.debug("TrackControlActivity showFabIfAvailable $itemId, $available")
        val fab = findViewById<FloatingActionButton>(itemId)
        fab?.visibility = if (available) View.VISIBLE else View.GONE
    }

    @OnClick(R.id.fab_start)
    fun onFabStart() {
        actionSelected(R.id.fab_start)
    }

    @OnClick(R.id.fab_stop)
    fun onFabStop() {
        actionSelected(R.id.fab_stop)
    }

    @OnClick(R.id.fab_pause)
    fun onFabPause() {
        actionSelected(R.id.fab_pause)
    }

    @OnClick(R.id.fab_resume)
    fun onFabResume() {
        actionSelected(R.id.fab_resume)
    }

    protected abstract fun isActionAvailable(actionId: Int): Boolean
    protected abstract fun actionSelected(itemId: Int): Boolean
}