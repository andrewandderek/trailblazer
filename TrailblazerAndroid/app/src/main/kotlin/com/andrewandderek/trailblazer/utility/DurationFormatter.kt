/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import kotlin.math.roundToLong

object DurationFormatter {
    private val MILLISECONDS_PER_MINUTE = 1000 * 60
    private val MILLISECONDS_PER_HOUR = MILLISECONDS_PER_MINUTE * 60

    private fun calculateHours(durationMilliseconds: Long): Long = durationMilliseconds / MILLISECONDS_PER_HOUR
    private fun calculateMinutes(durationMilliseconds: Long, hours: Long): Long = (durationMilliseconds - (hours * MILLISECONDS_PER_HOUR)) / MILLISECONDS_PER_MINUTE
    private fun calculateSeconds(durationMilliseconds: Long): Long = ((durationMilliseconds % MILLISECONDS_PER_MINUTE).toDouble() / 1000.0).roundToLong()

    fun formatMilliseconds(durationMilliseconds: Long): String {
        val hours = calculateHours(durationMilliseconds)
        val minutes = calculateMinutes(durationMilliseconds,hours)
        val seconds = calculateSeconds(durationMilliseconds)

        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    fun formatMillisecondsShort(durationMilliseconds: Long): String {
        val hours = calculateHours(durationMilliseconds)
        val minutes = calculateMinutes(durationMilliseconds,hours)

        return String.format("%02d:%02d", hours, minutes)
    }
}

