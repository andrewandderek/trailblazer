/*
 *  Copyright 2017-2024 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.data.utility

import com.andrewandderek.trailblazer.data.DatabaseHelper
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.TrackImportedEvent
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.utility.ICrashReporter
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.inject.Inject

interface IDatabaseCopier {
    // copy the app DB either import or export
    fun copyDb(sourceFullPathname: String, destinationFullPathname: String, updateUi: Boolean): Boolean
}

class DatabaseCopier @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val crashReporter: ICrashReporter,
    private val defaultDb: DatabaseHelper,
    private val eventBus: IEventBus,
    ) : IDatabaseCopier  {

    override fun copyDb(sourceFullPathname: String, destinationFullPathname: String, updateUi: Boolean): Boolean {
        try {
            loggerFactory.logger.debug("copyDb: copy from {} to {}", sourceFullPathname, destinationFullPathname)

            // the db should be closed either because we are overwriting it
            // or because all the journals need to be flushed before we copy it
            defaultDb.close()

            val sourceFile = File(sourceFullPathname)
            val destFile = File(destinationFullPathname)

            if (sourceFile.exists()) {
                var src = FileInputStream(sourceFile).channel
                var dst = FileOutputStream(destFile).channel
                var size = src.size()
                dst.transferFrom(src, 0, size)
                src.close()
                dst.close()
                loggerFactory.logger.debug("copyDatabaseFile: copier {} bytes from {} to {}", size, sourceFullPathname, destinationFullPathname)
            } else {
                loggerFactory.logger.debug("copyDatabaseFile: Cannot find database in path {}", sourceFullPathname)
                return false
            }
        } catch (e: Exception) {
            loggerFactory.logger.error("Error copying database: ", e)
            crashReporter.logNonFatalException(e)
            return false
        }
        finally {
            // the app expects it to be open
            defaultDb.open()
        }

        if (updateUi) {
            // the UI needs to refresh
            eventBus.publish(TrackImportedEvent(-1))
        }
        return true
    }

}