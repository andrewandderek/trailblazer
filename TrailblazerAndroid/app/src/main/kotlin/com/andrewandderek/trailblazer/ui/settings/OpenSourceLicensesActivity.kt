/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import dagger.android.AndroidInjection
import javax.inject.Inject
import android.graphics.Typeface
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.text.SpannableString
import android.text.Spannable
import android.text.Selection
import com.andrewandderek.trailblazer.ui.cheeseknife.CheeseKnife
import com.andrewandderek.trailblazer.ui.cheeseknife.annotations.BindView

class OpenSourceLicensesActivity : AppCompatActivity(), IOpenSourceLicensesView {

    @Inject lateinit var loggerFactory: ILoggerFactory
    @Inject lateinit var presenter: IOpenSourceLicensesPresenter

    @BindView(R.id.license_text)
    protected lateinit var licenseText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("OpenSourceLicensesActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opensourcelicenses)
        CheeseKnife.bind(this)

        presenter.bind(this)
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("OpenSourceLicensesActivity.onDestroy()")

        super.onDestroy()
        presenter.unbind(this)
    }

    private fun addText(textView: TextView, title: String, text: String) {
        loggerFactory.logger.debug("addText: title {}", title)

        val textTitle = SpannableString(title)
        val titleLength = textTitle.length
        textTitle.setSpan(UnderlineSpan(), 0, titleLength, 0)
        textTitle.setSpan(StyleSpan(Typeface.BOLD), 0, titleLength, 0)

        textView.append(textTitle)
        textView.append("\n\n")

        textView.append(text)

        textView.append("\n\n")
    }

    override fun addTextBlock(title: String, text: String) {
        addText(licenseText, title, text)
    }

    override fun scrollToTopOfText() {
        // Appending to the textview auto scrolls the text to the bottom - force it back to the top
        Selection.setSelection(licenseText.text as Spannable, 0)

    }
}