/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.event

/**
 * this is the event bus for the app
 * it could be implemented using any library for example otto or eventbus
 * but for now we are using a light weight hand rolled RxAndroid implementation
 */

interface IEventBusSubscription {
    fun unsubscribe()
}

interface IEventBus {
    /**
     * publish any event to the bus
     */
    fun publish(event: Any)

    /**
     * subscribe to a given event type with a default error handler
     * @param eventType, the type of event we are waiting for, we need to pass the actual type because of crappy JVM type erasure
     * @param receiver, code to execute when the event is triggered, its run on the android main thread if you want something else then schedule it
     * @return an IEventBusSubscription - remember to unsubscribe when you are no longer listening
     *
     * you can subscribe to all events like this, but then you must work out the type yourself
     *
     * var busSubscriber = eventBus.subscribe(Any::class.java)
     *  { event ->
     *   (event as? RecordingStatusChangedEvent)?.let {
     *      loggerFactory.logger.debug("recording status changed ${it.statusChangedTo}")
     *      view?.tracksLoaded()
     *   }
     *  }
     * busSubscriber?.unsubscribe()
     */
    fun <EVENT_TYPE> subscribe(eventType: Class<EVENT_TYPE>, receiver: (EVENT_TYPE) -> Unit): IEventBusSubscription

    /**
     * subscribe to a given event type with a default error handler
     * @param eventType, the type of event we are waiting for, we need to pass the actual type because of crappy JVM type erasure
     * @param receiver, code to execute when the event is triggered, its run on the android main thread if you want something else then schedule it
     * @param errorReceiver, code to execute when an error occurs, its run on the android main thread if you want something else then schedule it
     * @return an IEventBusSubscription - remember to unsubscribe when you are no longer listening
     */
    fun <EVENT_TYPE> subscribe(eventType: Class<EVENT_TYPE>, receiver: (EVENT_TYPE) -> Unit, errorReceiver: (Throwable) -> Unit): IEventBusSubscription
}

