/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.sharing.importexport

import com.andrewandderek.trailblazer.model.Track
import java.io.InputStream

enum class ImportResult {
    OK,                     // track loaded
    IO_ERROR,               // error accessing file or stream
    UNKNOWN_FORMAT,         // file opened but format is incorrect
    NO_TIMESTAMP,           // there are no timestamps in the file
    MISMATCHED_TIMESTAMP    // there are not the correct number of timestamps
}

data class ImportAndResult (
    val track: Track?,
    val result: ImportResult
)

interface IImporter {
    /**
     * attempt to import a track from the specified URI
     * will return as much of the track as it was able to parse
     * if it returns ImportResult.UNKNOWN_FORMAT then the input can be read but its not for this importer, try a different one
     * anything else returned indicates the input can be parsed OK or that its damaged, either way we should not retry
     */
    fun importTrack(uriString: String): ImportAndResult

    /**
     * this is the method that the base importer will call to unpack the specific format from the stream
     * it can also be used where we already have a stream available and dont need to decode the URI
     */
    fun importTrack(stream: InputStream): ImportAndResult
}