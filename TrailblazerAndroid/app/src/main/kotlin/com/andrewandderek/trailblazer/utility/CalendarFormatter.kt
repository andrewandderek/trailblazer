/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.utility

import java.text.SimpleDateFormat
import java.util.*

object CalendarFormatter {
    fun convertCalendarToDateTimeString(date: Calendar): String {
        val dateFormat = SimpleDateFormat("EEE d, MMM, yyyy HH:mm:ss", Locale.getDefault())
        return dateFormat.format(date.time)
    }

    fun convertCalendarToXmlSchemaDateTimeString(date: Calendar): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        // use UTC in the GPX file
        dateFormat.timeZone = TimeZone.getTimeZone("UTC");
        return dateFormat.format(date.time)
    }

    fun convertCalendarToSortableDate(date: Calendar): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return dateFormat.format(date.time)
    }

    fun convertCalendarToTrackName(date: Calendar): String {
        val dateFormat = SimpleDateFormat("d MMM, HH:mm", Locale.getDefault())
        return dateFormat.format(date.time)
    }

    fun convertCalendarToTimeString(date: Calendar): String {
        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        return dateFormat.format(date.time)
    }

    fun convertCalendarToShortTimeString(date: Calendar): String {
        val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        return dateFormat.format(date.time)
    }
}