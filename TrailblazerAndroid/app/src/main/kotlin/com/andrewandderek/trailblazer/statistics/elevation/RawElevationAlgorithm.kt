/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.statistics.elevation

import com.andrewandderek.trailblazer.model.Position

// Use raw differences to calculate elevation gain, without trying to filter out noise
// (Same as the threshold method using 0 as the threshold)
class RawElevationAlgorithm(
        altitudeProcessor: IAltitudeProcessor,
        elevationSettings: IElevationSettings
)
    : BaseElevationAlgorithm(altitudeProcessor, elevationSettings), IElevationAlgorithm {

    private lateinit var previousPosition: Position

    override fun reset(firstPosition: Position) {
        resetProcessor(firstPosition)
        previousPosition = firstPosition
    }

    override fun process(nextPosition: Position): ElevationGainAndLoss {
        processPosition(nextPosition)

        var gain = 0.0
        var loss = 0.0

        val altitude  = getAltitude(nextPosition)
        val previousAltitude = getAltitude(previousPosition)

        if (altitude > previousAltitude)
            gain = altitude - previousAltitude
        else
            loss = previousAltitude - altitude

        previousPosition = nextPosition

        return ElevationGainAndLoss(gain, loss)
    }
}