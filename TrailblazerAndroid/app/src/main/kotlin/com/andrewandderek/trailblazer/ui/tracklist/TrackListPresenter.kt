/*
 *  Copyright 2017-2019 Andrew Trevarrow and Derek Wilson
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.andrewandderek.trailblazer.ui.tracklist

import android.content.ActivityNotFoundException
import android.os.Bundle
import com.andrewandderek.trailblazer.R
import com.andrewandderek.trailblazer.data.repository.IPositionRepository
import com.andrewandderek.trailblazer.data.repository.ITrackRepository
import com.andrewandderek.trailblazer.di.NameLiterals
import com.andrewandderek.trailblazer.event.IEventBus
import com.andrewandderek.trailblazer.event.IEventBusSubscription
import com.andrewandderek.trailblazer.event.RecordingStatusChangedEvent
import com.andrewandderek.trailblazer.event.TrackImportedEvent
import com.andrewandderek.trailblazer.event.TrackMetadataUpdatedEvent
import com.andrewandderek.trailblazer.event.TrackRenamedEvent
import com.andrewandderek.trailblazer.exception.ExportException
import com.andrewandderek.trailblazer.exception.TrackCombineException
import com.andrewandderek.trailblazer.exception.TrackDeleteException
import com.andrewandderek.trailblazer.exception.TrackLoadException
import com.andrewandderek.trailblazer.exception.TrackSearchException
import com.andrewandderek.trailblazer.logging.ILoggerFactory
import com.andrewandderek.trailblazer.model.Position
import com.andrewandderek.trailblazer.model.Track
import com.andrewandderek.trailblazer.service.gpslogger.IGpsLoggerServiceController
import com.andrewandderek.trailblazer.sharing.importexport.IExporterFactory
import com.andrewandderek.trailblazer.sharing.importexport.gpx.GpxLiterals
import com.andrewandderek.trailblazer.ui.trackcontrol.TrackControlPresenter
import com.andrewandderek.trailblazer.units.formatting.IDistanceFormatter
import com.andrewandderek.trailblazer.units.formatting.IPaceFormatter
import com.andrewandderek.trailblazer.units.formatting.ISpeedFormatter
import com.andrewandderek.trailblazer.utility.AnalyticsConstants
import com.andrewandderek.trailblazer.utility.CalendarFormatter
import com.andrewandderek.trailblazer.utility.DurationFormatter
import com.andrewandderek.trailblazer.utility.IAnalyticsEngine
import com.andrewandderek.trailblazer.utility.ICrashReporter
import com.andrewandderek.trailblazer.utility.IFileSystemHelper
import com.andrewandderek.trailblazer.utility.IPermissionChecker
import com.andrewandderek.trailblazer.utility.IResourceProvider
import com.andrewandderek.trailblazer.utility.ISchedulerProvider
import com.andrewandderek.trailblazer.utility.ISystemTime
import com.andrewandderek.trailblazer.utility.ITextMatcher
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.FlowableEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.security.InvalidParameterException
import java.util.Calendar
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class TrackListPresenter
    @Inject constructor(
        private val loggerFactory: ILoggerFactory,
        private val schedulerProvider: ISchedulerProvider,
        private val trackRepository: ITrackRepository,
        private val positionRepository: IPositionRepository,
        private val resourceProvider: IResourceProvider,
        trackingServiceController: IGpsLoggerServiceController,
        private val crashReporter: ICrashReporter,
        eventBus: IEventBus,
        private val exporterFactory: IExporterFactory,
        private val analyticsEngine: IAnalyticsEngine,
        private val permissionChecker: IPermissionChecker,
        private val fileSystemHelper: IFileSystemHelper,
        private val textMatcher: ITextMatcher,
        private val systemTime: ISystemTime,
        private val distanceFormatter: IDistanceFormatter,
        private val paceFormatter: IPaceFormatter,
        private val speedFormatter: ISpeedFormatter,
        )
    : TrackControlPresenter<ITrackListView>(loggerFactory, schedulerProvider, trackingServiceController, permissionChecker, analyticsEngine, crashReporter, eventBus),
    ITrackListPresenter {

    companion object {
        // This is a compromise: because the update of the track list is animated, setting this too
        // low causes too many animated updates (so doesn't look great), and setting it too high
        // means the user waits too long for the list to update once they've finished typing.
        private const val SEARCH_TEXT_DEBOUNCE_TIME_MILLISECONDS = 250L
    }

    override fun onLocationTrackingServiceConnected() {
        loggerFactory.logger.debug("TrackListPresenter.onLocationTrackingServiceConnected")
        super.onLocationTrackingServiceConnected()
        // the state might have changed when the service started - because it reloaded saved state
        // so lets refresh the track list
        view?.tracksLoaded()
    }

    override val itemCount: Int
        get() {
            return visibleTracks.size
        }

    override fun getTrackId(position: Int): Long {
        if (position > itemCount-1) {
            return -1
        }
        val track = visibleTracks[position]
        return track.id
    }

    override var inMultiSelectMode: Boolean = false
        set(value) {
            field = value
            if (!value) {
                // we are switching the multiselect mode off
                clearSelections()
            }
        }

    override var trackNameSearch: String? = null
        private set(value) {
            val newValue = if (value.isNullOrEmpty()) null else value
            if (field != newValue) {
                field = newValue
                onTrackListUpdated()
            }
        }

    private val trackNameSearchSubject: PublishSubject<String> = PublishSubject.create()

    private val selectedPositions = ArrayList<Int>()

    override fun isItemSelected(position: Int): Boolean {
        return selectedPositions.contains(position)
    }

    private fun clearSelections() {
        loggerFactory.logger.debug("TrackListPresenter.clearSelections")
        selectedPositions.clear()
        view?.updateList(getSelectionTitle())
    }

    private fun getSelectedItemCount(): Int {
        return selectedPositions.size
    }

    private fun toggleSelection(pos: Int) {
        if (selectedPositions.contains(pos)) {
            selectedPositions.remove(pos)
        } else {
            selectedPositions.add(pos)
        }
        view?.updateList(getSelectionTitle())
    }

    private fun selectAllVisibleTracks() {
        loggerFactory.logger.debug("TrackListPresenter.selectAllVisibleTracks")
        selectedPositions.clear()
        selectedPositions.addAll(visibleTracks.indices)
        view?.updateList(getSelectionTitle())
    }

    private fun getSelectionTitle(): String {
        return String.format(resourceProvider.getString(R.string.track_list_multi_select_action_mode_title_fmt), getSelectedItemCount())
    }

    private fun getSelectedItemPositions(): List<Int> {
        return ArrayList<Int>(selectedPositions.sortedBy { p -> p })
    }

    private fun getSelectedTrackIds(): List<Long> {
        val itemPositions = getSelectedItemPositions()
        return itemPositions.map { visibleTracks[it].id }
    }

    private fun getSelectedTrackIdsSortedByStartTime(): List<Long> {
        val itemPositions = getSelectedItemPositions()
        val tracks = itemPositions.map { visibleTracks[it] }.sortedBy { it.started }
        return tracks.map { it.id }
    }

    override fun getTrackLabel(position: Int): String {
        if (position > itemCount-1) {
            return resourceProvider.getString(R.string.track_list_item_unknown)
        }
        val track = visibleTracks[position]
        if (isThisTrackCurrentlyBeingRecorded(track.id)) {
            return String.format(resourceProvider.getString(R.string.track_list_item_title_recording_fmt), track.name)
        } else if (isThisTrackCurrentlyPaused(track.id)) {
            return String.format(resourceProvider.getString(R.string.track_list_item_title_paused_fmt), track.name)
        }
        return String.format(resourceProvider.getString(R.string.track_list_item_title_fmt), track.name)
    }

    override fun getTrackSubLabel(position: Int): String {
        if (position > itemCount-1) {
            return resourceProvider.getString(R.string.track_list_item_unknown)
        }
        val track = visibleTracks[position]
        val metadata = track.metadata
        return if (metadata == null || isThisTrackCurrentlyBeingRecorded(track.id) || isThisTrackCurrentlyPaused(track.id) ) {
            // we only have the start time as the metadata has not been generated or the track hasnt been completed
            String.format(resourceProvider.getString(R.string.track_list_item_subtitle_1_fmt), CalendarFormatter.convertCalendarToDateTimeString(track.started))
        } else {
            // we have extra data as the metadata has been loaded
            var retval = String.format(resourceProvider.getString(R.string.track_list_item_subtitle_1_fmt), CalendarFormatter.convertCalendarToDateTimeString(track.started))
            val endTime: Calendar? = metadata.ended
            if (endTime != null) {
                retval = retval.plus(
                    String.format(
                        resourceProvider.getString(R.string.track_list_item_subtitle_2_fmt),
                        CalendarFormatter.convertCalendarToTimeString(endTime))
                )
            }
            retval = retval.plus(
                String.format(
                    resourceProvider.getString(R.string.track_list_item_subtitle_3_fmt),
                    distanceFormatter.formatDistance(metadata.statistics.distanceInMetres, true),
                    DurationFormatter.formatMilliseconds(metadata.statistics.movingTimeInMilliseconds),
                    paceFormatter.formatPace(metadata.statistics.averageMovingSpeedInMetresPerSecond, true),
                    speedFormatter.formatSpeed(metadata.statistics.averageMovingSpeedInMetresPerSecond, true),
                ))
            retval
        }
    }

    override fun trackSelected(trackId: Long, position: Int) {
        loggerFactory.logger.debug("TrackListPresenter.trackSelected ${trackId} ${position}")
        if (trackId < 0) {
            return
        }
        if (inMultiSelectMode) {
            toggleSelection(position)
        } else {
            view?.navigateToTrack(trackId)
        }
    }

    override fun trackLongSelected(trackId: Long, position: Int): Boolean {
        loggerFactory.logger.debug("TrackListPresenter.trackLongSelected ${trackId} ${position}")
        if (!inMultiSelectMode) {
            view?.startMultiSelectMode()
        }
        toggleSelection(position)
        return true
    }

    override fun editTrackName(trackId: Long): Boolean {
        loggerFactory.logger.debug("TrackListPresenter.editTrack ${trackId}")
        if (trackId < 0) {
            return true
        }
        val track = tracks.find { track -> track.id == trackId }
        view?.promptForTrackName(track!!.id.toString(), track.name)
        return true
    }

    override fun confirmedEditName(trackId: String, value: String) {
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_ITEM_RENAME_ID, AnalyticsConstants.ACTION_ITEM_RENAME_NAME, AnalyticsConstants.ACTION_TYPE)
        val id: Long = trackId.toLong()
        val track = tracks.find { track -> track.id == id }
        track?.let {
            val event = TrackRenamedEvent(id, it.name, value)
            it.name = value
            trackRepository.update(it)
            // publish an event so the UI gets updated
            // at the moment we are the only UI that cares but that might not always be the case
            eventBus.publish(event)
        } ?: run {
            loggerFactory.logger.warn("TrackListPresenter.confirmedEditName ${trackId}, cannot find track")
            crashReporter.logNonFatalException(InvalidParameterException("Track ID ${trackId}"))
        }
    }

    private fun getDeletePrompt(track: Track): String {
        if (isThisTrackCurrentlyBeingRecorded(track.id) || isThisTrackCurrentlyPaused(track.id)) {
            return String.format(resourceProvider.getString(R.string.track_list_delete_recording_prompt_fmt), track.name)
        }
        return String.format(resourceProvider.getString(R.string.track_list_delete_prompt_fmt), track.name)
    }

    override fun deleteTrack(trackId: Long): Boolean {
        loggerFactory.logger.debug("TrackListPresenter.deleteTrack ${trackId}")
        if (trackId < 0) {
            return true     // ignore
        }
        if (trackBeingDeleted) {
            // we are already doing a delete - lets wait
            view?.showMessage(R.string.track_being_deleted)
            return true
        }
        val track = tracks.find { track -> track.id == trackId }
        val prompt = getDeletePrompt(track!!)
        view?.promptToDelete(prompt, track.id.toString())
        return true
    }

    override fun confirmedDeleteTrack(trackId: String) {
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_ITEM_DELETE_ID, AnalyticsConstants.ACTION_ITEM_DELETE_NAME, AnalyticsConstants.ACTION_TYPE)
        val id: Long = trackId.toLong()
        trackDeleteSubscriber?.dispose()
        trackBeingDeleted = true
        trackDeleteSubscriber = Flowable.fromCallable { trackRepository.deleteById(id) }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        {
                            if (isThisTrackCurrentlyBeingRecorded(id) || isThisTrackCurrentlyPaused(id)) {
                                // get the UI back in sync
                                locationTrackingService?.stopTracking(false)
                            }
                            renderDelete(id)
                            view?.showMessage(R.string.track_deleted)
                            trackBeingDeleted = false
                        },
                        { error: Throwable ->
                            view?.showMessage(R.string.delete_track_error)
                            loggerFactory.logger.error("deleteTrack", error)
                            trackBeingDeleted = false
                        }
                )
    }

    private fun deleteSelectedTracks() {
        loggerFactory.logger.debug("TrackListPresenter.deleteSelectedTracks")
        val ids = getSelectedTrackIds()
        if (ids.isEmpty()) {
            return     // ignore
        }
        if (trackBeingDeleted) {
            // we are already doing a delete - lets wait
            view?.showMessage(R.string.track_being_deleted)
            return
        }
        loggerFactory.logger.debug("TrackListPresenter.deleteSelectedTracks ids= ${ids}")
        val prompt = getDeleteMultiplePrompt(ids)
        view?.promptToDeleteMultiple(prompt)
    }

    private fun getDeleteMultiplePrompt(ids: List<Long>): String {
        val isRecording = ids.any {
            isThisTrackCurrentlyBeingRecorded(it) || isThisTrackCurrentlyPaused(it)
        }
        if (isRecording) {
            return String.format(resourceProvider.getString(R.string.track_list_multi_delete_recording_prompt_fmt), ids.size)
        }
        return String.format(resourceProvider.getString(R.string.track_list_multi_delete_prompt_fmt), ids.size)
    }

    override fun confirmedMultipleDeleteTrack() {
        val ids = getSelectedTrackIds()
        val isRecording = ids.any {
            isThisTrackCurrentlyBeingRecorded(it) || isThisTrackCurrentlyPaused(it)
        }
        if (isRecording) {
            // get the UI back in sync - stop the track before we start to delete in case any additional data is written
            locationTrackingService?.stopTracking(false)
        }
        view?.startProgress(R.string.track_multi_delete_progress)
        trackDeleteSubscriber?.dispose()
        trackBeingDeleted = true
        trackDeleteSubscriber = Flowable.fromCallable { deleteMultiple(ids) }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        {
                            view?.completeProgress()
                            for (index in ids.indices) {
                                if (!renderDelete(ids[index])) {
                                    // we had to reload
                                    break
                                }
                            }
                            view?.showMessage(String.format(resourceProvider.getString(R.string.track_multi_deleted_fmt), ids.size))
                            trackBeingDeleted = false
                        },
                        { error: Throwable ->
                            view?.completeProgress()
                            view?.showMessage(R.string.delete_multi_track_error)
                            loggerFactory.logger.error("deleteMultipleTrack", error)
                            trackBeingDeleted = false
                            crashReporter.logNonFatalException(TrackDeleteException("IDs ${ids}", error))
                        }
                )
    }

    private fun deleteMultiple(ids: List<Long>) {
        // TODO - we could consider writing a deleteMultiple in the repo that would: delete where ID in [x,y,z]
        for (index in ids.indices) {
            trackRepository.deleteById(ids[index])
        }
    }

    private fun renderDelete(trackId: Long): Boolean {
        // try and do the fancy animation
        val track = tracks.find { track -> track.id == trackId }
        if (track != null) {
            tracks = tracks.minus(track)
            onTrackListUpdated()
            setTitle()
            return true     // we were successful
        }

        // we could not work it out so rebuild the list
        tracks = emptyList()
        onTrackListUpdated()
        loadTracks()
        return false
    }

    override fun onActionItemClicked(itemId: Int): Boolean {
        when (itemId) {
            R.id.action_track_delete -> {
                deleteSelectedTracks()
                return true
            }
            R.id.action_export_track -> {
                exportSelectedTracks()
                return true
            }
            R.id.action_combine_track -> {
                combineSelectedTracks()
                return true
            }
            R.id.action_compare_track -> {
                analyticsEngine.recordClick(AnalyticsConstants.ACTION_COMPARE_ID, AnalyticsConstants.ACTION_COMPARE_NAME, AnalyticsConstants.ACTION_TYPE)
                view?.navigateToCompare(getSelectedTrackIds())
                return true
            }
            R.id.action_select_all -> {
                selectAllVisibleTracks()
                return true
            }
        }
        return false
    }

    private fun combineSelectedTracks() {
        val ids = getSelectedTrackIds()
        if (ids.size < 2) {
            view?.showMessage(R.string.combine_select_error)
            return
        }
        var name = ""
        val track = tracks.find { track -> track.id == ids[0] }
        track?.let {
            name = String.format(resourceProvider.getString(R.string.combine_track_name_fmt), it.name)
        }
        view?.promptForCombinedTrackName(name)
    }

    override fun confirmedCombineSelectedTracks(name: String) {
        // we sort from oldest to newest track as they need to be added in order
        val ids = getSelectedTrackIdsSortedByStartTime()
        val numberOfSteps = ids.size + 1        // we also have a step to save the created new track as well as load all the tracks
        loggerFactory.logger.debug("TrackListPresenter.confirmedCombineSelectedTracks ${name} ${ids}")
        view?.startProgress(R.string.track_multi_combining_progress, numberOfSteps)
        trackCombineSubscriber?.dispose()
        trackCombineSubscriber = Flowable.create<List<Long>>(
            {
                    emitter -> combineTracks(name, ids, emitter)
            }, BackpressureStrategy.BUFFER)
            .subscribeOn(schedulerProvider.ioThread())
            .observeOn(schedulerProvider.androidMainThread())
            .doOnNext(
                { processedIds: List<Long> ->
                    loggerFactory.logger.debug("***combineTracks ${processedIds.size} of ${numberOfSteps}")
                    view?.updateProgress(processedIds.size)
                }
            )
            .doOnComplete(
                {
                    loggerFactory.logger.debug("***combineTracks complete")
                    view?.completeProgress()
                    view?.showMessage(String.format(resourceProvider.getString(R.string.track_multi_combined_fmt), ids.size))
                }
            )
            .subscribe(
                {
                },
                { error: Throwable ->
                    view?.completeProgress()
                    view?.showMessage(R.string.track_combine_error)
                    loggerFactory.logger.error("combineTracks", error)
                    crashReporter.logNonFatalException(TrackCombineException("IDs ${ids}", error))
                }
            )
    }

    private fun combineTracks(name: String, ids: List<Long>, emitter: FlowableEmitter<List<Long>>) {
        val processedIds = ArrayList<Long>()
        var combinedTrack = Track()
        combinedTrack.started = systemTime.getCurrentTime()
        combinedTrack.name = name
        // we need to create the track so it has an ID, we need this for the positions
        combinedTrack =  trackRepository.create(combinedTrack)
        loggerFactory.logger.debug("created combined track ${combinedTrack.id}")
        // load and add each track
        for (id in ids) {
            val track = trackRepository.getWithPositionsAndAccuracy(id, Position.ACCURACY_THRESHOLD, positionRepository)
            track?.let {
                loggerFactory.logger.debug("combined track ${combinedTrack.currentPositionsSnapshot.size} positions, last seg ${combinedTrack.lastPositionInSnapshotSegment}")
                loggerFactory.logger.debug("adding track ${it.name}, ${it.currentPositionsSnapshot.size} positions, last seg ${it.lastPositionInSnapshotSegment}")
                if (combinedTrack.isEmpty) {
                    // this is the first track to be added
                    // we also need to take the start time for the track
                    combinedTrack.started = it.started
                }
                combinedTrack.addTrack(it)
                processedIds.add(it.id)
                loggerFactory.logger.debug("zfter combining track ${combinedTrack.currentPositionsSnapshot.size} positions, last seg ${combinedTrack.lastPositionInSnapshotSegment}")
                // ping the progress
                emitter.onNext(processedIds)
            }
        }
        // save the new track positions
        loggerFactory.logger.debug("creating positions ${combinedTrack.currentPositionsSnapshot.size}")
        // update the track
        trackRepository.update(combinedTrack)
        // create all the positions
        positionRepository.createTrack(combinedTrack.currentPositionsSnapshot)
        processedIds.add(combinedTrack.id)
        emitter.onNext(processedIds)
        loggerFactory.logger.debug("positions created")
        eventBus.publish(TrackImportedEvent(combinedTrack.id))
        emitter.onComplete()
        loggerFactory.logger.debug("combine complete")
    }

    override fun exportSelectedTracks() {
        if (!permissionChecker.hasWriteStoragePermission()) {
            view?.requestWriteExternalStoragePermission()
            return
        }
        view?.promptForFileFormat(resourceProvider.getString(R.string.file_format_export_prompt), NameLiterals.FILE_FORMAT_GPX)
    }

    override fun confirmedMultipleExportTrack(format: String) {
        val ids = getSelectedTrackIds()
        loggerFactory.logger.debug("TrackListPresenter.confirmedMultipleExportTrack ${format} ${ids}")
        view?.startProgress(R.string.track_multi_exporting_progress, ids.size)
        trackExportSubscriber?.dispose()
        trackExportSubscriber = Flowable.create<List<String>>(
                {
                    emitter -> writeTracksInFormat(ids, format, emitter)
                }, BackpressureStrategy.BUFFER)
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .doOnNext(
                        { filenames: List<String> ->
                            loggerFactory.logger.debug("***exportTracks ${filenames.size} of ${ids.size}")
                            view?.updateProgress(filenames.size)
                        }
                )
                .doOnComplete(
                        {
                            loggerFactory.logger.debug("***exportTracks complete ${ids.size}")
                            view?.completeProgress()
                            view?.showMessage(String.format(resourceProvider.getString(R.string.track_multi_exported_fmt), ids.size))
                        }
                )
                .subscribe(
                        {
                        },
                        { error: Throwable ->
                            view?.completeProgress()
                            view?.showMessage(R.string.track_export_error)
                            loggerFactory.logger.error("exportTracks", error)
                            crashReporter.logNonFatalException(ExportException("IDs ${ids} format=${format}", error))
                        }
                )
    }

    private fun writeTracksInFormat(ids: List<Long>, format: String, emitter: FlowableEmitter<List<String>>) {
        val filenames = ArrayList<String>()
        val exporter = exporterFactory.getExporter(format)
        for (id in ids) {
            val track = trackRepository.getWithPositionsAndAccuracy(id, Position.ACCURACY_THRESHOLD, positionRepository)
            track?.let {
                val filename = exporter.getExportFilename(it.startDateAndName, true)
                exporter.exportTrack(it, filename)
                filenames.add(filename)
                // ping the progress
                emitter.onNext(filenames)

                when (format) {
                    NameLiterals.FILE_FORMAT_GPX -> {
                        analyticsEngine.recordClick(AnalyticsConstants.ACTION_EXPORT_ID, AnalyticsConstants.ACTION_EXPORT_NAME, AnalyticsConstants.ACTION_TYPE)
                    }
                    NameLiterals.FILE_FORMAT_KML -> {
                        analyticsEngine.recordClick(AnalyticsConstants.ACTION_EXPORT_KML_ID, AnalyticsConstants.ACTION_EXPORT_KML_NAME, AnalyticsConstants.ACTION_TYPE)
                    }
                }
            }
        }
        emitter.onComplete()
    }

    private fun setTitle() {
        if (itemCount > 1) {
            view?.setTitle(String.format(resourceProvider.getString(R.string.track_list_title_fmt), itemCount))
        } else {
            // for zero or one tracks we can rely on the user to count
            view?.setTitle(resourceProvider.getString(R.string.track_list_title))
        }
    }

    override fun loadTracks() {
        loggerFactory.logger.debug("TrackListPresenter.loadTracks")
        view?.startProgress()
        trackLoaderSubscriber?.dispose()
        trackLoaderSubscriber = Flowable.fromCallable { trackRepository.getTrackListWithoutAnyPositions() }
                .subscribeOn(schedulerProvider.ioThread())
                .observeOn(schedulerProvider.androidMainThread())
                .subscribe(
                        { loadedTracks ->
                            tracks = loadedTracks
                            onTrackListUpdated()
                            loggerFactory.logger.debug("TrackListPresenter tracks loaded ${itemCount}")
                            setTitle()
                            view?.completeProgress()
                        },
                        { error: Throwable ->
                            view?.showMessage(R.string.load_track_error)
                            loggerFactory.logger.error("loadTrack", error)
                            view?.completeProgress()
                            crashReporter.logNonFatalException(TrackLoadException("load track list", error))
                        }
                )
    }

    protected override fun onRecordingStatusChanged(event: RecordingStatusChangedEvent) {
        loggerFactory.logger.debug("TrackListPresenter: onRecordingStatusChanged: recording status changed ${event.statusChangedTo}, ${event.trackId}, ${event.fromNotification}")
        super.onRecordingStatusChanged(event)
        onTrackListUpdated()
        setTitle()

        if (event.shouldPromptForName) {
            loggerFactory.logger.debug("TrackListPresenter: onRecordingStatusChanged: ${event.trackId} - stopped")
            if (event.trackId < 0) {
                return
            }
            val track = tracks.find { track -> track.id == event.trackId }
            if (track == null) {
                loggerFactory.logger.debug("TrackListPresenter: onRecordingStatusChanged: cannot find track ${event.trackId}")
                return
            }

            // remember events fire async - we may not be on top and then we will not be able to prompt here
            view?.promptForTrackNameAfterStopping(track.id.toString(), track.name)
        }
    }

    override fun bind(theView: ITrackListView, savedInstanceState: Bundle?) {
        loggerFactory.logger.debug("TrackListPresenter.bind")
        super.bind(theView, savedInstanceState)

        trackMetadataUpdatedEventSubscriber = eventBus.subscribe(TrackMetadataUpdatedEvent::class.java)
        {
            // just treat it as if a track was imported which refreshes the list
            onTrackImported()
        }
        trackImportedEventSubscriber = eventBus.subscribe(TrackImportedEvent::class.java)
        {
            onTrackImported()
        }
        trackRenamedEventSubscriber = eventBus.subscribe(TrackRenamedEvent::class.java)
        {
            onTrackRenamed(it.trackId, it.oldName, it.newName)
        }
    }

    override fun unbind(theView: ITrackListView) {
        loggerFactory.logger.debug("TrackListPresenter.unbind")
        super.unbind(theView)

        trackLoaderSubscriber?.dispose()
        trackLoaderSubscriber = null
        trackDeleteSubscriber?.dispose()
        trackDeleteSubscriber = null
        trackExportSubscriber?.dispose()
        trackExportSubscriber = null
        trackCombineSubscriber?.dispose()
        trackCombineSubscriber = null
        trackNameSearchObserver?.dispose()
        trackNameSearchObserver = null
        trackMetadataUpdatedEventSubscriber?.unsubscribe()
        trackMetadataUpdatedEventSubscriber = null
        trackImportedEventSubscriber?.unsubscribe()
        trackImportedEventSubscriber = null
        trackRenamedEventSubscriber?.unsubscribe()
        trackRenamedEventSubscriber = null
    }

    override fun isActionAvailable(actionId: Int): Boolean {
        if (isTrackControlAction(actionId)) {
            return !inSearchMode && super.isActionAvailable(actionId)
        }
        when (actionId) {
            R.id.action_settings,
            R.id.action_import_track,
            R.id.action_search -> {
                return true
            }
        }
        return false
    }

    override fun actionSelected(itemId: Int): Boolean {
        loggerFactory.logger.debug("TrackListPresenter: actionSelected ${itemId}")
        if (super.actionSelected(itemId)) {
            return true
        }
        when (itemId) {
            R.id.action_settings -> {
                view?.navigateToSettings()
                return true
            }
            R.id.action_import_track -> {
                if (!permissionChecker.hasReadStoragePermission()) {
                    view?.requestReadExternalStoragePermission()
                    return true
                }
                importTrack()
                return true
            }
        }
        return false
    }

    override fun importTrack() {
        analyticsEngine.recordClick(AnalyticsConstants.ACTION_IMPORT_SELECTED_ID, AnalyticsConstants.ACTION_IMPORT_SELECTED_NAME, AnalyticsConstants.ACTION_TYPE)
        try {
            // this activity/presenter just prompts the user for a file using SAF
            // the actual import is handled un the Track Display Activity
            // the list gets updated via the event bus
            view?.promptForFile()
        }
        catch (ex: ActivityNotFoundException) {
            // not supported on this OS
            // we prompt for filenames using the SAF (structured access framework) which was added in API 19 (kitkat 4.4)
            // TODO in earlier versions of the OS we should do do something better than this but to be honest we dont have any users of earlier OS's
            val filename = "file://${ fileSystemHelper.getApplicationFolderOnSdCard()}import.${GpxLiterals.GPX_EXTENSION}"
            view?.navigateToImportTrack(filename, true)
        }
    }

    override fun trackNameSearchTextChange(newText: String?) {
        trackNameSearchSubject.onNext(newText ?: "")
    }

    override fun trackNameSearchTextClear() {
        trackNameSearch = null
    }

    override fun searchActionStart() {
        inSearchMode = true
        view?.hideNonSearchUi()
        // We want to display different text when the search gives no results
        view?.setEmptyListText(R.string.no_matching_tracks_text)
        trackNameSearchObserver?.dispose()
        trackNameSearchObserver = trackNameSearchSubject
            .debounce(SEARCH_TEXT_DEBOUNCE_TIME_MILLISECONDS, TimeUnit.MILLISECONDS, schedulerProvider.computation())
            .distinctUntilChanged()
            .switchMap { s -> Observable.just(s) }
            .observeOn(schedulerProvider.androidMainThread())
            .subscribe(
                {
                    query -> trackNameSearch = query
                },
                {
                    error: Throwable ->
                    loggerFactory.logger.error("trackNameSearchError", error)
                    crashReporter.logNonFatalException(TrackSearchException("Track search", error))
                }
            )
    }

    override fun searchActionEnd() {
        inSearchMode = false
        view?.showNonSearchUi()
        trackNameSearch = null
        view?.setEmptyListText(R.string.no_tracks_text)
    }

    protected override fun onTrackStarted(track: Track?) {
        super.onTrackStarted(track)
        insertNewTrackIntoList(track)
        view?.navigateToTrack(-1)
    }

    private fun insertNewTrackIntoList(track: Track?) {
        track?.let {
            // We just insert into the in-memory list, no need to force the UI to update as we're
            // navigating away - user will see the new track in the list when they navigate back
            tracks = listOf(it).plus(tracks)
        }
    }

    private fun onTrackImported() {
        loadTracks()
    }

    private fun onTrackRenamed(trackId: Long, oldName: String, newName: String) {
        loggerFactory.logger.debug("TrackListPresenter: onTrackRenamed ${trackId}, ${oldName} -> ${newName}")
        // try and do the efficient update
        val track = tracks.find { track -> track.id == trackId }
        if (track != null) {
            // we have loaded a copy of the DB record, update the in-memory copy
            track.name = newName
            onTrackListUpdated()
        } else {
            // we could not work it out so rebuild the list
            loadTracks()
        }
    }

    private fun onTrackListUpdated() {
        visibleTracks = tracks
        trackNameSearch?.let { searchText ->
            visibleTracks = visibleTracks.filter { t -> textMatcher.isMatch(t.name, searchText) }
        }
        view?.tracksLoaded()
    }

    private var tracks: List<Track> = emptyList()
    private var visibleTracks: List<Track> = emptyList()

    // Rx subscribers
    private var trackLoaderSubscriber: Disposable? = null
    private var trackDeleteSubscriber: Disposable? = null
    private var trackExportSubscriber: Disposable? = null
    private var trackCombineSubscriber: Disposable? = null
    private var trackNameSearchObserver: Disposable? = null

    // EventBus subscribers
    private var trackMetadataUpdatedEventSubscriber: IEventBusSubscription? = null
    private var trackImportedEventSubscriber: IEventBusSubscription? = null
    private var trackRenamedEventSubscriber: IEventBusSubscription? = null

    private var trackBeingDeleted: Boolean = false
    private var inSearchMode: Boolean = false
}
