# Trailblazer Privacy Policy #

Trailblazer is an application written by Andrew Trevarrow and Derek Wilson.

This policy describes the data that is collected by Andrew Trevarrow and Derek Wilson.


## What information is collected? ##

None. No personally identifiable information is collected by the app. 


## How is this information processed? ##

No information is collected.


## How do I know that this is true? ##

Trailblazer is open source and [this repository](https://bitbucket.org/andrewandderek/trailblazer/src/master/) contains the source code for the application. You can check the code.


## How do I ensure that all of the app data is removed if I want to un-install ##

All of the support files are removed when you un-install the app, including any tracks you have recorded. The only additional files that you may want to remove are in a folder called Trailblazer on your SD card storage, and these will only be present if you have exported tracks.

