# Release Notes for the Play Store listing

These are the release notes for the Play Store listing.

## v1.14.0 (27) - 22 Nov 2024
- Added Export DB and Import DB options to settings page
- Added Logging Level option to settings page
- Fixed a bug where the tracklist was not correctly updated when a track is started or stopped
- Target API 34
- Min API 21

## v1.13.0 (26) - 3 Oct 2024
- Track list search now updates as you type
- Added "Select all" menu item when in select mode (once you have selected at least one track)
- Target API 34
- Min API 21

## v1.12.0 (25) - 8 Aug 2024
- Track distance is now displayed in the track list
- DB Version 8
- Target API 33
- Min API 19

## v1.11.0 (24) - 25 Jul 2024
- Track end recording time is now displayed in the track list
- DB Version 7
- Target API 33
- Min API 19

## v1.10.0 (23) - 10 Apr 2024
- Added ability to combine tracks, by multiselecting them
- Added the display of segment information to the statistics page
- Fixed an out of memory issue when comparing large tracks
- Fixed an issue with the app loading slowly when there are a large number of tracks in the DB
- Fixed issue with importing KML tracks from Google Earth
- Added the ability to import a DB
- Target API 33
- Min API 19

## v1.9.1 (22) - 8 Jan 2024
- Fixed issue viewing old tracks when the current track is paused
- Target API 33
- Min API 19

## v1.9.0 (21) - 13 Dec 2023
- Added support for dark mode
- Track list is now searchable
- Track can now be renamed from the map view
- The app prompts for a track name when the track is stopped
- Track recording will continue even if the app is restarted by the OS
- Importing tracks without timestamp data (eg. MyMaps) is now supported
- Fixed permission issues on Android 13
- DB Version 6
- Target API raised to 33
- Min API 19

## v1.8.0 (20) - 12 Mar 2023
- Improved app navigation - track list is now the main screen
- Target API raised to 31
- Min API 19

## v1.7.0 (17) - 1 Feb 2022
- Fixed export on Android 11+
- Target API 30
- Min API raised to 19

## v1.6.0 (16) - 5 Aug 2021
- Updated libraries for GPS
- Improved the speed of loading the track list
- Target API raised to 30
- Min API 16

## v1.5.0 (15) - 11 Nov 2020
- Long press on tracks in the track list and then select compare to compare groups of your tracks 
- Target API 29
- Min API 16

## v1.4.0 (14) - 22 Aug 2020
- Updated for Android 10
- Added sharing track statistics screenshot
- Added multiple-select to track list
- Multi-select: export and delete multiple tracks
- Target API raised to 29
- Min API 16

## v1.3.0 (13) - 29 Feb 2020
- Added graph to stats page showing elevation by time
- Moves the map to the user's current position when opening the app (and not recording a track)
- Zooms the map to fit a loaded track
- Remembers the map zoom level
- Target API 28
- Min API raised to 16

## v1.2.0 (12) - 1 Aug 2019
- Fix bug - displaying a track and then starting a new one would still show the original on the map
- Reverted to original app icon background colour
- Target API 28
- Min API 14

## v1.1.0 (11) - 17 Jul 2029
- Fixed a crash on some Android 4 devices
- Fixed up UI warnings from the pre-launch report
- Target API 28
- Min API 14

## v1.0.9 (10) - 4 Jul 2019
- Added exporting of tracks in KML format
- Added importing of tracks in KML or KMZ format
- Target API 28
- Min API 14

## v1.0.8 (9) - 1 May 2019
- Internal release
- Updated SDK and libraries to latest versions including AndroidX
- Target API raised to 28
- Min API 14

## v1.0.7 (8) - 2 Mar 2019
- Snagging the initial release
- Fix bug where start track button not displayed after permission is granted
- Fix bug where export tracks are not written if the Tralblazer folder is missing
- Fix error message when exporting tracks
- Added play store link to the sharing text
- Target API 26
- Min API 14

## v1.0.6 (7) - 15 Feb 2019
- UI Fixes - menu order and tracklist order
- Target API 26
- Min API 14

## v1.0.5 (6) - 23 Jan 2019
- Improved altitude recording
- Made altitude elements of a position optional in the GPX file
- Added Pace to the statistics page
- DB Version 5
- Target API 26
- Min API 14

## v1.0.4 (5) - 9 Jan 2019
- Added buttons for Stop/Pause/Restart to the notification
- Fixed empty list text in the track list
- Fixed theoretical bug with graphing an empty track
- Target API 26
- Min API 14

## v1.0.3 (4) - 15 Dec 2018
- New start track button
- Fixed bug with paused tracks
- Added a clear button to edit track name
- Added ability to select imperial or metric units for speed, distance etc.
- Added altitude graph to the statistics display
- DB Version 4
- Target API 26
- Min API 14

## v1.0.2 (3) - 1 Oct 2018
- Added statistics to the track recording notification
- Target API 26
- Min API 14

## v1.0.1 (2) - 8 Sep 2018
- Fixed an issue where the statistics was not available after a track was stopped
- Target API 26
- Min API 14

## v1.0.0 (1) - 23 Aug 2018
- Initial internal build
- DB Version 3
- Target API 26
- Min API 14

