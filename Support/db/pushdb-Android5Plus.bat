REM Push the trailblazer db to the attached device
adb shell "run-as com.andrewandderek.trailblazer.debug chmod 666 /data/data/com.andrewandderek.trailblazer.debug/databases/trailblazer.db"
adb push ./trailblazer.db /storage/emulated/0/download/trailblazer.db
adb shell "run-as com.andrewandderek.trailblazer.debug cp /storage/emulated/0/download/trailblazer.db /data/data/com.andrewandderek.trailblazer.debug/databases/trailblazer.db"
adb shell "run-as com.andrewandderek.trailblazer.debug chmod 600 /data/data/com.andrewandderek.trailblazer.debug/databases/trailblazer.db"
