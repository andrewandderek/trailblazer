REM Pull the trailblazer db from the attached device - Android 5.0+
adb shell "run-as com.andrewandderek.trailblazer.debug chmod 666 /data/data/com.andrewandderek.trailblazer.debug/databases/trailblazer.db"
adb exec-out run-as com.andrewandderek.trailblazer.debug cat databases/trailblazer.db > trailblazer.db
adb shell "run-as com.andrewandderek.trailblazer.debug chmod 600 /data/data/com.andrewandderek.trailblazer.debug/databases/trailblazer.db"
